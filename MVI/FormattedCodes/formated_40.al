codeunit 50001 "Structured Attachements" newline
{ newline
tabulator var newline
tabulator tabulator u: Codeunit Utils; newline
tabulator /// <summary> newline
tabulator /// This function converts all data from Excel Buffer to Outstream. newline
tabulator /// Every single cell is separated with Delimeter. newline
tabulator /// </summary> newline
tabulator /// <param name="OutStream">Outstream - includes data from Excel Buffer</param> newline
tabulator /// <param name="TmpExcelBuffer">Excel Buffer to be converted to Outstream</param> newline
tabulator /// <param name="Delimeter">Separator for data in Outstream</param> newline
tabulator local procedure ConvertExcelBufferToOutstream(var OutStream: OutStream; var TmpExcelBuffer: Record "Excel Buffer" temporary; Delimeter: Text) newline
tabulator var newline
tabulator tabulator Column: Integer; newline
tabulator tabulator nbsp: char; newline
tabulator begin newline
tabulator tabulator nbsp assignment 160; newline
tabulator tabulator TmpExcelBuffer.FindLast(); newline
tabulator tabulator Column assignment TmpExcelBuffer."Column No."; newline
tabulator tabulator if TmpExcelBuffer.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator if TmpExcelBuffer."Cell Type" = TmpExcelBuffer."Cell Type"::Number then begin newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer."Cell Value as Text" assignment ConvertStr(TmpExcelBuffer."Cell Value as Text", nbsp, ' '); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer."Cell Value as Text" assignment DelChr(TmpExcelBuffer."Cell Value as Text"); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator if TmpExcelBuffer."Column No." = Column then begin newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(TmpExcelBuffer."Cell Value as Text"); newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText() newline
tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(TmpExcelBuffer."Cell Value as Text" + Delimeter); newline
tabulator tabulator tabulator until TmpExcelBuffer.Next() = 0; newline
tabulator end; newline
 newline
tabulator procedure CreateCSVFile(var TmpExcelBuffer: Record "Excel Buffer" temporary; AttachmentFilePath: Text; OutStream: OutStream) newline
tabulator var newline
tabulator tabulator Attachment: File; newline
tabulator begin newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::Windows); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
tabulator tabulator ConvertExcelBufferToOutstream(OutStream, TmpExcelBuffer, ';'); newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
 newline
tabulator procedure CreateExcelBuffer(var TmpExcelBuffer: Record "Excel Buffer" temporary; AttachmentFilePath: Text; Len: Integer; OutStream: OutStream; SheetName: Text) newline
tabulator var newline
tabulator tabulator Attachment: File; newline
tabulator tabulator BLFM: Codeunit "BL File Manager"; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator if Len notequal 0 then newline
tabulator tabulator tabulator BLFM.CustomizeCells(TmpExcelBuffer, Len); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
 newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::Windows); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
tabulator tabulator TmpExcelBuffer.SaveToStream(OutStream, true); newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
 newline
tabulator procedure CreateExcelBufferWithColoredText(var TmpExcelBuffer: Record "Excel Buffer" temporary; AttachmentFilePath: Text; OutStream: OutStream; SheetName: Text; Color: Text; FromColumnNo: Integer; ToColumnNo: Integer; FromRowNo: Integer) newline
tabulator var newline
tabulator tabulator Attachment: File; newline
tabulator tabulator BLFM: Codeunit "BL File Manager"; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator BLFM.MakeColoredText(TmpExcelBuffer, Color, FromColumnNo, ToColumnNo, FromRowNo); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
 newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::Windows); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
tabulator tabulator TmpExcelBuffer.SaveToStream(OutStream, true); newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
 newline
tabulator procedure GrandTotal(First: Decimal; Second: Decimal; var TmpExcelBuffer: Record "Excel Buffer" temporary; Len: Integer) newline
tabulator var newline
tabulator tabulator x: Integer; newline
tabulator tabulator Lbl_GrandTotal: Label 'Grand Total'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_GrandTotal, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator for x assignment 4 to Len do begin // Starts at  +1 on Grand Total +2 on First and Second newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator end; newline
tabulator tabulator TmpExcelBuffer.AddColumn(First, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Second, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator end; newline
 newline
tabulator procedure GrandTotal(First: Decimal; Second: Decimal; Third: Decimal; var TmpExcelBuffer: Record "Excel Buffer" temporary; Len: Integer) newline
tabulator var newline
tabulator tabulator x: Integer; newline
tabulator tabulator Lbl_GT: Label 'Grand Total'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_GT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator for x assignment 5 to Len do begin// Starts at  +1 on Grand Total +3 on First and Second and Third newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator end; newline
tabulator tabulator TmpExcelBuffer.AddColumn(First, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Second, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Third, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator end; newline
 newline
tabulator procedure GroupByMaintancePermision(var FCHCodes: List of [Code[20]]; SalesInvoiceheader: Record "Sales Invoice Header") newline
tabulator var newline
tabulator tabulator SalesInvoiceLine: Record "Sales Invoice Line"; newline
tabulator begin newline
tabulator tabulator SalesInvoiceLine.SetRange("Document No.", SalesInvoiceheader."No."); newline
tabulator tabulator SalesInvoiceLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator SalesInvoiceLine.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if not FCHCodes.Contains(SalesInvoiceLine."API Maintenance Approval No.") then newline
tabulator tabulator tabulator tabulator FCHCodes.Add(SalesInvoiceLine."API Maintenance Approval No."); newline
tabulator tabulator until SalesInvoiceLine.Next() = 0; newline
tabulator end; newline
 newline
tabulator procedure GroupByMaintancePermisionCrMemo(var FCHCodes: List of [Code[20]]; SalesCrMemoHeader: Record "Sales Cr.Memo Header") newline
tabulator var newline
tabulator tabulator SalesCrMemoLine: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SalesCrMemoLine.SetRange("Document No.", SalesCrMemoHeader."No."); newline
tabulator tabulator SalesCrMemoLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator SalesCrMemoLine.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if not FCHCodes.Contains(SalesCrMemoLine."API Maintenance Approval No.") then newline
tabulator tabulator tabulator tabulator FCHCodes.Add(SalesCrMemoLine."API Maintenance Approval No."); newline
tabulator tabulator until SalesCrMemoLine.Next() = 0; newline
tabulator end; newline
 newline
tabulator procedure GroupByShortcutDimension(var FCHCodes: List of [Code[20]]; SalesInvoiceheader: Record "Sales Invoice Header") newline
tabulator var newline
tabulator tabulator SalesInvoiceLine: Record "Sales Invoice Line"; newline
tabulator begin newline
tabulator tabulator SalesInvoiceLine.SetRange("Document No.", SalesInvoiceHeader."No."); newline
tabulator tabulator SalesInvoiceLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator SalesInvoiceLine.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if not FCHCodes.Contains(SalesInvoiceLine."Shortcut Dimension 2 Code") then newline
tabulator tabulator tabulator tabulator FCHCodes.Add(SalesInvoiceLine."Shortcut Dimension 2 Code"); newline
tabulator tabulator until SalesInvoiceLine.Next() = 0; newline
tabulator end; newline
 newline
tabulator procedure GroupByShortcutDimensionCrMemo(var FCHCodes: List of [Code[20]]; SalesCrMemoHeader: Record "Sales Cr.Memo Header") newline
tabulator var newline
tabulator tabulator SalesCrMemoLine: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SalesCrMemoLine.SetRange("Document No.", SalesCrMemoHeader."No."); newline
tabulator tabulator SalesCrMemoLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator SalesCrMemoLine.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if not FCHCodes.Contains(SalesCrMemoLine."Shortcut Dimension 2 Code") then newline
tabulator tabulator tabulator tabulator FCHCodes.Add(SalesCrMemoLine."Shortcut Dimension 2 Code"); newline
tabulator tabulator until SalesCrMemoLine.Next() = 0; newline
tabulator end; newline
 newline
tabulator procedure MakeTextWithSpaces(Input: Text; NoOfspaces: Integer): Text newline
tabulator var newline
tabulator tabulator Spaces: Text; newline
tabulator begin newline
tabulator tabulator Spaces assignment 'tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator '; // 60 spaces newline
tabulator tabulator if StrLen(Input) > NoOfspaces then newline
tabulator tabulator tabulator exit(Input.Substring(1, NoOfspaces)) newline
tabulator tabulator else newline
tabulator tabulator tabulator exit(Input + Spaces.Substring(1, NoOfspaces - StrLen(Input))); newline
tabulator end; newline
 newline
tabulator procedure MakeTextWithSpaces_2(Input: Text; NoOfspaces: Integer): Text newline
tabulator var newline
tabulator tabulator Spaces: Text; newline
tabulator begin newline
tabulator tabulator Spaces assignment 'tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator '; // 60 spaces newline
tabulator tabulator if StrLen(Input) > NoOfspaces then newline
tabulator tabulator tabulator exit(Input.Substring(1, NoOfspaces)) newline
tabulator tabulator else newline
tabulator tabulator tabulator exit(Spaces.Substring(1, NoOfspaces - StrLen(Input)) + Input); newline
tabulator end; newline
 newline
tabulator procedure FillZeros(Input: Text; NoOfspaces: Integer): Text newline
tabulator var newline
tabulator tabulator Zeros: Text; newline
tabulator begin newline
tabulator tabulator Zeros assignment '00000000000000000000'; // 20 zeros newline
tabulator tabulator if StrLen(Input) > NoOfspaces then newline
tabulator tabulator tabulator exit(Input.Substring(1, NoOfspaces)) newline
tabulator tabulator else newline
tabulator tabulator tabulator exit(Zeros.Substring(1, NoOfspaces - StrLen(Input)) + Input); newline
tabulator end; newline
 newline
tabulator procedure FormatDecimal(Input: Decimal; N: Integer): Text newline
tabulator var newline
tabulator tabulator prc: Text; newline
tabulator tabulator DF: Text; newline
tabulator begin newline
tabulator tabulator DF assignment '<Integer><Decimals,%1><Comma,.>'; newline
tabulator tabulator prc assignment StrSubstNo('%1', N + 1); newline
tabulator tabulator exit(Format(Input, 0, StrSubstNo(DF, prc))); newline
tabulator end; newline
 newline
tabulator procedure GetSalesCreditMemoLine("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Record "Sales Cr.Memo Line"; newline
tabulator var newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetFilter(Type, 'notequal%1', SCML.Type::" "); newline
tabulator tabulator if SCML.FindFirst() then; newline
tabulator tabulator exit(SCML); newline
tabulator end; newline
 newline
tabulator procedure GetSalesInvoiceLine("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Record "Sales Invoice Line"; newline
tabulator var newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator begin newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SIL.SetFilter(Type, 'notequal%1', SIL.Type::" "); newline
tabulator tabulator if SIL.FindFirst() then; newline
tabulator tabulator exit(SIL); newline
tabulator end; newline
 newline
tabulator procedure GetFinancingContractLineCM("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Record "API Financing Contract Line"; newline
tabulator var newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SCML.FindFirst() then // Potrebujeme najit radku faktury - je jedno jaka, vsechny k jedne smlouve by mely vest na stejnou splatku newline
tabulator tabulator tabulator if FCL.Get("Original Financing Contract No.", SCML."API Contract Line Type", SCML."API Contract Line No.") then  // radka splatkoveho kalendare pro danou smlouvu newline
tabulator tabulator tabulator tabulator exit(FCL); newline
tabulator end; newline
 newline
tabulator procedure GetFinancingContractLine("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Record "API Financing Contract Line"; newline
tabulator var newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator begin newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SIL.FindFirst() then // Potrebujeme najit radku faktury - je jedno jaka, vsechny k jedne smlouve by mely vest na stejnou splatku newline
tabulator tabulator tabulator if FCL.Get("Original Financing Contract No.", SIL."API Contract Line Type", SIL."API Contract Line No.") then  // radka splatkoveho kalendare pro danou smlouvu newline
tabulator tabulator tabulator tabulator exit(FCL); newline
tabulator end; newline
 newline
tabulator procedure GetSubsituteCarCM("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator SPL: Record "API Service Payment Line"; newline
tabulator tabulator SC_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator SC_Countrer assignment 0; newline
tabulator tabulator FCL assignment GetFinancingContractLineCM("Original Financing Contract No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator if not FCL.IsEmpty then begin // radka splatkoveho kalendare pro danou smlouvu newline
tabulator tabulator tabulator SPL.SetRange("Financing Contract No.", FCL."Financing Contract No."); newline
tabulator tabulator tabulator SPL.SetRange("Financing Part Payment No.", FCL."Part Payment No."); newline
tabulator tabulator tabulator SPL.SetRange("Service Kind", "API Service Kind"::"Replacement Car"); newline
tabulator tabulator tabulator if SPL.FindSet() then // radky splatek pro servis newline
tabulator tabulator tabulator tabulator repeat // Mela by byt vzdy a pouze jen 1, pro jistotu ale mame scitat pres vsechny, ktere tam nalezneme newline
tabulator tabulator tabulator tabulator tabulator SC_Countrer += SPL.Amount; newline
tabulator tabulator tabulator tabulator until SPL.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(u.FlipSign(SC_Countrer)); newline
tabulator end; newline
 newline
tabulator procedure GetSubsituteCar("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator SPL: Record "API Service Payment Line"; newline
tabulator tabulator SC_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator SC_Countrer assignment 0; newline
tabulator tabulator FCL assignment GetFinancingContractLine("Original Financing Contract No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator if not FCL.IsEmpty then begin // radka splatkoveho kalendare pro danou smlouvu newline
tabulator tabulator tabulator SPL.SetRange("Financing Contract No.", FCL."Financing Contract No."); newline
tabulator tabulator tabulator SPL.SetRange("Financing Part Payment No.", FCL."Part Payment No."); newline
tabulator tabulator tabulator SPL.SetRange("Service Kind", "API Service Kind"::"Replacement Car"); newline
tabulator tabulator tabulator if SPL.FindSet() then // radky splatek pro servis newline
tabulator tabulator tabulator tabulator repeat // Mela by byt vzdy a pouze jen 1, pro jistotu ale mame scitat pres vsechny, ktere tam nalezneme newline
tabulator tabulator tabulator tabulator tabulator SC_Countrer += SPL.Amount; newline
tabulator tabulator tabulator tabulator until SPL.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(SC_Countrer); newline
tabulator end; newline
 newline
tabulator procedure GetAmountCM("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator tabulator Amount_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator Amount_Countrer assignment 0; newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SCML.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator Amount_Countrer += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(u.FlipSign(Amount_Countrer)); newline
tabulator end; newline
 newline
tabulator procedure GetAmountInclVATCM("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator tabulator Amount_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator Amount_Countrer assignment 0; newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SCML.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator Amount_Countrer += SCML."Amount Including VAT"; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(u.FlipSign(Amount_Countrer)); newline
tabulator end; newline
 newline
tabulator procedure GetAmount("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator tabulator Amount_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator Amount_Countrer assignment 0; newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SIL.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator Amount_Countrer += SIL.Amount; newline
tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(Amount_Countrer); newline
tabulator end; newline
 newline
tabulator procedure GetAmountInclVAT("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator tabulator Amount_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator Amount_Countrer assignment 0; newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SIL.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator Amount_Countrer += SIL."Amount Including VAT"; newline
tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(Amount_Countrer); newline
tabulator end; newline
}