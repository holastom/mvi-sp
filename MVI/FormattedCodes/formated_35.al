report 51022 InvoiceSpecNo_8_9_13_L000_CM newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
tabulator Caption = 'Invoice Spec No 8-9-13 L000 Cr. Memo'; newline
 newline
tabulator dataset newline
tabulator { newline
tabulator tabulator dataitem(SCMH; "Sales Cr.Memo Header") newline
tabulator tabulator { newline
tabulator tabulator tabulator RequestFilterFields = "No."; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator trigger OnInitReport() newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator begin newline
tabulator tabulator ReportingParameters.GetCustInvSendingMethod(CustInvSendingMethod); newline
tabulator tabulator AttachmentFilePath assignment ReportingParameters.GetAttachmentFilePath(); newline
tabulator end; newline
 newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator Amount: Decimal; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator SA.GroupByShortcutDimensionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator SCML assignment SA.GetSalesCreditMemoLine(GroupedCode, SCMH."No."); newline
tabulator tabulator tabulator FCL assignment SA.GetFinancingContractLineCM(GroupedCode, SCMH."No."); newline
tabulator tabulator tabulator Amount assignment SA.GetAmountCM(GroupedCode, SCMH."No."); newline
 newline
tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CalcDate('<-CM>', SCMH."Posting Date"), false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCL."Posting Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(DueDate(), false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SA."MakeTextWithSpaces_2"(GroupedCode, 10), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Return Order No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('L000', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Amount, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCML."VAT %", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SA.GetAmountInclVATCM(GroupedCode, SCMH."No.") - Amount, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator end; newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Invoice Spec No 8-9-13 L000 Cr. Memo'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator CustInvSendingMethod: Record "API Cust. Inv. Sending Method"; newline
tabulator tabulator AttachmentFilePath: Text; newline
 newline
tabulator local procedure DueDate(): Date newline
tabulator var newline
tabulator tabulator TempText: Text; newline
tabulator tabulator Index: Integer; newline
tabulator tabulator Days: Integer; newline
tabulator begin newline
tabulator tabulator if SCMH."API Invoice Print Type" = "API Invoice Print Type"::Payment then newline
tabulator tabulator tabulator exit(CalcDate('<-CM>', SCMH."Posting Date")); newline
tabulator tabulator TempText assignment SCMH."Payment Terms Code"; newline
tabulator tabulator Index assignment TempText.IndexOf('D'); newline
tabulator tabulator Evaluate(Days, TempText.Substring(1, Index - 1)); newline
tabulator tabulator exit(SCMH."Posting Date" + Days); newline
tabulator end; newline
}