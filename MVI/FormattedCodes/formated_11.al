report 50016 FLEET01_LinkedUnlinked newline
{ newline
tabulator Caption = 'Linked Unlinked'; newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
 newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(Content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group(Filters) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Filters:', Comment = 'cs-CZ=Filtry:'; newline
tabulator tabulator tabulator tabulator tabulator field(Visible; Visible) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Visible'; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator trigger OnInitReport() newline
tabulator begin newline
tabulator tabulator Visible assignment true; newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator Catalogue: Record "API Catalogue"; newline
tabulator tabulator FileName: Label 'Linked_Unlinked_%1'; newline
tabulator tabulator SheetName: Label 'Linked Unlinked'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
tabulator tabulator Catalogue.SetRange("Object Visible", Visible); newline
tabulator tabulator if Catalogue.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator Catalogue.CalcFields("BLG RV Grid Code", "BLG MR Grid Code", "Catalogue Group Name", "Make Name", "Model Line Name", "Model Type Name", "BLG Body Type Name", "BLG JATO Segment Name"); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue.Active, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Object Visible", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."BLG RV Grid Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."BLG MR Grid Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Catalogue Group Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Make Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Model Line Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Model Type Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."BLG Trim Classification Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Type of Body Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."BLG Body Type Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LH.FormatToIntegerCZ(Catalogue."Number of Doors"), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LH.FormatToIntegerCZ(Catalogue."Engine CCM"), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Engine Power (kW)", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Fuel Consumption", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."CO2 Emissions (g/km)", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Emission Standard Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Object Price With VAT (LCY)", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Valid From", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue.SystemCreatedAt, false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Date Model Intro", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Drive Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Fuel Type Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Powertrain Type Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."Gearbox Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LH.FormatToIntegerCZ(Catalogue."Number of Gears"), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."BLG JATO Segment Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Catalogue."BLG BLG Segment Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LH.FormatToIntegerCZ(Catalogue.Cylinders), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LH.FormatToIntegerCZ(Catalogue."Engine Torque"), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator until Catalogue.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator TmpExcelBUffer: Record "Excel Buffer" temporary; newline
tabulator tabulator LH: Codeunit LanguageHandler; newline
tabulator tabulator Visible: Boolean; newline
 newline
tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_CatalogueNo: Label 'Catalogue No.'; newline
tabulator tabulator Lbl_Name: Label 'Name'; newline
tabulator tabulator Lbl_Activa: Label 'Active'; newline
tabulator tabulator Lbl_Visible: Label 'Visible'; newline
tabulator tabulator Lbl_RVGridCode: Label 'RV Grid Code'; newline
tabulator tabulator Lbl_MRGridCode: Label 'MR Grid Code'; newline
tabulator tabulator Lbl_GroupName: Label 'Group Name'; newline
tabulator tabulator Lbl_MakeName: Label 'Make Name'; newline
tabulator tabulator Lbl_ModeLineName: Label 'Model Line Name'; newline
tabulator tabulator Lbl_ModelTypeName: Label 'Model Type Name'; newline
tabulator tabulator Lbl_TrimClassificationCode: Label 'Trim Classification Code'; newline
tabulator tabulator Lbl_TypeOfBody: Label 'Type of Body'; newline
tabulator tabulator Lbl_BodyTypeName: Label 'Body Type Name'; newline
tabulator tabulator Lbl_NumberOfDoors: Label 'Number of Doors'; newline
tabulator tabulator Lbl_EngineDisplacement: Label 'Engine Displacement (ccm)'; newline
tabulator tabulator Lbl_EnginePower: Label 'Engine Power (kW)'; newline
tabulator tabulator Lbl_ConsumptionCombined: Label 'Consumption Combined (l/100 km)'; newline
tabulator tabulator Lbl_CO2Emissions: Label 'CO2 Emissions (g/km)'; newline
tabulator tabulator Lbl_EmissionStandardCode: Label 'Emission Standard Code'; newline
tabulator tabulator Lbl_ObjectPriceWithVAT: Label 'Object Price With VAT (LCY)'; newline
tabulator tabulator Lbl_ValidFrom: Label 'Valid from'; newline
tabulator tabulator Lbl_CreatedAt: Label 'Created At'; newline
tabulator tabulator Lbl_DateModelIntro: Label 'Date Model Intro'; newline
tabulator tabulator Lbl_DriveCode: Label 'Drive Code'; newline
tabulator tabulator Lbl_FuelType: Label 'Fuel Type'; newline
tabulator tabulator Lbl_PowerTrainCode: Label 'Power Train Code'; newline
tabulator tabulator Lbl_Gearbox: Label 'Gearbox'; newline
tabulator tabulator Lbl_NumberOfGears: Label 'Number of Gears'; newline
tabulator tabulator Lbl_JATOSegmentName: Label 'JATO Segment Name'; newline
tabulator tabulator Lbl_SegmentCode: Label 'Segment Code'; newline
tabulator tabulator Lbl_Cylinders: Label 'Cylinders'; newline
tabulator tabulator Lbl_EngineTorgue: Label 'Engine Torgue (Nm)'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CatalogueNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Activa, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Visible, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RVGridCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MRGridCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_GroupName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MakeName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModeLineName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelTypeName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TrimClassificationCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TypeOfBody, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_BodyTypeName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_NumberOfDoors, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EngineDisplacement, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EnginePower, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ConsumptionCombined, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CO2Emissions, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EmissionStandardCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ObjectPriceWithVAT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ValidFrom, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CreatedAt, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DateModelIntro, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DriveCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FuelType, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PowerTrainCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Gearbox, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_NumberOfGears, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_JATOSegmentName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_SegmentCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Cylinders, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EngineTorgue, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
 newline
tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 24); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 6); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 7); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 19); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 17); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 28); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('N', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('O', 25); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('P', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Q', 32); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('R', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('S', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('T', 26); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('U', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('V', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('W', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('X', 10); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Y', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Z', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('AA', 8); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('AB', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('AC', 25); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('AD', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('AE', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('AF', 19); newline
tabulator end; newline
}