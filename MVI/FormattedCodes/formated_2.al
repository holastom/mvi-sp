report 51027 Autocont_CM newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
tabulator Caption = 'Autocont Cr. Memo'; newline
 newline
tabulator dataset newline
tabulator { newline
tabulator tabulator dataitem(SCMH; "Sales Cr.Memo Header") newline
tabulator tabulator { newline
tabulator tabulator tabulator RequestFilterFields = "No."; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator trigger OnInitReport() newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator begin newline
tabulator tabulator AttachmentFilePath assignment ReportingParameters.GetAttachmentFilePath(); newline
tabulator tabulator ReportingParameters.GetCustInvSendingMethod(CustInvSendingMethod); newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
 newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator PojHav: Decimal; newline
tabulator tabulator PojPos: Decimal; newline
tabulator tabulator PojPov: Decimal; newline
tabulator tabulator AutFM: Decimal; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator SA.GroupByShortcutDimensionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator PojHav assignment 0; newline
tabulator tabulator tabulator PojPos assignment 0; newline
tabulator tabulator tabulator PojPov assignment 0; newline
tabulator tabulator tabulator AutFM assignment 0; newline
 newline
tabulator tabulator tabulator GetAmounts(PojHav, PojPos, PojPov, AutFM, GroupedCode, SCMH."No."); newline
 newline
tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Return Order No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PojHav, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PojPos, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PojPov, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(AutFM, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(1, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator end; newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBufferWithColoredText(TmpExcelBuffer, AttachmentFilePath, OutStream, 'Autocont', 'D9E1F2', 2, 6, 2); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator CustInvSendingMethod: record "API Cust. Inv. Sending Method"; newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator AttachmentFilePath: Text; newline
 newline
tabulator local procedure GetAmounts(var PojHav: Decimal; var PojPos: Decimal; var PojPov: Decimal; var AutFM: Decimal; "Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]) newline
tabulator var newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetRange("Gen. Prod. Posting Group", 'INS_CASCO'); newline
tabulator tabulator if SCML.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojHav += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator Clear(SCML); newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetRange("Gen. Prod. Posting Group", 'INS_SEATS'); newline
tabulator tabulator if SCML.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojPos += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator Clear(SCML); newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetRange("Gen. Prod. Posting Group", 'INS_TPL'); newline
tabulator tabulator if SCML.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojPov += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator Clear(SCML); newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetFilter("Gen. Prod. Posting Group", 'notequal%1|notequal%2|notequal%3', 'INS_CASCO', 'INS_SEATS', 'INS_TPL'); newline
tabulator tabulator if SCML.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator AutFM += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator end; newline
 newline
tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_Project: Label 'Projekt'; newline
tabulator tabulator Lbl_POJHAV: Label 'částka(cena bez DPH) - kategorie AUT_POJHAV'; newline
tabulator tabulator Lbl_POJPOS: Label 'částka(cena bez DPH) - kategorie AUT_POJOS'; newline
tabulator tabulator Lbl_POJPOV: Label 'částka(cena bez DPH) - kategorie  AUT_POJPOV'; newline
tabulator tabulator Lbl_AUTFM: Label 'částka(cena bez DPH) - kategorie  AUT_FM'; newline
tabulator tabulator Lbl_RegNo: Label 'text - SPZ'; newline
tabulator tabulator Lbl_Empty: Label 'prázdný sl.'; newline
tabulator tabulator Lbl_Quantity: Label 'množství'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Project, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_POJHAV, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_POJPOS, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_POJPOV, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AUTFM, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Quantity, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
}