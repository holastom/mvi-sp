report 50049 Deviation newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ProcessingOnly = true; newline
tabulator ApplicationArea = All; newline
tabulator Caption = 'Deviation', Comment = 'cs-CZ=Deviation'; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator OSH: Record "API Odometer Status History"; newline
 newline
tabulator tabulator FileName: Label 'Deviation_%1'; newline
tabulator tabulator SheetName: Label 'Deviation'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator FCH.SetRange(Status, FCH.Status::Active); newline
tabulator tabulator if FCH.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator tabulator if not FO."Used Financed Object" then begin newline
tabulator tabulator tabulator tabulator tabulator OSH.SetRange("Financing Contract No.", FCH."No."); newline
tabulator tabulator tabulator tabulator tabulator OSH.SetRange("BLG Error", false); newline
tabulator tabulator tabulator tabulator tabulator OSH.SetCurrentKey("Entry No."); newline
tabulator tabulator tabulator tabulator tabulator OSH.SetAscending("Entry No.", false); newline
tabulator tabulator tabulator tabulator tabulator if not OSH.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(OSH); newline
 newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Product Type Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Product No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Used Financed Object", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Handover Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Expected Termination Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Expec. Termin. Date after Ext.", false, '', false, false, false, 'dd.MM.yyyys', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Period (in Months)", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Fin. Period Extended", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Initial Mileage", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Yearly Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Contractual Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH."Mileage Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH.Mileage, false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH."Predicted Mileage", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH."Predicted Mileage Difference", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH."Ratio Km %", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Contract Extension", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Date2DMY(WorkDate(), 2) - Date2DMY(FCH."Handover Date", 2), false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until FCH.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
 newline
tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_ContractNumber: Label 'Contract number'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No'; newline
tabulator tabulator Lbl_AccountCode: Label 'Account Code'; newline
tabulator tabulator Lbl_AccountName: Label 'Account Name'; newline
tabulator tabulator Lbl_ProductType: Label 'Product Type'; newline
tabulator tabulator Lbl_Product: Label 'Product'; newline
tabulator tabulator Lbl_UsedVehicle: Label 'Used Vehicle'; newline
tabulator tabulator Lbl_StartDate: Label 'Start Date'; newline
tabulator tabulator Lbl_EndDate: Label 'End Date'; newline
tabulator tabulator Lbl_EndDateAfterExtension: Label 'End Date after Extension'; newline
tabulator tabulator Lbl_ContractPeriod: Label 'Contract Period'; newline
tabulator tabulator Lbl_ContractPeriodAfterExt: Label 'Contract Period after Ext.'; newline
tabulator tabulator Lbl_StartOdo: Label 'Start Odo'; newline
tabulator tabulator Lbl_AnnualMileage: Label 'Annual mileage'; newline
tabulator tabulator Lbl_ContractMileage: Label 'Contract mileage'; newline
tabulator tabulator Lbl_OdoReadingDate: Label 'Odo Reading Date'; newline
tabulator tabulator Lbl_ActualMileage: Label 'Actual Mileage'; newline
tabulator tabulator Lbl_FinalOdoReading: Label 'Final Odo Reading'; newline
tabulator tabulator Lbl_MileageDeviation: Label 'Mileage Deviation '; newline
tabulator tabulator Lbl_PercemtMileageDeviation: Label '% Mileage Deviation '; newline
tabulator tabulator Lbl_InformalBilling: Label 'Informal billing 1/0'; newline
tabulator tabulator Lbl_MonthsDriven: Label 'Months driven'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNumber, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ProductType, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Product, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_UsedVehicle, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StartDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EndDateAfterExtension, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractPeriod, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractPeriodAfterExt, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StartOdo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AnnualMileage, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractMileage, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_OdoReadingDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ActualMileage, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinalOdoReading, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MileageDeviation, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PercemtMileageDeviation, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InformalBilling, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MonthsDriven, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
 newline
tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 10); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 10); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('N', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('O', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('P', 17); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Q', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('R', 17); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('S', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('T', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('U', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('V', 14); newline
tabulator end; newline
}