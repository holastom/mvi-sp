report 50048 CMG14_EarlyTerminationReason newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ProcessingOnly = true; newline
tabulator ApplicationArea = All; newline
tabulator Caption = 'Early Termination reason YTD', Comment = 'cs-CZ=Early Termination reason YTD'; newline
 newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(Content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group(Filters) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Filters', Comment = 'cs-CZ=Filtry'; newline
tabulator tabulator tabulator tabulator tabulator field(Closed; Closed) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Closed'; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Settled; Settled) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Settled'; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Archived; Archived) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Archived'; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(DateFrom; DateFrom) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Date From', Comment = 'cs-CZ=Datum od'; newline
 newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(DateTo; DateTo) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Date To', Comment = 'cs-CZ=Datum do'; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
 newline
tabulator tabulator FileName: Label 'Early_Termination_reason_YTD_%1'; newline
tabulator tabulator SheetName: Label 'Early Termination reason YTD'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator FCH.SetFilter(Status, GetFilterOnStatus()); newline
tabulator tabulator FCH.SetFilter("Termination Date", '%1..%2', DateFrom, DateTo); newline
tabulator tabulator if FCH.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO.VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Termination Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Contractual End Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Period (in Months)", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Period Real", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Period Difference", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH.Status, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Detailed Contract Status", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Early Termination Reason", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator until FCH.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator Closed: Boolean; newline
tabulator tabulator Settled: Boolean; newline
tabulator tabulator Archived: Boolean; newline
tabulator tabulator DateFrom: Date; newline
tabulator tabulator DateTo: Date; newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
 newline
tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_ContractNumber: Label 'Contract number'; newline
tabulator tabulator Lbl_LicencePlate: Label 'Licence Plate'; newline
tabulator tabulator Lbl_VIN: Label 'VIN'; newline
tabulator tabulator Lbl_AccountCode: Label 'Account Code'; newline
tabulator tabulator Lbl_AccountName: Label 'Account Name'; newline
tabulator tabulator Lbl_ActualEndDate: Label 'Actual end date'; newline
tabulator tabulator Lbl_ContractualEndDate: Label 'Contractual End Date'; newline
tabulator tabulator Lbl_FinancingPeriod: Label 'Financing Period (in Months)'; newline
tabulator tabulator Lbl_FinancingPeriodReal: Label 'Financing Period Real'; newline
tabulator tabulator Lbl_FinancingPeriodDifference: Label 'Financing Period Difference'; newline
tabulator tabulator Lbl_Status: Label 'Status'; newline
tabulator tabulator Lbl_DetailedContractStatus: Label 'Detailed Contract Status'; newline
tabulator tabulator Lbl_EarlyTerminationReason: Label 'Early Termination Reason'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNumber, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_LicencePlate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ActualEndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractualEndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinancingPeriod, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinancingPeriodReal, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinancingPeriodDifference, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Status, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DetailedContractStatus, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EarlyTerminationReason, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
 newline
tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 19); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 27); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 26); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 24); newline
tabulator end; newline
 newline
tabulator local procedure GetFilterOnStatus(): Text newline
tabulator var newline
tabulator tabulator retval: Text; newline
tabulator begin newline
tabulator tabulator if Closed then newline
tabulator tabulator tabulator retval += '|Closed'; newline
tabulator tabulator if Settled then newline
tabulator tabulator tabulator retval += '|Settled'; newline
tabulator tabulator if Archived then newline
tabulator tabulator tabulator retval += '|Archived'; newline
tabulator tabulator exit(DelChr(retval, '<', '|')); newline
tabulator end; newline
}