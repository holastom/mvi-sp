report 51050 ValidateAndUpdate newline
{ newline
tabulator Permissions = TableData "Sales Invoice Header" = rm; newline
tabulator Caption = 'Validate And Update', Comment = 'cs-CZ=Validate And Update'; newline
tabulator ProcessingOnly = true; newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
 newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group(Functions) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Functions'; newline
tabulator tabulator tabulator tabulator tabulator field(UpdateSigners; UpdateSigners) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Set BLG Minimum Signers ≥ 1:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'Sets Minimum Signers for Contract on all Contact with type Company on ≥ 1 (leaves all > 0 unchanged).'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(ClearJRRoles; ClearJRRoles) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Clear all Roles Allowed in JR:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'Clear all "BLG Roles Allowed Assign" for every Job Responsibility.'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(UpdateActiveContractStatus; UpdateActiveContractStatus) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Set Detail Contract Statuses:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'Set for all fields in selected Detailed Contract Status on TRUE.'; newline
tabulator tabulator tabulator tabulator tabulator tabulator TableRelation = "API Detail Contract Status"; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(FixTires; FixTires) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Fix Tires on Contract:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator group(Tires) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Tires'; newline
tabulator tabulator tabulator tabulator tabulator Visible = FixTires; newline
tabulator tabulator tabulator tabulator tabulator field(FinCon; FinCon) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Financing Contract No.:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator tabulator TableRelation = "API Financing Contract Header"."No."; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(SSN; SSN) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Tire Season:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(TirCat; TirCat) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Tire Category:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Pos; Pos) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Tire Position:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Width; Width) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Width:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator tabulator TableRelation = "API Tire Width"; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(TProfile; TProfile) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Profile:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator tabulator TableRelation = "API Tire Profile"; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Diameter; Diameter) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'RIM:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator tabulator TableRelation = "API Rim Diameter"; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Index; Index) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Load Index:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator tabulator TableRelation = "API Tire Load Index"; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Speed; Speed) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Speed Category:'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator u: Codeunit Utils; newline
tabulator tabulator UpdateSigners: Boolean; newline
tabulator tabulator ClearJRRoles: Boolean; newline
tabulator tabulator UpdateActiveContractStatus: Code[10]; newline
tabulator tabulator FixTires: Boolean; newline
tabulator tabulator FinCon: Code[20]; newline
tabulator tabulator SSN: enum "API Tire Season"; newline
tabulator tabulator TirCat: enum "API Tire Category"; newline
tabulator tabulator Pos: enum "API Tire Position"; newline
tabulator tabulator Width: Code[10]; newline
tabulator tabulator TProfile: Code[10]; newline
tabulator tabulator Diameter: Code[10]; newline
tabulator tabulator Index: Code[10]; newline
tabulator tabulator Speed: Code[10]; newline
 newline
tabulator trigger OnInitReport() newline
tabulator begin newline
tabulator tabulator UpdateSigners assignment false; newline
tabulator tabulator ClearJRRoles assignment false; newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator begin newline
tabulator tabulator if UpdateSigners then UpdateMinimumSigners(); newline
tabulator tabulator if ClearJRRoles then ClearAllJRRoles(); newline
tabulator tabulator if UpdateActiveContractStatus notequal '' then UpdateActiveContractStatuses(UpdateActiveContractStatus); newline
tabulator tabulator if FixTires then FixAllTires(); newline
tabulator end; newline
 newline
tabulator local procedure UpdateMinimumSigners() newline
tabulator var newline
tabulator tabulator c: Record Contact; newline
tabulator begin newline
tabulator tabulator c.SetRange(Type, "Contact Type"::Company); newline
tabulator tabulator c.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if c."BLG Minimum Signers" < 1 then begin newline
tabulator tabulator tabulator tabulator c.Validate("BLG Minimum Signers", 1); newline
tabulator tabulator tabulator tabulator c.Modify(true) newline
tabulator tabulator tabulator end; newline
tabulator tabulator until c.Next() = 0; newline
tabulator end; newline
 newline
tabulator local procedure ClearAllJRRoles() newline
tabulator var newline
tabulator tabulator jr: Record "Job Responsibility"; newline
tabulator begin newline
tabulator tabulator jr.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator jr.Validate("BLG Roles Allowed Assign", ''); newline
tabulator tabulator tabulator jr.Modify(true); newline
tabulator tabulator until jr.Next() = 0; newline
tabulator end; newline
 newline
 newline
tabulator local procedure UpdateActiveContractStatuses(UpdateActiveContractStatus: Code[10]) newline
tabulator var newline
tabulator tabulator dcs: Record "API Detail Contract Status"; newline
tabulator begin newline
tabulator tabulator dcs.Get(UpdateActiveContractStatus); newline
tabulator tabulator dcs.Validate("Fill Termination Date", true); newline
tabulator tabulator dcs.Validate("Fill Fin. Settlement Date", true); newline
tabulator tabulator dcs.Validate("Fill Real Termination Date", true); newline
tabulator tabulator dcs.Validate(Charge, true); newline
tabulator tabulator dcs.Validate(Reminder, true); newline
tabulator tabulator dcs.Validate("Change without Protocol", true); newline
tabulator tabulator dcs.Validate("Allow Down Payment Posting", true); newline
tabulator tabulator dcs.Validate("Allow Posting from Paym. Cal.", true); newline
tabulator tabulator dcs.Validate("Fill Termination Date", true); newline
tabulator tabulator dcs.Validate("BLG Allow Creating Sales Inv.", true); newline
tabulator tabulator dcs.Validate("Allow Contract Transfer", true); newline
tabulator tabulator dcs.Validate("Allow Multi Calculation", true); newline
tabulator tabulator dcs.Validate(Calculation, true); newline
tabulator tabulator dcs.Validate("Maintenance Permission", true); newline
tabulator tabulator dcs.Validate("BLG Customer Limit Draw", true); newline
tabulator tabulator dcs.Validate("Limit Draw", true); newline
tabulator tabulator dcs.Validate("Reservation Limit Draw", true); newline
tabulator tabulator dcs.Validate("Allow Accr./Cal.In.Posting", true); newline
tabulator tabulator dcs.Validate("Check Unclosed Guarantee", true); newline
tabulator tabulator dcs.Validate("Delete Partial Payment Credit", true); newline
tabulator tabulator dcs.Validate("Allow Object Ordering", true); newline
tabulator tabulator dcs.Validate("BLG Check VIN", true); newline
tabulator tabulator dcs.Validate("BLG Invoicing Guarantee", true); newline
tabulator tabulator dcs.Validate("BLG Secure Replacement Mob.", true); newline
tabulator tabulator dcs.Validate("BLG Check Registration", true); newline
tabulator tabulator dcs.Validate("BLG Create Handover Protocol", true); newline
tabulator tabulator dcs.Validate(Active, true); newline
tabulator tabulator dcs.Modify(true); newline
tabulator end; newline
 newline
tabulator local procedure FixAllTires() newline
tabulator var newline
tabulator tabulator TireDetailLine: Record "API Tire Detail Line"; newline
tabulator tabulator SecondTireDetailLine: Record "API Tire Detail Line"; newline
tabulator begin newline
tabulator tabulator TireDetailLine.SetRange("Contract No.", FinCon); newline
tabulator tabulator TireDetailLine.SetRange("Service Contract Type", TireDetailLine."Service Contract Type"::Contract); newline
tabulator tabulator TireDetailLine.SetCurrentKey("Line No."); newline
tabulator tabulator TireDetailLine.SetAscending("Line No.", false); newline
tabulator tabulator if TireDetailLine.FindFirst() then begin newline
tabulator tabulator tabulator SecondTireDetailLine assignment TireDetailLine; newline
tabulator tabulator tabulator SecondTireDetailLine."Line No." += 10000; newline
tabulator tabulator tabulator SecondTireDetailLine."Tire Category" assignment TirCat; newline
tabulator tabulator tabulator SecondTireDetailLine.Position assignment Pos; newline
tabulator tabulator tabulator SecondTireDetailLine.Width assignment u.iif(Width notequal '', Width, TireDetailLine.Width); newline
tabulator tabulator tabulator SecondTireDetailLine.Profile assignment u.iif(TProfile notequal '', TProfile, TireDetailLine.Profile); newline
tabulator tabulator tabulator SecondTireDetailLine.RIM assignment u.iif(Diameter notequal '', Diameter, TireDetailLine.RIM); newline
tabulator tabulator tabulator SecondTireDetailLine."Load Index" assignment u.iif(Index notequal '', Index, TireDetailLine."Load Index"); newline
tabulator tabulator tabulator SecondTireDetailLine."Speed Category" assignment u.iif(Speed notequal '', Speed, TireDetailLine."Speed Category"); newline
tabulator tabulator tabulator SecondTireDetailLine.Insert(); newline
tabulator tabulator end; newline
tabulator end; newline
}