report 50047 CMG01_UAMK newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ProcessingOnly = true; newline
tabulator ApplicationArea = All; newline
tabulator Caption = 'UAMK - Activated / Terminated', Comment = 'cs-CZ=UAMK - Activated / Terminated'; newline
 newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(Content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group(Document) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Activated / Terminated', Comment = 'cs-CZ=Aktivované / Ukončené'; newline
tabulator tabulator tabulator tabulator tabulator field(Activated; Activated) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Activated', Comment = 'cs-CZ=Aktivované'; newline
tabulator tabulator tabulator tabulator tabulator tabulator trigger OnValidate() newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Terminated assignment not Activated; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Terminated; Terminated) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Terminated', Comment = 'cs-CZ=Ukončené'; newline
tabulator tabulator tabulator tabulator tabulator tabulator trigger OnValidate() newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Activated assignment not Terminated; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator group(DateRamge) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Visible = Terminated; newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Dates', Comment = 'cs-CZ=Datumy'; newline
tabulator tabulator tabulator tabulator tabulator field(DateFrom; DateFrom) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Date From', Comment = 'cs-CZ=Datum od'; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(DateTo; DateTo) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Date To', Comment = 'cs-CZ=Datum do'; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator } newline
tabulator trigger OnInitReport() newline
tabulator begin newline
tabulator tabulator Activated assignment true; newline
tabulator tabulator Terminated assignment false; newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator CS: Record "API Contract Service"; newline
tabulator tabulator FD: Record "API Fee Detail"; newline
 newline
tabulator tabulator FileName: Text; newline
tabulator tabulator SheetName: Text; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator FCH.SetRange("Calculation Variant", false); newline
tabulator tabulator FCH.SetRange("Change Copy", false); newline
tabulator tabulator if Activated then newline
tabulator tabulator tabulator FCH.SetRange(Status, FCH.Status::Active) newline
tabulator tabulator else//Terminated newline
tabulator tabulator tabulator FCH.SetFilter(Status, '%1|%2|%3', FCH.Status::Active, FCH.Status::Closed, FCH.Status::Settled); newline
tabulator tabulator if FCH.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FCH.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator tabulator CS.SetRange("Contract No.", FCH."No."); newline
tabulator tabulator tabulator tabulator CS.SetRange("Service Contract Type", CS."Service Contract Type"::Contract); newline
tabulator tabulator tabulator tabulator CS.SetRange("Service Kind", CS."Service Kind"::"Fee/Service"); newline
tabulator tabulator tabulator tabulator CS.SetRange("Service Type Code", 'ROAD_ASSIS'); newline
tabulator tabulator tabulator tabulator if Activated then newline
tabulator tabulator tabulator tabulator tabulator CS.SetFilter("Valid To after Extension", '>=%1', WorkDate()) newline
tabulator tabulator tabulator tabulator else //Terminated newline
tabulator tabulator tabulator tabulator tabulator CS.SetFilter("Valid To after Extension", '%1..%2', DateFrom, DateTo); newline
tabulator tabulator tabulator tabulator if CS.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator FD.Get(CS."Service Contract Type", CS."No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Make Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Model Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO.VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CS."Valid From", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator if Activated and (CS."Valid From" notequal 0D) then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CalcDate('<+60M>', CS."Valid From"), false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date) newline
tabulator tabulator tabulator tabulator tabulator tabulator else //Terminated newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CS."Valid To after Extension", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."1st Registration Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Product Type Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Address", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Address 2", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Post Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer City", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Driver Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Driver No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CS."Service Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(StandardVIP(FD."Extra Service"), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator until CS.Next() = 0; newline
tabulator tabulator tabulator until FCH.Next() = 0; newline
 newline
tabulator tabulator FileName assignment 'UAMK'; newline
tabulator tabulator SheetName assignment 'UAMK'; newline
tabulator tabulator if Activated then begin newline
tabulator tabulator tabulator FileName += '_Activated_%1'; newline
tabulator tabulator tabulator SheetName += ' Activated'; newline
tabulator tabulator end else begin //Terminated newline
tabulator tabulator tabulator FileName += '_Terminated_%1'; newline
tabulator tabulator tabulator SheetName += ' Terminated'; newline
tabulator tabulator end; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator Activated: Boolean; newline
tabulator tabulator Terminated: Boolean; newline
tabulator tabulator DateFrom: Date; newline
tabulator tabulator DateTo: Date; newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator LH: Codeunit LanguageHandler; newline
tabulator tabulator U: Codeunit Utils; newline
 newline
tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_RegNo: Label 'Reg No.'; newline
tabulator tabulator Lbl_ContractNo: Label 'Contract No.'; newline
tabulator tabulator Lbl_MakeDesc: Label 'Make Desc'; newline
tabulator tabulator Lbl_MakeModelDesc: Label 'Make Model Desc'; newline
tabulator tabulator Lbl_ModelDesc: Label 'Model Desc'; newline
tabulator tabulator Lbl_VIN: Label 'Vin'; newline
tabulator tabulator Lbl_StartDate: Label 'Start Date'; newline
tabulator tabulator Lbl_ValidUntil: Label 'Valid until'; newline
tabulator tabulator Lbl_ActualEndDate: Label 'Actual End Date'; newline
tabulator tabulator Lbl_RegDate: Label 'Reg Date'; newline
tabulator tabulator Lbl_FleetCategory: Label 'Fleet Category'; newline
tabulator tabulator Lbl_RegName: Label 'Reg Name'; newline
tabulator tabulator Lbl_StreetNo: Label 'Street No.'; newline
tabulator tabulator Lbl_AdressLine1: Label 'Adress Line 1'; newline
tabulator tabulator Lbl_Postcode: Label 'Postcode'; newline
tabulator tabulator Lbl_TownCity: Label 'Town City'; newline
tabulator tabulator Lbl_Driver: Label 'Driver'; newline
tabulator tabulator Lbl_ContactNumber: Label 'Contact Number'; newline
tabulator tabulator Lbl_ElementName: Label 'Element name'; newline
tabulator tabulator Lbl_StandardVIP: Label 'Standard/VIP'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MakeDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MakeModelDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StartDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator if Activated then newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ValidUntil, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text) newline
tabulator tabulator else //Terminated newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ActualEndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FleetCategory, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StreetNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AdressLine1, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Postcode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TownCity, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Driver, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContactNumber, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ElementName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StandardVIP, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
 newline
tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 10); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 19); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('N', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('O', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('P', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Q', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('R', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('S', 15); newline
tabulator end; newline
 newline
tabulator local procedure StandardVIP(ExtraService: Boolean): Text newline
tabulator begin newline
tabulator tabulator if ExtraService then newline
tabulator tabulator tabulator exit('VIP'); newline
tabulator tabulator exit('Standard'); newline
tabulator end; newline
}