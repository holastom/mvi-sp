report 51038 ContactPerClient newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ProcessingOnly = true; newline
tabulator ApplicationArea = All; newline
tabulator Caption = 'Contact per Client'; newline
 newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(Content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group(Filters) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Filters'; newline
tabulator tabulator tabulator tabulator tabulator field(CustomerNo; CustomerNo) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Customer No.'; newline
tabulator tabulator tabulator tabulator tabulator tabulator TableRelation = Customer."No."; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Driver; Driver) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Driver'; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(JRFilter; JRFilter) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Job Responsibility'; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator trigger OnDrillDown() newline
tabulator tabulator tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator JR: Record "Job Responsibility"; newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if PAGE.RunModal(0, JR) = ACTION::LookupOK then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if JRFilter = '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator JRFilter assignment JR.Code newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator JRFilter += '|' + JR.Code; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator Customer: Record Customer; newline
tabulator tabulator Company: Record Contact; newline
tabulator tabulator Person: Record Contact; newline
tabulator tabulator CBR: Record "Contact Business Relation"; newline
tabulator tabulator JR: Record "Contact Job Responsibility"; newline
 newline
tabulator tabulator FileName: Label 'Contact_per_Client_%1'; newline
tabulator tabulator SheetName: Label 'Contact per Client'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator if CustomerNo notequal '' then newline
tabulator tabulator tabulator Customer.SetRange("No.", CustomerNo); newline
tabulator tabulator Customer.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator CBR.SetRange("No.", Customer."No."); newline
tabulator tabulator tabulator CBR.SetRange("Business Relation Code", 'CLIENT'); newline
tabulator tabulator tabulator CBR.FindFirst(); newline
tabulator tabulator tabulator Company.Get(CBR."Contact No."); newline
tabulator tabulator tabulator Person.SetRange("Company No.", CBR."Contact No."); newline
tabulator tabulator tabulator Person.SetRange(Type, Person.Type::Person); newline
tabulator tabulator tabulator Person.SetRange("BLG Subject Status", "BLG Subject Status"::Active); newline
tabulator tabulator tabulator if Person.FindSet() then newline
tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator JR.SetFilter("Contact No.", Person."No."); newline
tabulator tabulator tabulator tabulator tabulator if JRFilter notequal '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator JR.SetFilter("Job Responsibility Code", JRFilter); newline
tabulator tabulator tabulator tabulator tabulator if JR.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if not ((not Driver) and (JR."Job Responsibility Code" = 'DRIVER')) or (not u.isJobRespValid(JR, Today())) then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Company.CalcFields("BLG Customer Support Name", "BLG Customer Owner Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator JR.CalcFields("Job Responsibility Description"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer."Registration No. CZL", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.Address, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.City + ' ' + Customer."Post Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer."Language Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(JR."Job Responsibility Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(JR."Job Responsibility Description", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Person.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Person."E-Mail", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(lh.FormatPhoneNumber(Person."Mobile Phone No."), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Company."BLG Customer Support Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Company."BLG Customer Owner Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator until JR.Next() = 0; newline
tabulator tabulator tabulator tabulator until Person.Next() = 0; newline
tabulator tabulator until Customer.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator CustomerNo: Code[20]; newline
tabulator tabulator JRFilter: Text; newline
tabulator tabulator Driver: Boolean; newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator lh: Codeunit LanguageHandler; newline
tabulator tabulator u: Codeunit Utils; newline
 newline
tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_CustomerNo: Label 'Customer No.'; newline
tabulator tabulator Lbl_CompRegNo: Label 'Comp Reg No.'; newline
tabulator tabulator Lbl_RegName: Label 'Reg Name'; newline
tabulator tabulator Lbl_BPA: Label 'Business Place - Ulice'; newline
tabulator tabulator Lbl_BPC: Label 'Business Place - Město'; newline
tabulator tabulator Lbl_Language: Label 'Language'; newline
tabulator tabulator Lbl_JR: Label 'Job resp'; newline
tabulator tabulator Lbl_JRDesc: Label 'Job resp - Description'; newline
tabulator tabulator Lbl_CName: Label 'Contact Name'; newline
tabulator tabulator Lbl_Email: Label 'E-Mail'; newline
tabulator tabulator Lbl_Mobile: Label 'Mobile'; newline
tabulator tabulator Lbl_AMBI: Label 'AMBI'; newline
tabulator tabulator Lbl_AMBU: Label 'AMBU'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CustomerNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CompRegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_BPA, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_BPC, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Language, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_JR, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_JRDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Email, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Mobile, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AMBI, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AMBU, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
 newline
tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 31); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 25); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 27); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 27); newline
tabulator end; newline
}