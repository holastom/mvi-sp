report 51032 Zentiva_CM newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
tabulator Caption = 'Zentiva Cr. memo'; newline
 newline
tabulator dataset newline
tabulator { newline
tabulator tabulator dataitem(SCMH; "Sales Cr.Memo Header") newline
tabulator tabulator { newline
tabulator tabulator tabulator RequestFilterFields = "No."; newline
tabulator tabulator } newline
tabulator } newline
tabulator trigger OnInitReport() newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator begin newline
tabulator tabulator ReportingParameters.GetCustInvSendingMethod(CustInvSendingMethod); newline
tabulator tabulator AttachmentFilePath assignment ReportingParameters.GetAttachmentFilePath(); newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
tabulator tabulator Amount: Decimal; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
 newline
tabulator tabulator Lbl_DescriptionText: Label 'Fakturace nájmu za období %1-%2'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator SA.GroupByShortcutDimensionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator FCH.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator FCL assignment SA.GetFinancingContractLineCM(GroupedCode, SCMH."No."); newline
tabulator tabulator tabulator Amount assignment SA.GetAmountCM(GroupedCode, SCMH."No."); newline
 newline
tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Sell-to Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Posting Date", false, '', false, false, false, 'dd-MM-yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(StrSubstNo(Lbl_DescriptionText, Format(FCL."Date From", 0, '<Day,2>-<Month,2>-<Year4>'), Format(FCL."Date To", 0, '<Day,2>-<Month,2>-<Year4>')), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Driver Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Shortcut Dimension 2 Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."BLG Name 2", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Amount, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(0, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Amount, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Amount, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator end; newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Zentiva'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator CustInvSendingMethod: record "API Cust. Inv. Sending Method"; newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator AttachmentFilePath: Text; newline
 newline
tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_AccCode: Label 'Acc'; newline
tabulator tabulator Lbl_InvoiceNo: Label 'Invoice number'; newline
tabulator tabulator Lbl_InvoiceDate: Label 'Invoice Date'; newline
tabulator tabulator Lbl_Description: Label 'Text'; newline
tabulator tabulator Lbl_Driver: Label 'Driver'; newline
tabulator tabulator Lbl_ContractNo: Label 'Contract No'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No'; newline
tabulator tabulator Lbl_ModelDescription: Label 'Model Desc'; newline
tabulator tabulator Lbl_VatChargeable: Label 'Vat chargeable'; newline
tabulator tabulator Lbl_VatNotChargeable: Label 'Vat not chargeable'; newline
tabulator tabulator Lbl_FuelProv: Label 'Fuel prov'; newline
tabulator tabulator Lbl_TotalNetto: Label 'Total netto'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InvoiceNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InvoiceDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Driver, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelDescription, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VatChargeable, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VatNotChargeable, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FuelProv, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TotalNetto, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
}