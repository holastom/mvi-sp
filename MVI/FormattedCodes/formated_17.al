report 51019 FuelSpecNo5ObvDoc_CM newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
tabulator Caption = 'Fuel spec no 5 obv doc Cr. Memo'; newline
 newline
tabulator dataset newline
tabulator { newline
tabulator tabulator dataitem(SCMH; "Sales Cr.Memo Header") newline
tabulator tabulator { newline
tabulator tabulator tabulator RequestFilterFields = "No."; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator trigger OnInitReport() newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator begin newline
tabulator tabulator AttachmentFilePath assignment ReportingParameters.GetAttachmentFilePath(); newline
tabulator tabulator ReportingParameters.GetCustInvSendingMethod(CustInvSendingMethod); newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator SalesCrMemoLine: Record "Sales Cr.Memo Line"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator Customer: Record Customer; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator Len: Integer; newline
tabulator tabulator Quantity: Decimal; newline
tabulator tabulator Amount: Decimal; newline
tabulator tabulator VAT: Decimal; newline
tabulator tabulator VatRateCustomer: Decimal; newline
tabulator tabulator VatCustomer: Decimal; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator Quantity assignment 0; newline
tabulator tabulator Amount assignment 0; newline
tabulator tabulator VAT assignment 0; newline
 newline
tabulator tabulator Customer.Get(SCMH."Sell-to Customer No."); newline
 newline
tabulator tabulator CreateHeader(); newline
tabulator tabulator Len assignment 13; newline
 newline
tabulator tabulator SalesCrMemoLine.SetRange("Document No.", SCMH."No."); newline
tabulator tabulator SalesCrMemoLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator if SalesCrMemoLine.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FinancingContractHeader.Get(SalesCrMemoLine."Shortcut Dimension 2 Code"); newline
tabulator tabulator tabulator tabulator FinancedObject.Get(FinancingContractHeader."Financed Object No."); newline
 newline
tabulator tabulator tabulator tabulator if Customer."VAT Bus. Posting Group" in ['EU', 'EXPORT'] then begin newline
tabulator tabulator tabulator tabulator tabulator VatRateCustomer assignment 0; newline
tabulator tabulator tabulator tabulator tabulator VatCustomer assignment 0; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator VatRateCustomer assignment SalesCrMemoLine."VAT %"; newline
tabulator tabulator tabulator tabulator tabulator VatCustomer assignment SalesCrMemoLine."Amount Including VAT" - SalesCrMemoLine."VAT Base Amount"; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Bill-to Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SalesCrMemoLine."Shortcut Dimension 2 Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Round(SalesCrMemoLine."Unit Price"), false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SalesCrMemoLine.Quantity, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator Quantity += SalesCrMemoLine.Quantity; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SalesCrMemoLine."VAT Base Amount", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator Amount += SalesCrMemoLine."VAT Base Amount"; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatCustomer, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator VAT += VatCustomer; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SalesCrMemoLine.Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatRateCustomer, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('CZK', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator until SalesCrMemoLine.Next() = 0; newline
tabulator tabulator SA.GrandTotal(Quantity, Amount, VAT, TmpExcelBuffer, Len); newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Fuel spec no 5 obv doc Cr. Memo'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator CustInvSendingMethod: record "API Cust. Inv. Sending Method"; newline
tabulator tabulator AttachmentFilePath: Text; newline
 newline
tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_AccountCode: Label 'Account code'; newline
tabulator tabulator Lbl_ContractNo: Label 'Contract No'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No'; newline
tabulator tabulator Lbl_CardNo: Label 'Card No'; newline
tabulator tabulator Lbl_Description: Label 'Description'; newline
tabulator tabulator Lbl_StatementID: Label 'Statement Id'; newline
tabulator tabulator Lbl_Date: Label 'Date'; newline
tabulator tabulator Lbl_Time: Label 'Time'; newline
tabulator tabulator Lbl_MerchantArea: Label 'Merchant Area'; newline
tabulator tabulator Lbl_PricePerLiter: Label 'Price p/ltr'; newline
tabulator tabulator Lbl_Quantity: Label 'Quantity'; newline
tabulator tabulator Lbl_AmountExclVAT: Label 'Amount Excl VAT'; newline
tabulator tabulator Lbl_VAT: Label 'VAT'; newline
tabulator tabulator Lbl_ProductDescription: Label 'Product description'; newline
tabulator tabulator Lbl_TaxRate: Label 'Tax Rate'; newline
tabulator tabulator Lbl_Currency: Label 'Currency'; newline
tabulator tabulator Lbl_Odoreading: Label 'Odoreading'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CardNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StatementID, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Date, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Time, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MerchantArea, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PricePerLiter, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Quantity, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AmountExclVAT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VAT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ProductDescription, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TaxRate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Currency, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Odoreading, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
}