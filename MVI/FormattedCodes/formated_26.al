report 51014 InvoiceSpecNo_8_9_13_L000_2 newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
tabulator Caption = 'Invoice Spec No 8-9-13 L000 2'; newline
tabulator dataset newline
tabulator { newline
tabulator tabulator dataitem(SIH; "Sales Invoice Header") newline
tabulator tabulator { newline
tabulator tabulator tabulator RequestFilterFields = "No."; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator trigger OnInitReport() newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator begin newline
tabulator tabulator AttachmentFilePath assignment ReportingParameters.GetAttachmentFilePath(); newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator Amount: Decimal; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator begin newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::Windows); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
 newline
tabulator tabulator SA.GroupByShortcutDimension(FCHCodes, SIH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator SIL assignment SA.GetSalesInvoiceLine(GroupedCode, SIH."No."); newline
tabulator tabulator tabulator FCL assignment SA.GetFinancingContractLine(GroupedCode, SIH."No."); newline
tabulator tabulator tabulator Amount assignment SA.GetAmount(GroupedCode, SIH."No."); newline
 newline
tabulator tabulator tabulator OutStream.WriteText(SIH."No." + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(Format(CalcDate('<-CM>', SIH."Posting Date"), 0, '<Day>/<Month>/<Year>') + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(Format(FCL."Posting Date", 0, '<Day>/<Month>/<Year>') + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(Format(DueDate(), 0, '<Day>/<Month>/<Year>') + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(GroupedCode + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(FO."Licence Plate No." + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(SIH."Order No." + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText('L000' + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FO.Name, 25) + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(SA.FormatDecimal(Amount, 2) + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(Format(SIL."VAT %", 0, '<Integer>') + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(SA.FormatDecimal(SA.GetAmountInclVAT(GroupedCode, SIH."No.") - Amount, 2) + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator end; newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator AttachmentFilePath: Text; newline
tabulator tabulator Attachment: File; newline
 newline
tabulator local procedure DueDate(): Date newline
tabulator var newline
tabulator tabulator TempText: Text; newline
tabulator tabulator Index: Integer; newline
tabulator tabulator Days: Integer; newline
tabulator begin newline
tabulator tabulator if SIH."API Invoice Print Type" = "API Invoice Print Type"::Payment then newline
tabulator tabulator tabulator exit(CalcDate('<-CM>', SIH."Posting Date")); newline
tabulator tabulator TempText assignment SIH."Payment Terms Code"; newline
tabulator tabulator Index assignment TempText.IndexOf('D'); newline
tabulator tabulator Evaluate(Days, TempText.Substring(1, Index - 1)); newline
tabulator tabulator exit(SIH."Posting Date" + Days); newline
tabulator end; newline
}