report 51021 InvoiceSpecNo_8_9_13_LINDE_CM newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
tabulator Caption = 'Invoice Spec No 8-9-13 LINDE Cr. Memo'; newline
 newline
tabulator dataset newline
tabulator { newline
tabulator tabulator dataitem(SCMH; "Sales Cr.Memo Header") newline
tabulator tabulator { newline
tabulator tabulator tabulator RequestFilterFields = "No."; newline
tabulator tabulator } newline
tabulator } newline
tabulator trigger OnInitReport() newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator begin newline
tabulator tabulator ReportingParameters.GetCustInvSendingMethod(CustInvSendingMethod); newline
tabulator tabulator AttachmentFilePath assignment ReportingParameters.GetAttachmentFilePath(); newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator SA.GroupByShortcutDimensionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
 newline
tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."No." + ', ' + FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SA.GetAmountCM(GroupedCode, SCMH."No."), false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('154707300', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Cost Center Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator end; newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Invoice Spec No 8-9-13 LINDE Cr. Memo'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator CustInvSendingMethod: Record "API Cust. Inv. Sending Method"; newline
tabulator tabulator AttachmentFilePath: Text; newline
}