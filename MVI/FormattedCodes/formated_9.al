report 51037 EXC03_DefineFinesEmailsExtract newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
tabulator Caption = 'Define Fines Emails Extract'; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator CDCDocument: Record "CDC Document"; newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator MasterAgreement: Record "API Master Agreement"; newline
tabulator tabulator contactJobResponsibilityRecord: Record "Contact Job Responsibility"; newline
 newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
 newline
tabulator tabulator FinesMethod: Text; newline
tabulator tabulator FileName: Label 'Define_Fines_Emails_Extract_%1'; newline
tabulator tabulator SheetName: Label 'Define Fines Emails Extract'; newline
tabulator tabulator NoDocumentError: Label 'There are no OPEN documents in the FINES section that are of the VÝZVA type.'; newline
tabulator begin newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(); newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator CDCDocument.SetFilter(Status, '%1', CDCDocument.Status::Open); newline
tabulator tabulator CDCDocument.SetRange("Document Category Code", 'FINES'); newline
tabulator tabulator CDCDocument.SetFilter("BLG Document Type", 'VÝZVA'); newline
tabulator tabulator if CDCDocument.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FinesMethod assignment 'VZZ'; newline
tabulator tabulator tabulator tabulator CDCDocument.CalcFields("BLG Data Message ID", "BLG File Name", "BLG Amount Incl. VAT", "BLG Vendor Invoice No.", "BLG Contract"); newline
tabulator tabulator tabulator tabulator Clear(FinancingContractHeader); newline
tabulator tabulator tabulator tabulator if FinancingContractHeader.Get(CDCDocument."BLG Contract") then begin newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("Licence Plate No."); newline
tabulator tabulator tabulator tabulator tabulator if MasterAgreement.Get(FinancingContractHeader."Master Agreement No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator if MasterAgreement."BLG Resolving Fines Method" = 'PAY' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinesMethod assignment 'VZP'; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Data Message ID", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_1 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_2 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinesMethod, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_3 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_4 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG File Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_5 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(GetContactsForFineCom(FinancingContractHeader."Primary Contact No."), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_6 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_7 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_8 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_9 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_10 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Amount Incl. VAT", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); //ID_11 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Vendor Invoice No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text) //ID_12 newline
tabulator tabulator tabulator until CDCDocument.Next() = 0; newline
tabulator tabulator end else newline
tabulator tabulator tabulator Error(NoDocumentError); newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator Lbl_Empty: Label ''; newline
 newline
tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_01: Label 'ID datové zprávy'; newline
tabulator tabulator Lbl_02: Label 'SPZ vozidla'; newline
tabulator tabulator Lbl_03: Label 'Typ dokladu'; newline
tabulator tabulator Lbl_04: Label 'Typ dokladu 2'; newline
tabulator tabulator Lbl_05: Label 'E-mailFile'; newline
tabulator tabulator Lbl_06: Label 'EmailToAddress'; newline
tabulator tabulator Lbl_10: Label 'Poznámka'; newline
tabulator tabulator Lbl_11: Label 'Částka pokuty'; newline
tabulator tabulator Lbl_12: Label 'Číslo jednací'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_01, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_02, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_03, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_04, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_05, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_06, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_10, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_11, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_12, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
 newline
tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 33); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 12); newline
tabulator end; newline
 newline
tabulator local procedure GetContactsForFineCom(CompanyNo: Code[20]): Text newline
tabulator var newline
tabulator tabulator ContactJobResponsibility: Record "Contact Job Responsibility"; newline
tabulator tabulator Contact: Record Contact; newline
tabulator tabulator Valid: Boolean; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator RetVal: Text; newline
tabulator begin newline
tabulator tabulator Contact.SetRange("Company No.", CompanyNo); newline
tabulator tabulator ContactJobResponsibility.SetFilter("Job Responsibility Code", 'FINE_COM'); newline
tabulator tabulator if Contact.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator ContactJobResponsibility.SetRange("Contact No.", Contact."No."); newline
tabulator tabulator tabulator tabulator if ContactJobResponsibility.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator valid assignment Utils.isJobRespValid(ContactJobResponsibility, Today); newline
tabulator tabulator tabulator tabulator tabulator until valid or (ContactJobResponsibility.Next = 0); newline
tabulator tabulator tabulator tabulator tabulator if valid then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator RetVal += ';' + Contact."E-Mail"; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until Contact.Next = 0; newline
tabulator tabulator RetVal assignment DelChr(RetVal, '<', ';'); newline
tabulator tabulator exit(RetVal); newline
tabulator end; newline
 newline
} newline
