codeunit 50000 "BL File Manager" newline
{ newline
tabulator /// <summary> newline
tabulator /// This function converts all data from Excel Buffer to Outstream. newline
tabulator /// Every single cell is separated with Delimeter. newline
tabulator /// </summary> newline
tabulator /// <param name="OutStream">Outstream - includes data from Excel Buffer</param> newline
tabulator /// <param name="TmpExcelBuffer">Excel Buffer to be converted to Outstream</param> newline
tabulator /// <param name="Delimeter">Separator for data in Outstream</param> newline
tabulator procedure ConvertExcelBufferToOutstream(var OutStream: OutStream; var TmpExcelBuffer: Record "Excel Buffer" temporary; Delimeter: Text) newline
tabulator var newline
tabulator tabulator Column: Integer; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.FindLast(); newline
tabulator tabulator Column assignment TmpExcelBuffer."Column No."; newline
tabulator tabulator if TmpExcelBuffer.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator if TmpExcelBuffer."Column No." = Column then begin newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(TmpExcelBuffer."Cell Value as Text"); newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText() newline
tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(TmpExcelBuffer."Cell Value as Text" + Delimeter); newline
tabulator tabulator tabulator until TmpExcelBuffer.Next() = 0; newline
tabulator end; newline
 newline
tabulator /// <summary> newline
tabulator /// This fucntion applies custom parameters for every cell in Excel Buffer. newline
tabulator /// </summary> newline
tabulator /// <param name="TmpExcelBuffer">Excel Buffer to be procesed</param> newline
tabulator /// <param name="Columns">Number of columns to be custom design applied</param> newline
tabulator procedure CustomizeCells(var TmpExcelBuffer: Record "Excel Buffer" temporary; Columns: Integer) newline
tabulator var newline
tabulator tabulator Row: Integer; newline
tabulator begin newline
tabulator tabulator Row assignment 0; newline
tabulator tabulator TmpExcelBuffer.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator TmpExcelBuffer."Font Name" assignment 'Arial'; newline
tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator until TmpExcelBuffer.Next() = 0; newline
 newline
tabulator tabulator repeat newline
tabulator tabulator tabulator Row assignment Row + 1; newline
tabulator tabulator tabulator TmpExcelBuffer.Get(1, Row); newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger('1a91bc')); newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Font Color", TmpExcelBuffer.HexARGBToInteger('FFFFFF')); newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Font Name", 'Arial'); newline
tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator until Row = Columns; newline
tabulator end; newline
 newline
tabulator procedure MakeColoredRows(var TmpExcelBuffer: Record "Excel Buffer" temporary) newline
tabulator var newline
tabulator tabulator Row: Integer; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator Row assignment TmpExcelBuffer."Row No."; newline
tabulator tabulator tabulator if (Row > 1) and (Row Mod 2 = 1) then begin newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger('CCDBDC')); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.Validate("Font Name", 'Arial'); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator tabulator end; newline
tabulator tabulator until TmpExcelBuffer.Next() = 0; newline
tabulator end; newline
 newline
tabulator procedure MakeColoredText(var TmpExcelBuffer: Record "Excel Buffer" temporary; Color: Text; FromColumnNo: Integer; ToColumnNo: Integer; FromRowNo: Integer) newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetFilter("Row No.", '>=%1', FromRowNo); newline
tabulator tabulator TmpExcelBuffer.SetFilter("Column No.", '%1..%2', FromColumnNo, ToColumnNo); newline
tabulator tabulator TmpExcelBuffer.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger(Color)); newline
tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator until TmpExcelBuffer.Next() = 0; newline
tabulator end; newline
 newline
tabulator procedure FillSalutation(): Text newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator tabulator PrimaryContact: Record Contact; newline
tabulator tabulator Salutation: Record Salutation; newline
tabulator tabulator Contact: Record Contact; newline
tabulator begin newline
tabulator tabulator ReportingParameters.GetContactPerson(Contact); newline
tabulator tabulator if Contact."No." notequal '' then begin newline
tabulator tabulator tabulator if (Contact."BLG Surname-Salutation" notequal '') and (Contact."Salutation Code" notequal '') then begin newline
tabulator tabulator tabulator tabulator if Salutation.Get(Contact."Salutation Code") then newline
tabulator tabulator tabulator tabulator tabulator exit(Salutation.Description + ' ' + Contact."BLG Surname-Salutation"); newline
tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator if PrimaryContact.Get(Contact."Company No.") then newline
tabulator tabulator tabulator tabulator tabulator if PrimaryContact."BLG Client Status" notequal "BLG Client Status"::Customer then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if Salutation.Get('GENERAL') then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator exit(Salutation.Description); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator end; newline
tabulator tabulator if Salutation.Get('GENCUST') then newline
tabulator tabulator tabulator exit(Salutation.Description); newline
tabulator end; newline
 newline
tabulator [EventSubscriber(ObjectType::Codeunit, Codeunit::"Report Distribution Management", 'OnAfterGetFullDocumentTypeText', '', false, false)] newline
tabulator local procedure OnAfterGetFullDocumentTypeText(var DocumentTypeText: Text[150]) newline
tabulator var newline
tabulator tabulator InvoiceLbl: Label 'Invoice'; newline
tabulator tabulator FakturaLbl: Label 'Faktura'; newline
tabulator begin newline
tabulator tabulator if DocumentTypeText = 'Prodejní faktura' then newline
tabulator tabulator tabulator DocumentTypeText assignment FakturaLbl; newline
tabulator tabulator if DocumentTypeText = 'Sales Invoice' then newline
tabulator tabulator tabulator DocumentTypeText assignment InvoiceLbl; newline
tabulator end; newline
}