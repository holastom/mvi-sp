report 51023 InvoiceSpecNo_8_9_13_RENT_CM newline
{ newline
tabulator UsageCategory = ReportsAndAnalysis; newline
tabulator ApplicationArea = All; newline
tabulator ProcessingOnly = true; newline
tabulator Caption = 'Invoice Spec No 8-9-13 RENTAL Cr. Memo'; newline
 newline
tabulator dataset newline
tabulator { newline
tabulator tabulator dataitem(SCMH; "Sales Cr.Memo Header") newline
tabulator tabulator { newline
tabulator tabulator tabulator RequestFilterFields = "No."; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator trigger OnInitReport() newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator begin newline
tabulator tabulator ReportingParameters.GetCustInvSendingMethod(CustInvSendingMethod); newline
tabulator tabulator AttachmentFilePath assignment ReportingParameters.GetAttachmentFilePath(); newline
tabulator end; newline
 newline
tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
tabulator tabulator SC: Decimal; newline
tabulator tabulator SC_total: Decimal; newline
tabulator tabulator Amount: Decimal; newline
tabulator tabulator Amount_total: Decimal; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator SA.GroupByShortcutDimensionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator FCH.CalcFields("BLG Driver Name", "BLG Cost Center Name"); newline
tabulator tabulator tabulator SC assignment SA.GetSubsituteCarCM(GroupedCode, SCMH."No."); newline
tabulator tabulator tabulator SC_total += SC; newline
tabulator tabulator tabulator Amount assignment SA.GetAmountCM(GroupedCode, SCMH."No."); newline
tabulator tabulator tabulator Amount -= SC; newline
tabulator tabulator tabulator Amount_total += Amount; newline
 newline
tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Cost Center Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Driver Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."BLG Name 2", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Amount, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SC, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator end; newline
tabulator tabulator SA.GrandTotal(Amount_total, SC_total, TmpExcelBuffer, 8); newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Invoice Spec No 8-9-13 RENTAL Cr. Memo'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
 newline
tabulator var newline
tabulator tabulator CustInvSendingMethod: Record "API Cust. Inv. Sending Method"; newline
tabulator tabulator TmpExcelBuffer: Record "Excel Buffer" temporary; newline
tabulator tabulator AttachmentFilePath: Text; newline
 newline
tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_InvoiceNumber: Label 'Invoice Number'; newline
tabulator tabulator Lbl_Department: Label 'Department'; newline
tabulator tabulator Lbl_Driver: Label 'Driver'; newline
tabulator tabulator Lbl_ContractNo: Label 'Contract No'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No'; newline
tabulator tabulator Lbl_ModelDesc: Label 'Model Desc'; newline
tabulator tabulator Lbl_BasicLeaseTerm: Label 'Basic Lease Term'; newline
tabulator tabulator Lbl_SubstCar: Label 'Subst Car'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InvoiceNumber, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Department, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Driver, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_BasicLeaseTerm, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_SubstCar, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
}