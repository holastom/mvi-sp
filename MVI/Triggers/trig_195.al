tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator VendorBusinessActivity: Record "BLG Vendor Bus. Activity"; newline
tabulator tabulator tabulator tabulator PersonVendBusLocation: Record "API Person Vend. Bus. Loc."; newline
tabulator tabulator tabulator tabulator Contact: Record Contact; newline
 newline
tabulator tabulator tabulator tabulator Tel: Text; newline
tabulator tabulator tabulator tabulator Activity: Text; newline
tabulator tabulator tabulator tabulator Activities: List of [Text]; newline
tabulator tabulator tabulator tabulator NotValid: Boolean; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator C_InformationColection assignment ''; newline
tabulator tabulator tabulator tabulator VBA_BusRelationDesc assignment ''; newline
tabulator tabulator tabulator tabulator Tel assignment ''; newline
tabulator tabulator tabulator tabulator Activities.Add('Insurance Event'); newline
tabulator tabulator tabulator tabulator Activities.Add('Maintenance'); newline
tabulator tabulator tabulator tabulator Activities.Add('Tire Service'); newline
tabulator tabulator tabulator tabulator PersonVendBusLocation.SetFilter("Contact No.", ContactBusinessLocation."Contact No."); newline
tabulator tabulator tabulator tabulator PersonVendBusLocation.SetFilter("Business Location No.", ContactBusinessLocation.Code); newline
tabulator tabulator tabulator tabulator VendorBusinessActivity.SetFilter("Contact No.", ContactBusinessLocation."Contact No."); newline
tabulator tabulator tabulator tabulator VendorBusinessActivity.SetFilter("Business Location No.", ContactBusinessLocation.Code); newline
tabulator tabulator tabulator tabulator foreach Activity in Activities do begin newline
tabulator tabulator tabulator tabulator tabulator NotValid assignment false; newline
tabulator tabulator tabulator tabulator tabulator VendorBusinessActivity.SetFilter("Business Activity Type", Activity); newline
tabulator tabulator tabulator tabulator tabulator if VendorBusinessActivity.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator while true newline
tabulator tabulator tabulator tabulator tabulator tabulator    do begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if CheckValid(VendorBusinessActivity) or (NotValid) then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator break; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if VendorBusinessActivity.Next() = 0 then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator NotValid assignment true newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator if not NotValid then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator VendorBusinessActivity.CalcFields("Business Relation Description"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if VBA_BusRelationDesc notequal '' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator VBA_BusRelationDesc += ', '; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator VBA_AfterMarket += ', '; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator VBA_BusRelationDesc += VendorBusinessActivity."Business Relation Description"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator VBA_AfterMarket += VendorBusinessActivity."Business Relation Code" + ' - ' + AMorOE(VendorBusinessActivity."After Market"); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PersonVendBusLocation.SetFilter("Contact Service Kind", Activity); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if PersonVendBusLocation.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if Contact.Get(PersonVendBusLocation."Contact Person No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if C_InformationColection notequal '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator C_InformationColection += Utils.crlf(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if Contact."Phone No." notequal '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Tel assignment LH.FormatPhoneNumber(Contact."Phone No.") newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Tel assignment LH.FormatPhoneNumber(Contact."Mobile Phone No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator C_InformationColection += Format(PersonVendBusLocation."Contact Service Kind") + ' - ' + Contact.Name + ', ' + Contact."E-Mail" + ', ' + Tel; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator } newline
tabulator var newline
tabulator tabulator C_InformationColection: Text; newline
tabulator tabulator VBA_BusRelationDesc: Text; newline
tabulator tabulator VBA_AfterMarket: Text; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator LH: Codeunit LanguageHandler; newline
 newline
tabulator local procedure AMorOE(AfterMarket: Boolean): Text newline
tabulator begin newline
tabulator tabulator if AfterMarket then newline
tabulator tabulator tabulator exit('AM'); newline
tabulator tabulator exit('OE'); newline
tabulator end; newline
