tabulator tabulator tabulator tabulator trigger OnPreDataItem() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator RIM_ACC_ContractService.SetFilter("Valid From", '<=%1', Today()); newline
tabulator tabulator tabulator tabulator tabulator RIM_ACC_ContractService.SetFilter("Valid To", '>=%1', Today()); newline
tabulator tabulator tabulator tabulator tabulator RIM_ACC_ContractService.SetRange("Service Type Code", 'RIM_ACC'); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem(RIM_ContractService; "API Contract Service") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Contract No." = field("Contract No."), "Service Contract Type" = field("Service Contract Type"); newline
 newline
tabulator tabulator tabulator tabulator dataitem(RimDetail; "API Rim Detail") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "Service No." = field("No."); newline
tabulator tabulator tabulator tabulator tabulator dataitem(RimDetailLine; "API Rim Detail Line") newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = "Service No." = field("Service No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator column(RDL_RimCategory; "Rim Category") { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(RDL_Position; Position) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(RDL_Width; Width) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(RDL_Suffix; Suffix) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(RDL_Diameter; Diameter) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(RDL_RimQuantity; "Rim Quantity") { } newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
 newline
