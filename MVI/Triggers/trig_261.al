tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator OptionalEquipment: Record "API Object Optional Equipment"; newline
tabulator tabulator tabulator tabulator AddEquipment: Record "API Object Add. Equipment"; newline
tabulator tabulator tabulator tabulator TireSpecification: Record "BLG Tire Specification"; newline
tabulator tabulator tabulator tabulator Front: Boolean; newline
tabulator tabulator tabulator tabulator Rear: Boolean; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator SeasonalTyresPlacement assignment ''; newline
tabulator tabulator tabulator tabulator TyresType assignment ''; newline
tabulator tabulator tabulator tabulator RimsType assignment ''; newline
tabulator tabulator tabulator tabulator OptionalEquipment.SetRange("Financed Object No.", FinancedObject."No."); newline
tabulator tabulator tabulator tabulator if OptionalEquipment.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator AdditionalEquipment += ', ' + OptionalEquipment.Description; newline
tabulator tabulator tabulator tabulator tabulator until OptionalEquipment.Next() = 0; newline
tabulator tabulator tabulator tabulator AddEquipment.SetRange("Financed Object No.", FinancedObject."No."); newline
tabulator tabulator tabulator tabulator AddEquipment.SetFilter("Vendor No.", 'notequal%1', FinancedObject."Vendor No."); newline
tabulator tabulator tabulator tabulator if AddEquipment.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator AdditionalEquipment += ', ' + AddEquipment.Description; newline
tabulator tabulator tabulator tabulator tabulator until AddEquipment.Next() = 0; newline
tabulator tabulator tabulator tabulator AdditionalEquipment assignment DelChr(AdditionalEquipment, '<', ', '); newline
tabulator tabulator tabulator tabulator /* # TODO CR185 - SeasonalTyresPlacement, TyresType, RimsType newline
tabulator tabulator tabulator tabulator TireSpecification.SetRange("Financed Object No.", FinancedObject."No."); newline
tabulator tabulator tabulator tabulator TireSpecification.SetRange("Record Type", TireSpecification."Record Type"::"Vehicle Return"); newline
tabulator tabulator tabulator tabulator if TireSpecification.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator if TireSpecification.Position = TireSpecification.Position::"Front+Rear" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Front assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Rear assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if TireSpecification.Position = TireSpecification.Position::Front then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Front assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if TireSpecification.Position = TireSpecification.Position::Rear then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Rear assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator SeasonalTyresPlacement += ', ' + Format(TireSpecification.Position).ToLower() + ': ' + Format(TireSpecification."Tire Set Location").ToLower(); newline
tabulator tabulator tabulator tabulator tabulator tabulator SeasonalTyresPlacement += Utils.iif(TireSpecification."Selected Tire Shop" = '', '', ' (' + TireSpecification."Selected Tire Shop" + ')'); newline
tabulator tabulator tabulator tabulator tabulator tabulator TyresType += ', ' + Format(TireSpecification.Position).ToLower() + ': ' + Format(TireSpecification.Season).ToLower() + ': ' + TireSpecification."Tire Make" + ' ('; newline
tabulator tabulator tabulator tabulator tabulator tabulator TyresType += Utils.iif(TireSpecification."Pattern Depth Front Left" = '', '-', TireSpecification."Pattern Depth Front Left" + ', '); newline
tabulator tabulator tabulator tabulator tabulator tabulator TyresType += Utils.iif(TireSpecification."Pattern Depth Front Right" = '', '-', TireSpecification."Pattern Depth Front Right" + ', '); newline
tabulator tabulator tabulator tabulator tabulator tabulator TyresType += Utils.iif(TireSpecification."Pattern Depth Rear Left" = '', '-', TireSpecification."Pattern Depth Rear Left" + ', '); newline
tabulator tabulator tabulator tabulator tabulator tabulator TyresType += Utils.iif(TireSpecification."Pattern Depth Rear Right" = '', '-', TireSpecification."Pattern Depth Rear Right" + ')'); newline
tabulator tabulator tabulator tabulator tabulator tabulator RimsType += ', ' + Format(TireSpecification.Position).ToLower() + ': ' +  TireSpecification."Rim Type Description"; newline
tabulator tabulator tabulator tabulator tabulator until TireSpecification.Next() = 0; newline
tabulator tabulator tabulator tabulator tabulator SeasonalTyresPlacement assignment DelChr(SeasonalTyresPlacement, '<', ', '); newline
tabulator tabulator tabulator tabulator tabulator TyresType assignment DelChr(TyresType, '<', ', '); newline
tabulator tabulator tabulator tabulator tabulator RimsType assignment DelChr(RimsType, '<', ', '); newline
tabulator tabulator tabulator tabulator tabulator if Front and not Rear then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator SeasonalTyresPlacement += ', ' + Lbl_Rear + ': ' + Lbl_Destroyer; newline
tabulator tabulator tabulator tabulator tabulator tabulator TyresType += ', ' + Lbl_Rear + ': ' + Lbl_NotKnown; newline
tabulator tabulator tabulator tabulator tabulator tabulator RimsType += ', ' + Lbl_Rear + ': ' + Lbl_NotKnown; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator if Rear and not Front then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator SeasonalTyresPlacement += ', ' + Lbl_Front + ': ' + Lbl_Destroyer; newline
tabulator tabulator tabulator tabulator tabulator tabulator TyresType += ', ' + Lbl_Front + ': ' + Lbl_NotKnown; newline
tabulator tabulator tabulator tabulator tabulator tabulator RimsType += ', ' + Lbl_Front + ': ' + Lbl_NotKnown; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator SeasonalTyresPlacement assignment Lbl_FrontRear + ': ' + Lbl_Destroyer; newline
tabulator tabulator tabulator tabulator tabulator TyresType assignment Lbl_FrontRear + ': ' + Lbl_NotKnown; newline
tabulator tabulator tabulator tabulator tabulator RimsType assignment Lbl_FrontRear + ': ' + Lbl_NotKnown; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator */ newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator } newline
 newline
