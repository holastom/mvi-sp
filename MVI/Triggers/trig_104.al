tabulator trigger OnPreReport() newline
tabulator begin newline
tabulator tabulator if Customer.Get(CustomerNo) then; newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(RequestPageLanguage, Customer."Language Code"); newline
tabulator end; newline
