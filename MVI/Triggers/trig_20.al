tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator OSH: Record "API Odometer Status History"; newline
 newline
tabulator tabulator FileName: Label 'Deviation_%1'; newline
tabulator tabulator SheetName: Label 'Deviation'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator FCH.SetRange(Status, FCH.Status::Active); newline
tabulator tabulator if FCH.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator tabulator if not FO."Used Financed Object" then begin newline
tabulator tabulator tabulator tabulator tabulator OSH.SetRange("Financing Contract No.", FCH."No."); newline
tabulator tabulator tabulator tabulator tabulator OSH.SetRange("BLG Error", false); newline
tabulator tabulator tabulator tabulator tabulator OSH.SetCurrentKey("Entry No."); newline
tabulator tabulator tabulator tabulator tabulator OSH.SetAscending("Entry No.", false); newline
tabulator tabulator tabulator tabulator tabulator if not OSH.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(OSH); newline
 newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Product Type Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Product No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Used Financed Object", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Handover Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Expected Termination Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Expec. Termin. Date after Ext.", false, '', false, false, false, 'dd.MM.yyyys', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Period (in Months)", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Fin. Period Extended", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Initial Mileage", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Yearly Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Contractual Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH."Mileage Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH.Mileage, false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH."Predicted Mileage", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH."Predicted Mileage Difference", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(OSH."Ratio Km %", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Contract Extension", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Date2DMY(WorkDate(), 2) - Date2DMY(FCH."Handover Date", 2), false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until FCH.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
