tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator CurrReport.Language assignment Number; newline
tabulator tabulator tabulator tabulator LanguageHandler.SetLanguage(Number); newline
tabulator tabulator tabulator tabulator Currency assignment LanguageHandler.CurrSymbol(''); newline
tabulator tabulator tabulator end; newline
 newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem(CI; "Company Information") newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = sorting("Primary Key"); newline
 newline
tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator column(CI_Addr; Address) { } newline
tabulator tabulator tabulator column(CI_Addr2; "Address 2") { } newline
tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator column(CI_PSC; "Post Code") { } newline
tabulator tabulator tabulator column(CI_Phone; LanguageHandler.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator column(CI_EMail; "E-mail") { } newline
tabulator tabulator tabulator column(CI_HomePage; "Home Page") { } newline
tabulator tabulator tabulator column(CI_VATRegNo; "VAT Registration No.") { } newline
tabulator tabulator tabulator column(CI_RegNo; "Registration No.") { } newline
tabulator tabulator tabulator column(CI_Country; Utils.CountryName("Country/Region Code")) { } newline
tabulator tabulator tabulator column(CI_Logo; Picture) { } newline
tabulator tabulator tabulator column(CompanyManager; CompanyManager) { } newline
tabulator tabulator tabulator column(CP_FullName; Employee."Full Name") { } newline
tabulator tabulator tabulator column(CP_Email; Employee."Company E-Mail") { } newline
tabulator tabulator tabulator column(CP_Phone; LanguageHandler.FormatPhoneNumber(Employee."Phone No.")) { } newline
tabulator tabulator tabulator column(CP_Mobile; LanguageHandler.FormatPhoneNumber(Employee."API Company Mobile Phone No.")) { } newline
 newline
