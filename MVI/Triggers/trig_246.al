tabulator tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator V_RegNos assignment CopyStr(Utils.CondCat("Registration No. CZL", ', ' + RegNoLbl + ': ') + Utils.CondCat("VAT Registration No.", ', ' + VATRegNoLbl + ': '), 3); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator tabulator dataitem("Purchase Line"; "Purchase Line") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLinkReference = "Purchase Header"; newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "Document No." = field("No."); newline
tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("Document Type", "Document No.", "Line No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator column(PL_Document_No_; "Document No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(PL_Type; "Type") { } newline
tabulator tabulator tabulator tabulator tabulator column(PLType_Text; Type = Type::" ") { } newline
tabulator tabulator tabulator tabulator tabulator column(PL_Description; Description) { } newline
tabulator tabulator tabulator tabulator tabulator column(PL_Direct_Unit_Cost; PL_Direct_Unit_Cost) { } newline
tabulator tabulator tabulator tabulator tabulator column(PL_Line_Discount_; PL_Line_Discount_) { } newline
tabulator tabulator tabulator tabulator tabulator column(PL_Amount; PL_Amount) { } newline
tabulator tabulator tabulator tabulator tabulator column(PL_AmtInclVAT; PL_AmtInclVAT) { } newline
 newline
