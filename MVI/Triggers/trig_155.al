tabulator tabulator tabulator tabulator trigger OnPreDataItem() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator if not (CSEmpty and CSBEmpty and "API Insurance Contract".IsEmpty) then CurrReport.Break; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem(CPSIGNAPPR; Contact) //CPSA newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Company No." = field("Primary Contact No."); newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
 newline
tabulator tabulator tabulator tabulator column(CPSA___; '') { } newline
tabulator tabulator tabulator tabulator column(CPSA_No; "No.") { } //ID_89 newline
tabulator tabulator tabulator tabulator column(CPSA_Name; Name) { } //ID_88 newline
tabulator tabulator tabulator tabulator column(CPSAType; SignType) { } newline
tabulator tabulator tabulator tabulator column(CPSAJobTit; "Job Title") { } newline
 newline
