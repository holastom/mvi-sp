tabulator tabulator trigger OnOpenPage() newline
tabulator tabulator begin newline
tabulator tabulator tabulator CIlng assignment LanguageHandler.GetCompanyLanguageId; newline
tabulator tabulator end; newline
tabulator } newline
 newline
tabulator labels newline
tabulator { newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator RequestPageLanguage: Enum LanguageEnum; newline
 newline
tabulator tabulator Renter: Record Contact; newline
tabulator tabulator RenterBank: Record "Customer Bank Account"; newline
tabulator tabulator Employee: Record Employee; newline
tabulator tabulator FOB: Record "API Financed Object"; newline
 newline
tabulator tabulator CompanyManager: Text; newline
tabulator tabulator RenterIds: Text; newline
tabulator tabulator RenterJurReg: Text; newline
tabulator tabulator AUserName: array[4] of Text; newline
tabulator tabulator DelivPlace: Text; newline
tabulator tabulator CarType: Text; newline
tabulator tabulator Currency: Text; newline
tabulator tabulator CarVendor: Text; newline
tabulator tabulator CarVendor2: Text; newline
tabulator tabulator CarModel, CarModelType, Gear : Text; newline
tabulator tabulator LicPlateNo: Text[10]; newline
 newline
tabulator tabulator tPerMonth: Label 'Za měsíc', Comment = 'en-US=Per month'; newline
tabulator tabulator tPerDay: Label 'Za den', Comment = 'en-US=Per day'; newline
 newline
tabulator tabulator ReportTitleLbl: Label 'Smlouva o nájmu dopravního prostředku č.', Comment = 'en-US=Vehicle Rental Contract No.'; newline
tabulator tabulator ContrLessorLbl: Label 'Dodavatel (pronajímatel)', Comment = 'en-US=Contractor (lessor)'; newline
tabulator tabulator LesseeLbl: Label 'Klient (nájemce)', Comment = 'en-US=Client (lessee)'; newline
tabulator tabulator RepresentedByLbl: Label 'Zastoupená', Comment = 'en-US=Represented by'; newline
tabulator tabulator ContactPersonLbl: Label 'Kontaktní osoba', Comment = 'en-US=Contact person'; newline
tabulator tabulator PhoneLbl: Label 'Tel.', Comment = 'en-US=Phone'; newline
tabulator tabulator MobileLbl: Label 'Mobil', Comment = 'en-US=Mobile'; newline
tabulator tabulator RentInfoLbl: Label 'INFORMACE O ZÁPŮJČCE', Comment = 'en-US=RENTAL INFORMATION'; newline
tabulator tabulator FileNoLbl: Label 'č. vložky', Comment = 'en-US=File No.'; newline
tabulator tabulator ContractLbl: Label 'Číslo smlouvy', Comment = 'en-US=Contract No.'; newline
tabulator tabulator RentFromLbl: Label 'Začátek pronájmu', Comment = 'en-US=Beginning of lease'; newline
tabulator tabulator RentToLbl: Label 'Předpokládaný konec pronájmu', Comment = 'en-US=Expected end of lease'; newline
tabulator tabulator DelivPlaceLbl: Label 'Místo přistavení vozidla', Comment = 'en-US=Place of delivery of the vehicle'; newline
tabulator tabulator WillBeSpecifiedLbl: Label 'bude upřesněno', Comment = 'en-US=will be specified'; newline
tabulator tabulator CarTypeLbl: Label 'Kategorie vozidla', Comment = 'en-US=Vehicle category'; newline
tabulator tabulator ProviderLbl: Label 'Poskytovatel vozidla', Comment = 'en-US=Vehicle provider'; newline
tabulator tabulator AuthPersonsLbl: Label 'OPRÁVNĚNÉ OSOBY', Comment = 'en-US=AUTHORIZED PERSONS'; newline
tabulator tabulator AuthPersTxt1: Label 'AuthPersTxt1'; //Klient stanovil následující osoby jako oprávněné vozidlo převzít / užívat / vrátit: newline
tabulator tabulator AuthPersTxt2: Label 'AuthPersTxt2'; //Níže uvedené osoby souhlasí s předáním a zpracováním svých osobních údajů pro účely uzavření této smlouvy. newline
tabulator tabulator NameLbl: Label 'Jméno', Comment = 'en-US=Name'; newline
tabulator tabulator IdCardNoLbl: Label 'Číslo OP', Comment = 'en-US=ID Card No.'; newline
tabulator tabulator DriverLicLbl: Label 'Číslo ŘP', Comment = 'en-US=Driver Licence No.'; newline
tabulator tabulator AuthPersTxt3: Label 'AuthPersTxt3'; //Dodavatel přenechává Klientovi vozidlo specifikované v předávacím protokolu k dočasnému užívání za níže uvedených podmínek... newline
tabulator tabulator RentLbl: Label 'NÁJEMNÉ', Comment = 'en-US=RENTAL PRICE'; newline
tabulator tabulator RentTxt1: Label 'RentTxt1'; //Všechny ceny jsou uvedeny v Kč bez DPH. Klient je povinen uhradit ceny zvýšené o DPH stanovenou podle příslušného právního předpisu... newline
tabulator tabulator RentTxt2: Label 'RentTxt2'; //Nájemné se fakturuje zpětně vždy na konci příslušného kalendářního měsíce. newline
tabulator tabulator OtherArrangementsLbl: Label 'Ostatní ujednání', Comment = 'en-US=Other arrangements'; newline
tabulator tabulator OtherArrangementsTxt1: Label 'OtherArrangementsTxt1'; //Smluvní vztah se řídí příslušnými ustanoveními Všeobecných obchodních podmínek Dodavatele (zejm. čl. 15), ... newline
tabulator tabulator ContractorLbl: Label 'Dodavatel', Comment = 'en-US=Contractor'; newline
tabulator tabulator ClientLbl: Label 'Klient', Comment = 'en-US=Client'; newline
tabulator tabulator InPragueOnLbl: Label 'V Praze, dne', Comment = 'en-US=In Prague on'; newline
tabulator tabulator InLbl: Label 'V', Comment = 'en-US=In'; newline
tabulator tabulator OnLbl: Label 'dne', Comment = 'en-US=on'; newline
tabulator tabulator RegNoLbl: Label 'IČO'; newline
tabulator tabulator VATRegNoLbl: Label 'DIČ'; newline
tabulator tabulator BankNameLbl: Label 'Banka', Comment = 'en-US=Bank'; newline
tabulator tabulator BankAcctLbl: Label 'Číslo bankovního účtu', Comment = 'en-US=Bank account No.'; newline
tabulator tabulator GearboxLbl: Label 'Převodovka', Comment = 'cs-CZ=Převodovka;en-US=Gearbox'; newline
tabulator tabulator ModelLbl: Label 'Značka / model', Comment = 'cs-CZ=Značka / model;en-US=Model'; newline
tabulator tabulator LicPlateLbl: Label 'Registrační značka', Comment = 'cs-CZ=Registrační značka;en-US=Licence Plate'; newline
 newline
}