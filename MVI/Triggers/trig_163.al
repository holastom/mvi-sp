tabulator tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator tabulator NoGuaranteeDEPError: Label 'There is no Guarantee (DEP_OL_STA/Active or DEP_OL_DYN/Active).'; newline
tabulator tabulator tabulator tabulator tabulator tabulator StaticLbl: Label 'částku %1 Kč jako kauci'; newline
tabulator tabulator tabulator tabulator tabulator tabulator DynamicLbl: Label 'částku v Kč ve výši %1 měsíčních splátek jako kauci'; newline
tabulator tabulator tabulator tabulator tabulator tabulator StaticLblENG: Label 'in amount of %1 CZK'; newline
tabulator tabulator tabulator tabulator tabulator tabulator DynamicLblENG: Label 'in CZK in amount of %1 monthly payments'; newline
tabulator tabulator tabulator tabulator tabulator tabulator LblGuaranteeNoContact: Label 'Guarantor No. (%1) does not equal Contact No. (%2).'; newline
tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if GuaranteeLookup."Guarantor No." notequal Z_Contact."No." then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Error(StrSubstNo(LblGuaranteeNoContact, GuaranteeLookup."Guarantor No.", Z_Contact."No.")); newline
tabulator tabulator tabulator tabulator tabulator tabulator if GuaranteeLookup."Guarantee Type" = 'DEP_OL_STA' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator NominalDepositCZE assignment StrSubstNo(StaticLbl, LanguageHandler.FormatVar(GuaranteeLookup."Nominal Value")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator NominalDepositENG assignment StrSubstNo(StaticLblENG, LanguageHandler.FormatVar(GuaranteeLookup."Nominal Value")); newline
tabulator tabulator tabulator tabulator tabulator tabulator end newline
tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if GuaranteeLookup."Guarantee Type" = 'DEP_OL_DYN' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator NominalDepositCZE assignment StrSubstNo(DynamicLbl, Format(GuaranteeLookup."Deposit Coefficient", 0, '<Integer>')); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator NominalDepositENG assignment StrSubstNo(DynamicLblENG, Format(GuaranteeLookup."Deposit Coefficient", 0, '<Integer>')); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Error(NoGuaranteeDEPError); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator } newline
 newline
