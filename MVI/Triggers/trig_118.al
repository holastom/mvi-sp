tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator NewFinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator PaymentCalendar: Record "API Financing Contract Line"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
 newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
 newline
tabulator tabulator ColumnCounter: Integer; newline
tabulator tabulator Column: Integer; newline
tabulator tabulator Row: Integer; newline
 newline
tabulator tabulator LBL_18: Label 'Negative numbers in "Settlement to" column mean overpayment, positive numbers mean underpayment.', Comment = 'cs-CZ=Záporná čísla v kolonce "Vyrovnání" znamenají, že Vám tuto částku vrátíme, kladná čísla znamenají doplatek.;en-US=Negative numbers in "Settlement to" column mean overpayment, positive numbers mean underpayment.'; newline
tabulator tabulator SheetName: Label 'Recalculation', Comment = 'cs-CZ=Recalculation;en-US=Recalculation'; newline
tabulator tabulator FileName: Label 'Recalculation_%1_%2', Comment = 'cs-CZ=Recalculation_%1_%2;en-US=Recalculation_%1_%2'; newline
tabulator begin newline
tabulator tabulator Row assignment 0; newline
tabulator tabulator Column assignment 1; newline
tabulator tabulator ColumnCounter assignment 1; newline
 newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator NewFinancingContractHeader.SetRange("Customer No.", Customer."No."); newline
tabulator tabulator NewFinancingContractHeader.SetRange("Calculation Variant", true); newline
tabulator tabulator NewFinancingContractHeader.SetRange("Calc. Variant Status", "API Calc. Variant Status"::Active); newline
tabulator tabulator if NewFinancingContractHeader.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator if FinancingContractHeader.Get(NewFinancingContractHeader."Change Copy to Contract No.") then newline
tabulator tabulator tabulator tabulator tabulator if FinancedObject.Get(NewFinancingContractHeader."Financed Object No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator NewFinancingContractHeader.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator PaymentCalendar.SetRange("Financing Contract No.", NewFinancingContractHeader."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator PaymentCalendar.SetRange(Type, "API Fin. Contract Line Type"::Payment); newline
tabulator tabulator tabulator tabulator tabulator tabulator PaymentCalendar.SetRange("Recalculation Settlement", true); newline
tabulator tabulator tabulator tabulator tabulator tabulator PaymentCalendar.SetRange(Posted, false); newline
tabulator tabulator tabulator tabulator tabulator tabulator if not PaymentCalendar.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PaymentCalendar."Lease Payment Total" assignment 0; newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator Column assignment Column + 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Financing Contract No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(NewFinancingContractHeader."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(NewFinancingContractHeader."BLG Driver Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Make Name" + ' ' + FinancedObject."Model Name" + ' ' + FinancedObject."Model Type Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Handover Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Contractual End Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Financing Period (in Months)", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Yearly Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Contractual Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Payment Excl. VAT", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(NewFinancingContractHeader."Financing Period (in Months)", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(NewFinancingContractHeader."Yearly Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(NewFinancingContractHeader."Contractual Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(NewFinancingContractHeader."Payment Excl. VAT", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PaymentCalendar."Lease Payment Total", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(NewFinancingContractHeader."Contractual End Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until NewFinancingContractHeader.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_18, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
 newline
tabulator tabulator BLFileManager.CustomizeCells(TmpExcelBuffer, 17); newline
 newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if ColumnCounter = Column then newline
tabulator tabulator tabulator tabulator break; newline
tabulator tabulator tabulator ColumnCounter assignment ColumnCounter + 1; newline
tabulator tabulator tabulator TmpExcelBuffer.Get(ColumnCounter, 1); newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger('1a91bc')); newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Font Name", 'Arial'); newline
tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator tabulator Row assignment 7; newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator Row assignment Row + 1; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.Get(ColumnCounter, Row); newline
tabulator tabulator tabulator tabulator if Row < 12 then newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger('fce4d6')) newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger('d9e1f2')); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.Validate("Font Name", 'Arial'); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator tabulator until Row = 17; newline
tabulator tabulator until ColumnCounter = Column; newline
tabulator tabulator CreateCellWidth(); newline
 newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, Customer.Name, Format(Today, 0, '<Day,2>.<Month,2>.<Year4>')) + '.xlsx'); newline
tabulator end; newline
