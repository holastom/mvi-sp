tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator HandoverDate: Record "API Vendor Confirmed Date"; newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator tabulator tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ExpectedHandoverDate assignment ''; newline
tabulator tabulator tabulator tabulator tabulator OriginalHandoverDate assignment ''; newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetFilter("Financed Object No.", PurchaseHeader."API Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetFilter("Object Purchase Order No.", PurchaseHeader."No."); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetRange("Record Type", "API VendorConfDateRecordType"::"Expected Handover Date to Customer"); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetRange("BLG Not. Est. HO To Cust.", false); newline
tabulator tabulator tabulator tabulator tabulator if not HandoverDate.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator CurrReport.Skip(); newline
 newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.SetRange("No.", PurchaseHeader."Shortcut Dimension 2 Code"); newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.SetRange(Status, "API Financing Contract Status"::Signed); newline
tabulator tabulator tabulator tabulator tabulator if FinancingContractHeader.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator Driver assignment FinancingContractHeader."BLG Driver Name"; newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancedObject.Get(FinancingContractHeader."Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator Model assignment FinancedObject."Make Name" + ' ' + FinancedObject."Model Name" + ' ' + FinancedObject."Model Type Name"; newline
tabulator tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator tabulator CurrReport.Skip(); newline
 newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.Reset(); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetFilter("Financed Object No.", PurchaseHeader."API Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetFilter("Object Purchase Order No.", PurchaseHeader."No."); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetRange("Record Type", "API VendorConfDateRecordType"::"Expected Handover Date to Customer"); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetAscending("Line No.", false); newline
tabulator tabulator tabulator tabulator tabulator if HandoverDate.FindFirst() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ExpectedHandoverDate assignment LanguageHandler.FormatDateNoSpace(HandoverDate."Confirmed Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator HandoverDate.Next(); newline
tabulator tabulator tabulator tabulator tabulator tabulator OriginalHandoverDate assignment LanguageHandler.FormatDateNoSpace(HandoverDate."Confirmed Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator if OriginalHandoverDate = ExpectedHandoverDate then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OriginalHandoverDate assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem(ConfirmedPurchaseHeader; "Purchase Header") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "BLG Customer No." = field("No."); newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("No.") where("API Order Content Type" = const(Object), "API Object Order Status" = const(Confirmed)); newline
tabulator tabulator tabulator tabulator column(CPH_No; "No.") { } newline
tabulator tabulator tabulator tabulator column(CPH_ContractNo; "Shortcut Dimension 2 Code") { } newline
tabulator tabulator tabulator tabulator column(CPH_DocumentDate; LanguageHandler.FormatDateNoSpace("Document Date")) { } newline
tabulator tabulator tabulator tabulator column(CPH_BuyFromVendorName; "Buy-from Vendor Name") { } newline
tabulator tabulator tabulator tabulator column(CPH_ExpectedHandoverDate; ExpectedHandoverDate) { } newline
tabulator tabulator tabulator tabulator column(CPH_Driver; Driver) { } newline
tabulator tabulator tabulator tabulator column(CPH_Model; Model) { } newline
 newline
