tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator Customer: Record Customer; newline
 newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
 newline
tabulator tabulator FileName: Label 'Serv_Activation_Tyres_%1'; newline
tabulator tabulator SheetName: Label 'ServActivtion-Tyres'; newline
tabulator begin newline
tabulator tabulator FinancingContractHeader.SetFilter("Financing Product Type Code", 'notequal%1', 'FUEL'); newline
tabulator tabulator FinancingContractHeader.SetFilter("Financed Object No.", 'notequal%1', ''); newline
tabulator tabulator FinancingContractHeader.SetFilter("Customer No.", 'notequal%1', ''); newline
tabulator tabulator FinancingContractHeader.SetFilter(Status, 'Active'); newline
 newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator if FinancingContractHeader.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator if FinancedObject.Get(FinancingContractHeader."Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator if Customer.Get(FinancingContractHeader."Customer No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("Financing Product Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Make Name" + ' ' + FinancedObject."Model Name" + ' ' + FinancedObject."Model Type Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Financing Product Type Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Financing Product Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until FinancingContractHeader.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
 newline
tabulator tabulator BLFileManager.CustomizeCells(TmpExcelBuffer, 5); newline
 newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
