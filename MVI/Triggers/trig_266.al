tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator OrderDate assignment LH.FormatDateNoSpace("Order Date"); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem(FinancingContractHeader; "API Financing Contract Header") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Financing Contract No."); newline
 newline
tabulator tabulator tabulator tabulator column(ContractSignedDate; LH.FormatDateNoSpace("Contract Signing Date (Comp.)")) { } newline
tabulator tabulator tabulator tabulator column(CustomerNo; "Customer No.") { } newline
tabulator tabulator tabulator tabulator column(CustomerName; "Customer Name") { } newline
tabulator tabulator tabulator tabulator column(ContractCostCenter; "BLG Cost Center Code") { } newline
tabulator tabulator tabulator tabulator column(CreatedBySalesperson; "Salesperson Code") { } newline
tabulator tabulator tabulator tabulator column(CntLng; LH.FormatVar("Financing Period (in Months)")) { } newline
tabulator tabulator tabulator tabulator column(YrlyDist; LH.FormatVar("Yearly Distance")) { } newline
tabulator tabulator tabulator tabulator column(CntInt; LH.FormatVar("Interest Calculation %")) { } newline
tabulator tabulator tabulator tabulator column(MonthlyPayment; LH.FormatVar("Payment Excl. VAT")) { } newline
tabulator tabulator tabulator tabulator column(FinancingProductNo; "Financing Product No.") { } newline
tabulator tabulator tabulator tabulator column(DriverName; "BLG Driver Name") { } newline
tabulator tabulator tabulator tabulator column(CustomerRegNo; "Registration No.") { } newline
tabulator tabulator tabulator tabulator column(ExteriorColor; ExteriorColor) { } newline
tabulator tabulator tabulator tabulator column(InteriorColor; InteriorColor) { } newline
tabulator tabulator tabulator tabulator column(SpecialAgreements; Utils.iif(SpecialAgreements = '', Lbl_NoneE, SpecialAgreements)) { } newline
tabulator tabulator tabulator tabulator dataitem(OriginalFinancingContractHeader; "API Financing Contract Header") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("BLG Original Contract No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator dataitem(OriginalFinancedObject; "API Financed Object") newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Financed Object No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator column(RenewalForLicNo; RenewalForLicNo) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(Renewal; Utils.iif("Licence Plate No." = '', '', 'ANO')) { } newline
 newline
