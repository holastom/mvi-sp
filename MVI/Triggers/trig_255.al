tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator CalcFields(Picture); newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator labels newline
tabulator { newline
tabulator tabulator CultureCode = 'CultureCode'; newline
tabulator tabulator CustomerLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_06'; //Zákazník newline
tabulator tabulator DateLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_07'; //Datum newline
tabulator tabulator VehicleLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_08'; //VOZIDLO newline
tabulator tabulator VehTypeLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_09'; //Značka / Typ / Varianta newline
tabulator tabulator FuelLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_10'; //Druh paliva newline
tabulator tabulator BodyColorLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_11'; //Barva karoserie newline
tabulator tabulator InterierColorLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_12'; //Barva interiéru newline
tabulator tabulator VehCategoryLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_13'; //Kategorie vozidla newline
tabulator tabulator VehVendorLbl = 'DED02_Purchase_Order_New_Vehicle_VehVendorLbl'; //DODAVATEL VOZIDLA newline
tabulator tabulator VendorLbl = 'DED02_Purchase_Order_New_Vehicle_VendorLbl'; //Dodavatel newline
tabulator tabulator AddressLbl = 'DED02_Purchase_Order_New_Vehicle_AddressLbl'; //Adresa newline
tabulator tabulator ExpctdHandovrDtLbl = 'DED02_Purchase_Order_New_Vehicle_ExpctdHandovrDtLbl'; //Očekávané datum předání vozidla klientovi newline
tabulator tabulator EquipmtSpecLbl = 'DED02_Purchase_Order_New_Vehicle_EquipmtSpecLbl'; //Specifikace výbavy objednaného vozidla pro společnost BUSINESS LEASE s.r.o. newline
tabulator tabulator SpecifLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_14'; //SPECIFIKACE newline
tabulator tabulator ListPriceLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_15'; //Katalogová cena (Kč) newline
tabulator tabulator DiscountLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_16'; //Sleva newline
tabulator tabulator PurchPriceLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_17'; //Pořizovací cena (Kč) newline
tabulator tabulator ExclVATlbl = 'DED02_Purchase_Order_New_Vehicle_exclVAT'; //bez DPH newline
tabulator tabulator InclVATlbl = 'DED02_Purchase_Order_New_Vehicle_inclVAT'; //s DPH newline
tabulator tabulator TotalLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_18'; //Celkem newline
tabulator tabulator NotesLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_20'; //Poznámky newline
tabulator tabulator AuthPersNameLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_21'; //Jméno a příjmení oprávněné osoby (hůlkovým písmem) newline
tabulator tabulator AuthPersSignLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_22'; //Datum a podpis oprávněné osoby newline
tabulator tabulator CzechRepLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_23'; //Česká republika newline
tabulator tabulator ContactPersonLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_31'; //Kontaktní osoba newline
tabulator tabulator PhoneLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_32'; //Telefon newline
tabulator tabulator EMailLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_33'; //E-mail newline
tabulator tabulator OrderNoLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_34'; //Číslo objednávky newline
tabulator tabulator ContractNoLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_35'; //Číslo leasingové smlouvy newline
tabulator tabulator RegNoLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_36'; //IČO newline
tabulator tabulator VATRegNoLbl = 'DIČ'; newline
tabulator tabulator TyresFormLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_42'; //Formulář k pneumatikám newline
tabulator tabulator FillPleaseLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_43'; //Prosíme o vyplnění níže uvedené tabulky: newline
tabulator tabulator TyresLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_44'; //PNEUMATIKY newline
tabulator tabulator TyreSpecLbl = 'DED02_Purchase_Order_New_Vehicle_TyreSpecLbl'; //Specifikace rozměrů pneumatik, na kterých přijelo vozidlo z výroby: newline
tabulator tabulator WidthLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_49'; //Nominální šířka a profilové číslo (př. 195/95) newline
tabulator tabulator DiameterLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_50'; //Průměr (př. R16) newline
tabulator tabulator TyreIndexesLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_51'; //Indexy (př. 91H) newline
tabulator tabulator BrandLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_52'; //Značka (př. Barum) newline
tabulator tabulator TireTreadLbl = 'DED02_Purchase_Order_New_Vehicle_LBL_53'; //Typ dezénu (př. Bravuris 2) newline
tabulator tabulator FrontAxleLbl = 'DED02_Purchase_Order_New_Vehicle_FrontAxleLbl'; //Přední náprava newline
tabulator tabulator RearAxleLbl = 'DED02_Purchase_Order_New_Vehicle_RearAxleLbl'; //Zadní náprava newline
tabulator tabulator TyresNoteLbl = 'DED02_Purchase_Order_New_Vehicle_TyresNoteLbl'; //Specifikace rozměrů pneumatik, na kterých přijelo vozidlo z výroby newline
tabulator tabulator AxleNoteLbl = 'DED02_Purchase_Order_New_Vehicle_AxleNoteLbl'; //V případě, že se od sebe nápravy neliší, není nutné údaje vyplňovat údaje o zadní nápravě. newline
tabulator tabulator TPMSlbl = 'DED02_Purchase_Order_New_Vehicle_TPMSlbl'; //TPMS newline
tabulator tabulator RunFlatLbl = 'DED02_Purchase_Order_New_Vehicle_RunFlatLbl'; //RunFlat newline
tabulator tabulator SelfSealingTyresLbl = 'DED02_Purchase_Order_New_Vehicle_SelfSealingTyresLbl'; //Samozacelující pneumatiky newline
tabulator tabulator PossibleHandoverDateLbl = 'DED02_Purchase_Order_New_Vehicle_PossibleHandoverDateLbl'; //Prosím upřesněte termín možného předání zákazníkovi včetně registrace newline
tabulator tabulator ReqDelivDateLbl = 'DED02_Purchase_Order_New_Vehicle_ReqDelivDateLbl'; //Nedodávat vozidlo dříveve než newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator Currency: Text; newline
tabulator tabulator PL_Direct_Unit_Cost: Text; newline
tabulator tabulator PL_Line_Discount_: Text; newline
tabulator tabulator PL_Amount: Text; newline
tabulator tabulator PL_AmtInclVAT: Text; newline
tabulator tabulator Sum_PL_Direct_Unit_Cost: Decimal; newline
tabulator tabulator Sum_PL_Amount: Decimal; newline
tabulator tabulator Sum_PL_AmtInclVAT: Decimal; newline
tabulator tabulator VehVenBussLocAddr: Text; newline
tabulator tabulator VehVenContPhoneMail: Text; newline
tabulator tabulator FuelType: Text; newline
tabulator tabulator V_RegNos: Text; newline
tabulator tabulator C_RegNos: Text; newline
 newline
tabulator tabulator LBL_01: Text; newline
tabulator tabulator LBL_01v: Label 'DED02_Purchase_Order_New_Vehicle_LBL_01v'; //Objednávka vozidla newline
tabulator tabulator LBL_01e: Label 'DED02_Purchase_Order_New_Vehicle_LBL_01e'; //Objednávka výbavy newline
tabulator tabulator LBL_02: Text; newline
tabulator tabulator LBL_02v: Label 'DED02_Purchase_Order_New_Vehicle_LBL_02v'; //Dodavatel vozidla newline
tabulator tabulator LBL_02e: Label 'DED02_Purchase_Order_New_Vehicle_LBL_02e'; //Dodavatel výbavy newline
tabulator tabulator LBL_03: Text; newline
tabulator tabulator LBL_03v: Label 'DED02_Purchase_Order_New_Vehicle_LBL_03v'; //Specifikace objednaného vozidla pro společnost BUSINESS LEASE s.r.o. newline
tabulator tabulator LBL_03e: Label 'DED02_Purchase_Order_New_Vehicle_LBL_03e'; //Příplatková výbava je určena pro vozidlo: newline
tabulator tabulator LBL_04: Text; newline
tabulator tabulator LBL_04v: Label 'DED02_Purchase_Order_New_Vehicle_LBL_04'; //Povinná výbava musí minimálně obsahovat výstražný trojúhelník a lékárničku. newline
tabulator tabulator LBL_05: Text; newline
tabulator tabulator LBL_05v: Label 'DED02_Purchase_Order_New_Vehicle_LBL_05v'; //Tímto potvrzujeme objednávku vozidla dle uvedené specifikace včetně podmínek v průvodním emailu newline
tabulator tabulator LBL_05e: Label 'DED02_Purchase_Order_New_Vehicle_LBL_05e'; //Tímto potvrzujeme objednávku výbavy dle výše uvedené specifikace: newline
tabulator tabulator LBL_05t: Label 'DED02_Purchase_Order_New_Vehicle_TyresNoteLbl'; //Tímto potvrzujeme, že vozidlo bylo dodáno na výše uvedených pneumatikách newline
tabulator tabulator LBL_09: Text; newline
tabulator tabulator LBL_09v: Label 'DED02_Purchase_Order_New_Vehicle_LBL_09v'; //Vozidlo v základním provedení: newline
tabulator tabulator LBL_09e: Label 'DED02_Purchase_Order_New_Vehicle_LBL_09e'; //Příplatková výbava: newline
tabulator tabulator LBL_10: Text; newline
tabulator tabulator //LBL_10v = '' newline
tabulator tabulator LBL_10e: Label 'DED02_Purchase_Order_New_Vehicle_LBL_10e'; //Cenové podmínky položek uvedených s nulovými cenami se řídí platnou smlouvou. newline
tabulator tabulator EmailTxt1: Text; newline
tabulator tabulator EmailTXT2: Text; newline
tabulator tabulator EmailTXT3: Text; newline
 newline
tabulator tabulator RegNoLbl: Label 'IČO'; newline
tabulator tabulator VATRegNoLbl: Label 'DIČ'; newline
 newline
