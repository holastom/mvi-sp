tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator Contact: Record Contact; newline
tabulator tabulator tabulator tabulator Vendor: Record Vendor; newline
tabulator tabulator tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator tabulator tabulator MobilePhone: Text; newline
tabulator tabulator tabulator tabulator Phone: Text; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator PurchaseHeader.CalcFields(Amount); newline
 newline
tabulator tabulator tabulator tabulator if FinancedObject.Get(PurchaseHeader."API Financed Object No.") then begin newline
tabulator tabulator tabulator tabulator tabulator CarName assignment FinancedObject."Make Name" + ' ' + FinancedObject."Model Name" + ' ' + FinancedObject."Model Type Name"; newline
tabulator tabulator tabulator tabulator tabulator VIN assignment FinancedObject.VIN; newline
tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator Error(StrSubstNo(Lbl_NoFO, PurchaseHeader."API Financed Object No.")); newline
 newline
tabulator tabulator tabulator tabulator if not Contact.Get(PurchaseHeader."BLG Vendor Contact Person No.") then begin newline
tabulator tabulator tabulator tabulator tabulator if Vendor.Get(FinancedObject."Vendor No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator VendorName assignment Vendor.Name; newline
tabulator tabulator tabulator tabulator tabulator tabulator PersonName assignment Vendor.Contact; newline
tabulator tabulator tabulator tabulator tabulator tabulator MobilePhone assignment Vendor."Mobile Phone No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator Phone assignment Vendor."Phone No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator EmailDod assignment Vendor."BLG Object Order Email Address"; newline
tabulator tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator tabulator Error(StrSubstNo(Lbl_NoContactNoVendorError, PurchaseHeader."No.", FinancedObject."No.")); newline
tabulator tabulator tabulator tabulator end newline
tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator VendorName assignment PurchaseHeader."BLG Vendor Business Place Name"; newline
tabulator tabulator tabulator tabulator tabulator PersonName assignment PurchaseHeader."BLG Vendor Contact Person Name"; newline
tabulator tabulator tabulator tabulator tabulator MobilePhone assignment Contact."Mobile Phone No."; newline
tabulator tabulator tabulator tabulator tabulator Phone assignment Contact."Phone No."; newline
tabulator tabulator tabulator tabulator tabulator EmailDod assignment Contact."E-Mail"; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator MobilePhone assignment LanguageHandler.FormatPhoneNumber(MobilePhone); newline
tabulator tabulator tabulator tabulator Phone assignment LanguageHandler.FormatPhoneNumber(Phone); newline
tabulator tabulator tabulator tabulator TelDodMerged assignment MergePhones(MobilePhone, Phone); newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
 newline
tabulator } newline
tabulator var newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator VendorName: Text; newline
tabulator tabulator PersonName: Text; newline
tabulator tabulator CarName: Text; newline
tabulator tabulator TelDodMerged: Text; newline
tabulator tabulator EmailDod: Text; newline
tabulator tabulator VIN: Text; newline
tabulator tabulator Lbl_NoContactNoVendorError: Label 'There is no Vendor Contact Person for Purchase Order: %1 and there is no Vendor for Financed Object: %2'; newline
tabulator tabulator Lbl_NoFO: Label 'There is No Financed Object with No.: %1'; newline
 newline
