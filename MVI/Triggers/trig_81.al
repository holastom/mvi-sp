tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator Driver: Record Contact; newline
tabulator tabulator ROH: Record "API Rent Order Header"; newline
tabulator tabulator ROL: Record "API Rent Order Line"; newline
 newline
tabulator tabulator CountPermLine: Record "Sales Cr.Memo Line"; newline
tabulator tabulator SalesCrMemoLine2: Record "Sales Cr.Memo Line"; newline
tabulator tabulator MaintPermissionLine: Record "API Maint. Permission Line"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator TotalNetto: Decimal; newline
tabulator tabulator VatChargeable: Decimal; newline
tabulator tabulator VatNotChargeable: Decimal; newline
 newline
tabulator tabulator OutStream: OutStream; newline
 newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
 newline
tabulator tabulator LineNo: Integer; newline
tabulator tabulator First: Boolean; newline
tabulator tabulator Amount: Decimal; newline
tabulator tabulator LicPlateNo: Text; newline
tabulator tabulator DriverName: Text; newline
tabulator tabulator FinObjName: Text; newline
tabulator tabulator CostCenter: Text; newline
tabulator tabulator CustNo: Code[20]; newline
tabulator tabulator ContractNo: Text; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator SA.GroupByMaintancePermisionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator SCML.SetRange("Document No.", SCMH."No."); newline
tabulator tabulator tabulator SCML.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator tabulator SCML.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator if GroupedCode notequal '' then begin newline
tabulator tabulator tabulator tabulator CountPermLine.SetRange("Document No.", SCMH."No."); newline
tabulator tabulator tabulator tabulator CountPermLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator tabulator tabulator CountPermLine.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator CountPermLine.FindSet(); newline
tabulator tabulator tabulator tabulator if CountPermLine.Count() notequal 1 then begin newline
tabulator tabulator tabulator tabulator tabulator MaintPermissionLine.SetRange("Maintenance Permission No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator MaintPermissionLine.SetRange("Category Code", ''); newline
tabulator tabulator tabulator tabulator tabulator if MaintPermissionLine.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator LineNo assignment MaintPermissionLine."Line No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator SalesCrMemoLine2.SetRange("Document No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator SalesCrMemoLine2.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator tabulator SalesCrMemoLine2.SetRange("BLG Maint. Perm. Line No.", LineNo); newline
tabulator tabulator tabulator tabulator tabulator tabulator SalesCrMemoLine2.FindFirst(); newline
tabulator tabulator tabulator tabulator tabulator tabulator LineNo assignment SalesCrMemoLine2."Line No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator First assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator Amount assignment SalesCrMemoLine2.GetLineAmountExclVAT(); newline
tabulator tabulator tabulator tabulator tabulator tabulator SCML.SetFilter("Line No.", 'notequal%1', LineNo); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator tabulator SCML.FindSet(); newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator VatChargeable assignment 0; newline
tabulator tabulator tabulator tabulator VatNotChargeable assignment 0; newline
tabulator tabulator tabulator tabulator TotalNetto assignment 0; newline
 newline
tabulator tabulator tabulator tabulator if SCML."VAT %" notequal 0 then newline
tabulator tabulator tabulator tabulator tabulator VatChargeable assignment SCML.GetLineAmountExclVAT() newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator VatNotChargeable assignment SCML.GetLineAmountExclVAT(); newline
 newline
tabulator tabulator tabulator tabulator if First then begin newline
tabulator tabulator tabulator tabulator tabulator VatChargeable += Amount; newline
tabulator tabulator tabulator tabulator tabulator First assignment false; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TotalNetto assignment VatChargeable + VatNotChargeable; newline
 newline
tabulator tabulator tabulator tabulator if FCH.Get(SCML."Shortcut Dimension 2 Code") then begin newline
tabulator tabulator tabulator tabulator tabulator FCH.CalcFields("BLG Driver Name", "BLG Cost Center Name"); newline
tabulator tabulator tabulator tabulator tabulator if FO.Get(FCH."Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment FO."Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator DriverName assignment FCH."BLG Driver Name"; newline
tabulator tabulator tabulator tabulator tabulator FinObjName assignment FO."BLG Name 2"; newline
tabulator tabulator tabulator tabulator tabulator CostCenter assignment FCH."BLG Cost Center Name"; newline
tabulator tabulator tabulator tabulator tabulator CustNo assignment FCH."Customer No."; newline
tabulator tabulator tabulator tabulator tabulator ContractNo assignment FCH."No."; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator if ROH.Get(ROH."Contract Type"::Rent, SCML."API Rent No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ROL.SetRange("Contract Type", ROH."Contract Type"); newline
tabulator tabulator tabulator tabulator tabulator tabulator ROL.SetRange("Contract No.", ROH."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator if not ROL.FindFirst then Clear(ROL); //řádek má být vždy jediný newline
tabulator tabulator tabulator tabulator tabulator tabulator ROL.CalcFields("Car Description"); newline
tabulator tabulator tabulator tabulator tabulator tabulator ROH.CalcFields("BLG Cost Center Name"); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator DriverName assignment Utils.iif(Driver.Get(ROH."Authorized User 1"), Driver.Name, ROH."Authorized User 1"); newline
tabulator tabulator tabulator tabulator tabulator tabulator FinObjName assignment ROL."Car Description"; newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment SCML."API RC Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator CostCenter assignment ROH."BLG Cost Center Name"; newline
tabulator tabulator tabulator tabulator tabulator tabulator CustNo assignment ROH."Renter No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator ContractNo assignment SCML."API Rent No."; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(ROH); newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(ROL); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Return Order No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinObjName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCML.Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LicPlateNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCML."VAT %", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatChargeable, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatNotChargeable, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(TotalNetto, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Document Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."VAT Date CZL", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Due Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCML.Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(DriverName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CostCenter, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CustNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
 newline
tabulator tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Invoice Spec No 8-9-13 RENTAL Cr. Memo'); newline
tabulator tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator tabulator end; newline
tabulator tabulator end; newline
tabulator end; newline
