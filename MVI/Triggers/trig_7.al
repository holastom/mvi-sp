tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FuelCardTransaction: Record "API Fuel Card Transaction"; newline
tabulator tabulator FuelCardTypeProduct: Record "API Fuel Card Type Product"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator Attachment: File; newline
tabulator begin newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::UTF8); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
 newline
tabulator tabulator FuelCardTransaction.SetRange("Posted Sales Invoice No.", SIH."No."); newline
tabulator tabulator if FuelCardTransaction.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FinancingContractHeader.Get(FuelCardTransaction."Financing Contract No."); newline
tabulator tabulator tabulator tabulator FinancedObject.Get(FinancingContractHeader."Financed Object No."); newline
tabulator tabulator tabulator tabulator FuelCardTypeProduct.Get(FuelCardTransaction."Product Code"); newline
tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("BLG Driver Name"); newline
 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('B'); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FuelCardTransaction."Card No.", 22)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FinancedObject."Licence Plate No.", 31)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FinancingContractHeader."BLG Driver Name", 31)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 31)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(Format(FuelCardTransaction."Transaction Date", 0, '<Day,2><Month,2><Year4>')); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(Format(FuelCardTransaction."Transaction Time", 0, '<Hours24,2><Minutes,2>')); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 22)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(FuelCardTransaction."Petrol Station No.", 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FuelCardTransaction."Petrol Station Location", 38)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(CopyStr(FuelCardTransaction."Item No.", 2), 4)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FuelCardTypeProduct."Product Name", 35)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(FuelCardTransaction.Quantity, 2), 8)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText('Kc '); newline
tabulator tabulator tabulator tabulator if FuelCardTransaction."Item No." = '00088' then newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('3') newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator if FuelCardTransaction.VAT = 0 then newline
tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('9') newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('2'); newline
 newline
tabulator tabulator tabulator tabulator if FuelCardTransaction.VAT = 0 then newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('0') newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('1'); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(FuelCardTransaction."VAT Rate % - Customer", 2), 5)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(FuelCardTransaction."Unit Price Incl. VAT", 4), 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(FuelCardTransaction."Tot. Pr. In. VAT (LCY) - Cust.", 2), 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(FuelCardTransaction."VAT Amount - Customer", 2), 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(FuelCardTransaction."Tot. Pr. In. VAT (LCY) - Cust.", 2), 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Format(FuelCardTransaction."Odometer Status"), 7)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText('01'); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator tabulator until FuelCardTransaction.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
