tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator MPL: Record "API Maint. Permission Line"; newline
tabulator tabulator tabulator tabulator tabulator Vendor: Record Vendor; newline
tabulator tabulator tabulator tabulator tabulator Damages: List of [Text]; newline
tabulator tabulator tabulator tabulator tabulator PartDamage: Text; newline
tabulator tabulator tabulator tabulator tabulator Parts: List of [Text]; newline
tabulator tabulator tabulator tabulator tabulator Part: Text; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator InsuranceClaim.CalcFields("Expected Price excl. VAT"); newline
tabulator tabulator tabulator tabulator tabulator MPL.SetRange("Insurance Claim No.", InsuranceClaim."No."); newline
tabulator tabulator tabulator tabulator tabulator MPL.SetFilter("Vendor No.", 'notequal%1', ''); newline
tabulator tabulator tabulator tabulator tabulator if MPL.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator Vendor.Get(MPL."Vendor No."); newline
tabulator tabulator tabulator tabulator tabulator CVendor assignment Utils.iif(Vendor.Name = '', '-', Vendor.Name); newline
 newline
tabulator tabulator tabulator tabulator tabulator if InsuranceClaim."BLG Damage Extent" = '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator CDescription assignment InsuranceClaim.Description newline
tabulator tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator CDescription assignment ''; newline
tabulator tabulator tabulator tabulator tabulator tabulator // 1. split the string by semicolon ";" (= array of N items) newline
tabulator tabulator tabulator tabulator tabulator tabulator Damages assignment InsuranceClaim."BLG Damage Extent".Split(';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator foreach PartDamage in Damages do newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator // 2. remove items that end with "/Žádné poškození" newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if not (PartDamage.Contains('/Žádné poškození') or PartDamage.Contains('/ Žádné poškození')) then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // 3. take the rest of the items and split one by one by slash "/"(= two parts from each item) newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Parts assignment PartDamage.Split('/'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // 4. make a trim() on the first parts to remove leading or trailing spaces newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Part assignment Parts.Get(1).Trim(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // 5. print the first parts of the items joined with “, “ newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CDescription += ', ' + Part.ToLower(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator CDescription assignment DelChr(CDescription, '<', ', '); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem(ContractService; "API Contract Service") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Financed Object No." = field("No."); newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("No.") where("Service Kind" = filter("Highway Ticket")); newline
tabulator tabulator tabulator tabulator dataitem(HighwayTicketDetailLine; "API Highway Ticket Detail Line") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "Service No." = field("No."); newline
tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("Line No.") order(descending); newline
tabulator tabulator tabulator tabulator tabulator column(HWStampValidity; LH.FormatDateNoSpace("Valid To")) { } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
 newline
