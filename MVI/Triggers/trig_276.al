tabulator trigger OnPreReport() newline
tabulator var newline
tabulator tabulator ErrorNoSaleLbl: Label 'Field "%1" must be TRUE in table "%2".'; newline
tabulator begin newline
tabulator tabulator if FO.Count notequal 1 then CurrReport.Quit; newline
tabulator tabulator FO.FindFirst; newline
tabulator tabulator LangHndl.ReportLanguage(); newline
tabulator tabulator OSP.SetRange("Financed Object No.", FO."No."); newline
tabulator tabulator OSP.SetRange("BLG Sale", true); newline
tabulator tabulator if not OSP.FindFirst then newline
tabulator tabulator tabulator Error(StrSubstNo(ErrorNoSaleLbl, OSP.FieldCaption("BLG Sale"), OSP.TableCaption)); newline
tabulator tabulator Customer.Get(OSP."Buyer No."); newline
tabulator tabulator if DateOfSignature = 0D then DateOfSignature assignment Today; newline
tabulator tabulator if AddText = '' then begin newline
tabulator tabulator tabulator AddTextCZ assignment AddTextInitCZ; newline
tabulator tabulator tabulator AddTextEN assignment AddTextInitEN; newline
tabulator tabulator end newline
tabulator tabulator else begin newline
tabulator tabulator tabulator AddTextCZ assignment AddText; newline
tabulator tabulator tabulator AddTextEN assignment AddText; newline
tabulator tabulator end; newline
tabulator end; newline
