tabulator trigger OnPreReport() newline
tabulator var newline
tabulator tabulator customerRecord: Record Customer; newline
tabulator tabulator Language: Codeunit Language; newline
tabulator begin newline
tabulator tabulator Utils.ErrorIfFilterEmpty("API Financing Contract Header".GetFilter("No."), 'No'); newline
tabulator tabulator "API Financing Contract Header".FindFirst; newline
tabulator tabulator customerRecord.Get("API Financing Contract Header"."Customer No."); newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(RequestPageLanguage, customerRecord."Language Code"); newline
tabulator tabulator isLocalLanguage assignment CurrReport.Language = LanguageHandler.GetCompanyLanguageId; newline
tabulator tabulator ENLangId assignment LanguageHandler.GetENlanguageId; newline
tabulator tabulator CZLangId assignment LanguageHandler.GetCZlanguageId; newline
 newline
tabulator tabulator CompanyInformation.Get(); newline
tabulator tabulator CI_CountryRegion assignment Utils.CountryName(CompanyInformation."Country/Region Code"); newline
tabulator tabulator StartOfNextMonth assignment CalcDate('<-CM+1M>', Today); newline
tabulator end; newline
