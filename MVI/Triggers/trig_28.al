tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
 newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator PojHav: Decimal; newline
tabulator tabulator PojPos: Decimal; newline
tabulator tabulator PojPov: Decimal; newline
tabulator tabulator AutFM: Decimal; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator SA.GroupByShortcutDimensionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator PojHav assignment 0; newline
tabulator tabulator tabulator PojPos assignment 0; newline
tabulator tabulator tabulator PojPov assignment 0; newline
tabulator tabulator tabulator AutFM assignment 0; newline
 newline
tabulator tabulator tabulator GetAmounts(PojHav, PojPos, PojPov, AutFM, GroupedCode, SCMH."No."); newline
 newline
tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Return Order No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PojHav, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PojPos, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PojPov, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(AutFM, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(1, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator end; newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBufferWithColoredText(TmpExcelBuffer, AttachmentFilePath, OutStream, 'Autocont', 'D9E1F2', 2, 6, 2); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
