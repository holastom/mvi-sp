tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator OSH: Record "API Odometer Status History"; newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator tabulator SIL2: Record "Sales Invoice Line"; newline
tabulator tabulator MPL: Record "API Maint. Permission Line"; newline
tabulator tabulator CPL: Record "Sales Invoice Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator Customer: Record Customer; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator Attachment: File; newline
 newline
tabulator tabulator TotalAmountVat: Decimal; newline
tabulator tabulator x: Integer; newline
tabulator tabulator CountLines: Integer; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator tabulator LineNo: Integer; newline
tabulator tabulator First: Boolean; newline
tabulator tabulator Amount: Decimal; newline
tabulator tabulator AmountExclVat: Decimal; newline
tabulator tabulator LicPlateNo: Text; newline
tabulator begin newline
tabulator tabulator CountLines assignment 0; newline
tabulator tabulator TotalAmountVat assignment 0; newline
tabulator tabulator Customer.Get(SIH."Sell-to Customer No."); newline
 newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::UTF8); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
 newline
tabulator tabulator //Header newline
tabulator tabulator OutStream.WriteText('H'); newline
tabulator tabulator OutStream.WriteText(CopyStr(Customer."No.", 2)); newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 8)); newline
tabulator tabulator OutStream.WriteText(Format(Today(), 0, '<Day,2><Month,2><Year4>')); newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SIH."Sell-to Customer Name", 31)); newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(Customer.Address, 31)); newline
tabulator tabulator Customer."Post Code" assignment DelChr(Customer."Post Code"); newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(Customer."Post Code", 5)); newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(Customer.City, 31)); newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 6)); newline
tabulator tabulator OutStream.WriteText('BusinessLease  1  '); newline
tabulator tabulator OutStream.WriteText(SA.FillZeros(CopyStr(Customer."No.", 2), 8)); newline
tabulator tabulator for x assignment 1 to 130 do begin newline
tabulator tabulator tabulator OutStream.WriteText(' '); newline
tabulator tabulator end; newline
 newline
tabulator tabulator SA.GroupByMaintancePermision(FCHCodes, SIH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator SIL.SetRange("Document No.", SIH."No."); newline
tabulator tabulator tabulator SIL.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator tabulator SIL.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator if GroupedCode notequal '' then begin newline
tabulator tabulator tabulator tabulator CPL.SetRange("Document No.", SIH."No."); newline
tabulator tabulator tabulator tabulator CPL.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator tabulator tabulator CPL.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator CPL.FindSet(); newline
tabulator tabulator tabulator tabulator if CPL.Count() notequal 1 then begin newline
tabulator tabulator tabulator tabulator tabulator MPL.SetRange("Maintenance Permission No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator MPL.SetRange("Category Code", ''); newline
tabulator tabulator tabulator tabulator tabulator if MPL.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator LineNo assignment MPL."Line No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator SIL2.SetRange("Document No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator SIL2.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator tabulator SIL2.SetRange("BLG Maint. Perm. Line No.", LineNo); newline
tabulator tabulator tabulator tabulator tabulator tabulator SIL2.FindFirst(); newline
tabulator tabulator tabulator tabulator tabulator tabulator LineNo assignment SIL2."Line No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator First assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator Amount assignment SIL2.GetLineAmountExclVAT(); newline
tabulator tabulator tabulator tabulator tabulator tabulator SIL.SetFilter("Line No.", 'notequal%1', LineNo); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator tabulator SIL.FindSet(); newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator AmountExclVat assignment 0; newline
 newline
tabulator tabulator tabulator tabulator AmountExclVat assignment SIL.GetLineAmountExclVAT(); newline
 newline
tabulator tabulator tabulator tabulator if First then begin newline
tabulator tabulator tabulator tabulator tabulator AmountExclVat += Amount; newline
tabulator tabulator tabulator tabulator tabulator First assignment false; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator if FCH.Get(SIL."Shortcut Dimension 2 Code") then begin newline
tabulator tabulator tabulator tabulator tabulator if FO.Get(FCH."Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment FO."Licence Plate No."; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator Clear(FCH); newline
tabulator tabulator tabulator tabulator tabulator Clear(FO); newline
tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment SIL."API RC Licence Plate No."; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator //Body newline
tabulator tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator tabulator tabulator OutStream.WriteText('B');//b1 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 22));//b2 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(LicPlateNo, 31));//b3 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 31));//b4 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(Format(SIH."Posting Date", 0, '<Day,2><Month,2><Year4>'), 8));//b5 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 4));//b6 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('N Česka republikatabulator  ');//b7 + b8 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 50));//b9 + b10 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SIL.Description, 39));//b12 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(SIL.Quantity, 2), 8));//b13 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('Kc 21');//b14 + b15 + b16 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(SIL."VAT %", 2), 5));//b17 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(AmountExclVat / SIL.Quantity, 4), 12));//b18 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(AmountExclVat, 2), 12));//b19 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(SIL."VAT Base Amount", 2), 12));//b20 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(AmountExclVat, 2), 12));//b21 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(Format(OSH.Mileage), 7));//b22 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('02');//b23 newline
tabulator tabulator tabulator tabulator CountLines += 1; newline
tabulator tabulator tabulator tabulator TotalAmountVat += AmountExclVat; newline
tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator //Trailer newline
tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator OutStream.WriteText('T');//t1 newline
tabulator tabulator OutStream.WriteText(SA.FillZeros(Format(CountLines), 8));//t2 newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(Sa.FormatDecimal(TotalAmountVat, 2), 14));//t3 newline
tabulator tabulator for x assignment 1 to 260 do begin newline
tabulator tabulator tabulator OutStream.WriteText(' '); newline
tabulator tabulator end; //t4 newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
