tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator MA_Special_Agreement: Record "BLG MA Special Agreement"; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator MA_Special_Agreement.SetFilter("Master Agreement No.", MasterAgreement."No."); newline
tabulator tabulator tabulator tabulator tabulator MA_Special_Agreement.SetFilter(Status, '%1|%2', "BLG Special Agr. Status"::Active, "BLG Special Agr. Status"::Approved); newline
tabulator tabulator tabulator tabulator tabulator MA_Special_Agreement.SetFilter("Valid From", '<=%1|%2', Today(), 0D); newline
tabulator tabulator tabulator tabulator tabulator MA_Special_Agreement.SetFilter("Valid To", '>=%1|%2', Today(), 0D); newline
tabulator tabulator tabulator tabulator tabulator if MA_Special_Agreement.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator SAParameters += Utils.crlf() + MA_Special_Agreement."SA Parameters"; newline
tabulator tabulator tabulator tabulator tabulator tabulator until MA_Special_Agreement.Next() = 0; newline
tabulator tabulator tabulator tabulator tabulator SAParameters assignment DelChr(SAParameters, '<', Utils.crlf()); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator } newline
 newline
