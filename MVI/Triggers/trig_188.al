tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator MPL: Record "API Maint. Permission Line"; newline
tabulator tabulator tabulator tabulator InsuranceCompany: Record "API Insurance Company"; newline
tabulator tabulator tabulator tabulator Contact: Record Contact; newline
tabulator tabulator tabulator tabulator Customer: Record Customer; newline
tabulator tabulator tabulator tabulator Vendor: Record Vendor; newline
tabulator tabulator tabulator tabulator ErrorMoreMP: Label 'There is more than 1 Maintenance Permission for Insurance Claim No. %1.'; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator if MP."No." = '' then begin newline
tabulator tabulator tabulator tabulator tabulator MP.SetRange("Insurance Claim No.", InsuranceClaim."No."); newline
tabulator tabulator tabulator tabulator tabulator MP.SetRange("Financed Object No.", InsuranceClaim."Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator MP.SetRange("Financing Contract No.", InsuranceClaim."Financing Contract No."); newline
tabulator tabulator tabulator tabulator tabulator MP.FindSet(); newline
tabulator tabulator tabulator tabulator tabulator if MP.Count > 1 then newline
tabulator tabulator tabulator tabulator tabulator tabulator Error(StrSubstNo(ErrorMoreMP, InsuranceClaim."No.")); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator MPL.SetRange("Maintenance Permission No.", MP."No."); newline
tabulator tabulator tabulator tabulator MPL.SetRange("Category Code", 'I_REPAIR_D'); newline
tabulator tabulator tabulator tabulator MPL.FindFirst(); newline
tabulator tabulator tabulator tabulator if ReceipmentRP = ReceipmentRP::Insurance then begin newline
tabulator tabulator tabulator tabulator tabulator InsuranceCompany.Get(InsuranceClaim."Insurance Company No."); newline
tabulator tabulator tabulator tabulator tabulator Contact.Get(InsuranceCompany."Contact No."); newline
tabulator tabulator tabulator tabulator tabulator R_Name assignment Contact.Name; newline
tabulator tabulator tabulator tabulator tabulator R_Address assignment Contact.Address; newline
tabulator tabulator tabulator tabulator tabulator R_PSC assignment Contact."Post Code"; newline
tabulator tabulator tabulator tabulator tabulator R_City assignment Contact.City; newline
tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator if ReceipmentRP = ReceipmentRP::Client then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator Customer.Get(MPL."Customer No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator R_Name assignment Customer.Name; newline
tabulator tabulator tabulator tabulator tabulator tabulator R_Address assignment Customer.Address; newline
tabulator tabulator tabulator tabulator tabulator tabulator R_PSC assignment Customer."Post Code"; newline
tabulator tabulator tabulator tabulator tabulator tabulator R_City assignment Customer.City; newline
tabulator tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator tabulator if ReceipmentRP = ReceipmentRP::Service then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Vendor.Get(MPL."Vendor No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator R_Name assignment Vendor.Name; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator R_Address assignment Vendor.Address; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator R_PSC assignment Vendor."Post Code"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator R_City assignment Vendor.City; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator if CustomerRP = CustomerRP::Client then begin newline
tabulator tabulator tabulator tabulator tabulator Customer.Get(MPL."Customer No."); newline
tabulator tabulator tabulator tabulator tabulator C_Name assignment Customer.Name; newline
tabulator tabulator tabulator tabulator tabulator C_Address assignment Customer.Address; newline
tabulator tabulator tabulator tabulator tabulator C_PSC assignment Customer."Post Code"; newline
tabulator tabulator tabulator tabulator tabulator C_City assignment Customer.City; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator Vendor.Get(MPL."Vendor No."); newline
tabulator tabulator tabulator tabulator tabulator C_Name assignment Vendor.Name; newline
tabulator tabulator tabulator tabulator tabulator C_Address assignment Vendor.Address; newline
tabulator tabulator tabulator tabulator tabulator C_PSC assignment Vendor."Post Code"; newline
tabulator tabulator tabulator tabulator tabulator C_City assignment Vendor.City; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(Content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group(RecipientSelection) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Recipient Selection'; newline
tabulator tabulator tabulator tabulator tabulator field(Receipment; ReceipmentRP) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Letter Recipient'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(Customer; CustomerRP) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Money Recipient'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = All; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(MaintenancePermission; MP."No.") newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Maintenance Permission'; newline
 newline
