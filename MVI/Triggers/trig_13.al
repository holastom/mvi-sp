tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator SalesCrMemoLine: Record "Sales Cr.Memo Line"; newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FuelCardTypeProduct: Record "API Fuel Card Type Product"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator Attachment: File; newline
tabulator begin newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::UTF8); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
 newline
tabulator tabulator SalesCrMemoLine.SetRange("Document No.", SCMH."No."); newline
tabulator tabulator SalesCrMemoLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
 newline
tabulator tabulator if SalesCrMemoLine.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FinancingContractHeader.Get(SalesCrMemoLine."Shortcut Dimension 2 Code"); newline
tabulator tabulator tabulator tabulator FinancedObject.Get(FinancingContractHeader."Financed Object No."); newline
tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("BLG Driver Name"); newline
 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('B'); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 22)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FinancedObject."Licence Plate No.", 31)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FinancingContractHeader."BLG Driver Name", 31)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 31)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText('', 22); newline
tabulator tabulator tabulator tabulator OutStream.WriteText('', 22); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 22)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2('', 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 38)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 4)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SalesCrMemoLine.Description, 35)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(SalesCrMemoLine.Quantity, 2), 8)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText('Kc '); newline
tabulator tabulator tabulator tabulator if SalesCrMemoLine."VAT %" = 0 then newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('9') newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('2'); newline
 newline
tabulator tabulator tabulator tabulator if SalesCrMemoLine."VAT %" = 0 then newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('0') newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('1'); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(SalesCrMemoLine."VAT %", 2), 5)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(SalesCrMemoLine."Unit Price", 4), 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(SalesCrMemoLine."Amount Including VAT", 2), 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(SalesCrMemoLine."Amount Including VAT" - SalesCrMemoLine."VAT Base Amount", 2), 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(Sa.FormatDecimal(SalesCrMemoLine."Amount Including VAT", 2), 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2('', 7)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText('01'); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator tabulator until SalesCrMemoLine.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
