tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator fob: Record "API Financed Object"; newline
tabulator tabulator tabulator tabulator tabulator Contact: Record Contact; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator if FCH."No." notequal "Financing Contract No." then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator FCH.Get("Financing Contract No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator //FCH.CalcFields("BLG Driver Name", "BLG Cost Center Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator if fob.Get(FCH."Financed Object No.") then FinObjName assignment StrSubstNo('%1 %2 %3', fob."Make Name", fob."Model Name", fob."Model Type Name") else FinObjName assignment ''; newline
tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator if Contact.Get("Driver No.") then DriverName assignment Contact.Name; newline
 newline
tabulator tabulator tabulator tabulator tabulator //Konzultace s P.Michalem a M.Šašikovou 26.5.22 + potvrzovací mail od P.M. z 30.5.22 newline
tabulator tabulator tabulator tabulator tabulator // - sazby DPH u přefakt. paliva u Bus. Posting Group EX nebo EXPORT (rev.charge) se nastaví na 0 newline
tabulator tabulator tabulator tabulator tabulator // - řádky rekapitulace DPH s nulovou sazbou se sečtou v layoutu, protože je to mnohem jednodušší newline
tabulator tabulator tabulator tabulator tabulator if SIH."VAT Bus. Posting Group" in ['EU', 'EXPORT'] then "VAT Rate % - Customer" assignment 0; newline
tabulator tabulator tabulator tabulator tabulator LineID assignment 'SumLine' + '-' + "Product Code" + '-' + Format("VAT Rate % - Customer"); newline
tabulator tabulator tabulator tabulator tabulator CardID assignment 'CardSumLine' + '-' + "Card No. - Company" + '-' + DriverName; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem(VATCounter; "VAT Amount Line") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator UseTemporary = true; newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("VAT Identifier", "VAT Calculation Type", "Tax Group Code", "Use Tax", Positive); newline
 newline
tabulator tabulator tabulator tabulator column(VAT_Id; "VAT Identifier") { } newline
tabulator tabulator tabulator tabulator column(VAT_pct; "VAT %") { } newline
tabulator tabulator tabulator tabulator column(VAT_Base; "VAT Base") { } newline
tabulator tabulator tabulator tabulator column(VAT_Amount; "VAT Amount") { } newline
tabulator tabulator tabulator tabulator column(VAT_AmtInclVAT; "Amount Including VAT") { } newline
tabulator tabulator tabulator tabulator column(VAT_BaseLCY; "VAT Base (LCY) CZL") { } newline
tabulator tabulator tabulator tabulator column(VAT_AmountLCY; "VAT Amount (LCY) CZL") { } newline
tabulator tabulator tabulator tabulator column(VAT_AmtInclVATlcy; "VAT Base (LCY) CZL" + "VAT Amount (LCY) CZL") { } newline
tabulator tabulator tabulator tabulator column(VAT_ClauseCode; VATClauseCode) { } newline
tabulator tabulator tabulator tabulator column(VAT_ClauseText; VATClauseText) { } newline
 newline
