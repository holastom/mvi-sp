tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() //FO newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator fuelTypeRecord: Record "API Fuel Type"; newline
tabulator tabulator tabulator tabulator tabulator gearboxRecord: Record "API Gearbox"; newline
 newline
tabulator tabulator tabulator tabulator tabulator ooeRecord: Record "API Object Optional Equipment"; newline
tabulator tabulator tabulator tabulator tabulator oaeRecord: Record "API Object Add. Equipment"; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator if fuelTypeRecord.Get("Fuel Type Code") then newline
tabulator tabulator tabulator tabulator tabulator tabulator FO_FuelType assignment fuelTypeRecord.Name newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator FO_FuelType assignment "Fuel Type Code"; newline
tabulator tabulator tabulator tabulator tabulator if gearboxRecord.Get(Gearbox) then newline
tabulator tabulator tabulator tabulator tabulator tabulator FO_Gearbox assignment gearboxRecord.Description newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator FO_Gearbox assignment Gearbox; newline
 newline
tabulator tabulator tabulator tabulator tabulator if "Type of Body" notequal '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator typeOfBody.Get("Type of Body") newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(typeOfBody); newline
 newline
tabulator tabulator tabulator tabulator tabulator ooeRecord.SetRange("Financed Object No.", "API Financing Contract Header"."Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator ooeRecord.CalcSums("Price Excl. VAT (LCY)", "List Price Excl. VAT (LCY)"); newline
tabulator tabulator tabulator tabulator tabulator oaeRecord.SetRange("Financed Object No.", "API Financing Contract Header"."Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator oaeRecord.CalcSums("Price Excl. VAT (LCY)", "List Price Excl. VAT (LCY)"); newline
 newline
tabulator tabulator tabulator tabulator tabulator Equipment_List_Price_Sum assignment ooeRecord."List Price Excl. VAT (LCY)" + oaeRecord."List Price Excl. VAT (LCY)"; newline
tabulator tabulator tabulator tabulator tabulator Equipment_Price_Sum assignment ooeRecord."Price Excl. VAT (LCY)" + oaeRecord."Price Excl. VAT (LCY)"; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem("BLG Contract Mileage Rate Buffer"; "BLG Contract Mileage Rate") //CM newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator UseTemporary = true; newline
tabulator tabulator tabulator tabulator //DataItemTableView = sorting("Financing Contract No.", "Code"); newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("Mileage From"); newline
 newline
tabulator tabulator tabulator tabulator column(CM___; '') { } newline
tabulator tabulator tabulator tabulator column(CM_MILEAGE_RANGE; StrSubstNo('%1 - %2 km', LanguageHandler.FormatVar("Mileage From"), LanguageHandler.FormatVar("Mileage To (Incl.)"))) { } //ID_103 newline
tabulator tabulator tabulator tabulator column(CM_MILEAGE_OVER; LanguageHandler.FormatVar("Rate excl.VAT")) { } //ID_104 newline
tabulator tabulator tabulator tabulator column(CM_MILEAGE_UNFINISHED; LanguageHandler.FormatVar("Purchase Price Coefficient")) { } //ID_105 newline
 newline
