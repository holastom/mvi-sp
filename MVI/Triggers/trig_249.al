tabulator tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator tabulator ft: Record "API Fuel Type"; newline
tabulator tabulator tabulator tabulator tabulator tabulator cbl: Record "API Contact Business Location"; newline
tabulator tabulator tabulator tabulator tabulator tabulator ven: Record Vendor; newline
tabulator tabulator tabulator tabulator tabulator tabulator vcon: Record Contact; newline
tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if ("Purchase Header"."API Order Content Type" = "Purchase Header"."API Order Content Type"::Object) then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if ft.Get("Fuel Type Code") then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FuelType assignment ft.Name newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FuelType assignment "Fuel Type Code"; newline
tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ven.Get("Vendor No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if cbl.Get(ven."Primary Contact No.", "Vendor Business Place No.") then; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator VehVenBussLocAddr assignment cbl."BLG Geographic Address"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if vcon.Get("BLG Vendor Contact Person No.") then; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator VehVenContPhoneMail assignment Utils.CondCat(vcon."E-Mail", '; '); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator VehVenContPhoneMail += Utils.CondCat(LanguageHandler.FormatPhoneNumber(vcon."Mobile Phone No.")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator VehVenContPhoneMail assignment CopyStr(VehVenContPhoneMail, 3); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator tabulator dataitem(Customer; Customer) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLinkReference = "Purchase Header"; newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("BLG Customer No."); newline
tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator column(C_No_; "No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(C_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator column(C_Address; Address) { } newline
tabulator tabulator tabulator tabulator tabulator column(C_Post_Code; "Post Code") { } newline
tabulator tabulator tabulator tabulator tabulator column(C_City; City) { } newline
tabulator tabulator tabulator tabulator tabulator column(C_Country; Utils.ForeignCountryName("Country/Region Code")) { } newline
tabulator tabulator tabulator tabulator tabulator column(C_Registration_No_CZL; "Registration No. CZL") { } newline
tabulator tabulator tabulator tabulator tabulator column(C_RegNos; C_RegNos) { } newline
 newline
