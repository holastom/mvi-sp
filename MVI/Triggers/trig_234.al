tabulator trigger OnPreReport() newline
tabulator var newline
tabulator tabulator customerLanguage: Enum LanguageEnum; newline
tabulator tabulator customerRecord: Record Customer; newline
tabulator begin newline
tabulator tabulator /* MVan 220518 - vyřazení volby jazyka newline
tabulator tabulator BHP.FindFirst; newline
tabulator tabulator customerRecord.Get(BHP."Customer No."); newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(ReqPageLanguage, customerRecord."Language Code"); newline
tabulator tabulator */ newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(); newline
 newline
tabulator tabulator CI.Get; newline
tabulator tabulator CountryRegion.Get(CI."Country/Region Code") newline
tabulator end; newline
