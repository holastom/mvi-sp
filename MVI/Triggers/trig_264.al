tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator contact: Record Contact; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator OSP.SetRange("Financed Object No.", "No."); newline
tabulator tabulator tabulator tabulator OSP.SetRange("BLG Sale", true); newline
tabulator tabulator tabulator tabulator if not OSP.FindFirst then CurrReport.Skip; newline
tabulator tabulator tabulator tabulator OSP.CalcFields("Buyer Name"); newline
tabulator tabulator tabulator tabulator Customer.Get(OSP."Buyer No."); newline
tabulator tabulator tabulator tabulator Contact.Get(Customer."Primary Contact No."); newline
tabulator tabulator tabulator tabulator CustIdentification assignment DelChr(contact."API Personal ID. No." + '/' + contact."Registration No. CZL", 'notequal', '/'); newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem(Company_Information; "Company Information") newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = sorting("Primary Key"); newline
 newline
tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator column(CI_Addr; Address) { } newline
tabulator tabulator tabulator column(CI_Addr2; "Address 2") { } newline
tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator column(CI_PSC; "Post Code") { } newline
tabulator tabulator tabulator column(CI_PhoneNo; LangHndl.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator column(CI_Email; "E-Mail") { } newline
tabulator tabulator tabulator column(CI_HomePage; "Home Page") { } newline
tabulator tabulator tabulator column(CI_RegNo; "Registration No.") { } newline
tabulator tabulator tabulator column(CI_VATRegNo; "VAT Registration No.") { } newline
tabulator tabulator tabulator column(CI_Country; Utils.CountryName("Country/Region Code")) { } newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator LangHndl: Codeunit LanguageHandler; newline
tabulator tabulator OSP: Record "API Evid. of Obj. Sales Price"; newline
tabulator tabulator Customer: Record Customer; newline
tabulator tabulator CustIdentification: Text; newline
 newline
