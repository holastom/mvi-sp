tabulator tabulator tabulator trigger OnAfterGetRecord() //"Purchase Header" newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator PLrec: Record "Purchase Line"; newline
 newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator if "API Order Content Type" = "API Order Content Type"::Object then begin newline
tabulator tabulator tabulator tabulator tabulator LBL_01 assignment LBL_01v; newline
tabulator tabulator tabulator tabulator tabulator LBL_02 assignment LBL_02v; newline
tabulator tabulator tabulator tabulator tabulator LBL_03 assignment LBL_03v; newline
tabulator tabulator tabulator tabulator tabulator LBL_04 assignment LBL_04v; newline
tabulator tabulator tabulator tabulator tabulator LBL_05 assignment LBL_05v; //v 2. kole PageRepeater se změní na LBL_05t newline
tabulator tabulator tabulator tabulator tabulator LBL_09 assignment LBL_09v; newline
tabulator tabulator tabulator tabulator tabulator LBL_10 assignment ''; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator LBL_01 assignment LBL_01e; newline
tabulator tabulator tabulator tabulator tabulator LBL_02 assignment LBL_02e; newline
tabulator tabulator tabulator tabulator tabulator LBL_03 assignment LBL_03e; newline
tabulator tabulator tabulator tabulator tabulator LBL_04 assignment ''; newline
tabulator tabulator tabulator tabulator tabulator LBL_05 assignment LBL_05e; newline
tabulator tabulator tabulator tabulator tabulator LBL_09 assignment LBL_09e; newline
tabulator tabulator tabulator tabulator tabulator LBL_10 assignment LBL_10e; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator FillEmailTexts("Purchase Header"."API Order Content Type"); newline
tabulator tabulator tabulator tabulator PLrec.SetRange("Document Type", "Purchase Header"."Document Type"); newline
tabulator tabulator tabulator tabulator PLrec.SetRange("Document No.", "Purchase Header"."No."); newline
tabulator tabulator tabulator tabulator PLrec.SetFilter(Type, 'notequal%1', PLrec.Type::" "); newline
tabulator tabulator tabulator tabulator PLrec.CalcSums("Direct Unit Cost", Amount, "Amount Including VAT"); newline
tabulator tabulator tabulator tabulator Sum_PL_Direct_Unit_Cost assignment PLrec."Direct Unit Cost"; newline
tabulator tabulator tabulator tabulator Sum_PL_Amount assignment PLrec.Amount; newline
tabulator tabulator tabulator tabulator Sum_PL_AmtInclVAT assignment PLrec."Amount Including VAT"; newline
tabulator tabulator tabulator tabulator Currency assignment LanguageHandler.CurrSymbol("Currency Code"); newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem(UserSetup; "User Setup") newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = sorting("User ID"); newline
 newline
tabulator tabulator tabulator dataitem("Employee"; "Employee") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "No." = field("API Employee No."); newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
 newline
tabulator tabulator tabulator tabulator column(E_Job_Title; "Job Title") { } newline
tabulator tabulator tabulator tabulator column(E_Full_Name; "Full Name") { } newline
tabulator tabulator tabulator tabulator column(E_Company_E_Mail; "Company E-Mail") { } newline
tabulator tabulator tabulator tabulator column(E_Phone_No_; LanguageHandler.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator tabulator column(E_API_Company_Mobile_Phone_No_; LanguageHandler.FormatPhoneNumber("API Company Mobile Phone No.")) { } newline
tabulator tabulator tabulator } newline
 newline
