tabulator tabulator trigger OnOpenPage() newline
tabulator tabulator begin newline
tabulator tabulator tabulator CIlng assignment LangHndl.GetCompanyLanguageId; newline
tabulator tabulator end; newline
tabulator } newline
 newline
tabulator labels newline
tabulator { newline
tabulator tabulator CultureCode = 'CultureCode', Comment = 'cs-CZ=cs-CZ;en-US=en-US'; newline
tabulator tabulator ContrStartDt = 'Contract start date', Comment = 'cs-CZ=Začátek leasingové smlouvy'; //LBL1 newline
tabulator tabulator ContrEndDt = 'Contract end date', Comment = 'cs-CZ=Plánovaný konec leasingové smlouvy'; //LBL2 newline
tabulator tabulator ContrMiles = 'Contractual distance', Comment = 'cs-CZ=Plánovaný nájezd'; //LBL3 newline
tabulator tabulator ContrNo = 'Number of lease contract', Comment = 'cs-CZ=Číslo leasingové smlouvy'; //LBL4 newline
tabulator tabulator IssueDate = 'Date of issue', Comment = 'cs-CZ=Datum vytvoření'; //LBL5 newline
tabulator tabulator ValidDate = 'Date of validity', Comment = 'cs-CZ=Datum platnosti'; //LBL6 newline
tabulator tabulator ActMiles = 'Current odometer', Comment = 'cs-CZ=Nájezd v době ukončení'; //LBL22 newline
tabulator tabulator TermDate = 'Early termination date', Comment = 'cs-CZ=Datum ukončení'; //LBL7 newline
tabulator tabulator FinSettlement = 'Financial settlement', Comment = 'cs-CZ=Finanční vyrovnání'; //LBL8 newline
tabulator tabulator Compensation = 'Early termination compensation*', Comment = 'cs-CZ=Kompenzace předčasného ukončení smlouvy*'; //LBL9 newline
tabulator tabulator ExcsMilesCharge = 'Excess mileage charge', Comment = 'cs-CZ=Doplatek za více najeté kilometry'; //LBL10 newline
tabulator tabulator LicPlateLbl = 'Licence Plate No.', Comment = 'cs-CZ=Registrační značka'; //LBL11 newline
tabulator tabulator VINlbl = 'VIN', Comment = 'cs-CZ=VIN'; //LBL12 newline
tabulator tabulator ModelLbl = 'Model', Comment = 'cs-CZ=Popis vozidla'; //LBL13 newline
tabulator tabulator Client = 'Client', Comment = 'cs-CZ=Klient'; //LBL14 newline
tabulator tabulator ReportTitle = 'Early termination proposal', Comment = 'cs-CZ=Návrh předčasného ukončení'; //LBL15 newline
tabulator tabulator CreationDate = 'Creation date', Comment = 'cs-CZ=Datum vytvoření'; //LBL16 newline
tabulator tabulator woVAT = 'excl. VAT', Comment = 'cs-CZ=bez DPH'; //LBL20 newline
tabulator tabulator LegalTxt1 = 'Compensation charged to the customer for non-compliance with the minimum contractual commitment period is subject to VAT.', newline
tabulator tabulator tabulator Comment = 'cs-CZ=Kompenzace účtovaná zákazníkovi při nedodržení minimální doby vázanosti vyplývající ze smlouvy podléhá DPH.'; //LBL21 newline
tabulator tabulator LegalTxt2 = 'This change is supported by the rulings of the European Court of Justice (in particular in Case C-43/19 and also e.g. C-242/18 and C-295/17).', newline
tabulator tabulator tabulator Comment = 'cs-CZ=Tato změna je podpořena rozhodnutími Evropského soudního dvoru (zejména v rozsudcích ve věci C-43/19 a také např. C-242/18 a C-295/17).'; //LBL22 newline
tabulator tabulator RegNoLbl = 'Id. No.', Comment = 'cs-CZ=IČO'; newline
tabulator tabulator VATRegNoLbl = 'VAT Id. No.', Comment = 'cs-CZ=DIČ'; newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator FIS: Record "API Financial Settlement"; newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator Cust: Record Customer; newline
tabulator tabulator FOBJ: Record "API Financed Object"; newline
tabulator tabulator CI: Record "Company Information"; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator LangHndl: Codeunit LanguageHandler; newline
tabulator tabulator RequestPageLanguage: Enum LanguageEnum; newline
tabulator tabulator Settlmt1to3: boolean; newline
 newline
