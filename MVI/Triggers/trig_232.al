tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator Country: Record "Country/Region"; newline
tabulator tabulator tabulator tabulator tManDir: Label 'jednatel'; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator Vendor.Get("Vendor No."); newline
tabulator tabulator tabulator tabulator AFO.Get("Financed Object No."); newline
tabulator tabulator tabulator tabulator if not ATB.Get(AFO."Type of Body") then ATB.Description assignment AFO."Type of Body"; newline
tabulator tabulator tabulator tabulator Clear(Customer); newline
tabulator tabulator tabulator tabulator Clear(Contact); newline
tabulator tabulator tabulator tabulator if Customer.Get("Customer No.") then newline
tabulator tabulator tabulator tabulator tabulator Utils.ComposeFullAdress(Cust_FullAddr, Customer.Address, Customer."Address 2", Customer."Post Code", Customer.City, '') newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator Clear(Cust_FullAddr); newline
tabulator tabulator tabulator tabulator if Contact.Get("Driver No.") then; newline
tabulator tabulator tabulator tabulator UserSetup.Get(UserId); newline
tabulator tabulator tabulator tabulator Employee.Get(UserSetup."API Employee No."); newline
tabulator tabulator tabulator tabulator CO.SetRange("Job Title", 'Managing Director'); newline
tabulator tabulator tabulator tabulator if CO.FindFirst then CO."Job Title" assignment tManDir; newline
 newline
tabulator tabulator tabulator tabulator if not (VendBussLoc.Get("Vendor Contact No.", "Vendor Business Place No.") newline
tabulator tabulator tabulator tabulator tabulator and ContAltAddr.Get("Vendor Contact No.", VendBussLoc."BLG Geographic Address Code")) then begin newline
tabulator tabulator tabulator tabulator tabulator ContAltAddr.Address assignment Vendor.Address; newline
tabulator tabulator tabulator tabulator tabulator ContAltAddr."Post Code" assignment Vendor."Post Code"; newline
tabulator tabulator tabulator tabulator tabulator ContAltAddr.City assignment Vendor.City; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator if not FuelType.Get(AFO."Fuel Type Code") then FuelType.Name assignment AFO."Fuel Type Code"; newline
tabulator tabulator tabulator tabulator /* //see DBTST-17132 - 24.6.22 newline
tabulator tabulator tabulator tabulator case "Fuel Level" of newline
tabulator tabulator tabulator tabulator tabulator "Fuel Level"::" ": newline
tabulator tabulator tabulator tabulator tabulator tabulator FuelLvl assignment 0; newline
tabulator tabulator tabulator tabulator tabulator "Fuel Level"::"1/4": newline
tabulator tabulator tabulator tabulator tabulator tabulator FuelLvl assignment 1; newline
tabulator tabulator tabulator tabulator tabulator "Fuel Level"::"1/2": newline
tabulator tabulator tabulator tabulator tabulator tabulator FuelLvl assignment 2; newline
tabulator tabulator tabulator tabulator tabulator "Fuel Level"::"3/4": newline
tabulator tabulator tabulator tabulator tabulator tabulator FuelLvl assignment 3; newline
tabulator tabulator tabulator tabulator tabulator "Fuel Level"::"1/1": newline
tabulator tabulator tabulator tabulator tabulator tabulator FuelLvl assignment 4; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator FuelRange assignment SelectStr(FuelLvl + 1, ',0 - 24,25 - 49,50 - 74,75 - 100'); newline
tabulator tabulator tabulator tabulator */ newline
tabulator tabulator tabulator tabulator FuelLvl assignment -1; newline
tabulator tabulator tabulator tabulator FuelRange assignment ''; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator /* MVan 220518 - vyřazení volby jazyka newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group("Language") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Options'; newline
tabulator tabulator tabulator tabulator tabulator field(CsLang; ReqPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1029; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1029; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(HuLang; ReqPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1038; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1038; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(PlLang; ReqPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1045; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1045; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(RoLang; ReqPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1048; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1048; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(SkLang; ReqPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1051; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1051; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator tabulator var newline
tabulator tabulator tabulator CIlng: Integer; newline
 newline
