tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator RegNoVATRegNoCZ assignment Utils.IdVatIdNo(MA_Customer, LanguageHandler.GetCompanyLanguageId()); newline
tabulator tabulator tabulator tabulator tabulator RegNoVATRegNoEN assignment Utils.IdVatIdNo(MA_Customer, LanguageHandler.GetENlanguageId()); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem(MasterAgreement; "API Master Agreement") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Master Agreement No."); newline
tabulator tabulator tabulator tabulator column(MA_No; "No.") { } newline
tabulator tabulator tabulator tabulator column(MA_AppendixNo; "BLG New Appendix Number") { } newline
tabulator tabulator tabulator tabulator column(MA_SigningDate; LanguageHandler.FormatVar("Signing Date")) { } newline
tabulator tabulator tabulator tabulator column(MA_SAParam; SAParameters) { } newline
 newline
tabulator tabulator tabulator tabulator dataitem(MA_Supplier; Contact) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Contact No."); newline
tabulator tabulator tabulator tabulator tabulator column(MA_Supp_JuridictionPlace; "API Jurisdiction Place") { } newline
tabulator tabulator tabulator tabulator tabulator column(MA_Supp_FileNo; Utils.nbsp("API File No.")) { } newline
tabulator tabulator tabulator tabulator tabulator column(MA_Supp_OwnerName; "BLG Customer Owner Name") { } newline
tabulator tabulator tabulator tabulator tabulator dataitem(Supp_User_Setup; "User Setup") newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = "User ID" = field("BLG Customer Owner Code"); newline
tabulator tabulator tabulator tabulator tabulator tabulator dataitem(Supp_Employee; "Employee") newline
tabulator tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("API Employee No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator column(US_EMail; "Company E-Mail") { } newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator column(SE_Mobile; LanguageHandler.FormatPhoneNumber("API Company Mobile Phone No.")) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator column(SE_Phone; LanguageHandler.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator tabulator dataitem(CI; "Company Information") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_Addr; Address) { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_Addr2; "Address 2") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_PSC; "Post Code") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_Phone; LanguageHandler.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_Email; "E-Mail") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_HomePage; "Home Page") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_RegNo; "Registration No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_VATRegNo; "VAT Registration No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_BankName; "Bank Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_BankAccNo; "Bank Account No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_IBAN; IBAN) { } newline
 newline
tabulator tabulator tabulator tabulator tabulator dataitem(CI_CounReg; "Country/Region") newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = Code = field("Country/Region Code"); newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CI_CounReg_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator tabulator tabulator dataitem(CI_Contact; Contact) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = "Registration No. CZL" = field("Registration No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CI_JurisdictionPlace; "API Jurisdiction Place") { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CI_FileNo; Utils.nbsp("API File No.")) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CI_M_Director; FirstLastName) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CompOff_JobTitle; JobTitle) { } newline
tabulator tabulator tabulator tabulator tabulator } newline
 newline
