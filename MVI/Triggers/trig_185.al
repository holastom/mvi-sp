tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator us: Record "User Setup"; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator CalcFields(Picture); newline
tabulator tabulator tabulator tabulator us.Get(UserId); newline
tabulator tabulator tabulator tabulator Employee.Get(us."Employee No. CZL"); newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator labels newline
tabulator { newline
tabulator tabulator ReportTitleLbl = 'Handover protocol BUSINESSRENT', Comment = 'cs-CZ=Předávací protokol BUSINESSRENT'; newline
tabulator tabulator ReportTitle2Lbl = 'Authorization to use the rented vehicle', Comment = 'cs-CZ=Autorizace k užívání zapůjčeného vozidla'; newline
tabulator tabulator ContractNoLbl = 'Rental contract No.', Comment = 'cs-CZ=Číslo smlouvy o zápůjčce'; newline
tabulator tabulator ContactPersonLbl = 'Contact person', Comment = 'cs-CZ=Kontaktní osoba'; newline
tabulator tabulator MobileLbl = 'Mobile', Comment = 'cs-CZ=Mobil'; newline
tabulator tabulator EmailLbl = 'E-mail', Locked = true; newline
tabulator tabulator PhoneLbl = 'Phone', Comment = 'cs-CZ=Telefon'; newline
tabulator tabulator HandovererLbl = 'Handoverer', Comment = 'cs-CZ=Předávající'; newline
tabulator tabulator ReceivingLbl = 'Receiving', Comment = 'cs-CZ=Přejímající'; newline
tabulator tabulator RentRecLbl = 'Renter (receiving)', Comment = 'cs-CZ=Nájemce (přejímající)'; newline
tabulator tabulator RenterLbl = 'Renter', Comment = 'cs-CZ=Nájemce'; newline
tabulator tabulator RegNoLbl = 'RegNoLbl', Comment = 'cs-CZ=IČO'; newline
tabulator tabulator VATRegNoLbl = 'VATRegNoLbl', Comment = 'cs-CZ=DIČ'; newline
tabulator tabulator DriverLbl = 'Driver', Comment = 'cs-CZ=Řidič'; newline
tabulator tabulator NameLbl = 'Name', Comment = 'cs-CZ=Jméno'; newline
tabulator tabulator RentInfoLbl = 'RentInfo', Comment = 'cs-CZ=INFORMACE O ZÁPŮJČCE'; newline
tabulator tabulator LicPlateLbl = 'LicPlate', Comment = 'cs-CZ=Registrační značka'; newline
tabulator tabulator VINlbl = 'VIN', Locked = true; newline
tabulator tabulator ModelLbl = 'Model', Comment = 'cs-CZ=Značka / model'; newline
tabulator tabulator EngineCubatLbl = 'Engine cubature', Comment = 'cs-CZ=Objem motoru'; newline
tabulator tabulator EnginePowerLbl = 'Engine power', Comment = 'cs-CZ=Výkon motoru'; newline
tabulator tabulator GearboxLbl = 'Gearbox', Comment = 'cs-CZ=Převodovka'; newline
tabulator tabulator FuelLbl = 'Fuel type', Comment = 'cs-CZ=Typ paliva'; newline
tabulator tabulator ColourLbl = 'Body colour', Comment = 'cs-CZ=Barva karoserie'; newline
tabulator tabulator CarCategoryLbl = 'Car category', Comment = 'cs-CZ=Kategorie vozidla'; newline
tabulator tabulator PurchPriceLbl = 'Purchase price', Comment = 'cs-CZ=Pořizovací cena vč. DPH'; newline
tabulator tabulator FreeKmLbl = 'Free km', Comment = 'cs-CZ=Smluvní limit kilometrů'; newline
tabulator tabulator InsuranceLbl = 'Accident insurance - co-participation', Comment = 'cs-CZ=Havarijní pojištění – spoluúčast'; newline
tabulator tabulator ExpctdReturnLbl = 'Expected return', Comment = 'cs-CZ=Předpokládaný termín vrácení vozidla'; newline
tabulator tabulator RentPerLbl = 'Rent', Comment = 'cs-CZ=Nájemné'; newline
tabulator tabulator CarDamageLbl = 'CAR DAMAGE', Comment = 'cs-CZ=POŠKOZENÍ VOZIDLA'; newline
tabulator tabulator StateLbl = 'Status', Comment = 'cs-CZ=Stav'; newline
tabulator tabulator AtHandoverLbl = 'at handover', Comment = 'cs-CZ=při předání'; newline
tabulator tabulator AtReturnLbl = 'at return', Comment = 'cs-CZ=při vrácení'; newline
tabulator tabulator AccsStateLbl = 'STATUS OF ACCESSORIES', Comment = 'cs-CZ=STAV PŘÍSLUŠENSTVÍ'; newline
tabulator tabulator AccessoryLbl = 'Accessory', Comment = 'cs-CZ=Příslušenství'; newline
tabulator tabulator HandedOverLbl = 'Handed over', Comment = 'cs-CZ=Předáno'; newline
tabulator tabulator ReturnedLbl = 'Returned', Comment = 'cs-CZ=Vráceno'; newline
tabulator tabulator WithCarLbl = 'with car', Comment = 'cs-CZ=s vozidlem'; newline
tabulator tabulator NoteLbl = 'Note', Comment = 'cs-CZ=Poznámka'; newline
tabulator tabulator SpareWheelLbl = 'Spare wheel', Comment = 'cs-CZ=Rezervní kolo'; newline
tabulator tabulator MandEquipmtLbl = 'Mandatory equipment', Comment = 'cs-CZ=Povinná výbava'; newline
tabulator tabulator CarDocsLbl = 'Car documents', Comment = 'cs-CZ=Doklady k vozidlu'; newline
tabulator tabulator FuelCardLbl = 'Fuel card/s', Comment = 'cs-CZ=Tankovací karta (-y)'; newline
tabulator tabulator OthersLbl = 'Others', Comment = 'cs-CZ=Jiné'; newline
tabulator tabulator TyresLbl = 'Tire type / brand / size', Comment = 'cs-CZ=Typ pneumatik / značka / rozměr'; newline
tabulator tabulator NextService = 'Příští servisní prohlídka (v km)', Comment = 'cs-CZ=Příští servisní prohlídka (v km)'; newline
tabulator tabulator ConfirmationLbl = 'HANDOVER / RETURN CONFIRMATION', Comment = 'cs-CZ=POTVRZENÍ PŘEDÁNÍ / VRÁCENÍ'; newline
tabulator tabulator HandoverPointLbl = 'Handover place', Comment = 'cs-CZ=Místo předání'; newline
tabulator tabulator ReturnPointLbl = 'Return place', Comment = 'cs-CZ=Místo vrácení'; newline
tabulator tabulator DateTimeLbl = 'Date and time', Comment = 'cs-CZ=Datum a čas'; newline
tabulator tabulator MileageLbl = 'Mileage', Comment = 'cs-CZ=Stav tachometru'; newline
tabulator tabulator FuelAmountLbl = 'Fuel amount', Comment = 'cs-CZ=Stav nádrže'; newline
tabulator tabulator RenterAuthPersonLbl = 'Renter auth. person', Comment = 'cs-CZ=Oprávněná osoba nájemce'; newline
tabulator tabulator FullNameLbl = 'Full name', Comment = 'cs-CZ=jméno a příjmení'; newline
tabulator tabulator BlockLettLbl = 'in block letters', Comment = 'cs-CZ=hůlkovým písmem'; newline
tabulator tabulator SignatureLbl = 'signature', Comment = 'cs-CZ=podpis'; newline
tabulator tabulator MonthLbl = 'month', Comment = 'cs-CZ=měsíc'; newline
tabulator tabulator PINlbl = 'PIN', Locked = true; newline
tabulator tabulator AuthPersonsLbl = 'AuthPersonsLbl', Comment = 'cs-CZ=OPRÁVNĚNÉ OSOBY'; newline
tabulator tabulator AuthPersTxt1 = 'AuthPersTxt1', Comment = 'cs-CZ=Klient stanovil následující osoby jako oprávněné vozidlo převzít / užívat / vrátit:'; newline
tabulator tabulator AuthPersTxt2 = 'AuthPersTxt2', Comment = 'cs-CZ=Níže uvedené osoby souhlasí s předáním a zpracováním svých osobních údajů pro účely uzavření této smlouvy.'; newline
tabulator tabulator AuthPersTxt3 = 'AuthPersTxt3', Comment = 'cs-CZ=Dodavatel přenechává Klientovi vozidlo specifikované v předávacím protokolu k dočasnému užívání za níže uvedených podmínek...'; newline
tabulator tabulator IdCardNoLbl = 'IdCardNoLbl', Comment = 'cs-CZ=Číslo OP'; newline
tabulator tabulator DriverLicLbl = 'DriverLicLbl', Comment = 'cs-CZ=Číslo ŘP'; newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator LangHndl: Codeunit LanguageHandler; newline
tabulator tabulator Utils: Codeunit Utils; newline
 newline
tabulator tabulator CompLangId, ENLangId : Integer; newline
tabulator tabulator ROH: Record "API Rent Order Header"; newline
tabulator tabulator Renter: Record Contact; newline
tabulator tabulator AUser: Record Contact; newline
tabulator tabulator AUser2: Record Contact; newline
tabulator tabulator Employee: Record Employee; newline
tabulator tabulator FOB: Record "API Financed Object"; newline
 newline
tabulator tabulator RenterIds: Text; newline
tabulator tabulator DelivPlace, ReturnPlace : Text; newline
tabulator tabulator Currency, CarDescription, CarModel, CarModelType, Fuel, TariffSubgr, Gear, LicPlate, Price, EnginePower, EngineCubat : Text; newline
tabulator tabulator CompanyManager: Text; newline
tabulator tabulator LicPlateNo: Text[10]; newline
tabulator tabulator VIN: Text[17]; newline
tabulator tabulator Colour: Text[250]; newline
 newline
tabulator tabulator tPerMonth: Label 'per month', Comment = 'cs-CZ=za měsíc;EN-us=Per month'; newline
tabulator tabulator tPerDay: Label 'per day', Comment = 'cs-CZ=za den;EN-us=Per day'; newline
tabulator tabulator tWillBeSpecified: Label 'will be specified', Comment = 'cs-CZ=bude upřesněno;EN-us=will be specified'; newline
tabulator tabulator tRegNo: Label 'RegNo', Comment = 'cs-CZ=IČO;en-US=Id. No.'; newline
tabulator tabulator tVATRegNo: Label 'VATRegNo', Comment = 'cs-CZ=DIČ;en-US=VAT Id. No.'; newline
tabulator tabulator tWhom: Label 'To all whom it may concern', Comment = 'cs-CZ=Všem, kterých se týká následující'; newline
tabulator tabulator tSentence1a: Label 'The undersigned Mr. %1, Managing Director of %2', Comment = 'cs-CZ=Níže podepsaný pan %1, ředitel společnosti %2'; newline
tabulator tabulator tSentence1b: Label '(operational lease company) declares that', Comment = 'cs-CZ=(společnost zabývající se operativním leasingem) tímto potvrzuje, že'; newline
tabulator tabulator tSentence2a: Label 'Mr / Mrs. <b>%1</b>, employee of <b>%2</b>', Comment = 'cs-CZ=pan / paní <b>%1</b>, zaměstnanec firmy <b>%2</b>'; newline
tabulator tabulator tSentence2b: Label 'is authorized to use the car with licence plate number: <b>%3</b>', Comment = 'cs-CZ=je oprávněn používat vozidlo registrační značky: <b>%3</b>'; newline
tabulator tabulator tSentence2c: Label 'also for his private purposes on the territory and outside the territory of the Czech Republic.', Comment = 'cs-CZ=i k soukromým cestám na území i mimo území České republiky.'; newline
tabulator tabulator tFamily: Label 'This authorization also applies to his family members.', Comment = 'cs-CZ=Toto povolení se vztahuje i na jeho rodinné příslušníky.'; newline
tabulator tabulator tManagDirector: Label 'managing director', Comment = 'cs-CZ=ředitel společnosti'; newline
 newline
