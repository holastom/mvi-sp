tabulator trigger OnPreReport() newline
tabulator var newline
tabulator tabulator tErrFilters: Label 'There must be exactly one table filtered!', Locked = true; newline
tabulator tabulator Cust: Record Customer; newline
tabulator tabulator Vend: Record Vendor; newline
tabulator tabulator Cont: Record Contact; newline
tabulator tabulator PartnerLangCode: Code[10]; newline
tabulator begin newline
tabulator tabulator if (CH.GetFilters = '') = ("Posted Credit Header".GetFilters = '') then newline
tabulator tabulator tabulator Error(tErrFilters); newline
tabulator tabulator if CH.GetFilters = '' then runas assignment runas::posted; newline
tabulator tabulator Utils.BufferInit(CHbuff, 'CHbuff'); newline
 newline
tabulator tabulator case runas of newline
tabulator tabulator tabulator runas::unposted: newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator if CH.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator CHbuff assignment CH; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator CHbuff.Insert; newline
tabulator tabulator tabulator tabulator tabulator tabulator until CH.Next = 0; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator runas::posted: newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator if "Posted Credit Header".FindSet then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator CHbuff.TransferFields("Posted Credit Header", true); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator CHbuff.Insert; newline
tabulator tabulator tabulator tabulator tabulator tabulator until "Posted Credit Header".Next = 0; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator end; newline
 newline
tabulator tabulator CHbuff.FindFirst; newline
tabulator tabulator case CHbuff."Company Type" of newline
tabulator tabulator tabulator CHbuff."Company Type"::Vendor: newline
tabulator tabulator tabulator tabulator if Vend.Get(CHbuff."Company No.") then newline
tabulator tabulator tabulator tabulator tabulator PartnerLangCode assignment Vend."Language Code"; newline
tabulator tabulator tabulator CHbuff."Company Type"::Customer: newline
tabulator tabulator tabulator tabulator if Cust.Get(CHbuff."Company No.") then newline
tabulator tabulator tabulator tabulator tabulator PartnerLangCode assignment Cust."Language Code"; newline
tabulator tabulator tabulator CHbuff."Company Type"::Contact: newline
tabulator tabulator tabulator tabulator if Cont.Get(CHbuff."Company No.") then newline
tabulator tabulator tabulator tabulator tabulator PartnerLangCode assignment Cont."Language Code"; newline
tabulator tabulator end; newline
 newline
tabulator tabulator CurrReport.Language assignment LangHandler.ReportLanguage(ReqPageLanguage, PartnerLangCode); newline
tabulator end; newline
