tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator pih: Record "Purch. Inv. Header"; newline
tabulator tabulator tabulator tabulator fch: Record "API Financing Contract Header"; newline
tabulator tabulator tabulator tabulator fobj: Record "API Financed Object"; newline
tabulator tabulator tabulator tabulator puh: Record "Purchase Header"; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator CalcFields("Remaining Amt. (LCY)"); newline
tabulator tabulator tabulator tabulator if Vend."No." notequal "Vendor No." then Vend.Get("Vendor No."); newline
tabulator tabulator tabulator tabulator FCH_No assignment ''; newline
tabulator tabulator tabulator tabulator FCH_DetStat assignment ''; newline
tabulator tabulator tabulator tabulator FCH_Status assignment ''; newline
tabulator tabulator tabulator tabulator FOBJ_LicPlate assignment ''; newline
tabulator tabulator tabulator tabulator PH_OrderNo assignment ''; newline
tabulator tabulator tabulator tabulator PH_ContentType assignment ''; newline
tabulator tabulator tabulator tabulator ContrStatOrder assignment 0; newline
tabulator tabulator tabulator tabulator ConttTypeOrder assignment 0; newline
 newline
tabulator tabulator tabulator tabulator if "Document Type" = "Document Type"::Invoice then begin newline
tabulator tabulator tabulator tabulator tabulator pih.Get("Document No."); newline
tabulator tabulator tabulator tabulator tabulator FCH_No assignment pih."Shortcut Dimension 2 Code"; newline
tabulator tabulator tabulator tabulator tabulator if not fch.Get(FCH_No) then newline
tabulator tabulator tabulator tabulator tabulator tabulator clear(fch); newline
tabulator tabulator tabulator tabulator tabulator FCH_DetStat assignment fch."Detailed Contract Status"; newline
tabulator tabulator tabulator tabulator tabulator FCH_Status assignment Format(fch.Status); newline
tabulator tabulator tabulator tabulator tabulator ContrStatOrder assignment _ContrStatOrder(fch); newline
tabulator tabulator tabulator tabulator tabulator if fobj.Get(fch."Financed Object No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator FOBJ_LicPlate assignment FOBJ."Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator if puh.Get(puh."Document Type"::Order, fobj."Purchase Order No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PH_OrderNo assignment puh."No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PH_ContentType assignment Format(puh."API Order Content Type"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ConttTypeOrder assignment _ContTypeOrder(puh); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator tabulator dataitem(CompanyInfo; "Company Information") newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = sorting("Primary key"); newline
 newline
tabulator tabulator tabulator column(COMPANYNAME; COMPANYPROPERTY.DisplayName) { } newline
tabulator tabulator tabulator column(TableCaption; VLE.TableCaption) { } newline
tabulator tabulator tabulator column(VendLedgEntryFilter; VLE.GetFilters) { } newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator labels newline
tabulator { newline
tabulator tabulator DueDateLbl = 'Due Date', Comment = 'cs-CZ=Datum splatnosti'; newline
tabulator tabulator PostingDateLbl = 'Posting Date', Comment = 'cs-CZ=Datum zaúčtování'; newline
tabulator tabulator DocTypeLbl = 'Document Type', Comment = 'cs-CZ=Typ dokumentu'; newline
tabulator tabulator DocNoLbl = 'Document (Invoice) No.', Comment = 'cs-CZ=Číslo dokumentu (faktury)'; newline
tabulator tabulator DescriptionLbl = 'Description', Comment = 'cs-CZ=Popis'; newline
tabulator tabulator RemAmountLbl = 'Remaining Amount', Comment = 'cs-CZ=Zbývající částka k úhradě'; newline
tabulator tabulator VendNoLbl = 'Vendor No.', Comment = 'cs-CZ=Id dodavatele'; newline
tabulator tabulator VendNameLbl = 'Name', Comment = 'cs-CZ=Název'; newline
tabulator tabulator CurrCodeLbl = 'Currency Code', Comment = 'cs-CZ=Kód měny'; newline
tabulator tabulator OnHoldLbl = 'On Hold', Comment = 'cs-CZ=On hold'; newline
tabulator tabulator TotalLCYlbl = 'Total (LCY)', Comment = 'cs-CZ=Součet částek k úhradě'; newline
tabulator tabulator FCH_NoLbl = 'Lease contract No.', Comment = 'cs-CZ=Číslo leasingové smlouvy'; newline
tabulator tabulator FCH_DetStatLbl = 'Detailed Contract Status', Comment = 'cs-CZ=Detailní stav smlouvy'; newline
tabulator tabulator FCH_StatusLbl = 'Contract Status', Comment = 'cs-CZ=Stav smlouvy'; newline
tabulator tabulator FCH_LicPlateLbl = 'License Plate', Comment = 'cs-CZ=Registrační značka'; newline
tabulator tabulator PH_OrderNoLbl = 'Order No.', Comment = 'cs-CZ=Číslo objednávky'; newline
tabulator tabulator PH_ContentTypeLbl = 'Order Content Type', Comment = 'cs-CZ=Druh objednávky'; newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator LangHndlr: Codeunit LanguageHandler; newline
tabulator tabulator Vend: Record Vendor; newline
tabulator tabulator FCH_No, FCH_DetStat, FCH_Status, FOBJ_LicPlate, PH_OrderNo, PH_ContentType : Text; newline
 newline
tabulator tabulator ContrStatOrder, ConttTypeOrder : Integer; newline
 newline
