tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator HandoverDate: Record "API Vendor Confirmed Date"; newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator tabulator tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ExpectedHandoverDate assignment ''; newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetFilter("Financed Object No.", ConfirmedPurchaseHeader."API Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetFilter("Object Purchase Order No.", ConfirmedPurchaseHeader."No."); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetRange("Record Type", "API VendorConfDateRecordType"::"Expected Handover Date to Customer"); newline
tabulator tabulator tabulator tabulator tabulator HandoverDate.SetAscending("Line No.", false); newline
tabulator tabulator tabulator tabulator tabulator if HandoverDate.FindFirst() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if HandoverDate."Confirmed Date" >= Today() then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ExpectedHandoverDate assignment LanguageHandler.FormatDateNoSpace(HandoverDate."Confirmed Date") newline
tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ExpectedHandoverDate assignment Lbl_WillBeDetermined; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator HandoverDate.SetRange("Record Type", "API VendorConfDateRecordType"::"Confirmed Delivery Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator HandoverDate.SetFilter("Confirmed Date", '>=%1', Today()); newline
tabulator tabulator tabulator tabulator tabulator tabulator if HandoverDate.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ExpectedHandoverDate assignment LanguageHandler.FormatDateNoSpace(CalcDate('<+14D>', HandoverDate."Confirmed Date")) newline
tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ExpectedHandoverDate assignment Lbl_WillBeDetermined; newline
tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.SetRange("No.", ConfirmedPurchaseHeader."Shortcut Dimension 2 Code"); newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.SetRange(Status, "API Financing Contract Status"::Signed); newline
tabulator tabulator tabulator tabulator tabulator if FinancingContractHeader.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator Driver assignment FinancingContractHeader."BLG Driver Name"; newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancedObject.Get(FinancingContractHeader."Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator Model assignment FinancedObject."Make Name" + ' ' + FinancedObject."Model Name" + ' ' + FinancedObject."Model Type Name"; newline
tabulator tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator tabulator CurrReport.Skip(); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator Model: Text; newline
tabulator tabulator Driver: Text; newline
tabulator tabulator OriginalHandoverDate: Text; newline
tabulator tabulator ExpectedHandoverDate: Text; newline
tabulator tabulator Lbl_WillBeDetermined: Label 'bude upřesněno'; newline
 newline
