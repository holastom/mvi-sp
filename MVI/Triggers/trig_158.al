tabulator tabulator tabulator trigger OnAfterGetRecord() //FCH newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator contractServiceIntrecalcRecord: record "API Contract Service"; newline
tabulator tabulator tabulator tabulator aaRec: Record "API Attribute Assign"; newline
tabulator tabulator tabulator tabulator contractHeaderLinkedRec: Record "API Financing Contract Header"; newline
tabulator tabulator tabulator tabulator financedObjectRec: Record "API Financed Object"; newline
tabulator tabulator tabulator tabulator contact: Record Contact; newline
tabulator tabulator tabulator tabulator userSetup: Record "User Setup"; newline
tabulator tabulator tabulator tabulator employee: Record Employee; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator Currency assignment LanguageHandler.CurrSymbol("Currency Code"); newline
 newline
tabulator tabulator tabulator tabulator FPTrec.Get(FPTrec.Type::"Financing Product", "Financing Product No."); newline
tabulator tabulator tabulator tabulator FPTypeRec.Get("API Financing Contract Header"."Financing Product Type Code"); newline
 newline
tabulator tabulator tabulator tabulator CustIdentInfo assignment Utils.CondCat("Registration No.", ', ' + LBL_10 + ': '); newline
tabulator tabulator tabulator tabulator CustIdentInfo += Utils.CondCat("VAT Registration No.", ', ' + LBL_11 + ': '); newline
tabulator tabulator tabulator tabulator CustIdentInfo assignment CopyStr(CustIdentInfo, 3); newline
 newline
tabulator tabulator tabulator tabulator FPT_Name assignment ParseAndTrimDesc(FptRec.Name, '|'); newline
tabulator tabulator tabulator tabulator FPT_Descr assignment Utils.iif(isLocalLanguage, FPTypeRec."BLG Description Alt.", FPTypeRec.Description); newline
tabulator tabulator tabulator tabulator if FPT_Name = FPT_Descr then newline
tabulator tabulator tabulator tabulator tabulator Clear(FPT_Descr) newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator FPT_Descr assignment Utils.CondCat(FPT_Descr, ' '); newline
 newline
tabulator tabulator tabulator tabulator Clear(InterestRateTypeInfo); newline
tabulator tabulator tabulator tabulator case "Interest Rate Type" of newline
tabulator tabulator tabulator tabulator tabulator "Interest Rate Type"::Variable: newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator case "REFI Code" of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'VAR_3M': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InterestRateTypeInfo assignment Utils.CondCat(LBL_180 + LBL_181a, ', '); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'VAR_6M': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InterestRateTypeInfo assignment Utils.CondCat(LBL_180 + LBL_181b, ', '); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'VAR_1Y': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InterestRateTypeInfo assignment Utils.CondCat(LBL_180 + LBL_181c, ', '); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator "Interest Rate Type"::Fix: newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator case "REFI Code" of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'FIX_ACT': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InterestRateTypeInfo assignment Utils.CondCat(LBL_180 + LBL_181d, ', '); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator if InterestRateTypeInfo = '' then newline
tabulator tabulator tabulator tabulator tabulator InterestRateTypeInfo assignment Utils.CondCat("REFI Code", ', '); newline
 newline
tabulator tabulator tabulator tabulator if not MArec.Get("Master Agreement No.") then Clear(MArec); newline
 newline
tabulator tabulator tabulator tabulator AA_IFRS assignment false; newline
tabulator tabulator tabulator tabulator aaRec.SetRange(Type, aaRec.Type::Contact); newline
tabulator tabulator tabulator tabulator aaRec.SetRange("No.", "Primary Contact No."); newline
tabulator tabulator tabulator tabulator aaRec.SetRange("Code", 'IFRS'); newline
tabulator tabulator tabulator tabulator AA_IFRS assignment not aaRec.IsEmpty; newline
tabulator tabulator tabulator tabulator aaRec.SetRange("Code", 'CALC_OPEN'); newline
tabulator tabulator tabulator tabulator if not aaRec.IsEmpty then FPTrec."Calculation Type" assignment FPTrec."Calculation Type"::Open; newline
tabulator tabulator tabulator tabulator aaRec.SetRange("Code", 'CALC_RV'); newline
tabulator tabulator tabulator tabulator AA_CALC_RV assignment not aaRec.IsEmpty; newline
tabulator tabulator tabulator tabulator aaRec.SetRange("Code", 'CALC_INTER'); newline
tabulator tabulator tabulator tabulator AA_CALC_INTER assignment not aaRec.IsEmpty; newline
 newline
tabulator tabulator tabulator tabulator Clear(ReLicPlateNo); newline
tabulator tabulator tabulator tabulator if contractHeaderLinkedRec.Get("BLG Original Contract No.") then newline
tabulator tabulator tabulator tabulator tabulator if financedObjectRec.Get(contractHeaderLinkedRec."Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator ReLicPlateNo assignment financedObjectRec."Licence Plate No."; newline
 newline
tabulator tabulator tabulator tabulator Clear(C_BLG_Customer_Support_Name); newline
tabulator tabulator tabulator tabulator Clear(E_Job_Title); newline
tabulator tabulator tabulator tabulator Clear(E_Company_E_Mail); newline
tabulator tabulator tabulator tabulator Clear(E_API_Company_Mobile_Phone_No); newline
tabulator tabulator tabulator tabulator if contact.Get("Primary Contact No.") then begin newline
tabulator tabulator tabulator tabulator tabulator contact.CalcFields("BLG Customer Support Name"); newline
tabulator tabulator tabulator tabulator tabulator C_BLG_Customer_Support_Name assignment contact."BLG Customer Support Name"; newline
tabulator tabulator tabulator tabulator tabulator userSetup.SetRange("Salespers./Purch. Code", contact."BLG Customer Support Code"); newline
tabulator tabulator tabulator tabulator tabulator if userSetup.FindFirst then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if employee.Get(userSetup."API Employee No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator E_Job_Title assignment employee."Job Title"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator E_Company_E_Mail assignment employee."Company E-Mail"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator E_API_Company_Mobile_Phone_No assignment LanguageHandler.FormatPhoneNumber(employee."API Company Mobile Phone No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator FCH_NOTIFICATION assignment GetNotification(Status); newline
tabulator tabulator tabulator tabulator SetVersionCalculationOfferCreationDate("No.", "Offer Creation Date"); newline
 newline
tabulator tabulator tabulator tabulator Clear(ReINTRECALC); newline
 newline
tabulator tabulator tabulator tabulator contractServiceIntrecalcRecord.SetRange("Contract No.", "No."); newline
tabulator tabulator tabulator tabulator contractServiceIntrecalcRecord.SetFilter("Service Status", 'Active | Preparation'); newline
tabulator tabulator tabulator tabulator contractServiceIntrecalcRecord.SetFilter("Valid From", 'notequal%1&..%2', 0D, StartOfNextMonth); newline
tabulator tabulator tabulator tabulator contractServiceIntrecalcRecord.SetRange("Service Type Code", 'INTRECALC'); newline
 newline
tabulator tabulator tabulator tabulator if contractServiceIntrecalcRecord.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator if ServiceValid(contractServiceIntrecalcRecord) then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ReINTRECALC += contractServiceIntrecalcRecord."Calculation Amount Per Payment"; newline
tabulator tabulator tabulator tabulator tabulator until contractServiceIntrecalcRecord.Next() = 0; newline
 newline
tabulator tabulator tabulator tabulator Clear(CSB_Billable_Calculation_PerPayment_Sum); newline
 newline
tabulator tabulator tabulator tabulator if FCH_CALCULATION then begin newline
tabulator tabulator tabulator tabulator tabulator ReportTitle assignment LBL_1a; newline
tabulator tabulator tabulator tabulator tabulator if "Financing Product Type Code" in ['OL', 'SLB'] then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator OpeningText assignment Utils.iif(MArec."Validity Date From" = 0D, LBL_112, StrSubstNo(LBL_111, MArec."No.", LanguageHandler.FormatVar(MArec."Validity Date From"))); //BL_14 newline
tabulator tabulator tabulator tabulator tabulator tabulator FinalText assignment LBL_154; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator OpeningText assignment Utils.iif(MArec."Validity Date From" = 0D, LBL_114, StrSubstNo(LBL_113, MArec."No.", LanguageHandler.FormatVar(MArec."Validity Date From"))); //BL_28 newline
tabulator tabulator tabulator tabulator tabulator tabulator FinalText assignment Utils.iif("Financing Product Type Code" = 'CFM', LBL_155, LBL_156); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator if FCH_ACTIVATION then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if "Financing Product Type Code" in ['OL', 'SLB'] then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OpeningText assignment StrSubstNo(LBL_152, Utils.iif("Source Calculation No." notequal '', "Source Calculation No.", "No."), "No.", MArec."No.", LanguageHandler.FormatVar(MArec."Validity Date From")) newline
tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OpeningText assignment StrSubstNo(LBL_153, Utils.iif("Source Calculation No." notequal '', "Source Calculation No.", "No."), "No.", MArec."No.", LanguageHandler.FormatVar(MArec."Validity Date From")); newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if "Financing Product Type Code" in ['OL', 'SLB'] then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OpeningText assignment StrSubstNo(LBL_150, MArec."No.", LanguageHandler.FormatVar(MArec."Validity Date From"), LanguageHandler.FormatVar(StartOfNextMonth)) newline
tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OpeningText assignment StrSubstNo(LBL_151, MArec."No.", LanguageHandler.FormatVar(MArec."Validity Date From"), LanguageHandler.FormatVar(StartOfNextMonth)); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator if "Financing Product Type Code" in ['OL', 'SLB'] then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ReportTitle assignment LBL_1b; newline
tabulator tabulator tabulator tabulator tabulator tabulator FinalText assignment LBL_157; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ReportTitle assignment LBL_1c; newline
tabulator tabulator tabulator tabulator tabulator tabulator FinalText assignment Utils.iif("Financing Product Type Code" = 'CFM', LBL_158, LBL_159); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator } newline
 newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group(Options) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Options', Comment = 'cs-CZ=Možnosti;en-US=Options'; newline
tabulator tabulator tabulator tabulator tabulator field(CsLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language', Comment = 'cs-CZ=Vyberte jazyk;en-US=Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language', Comment = 'cs-CZ=Při volbě "Automaticky" je použit jazyk společnosti;en-US=If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1029; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1029; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(HuLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language', Comment = 'cs-CZ=Vyberte jazyk;en-US=Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language', Comment = 'cs-CZ=Při volbě "Automaticky" je použit jazyk společnosti;en-US=If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1038; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1038; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(PlLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language', Comment = 'cs-CZ=Vyberte jazyk;en-US=Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language', Comment = 'cs-CZ=Při volbě "Automaticky" je použit jazyk společnosti;en-US=If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1045; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1045; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(RoLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language', Comment = 'cs-CZ=Vyberte jazyk;en-US=Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language', Comment = 'cs-CZ=Při volbě "Automaticky" je použit jazyk společnosti;en-US=If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1048; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1048; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(SkLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language', Comment = 'cs-CZ=Vyberte jazyk;en-US=Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language', Comment = 'cs-CZ=Při volbě "Automaticky" je použit jazyk společnosti;en-US=If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1051; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1051; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(NotificationSelectEnum; NotificationSelectEnum) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Without signatures', Comment = 'cs-CZ=Bez podpisů;en-US=Without signatures'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator tabulator var newline
tabulator tabulator tabulator CIlng: Integer; newline
 newline
