tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator Amount: Decimal; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator begin newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::Windows); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
 newline
tabulator tabulator SA.GroupByShortcutDimension(FCHCodes, SIH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator SIL assignment SA.GetSalesInvoiceLine(GroupedCode, SIH."No."); newline
tabulator tabulator tabulator FCL assignment SA.GetFinancingContractLine(GroupedCode, SIH."No."); newline
tabulator tabulator tabulator Amount assignment SA.GetAmount(GroupedCode, SIH."No."); newline
 newline
tabulator tabulator tabulator OutStream.WriteText(SIH."No." + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(Format(CalcDate('<-CM>', SIH."Posting Date"), 0, '<Day>/<Month>/<Year>') + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(Format(FCL."Posting Date", 0, '<Day>/<Month>/<Year>') + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(Format(DueDate(), 0, '<Day>/<Month>/<Year>') + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(GroupedCode + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(FO."Licence Plate No." + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(SIH."Order No." + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText('L000' + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FO.Name, 25) + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(SA.FormatDecimal(Amount, 2) + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(Format(SIL."VAT %", 0, '<Integer>') + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(SA.FormatDecimal(SA.GetAmountInclVAT(GroupedCode, SIH."No.") - Amount, 2) + Utils.tabChar()); newline
tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator end; newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
