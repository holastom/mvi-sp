tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator PH: Record "Purchase Header"; newline
tabulator tabulator tabulator tabulator CS: Record "API Contract Service"; newline
tabulator tabulator tabulator tabulator AddEq: Record "API Object Add. Equipment"; newline
tabulator tabulator tabulator tabulator OptEq: Record "API Object Optional Equipment"; newline
tabulator tabulator tabulator tabulator CC: Record "BLG Catalogue Color"; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator PH.SetRange("API Financed Object No.", FinancedObject."No."); newline
tabulator tabulator tabulator tabulator PH.SetRange("API Order Content Type", "API OrderContentType"::"Additional Equipment"); newline
tabulator tabulator tabulator tabulator PH.SetFilter("API Object Order Status", 'notequal%1', "API ObjectOrderStatus"::Canceled); newline
tabulator tabulator tabulator tabulator if PH.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator AddEqPONumbers += ', ' + PH."No."; newline
tabulator tabulator tabulator tabulator tabulator until PH.Next() = 0; newline
tabulator tabulator tabulator tabulator tabulator AddEqPONumbers assignment DelChr(AddEqPONumbers, '<', ', '); newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator CS.SetRange("Financed Object No.", FinancedObject."No."); newline
tabulator tabulator tabulator tabulator CS.SetRange("Service Kind", "API Service Kind"::"Fuel Card"); newline
tabulator tabulator tabulator tabulator if CS.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator FuelCards += ', ' + CS."Service Code"; newline
tabulator tabulator tabulator tabulator tabulator until CS.Next() = 0; newline
tabulator tabulator tabulator tabulator FuelCards assignment DelChr(FuelCards, '<', ', '); newline
 newline
tabulator tabulator tabulator tabulator CS.SetRange("Service Kind"); newline
tabulator tabulator tabulator tabulator CS.SetFilter("Service Status", 'notequal%1', "API Service Status"::Cancelled); newline
tabulator tabulator tabulator tabulator CS.SetFilter("Service Type Code", 'RIM|RIM_ACC|T_CHANGE|T_STORAGE|TIRE|HW_STAMP|GPS_LOG|GPS_SHARE|ROAD_ASSIS|FUEL_CARD'); newline
tabulator tabulator tabulator tabulator if CS.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator ContractServices += ' / ' + CS."Service Code" + ' ' + CS."Service Description"; newline
tabulator tabulator tabulator tabulator tabulator until CS.Next() = 0; newline
tabulator tabulator tabulator tabulator ContractServices assignment DelChr(ContractServices, '<', ' / '); newline
 newline
tabulator tabulator tabulator tabulator AddEq.SetRange("Financed Object No.", FinancedObject."No."); newline
tabulator tabulator tabulator tabulator if AddEq.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator AdditionalEquipment += ' / ' + AddEq.Description; newline
tabulator tabulator tabulator tabulator tabulator until AddEq.Next() = 0; newline
tabulator tabulator tabulator tabulator AdditionalEquipment assignment DelChr(AdditionalEquipment, '<', ' / '); newline
 newline
tabulator tabulator tabulator tabulator if CC.Get(FinancedObject."Catalogue Card No.", "BLG Ext. Colour Code") then newline
tabulator tabulator tabulator tabulator tabulator ExteriorColor assignment CC.Description + ' (' + CC."Color Code" + ')'; newline
tabulator tabulator tabulator tabulator if CC.Get(FinancedObject."Catalogue Card No.", "BLG Int. Colour Code") then newline
tabulator tabulator tabulator tabulator tabulator InteriorColor assignment CC.Description + ' (' + CC."Color Code" + ')'; newline
 newline
tabulator tabulator tabulator tabulator RecordRef.GetTable(FinancedObject); newline
 newline
tabulator tabulator tabulator tabulator OptEq.SetRange("Financed Object No.", FinancedObject."No."); newline
tabulator tabulator tabulator tabulator AddEq.SetRange("Financed Object No.", FinancedObject."No."); newline
tabulator tabulator tabulator tabulator OptEq.SetFilter(Description, '(%1|%2|%3)&notequal%4&notequal%5&notequal%6', '@*tažné*', '@*tazne*', '@*tow*', '@*schůdek*', '@*příprava*', '@*lano*'); //(*tažné*|*tazne*|*tow*)&notequal*schůdek*&notequal*příprava*&notequal*lano* newline
tabulator tabulator tabulator tabulator AddEq.SetFilter(Description, '(%1|%2|%3)&notequal%4&notequal%5&notequal%6', '@*tažné*', '@*tazne*', '@*tow*', '@*schůdek*', '@*příprava*', '@*lano*'); //(*tažné*|*tazne*|*tow*)&notequal*schůdek*&notequal*příprava*&notequal*lano* newline
tabulator tabulator tabulator tabulator if OptEq.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator TH assignment true newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator if AddEq.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator TH assignment true; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator } newline
 newline
