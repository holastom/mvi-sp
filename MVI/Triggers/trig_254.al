tabulator tabulator tabulator trigger OnPreDataItem() newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator SetRange("Salespers./Purch. Code", "Purchase Header"."Purchaser Code"); newline
tabulator tabulator tabulator tabulator FindFirst; newline
tabulator tabulator tabulator tabulator SetRecFilter; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem("Company Information"; "Company Information") newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = sorting("Primary Key"); newline
 newline
tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator column(CI_Address; Address) { } newline
tabulator tabulator tabulator column(CI_Address_2; "Address 2") { } newline
tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator column(CI_Post_Code; "Post Code") { } newline
tabulator tabulator tabulator column(CI_Phone_No_; LanguageHandler.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator column(CI_E_Mail; "E-Mail") { } newline
tabulator tabulator tabulator column(CI_Home_Page; "Home Page") { } newline
tabulator tabulator tabulator column(CI_VAT_Registration_No_; "VAT Registration No.") { } newline
tabulator tabulator tabulator column(CI_Registration_No_; "Registration No.") { } newline
tabulator tabulator tabulator column(CI_CountryRegion; Utils.CountryName("Country/Region Code")) { } newline
tabulator tabulator tabulator column(CI_Picture; Picture) { } newline
tabulator tabulator tabulator column(CI_IBAN; IBAN) { } newline
tabulator tabulator tabulator column(CI_Bank_Account_No_; "Bank Account No.") { } newline
 newline
