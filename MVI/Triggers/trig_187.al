tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator MakeModelName assignment FinancedObject."Make Name" + ' ' + FinancedObject."Model Name"; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem("User Setup"; "User Setup") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Salespers./Purch. Code" = field(Administrator); newline
tabulator tabulator tabulator tabulator dataitem("Employee"; "Employee") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("API Employee No."); newline
tabulator tabulator tabulator tabulator tabulator column(E_Full_Name; "Full Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(E_Company_E_Mail; "Company E-Mail") { } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem(CI; "Company Information") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator tabulator column(CI_Addr; Address) { } newline
tabulator tabulator tabulator tabulator column(CI_PSC; "Post Code") { } newline
tabulator tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator tabulator column(CI_Phone; "Phone No.") { } newline
tabulator tabulator tabulator tabulator column(CI_Email; "E-Mail") { } newline
tabulator tabulator tabulator tabulator column(CI_HomePage; "Home Page") { } newline
tabulator tabulator tabulator tabulator column(CI_RegNo; "Registration No.") { } newline
tabulator tabulator tabulator tabulator column(CI_VATRegNo; "VAT Registration No.") { } newline
tabulator tabulator tabulator tabulator dataitem(CI_CounReg; "Country/Region") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = Code = field("Country/Region Code"); newline
tabulator tabulator tabulator tabulator tabulator column(CI_CRegName; Name) { } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
 newline
