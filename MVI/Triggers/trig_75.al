tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator CS: Record "API Contract Service"; newline
tabulator tabulator FD: Record "API Fee Detail"; newline
 newline
tabulator tabulator FileName: Text; newline
tabulator tabulator SheetName: Text; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator FCH.SetRange("Calculation Variant", false); newline
tabulator tabulator FCH.SetRange("Change Copy", false); newline
tabulator tabulator if Activated then newline
tabulator tabulator tabulator FCH.SetRange(Status, FCH.Status::Active) newline
tabulator tabulator else//Terminated newline
tabulator tabulator tabulator FCH.SetFilter(Status, '%1|%2|%3', FCH.Status::Active, FCH.Status::Closed, FCH.Status::Settled); newline
tabulator tabulator if FCH.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FCH.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator tabulator CS.SetRange("Contract No.", FCH."No."); newline
tabulator tabulator tabulator tabulator CS.SetRange("Service Contract Type", CS."Service Contract Type"::Contract); newline
tabulator tabulator tabulator tabulator CS.SetRange("Service Kind", CS."Service Kind"::"Fee/Service"); newline
tabulator tabulator tabulator tabulator CS.SetRange("Service Type Code", 'ROAD_ASSIS'); newline
tabulator tabulator tabulator tabulator if Activated then newline
tabulator tabulator tabulator tabulator tabulator CS.SetFilter("Valid To after Extension", '>=%1', WorkDate()) newline
tabulator tabulator tabulator tabulator else //Terminated newline
tabulator tabulator tabulator tabulator tabulator CS.SetFilter("Valid To after Extension", '%1..%2', DateFrom, DateTo); newline
tabulator tabulator tabulator tabulator if CS.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator FD.Get(CS."Service Contract Type", CS."No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Make Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Model Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO.VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CS."Valid From", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator if Activated and (CS."Valid From" notequal 0D) then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CalcDate('<+60M>', CS."Valid From"), false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date) newline
tabulator tabulator tabulator tabulator tabulator tabulator else //Terminated newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CS."Valid To after Extension", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."1st Registration Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Financing Product Type Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Address", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer Address 2", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Post Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."Customer City", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Driver Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Driver No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CS."Service Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(StandardVIP(FD."Extra Service"), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator until CS.Next() = 0; newline
tabulator tabulator tabulator until FCH.Next() = 0; newline
 newline
tabulator tabulator FileName assignment 'UAMK'; newline
tabulator tabulator SheetName assignment 'UAMK'; newline
tabulator tabulator if Activated then begin newline
tabulator tabulator tabulator FileName += '_Activated_%1'; newline
tabulator tabulator tabulator SheetName += ' Activated'; newline
tabulator tabulator end else begin //Terminated newline
tabulator tabulator tabulator FileName += '_Terminated_%1'; newline
tabulator tabulator tabulator SheetName += ' Terminated'; newline
tabulator tabulator end; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
