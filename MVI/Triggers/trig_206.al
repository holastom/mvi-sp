tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator ContractService: Record "API Contract Service"; newline
tabulator tabulator Customer: Record Customer; newline
tabulator tabulator Contact: Record Contact; newline
 newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator TempBlob: Codeunit "Temp Blob"; newline
tabulator tabulator InStream: InStream; newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator FileName: Text; newline
tabulator begin newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(); newline
tabulator tabulator FileName assignment 'Service_Activation_Roadside_Assistance.csv'; newline
tabulator tabulator TempBlob.CreateOutStream(OutStream, TextEncoding::Windows); newline
 newline
tabulator tabulator FinancingContractHeader.SetFilter("Financing Type", 'notequal%1', "API Financing Type"::"Fleet Management"); newline
tabulator tabulator FinancingContractHeader.SetFilter(Status, 'Active'); newline
 newline
tabulator tabulator OutStream.WriteText('LS number;Reg No;Model Desc;Vin;Start Date;Valid until;Actual End Date;Reg Date;Account Name;Driver;Contact Number;Element Name;Operation Type;TIMESTAMP'); newline
 newline
tabulator tabulator if FinancingContractHeader.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator if FinancedObject.Get(FinancingContractHeader."Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator if Customer.Get(FinancingContractHeader."Customer No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator ContractService.SetFilter("Contract No.", FinancingContractHeader."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator ContractService.SetFilter("Service Type Code", 'ROAD_ASSIS'); newline
tabulator tabulator tabulator tabulator tabulator tabulator // (StartA <= EndB) and (EndA >= StartB) - formule pro 2 prekryvajici se intervaly. StartA = DateFrom, EndB = Valid To, EndA = DateTo, StartB = Valid From newline
tabulator tabulator tabulator tabulator tabulator tabulator if (DateTo notequal 0D) and (DateFrom notequal 0D) then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ContractService.SetFilter("Valid From", '<=%1', DateTo); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ContractService.SetFilter("Valid To", '>=%1', DateFrom); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator if ContractService.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(FinancingContractHeader."No." + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(FinancedObject."Licence Plate No." + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(FinancedObject."Make Name" + ' ' + FinancedObject."Model Name" + ' ' + FinancedObject."Model Type Name" + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(FinancedObject.VIN + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(LanguageHandler.FormatDateNoSpace(ContractService."Valid From") + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(LanguageHandler.FormatDateNoSpace(ContractService."Valid To") + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(LanguageHandler.FormatDateNoSpace(FinancingContractHeader."Actual Termination Date") + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(LanguageHandler.FormatDateNoSpace(FinancedObject."1st Registration Date") + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(Customer.Name + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(FinancingContractHeader."BLG Driver Name" + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if not Contact.Get(FinancingContractHeader."BLG Driver No.") then; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(LanguageHandler.FormatPhoneNumber(Contact."Mobile Phone No.") + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(ContractService."Service Code" + ';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(';'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(LanguageHandler.FormatDateNoSpace(Today())); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator until ContractService.Next() = 0; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until FinancingContractHeader.Next() = 0; newline
 newline
tabulator tabulator TempBlob.CreateInStream(InStream, TextEncoding::Windows); newline
tabulator tabulator DownloadFromStream(InStream, '', '', '', FileName) newline
tabulator end; newline
