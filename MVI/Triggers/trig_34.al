tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator // FCL: Record "API Financing Contract Line"; newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator CPL: Record "Sales Invoice Line"; newline
tabulator tabulator SIL2: Record "Sales Invoice Line"; newline
tabulator tabulator MPL: Record "API Maint. Permission Line"; newline
tabulator tabulator ROH: Record "API Rent Order Header"; newline
tabulator tabulator ROL: Record "API Rent Order Line"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator Amount: Decimal; newline
tabulator tabulator Total_amount: Decimal; newline
tabulator tabulator VATamount: Decimal; newline
tabulator tabulator Total_VATamount: Decimal; newline
tabulator tabulator CountLines: Integer; newline
tabulator tabulator Attachment: File; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator tabulator LineNo: Integer; newline
tabulator tabulator First: Boolean; newline
tabulator tabulator AmountCL: Decimal; newline
tabulator tabulator LicPlateNo: Text; newline
tabulator tabulator FinObjName: Text; newline
tabulator begin newline
tabulator tabulator CountLines assignment 0; newline
tabulator tabulator Total_amount assignment 0; newline
tabulator tabulator VATamount assignment 0; newline
 newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::UTF8); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
 newline
tabulator tabulator SIH.CalcFields(Amount); newline
tabulator tabulator if SIH."API Invoice Print Type" = "API Invoice Print Type"::Payment then begin newline
tabulator tabulator tabulator //Header newline
tabulator tabulator tabulator OutStream.WriteText('H'); //h1 newline
tabulator tabulator tabulator OutStream.WriteText('25361478'); //h2 newline
tabulator tabulator tabulator OutStream.WriteText('N'); //h3 newline
tabulator tabulator tabulator OutStream.WriteText(Format(Today(), 0, '<Day,2><Month,2><Year4>')); //h4 newline
tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SIH."No.", 10)); //h5 newline
tabulator tabulator tabulator OutStream.WriteText(Format(CalcDate('<CM>', SIH."Posting Date"), 0, '<Day,2><Month,2><Year4>')); newline
tabulator tabulator tabulator OutStream.WriteText(Format(SIH."Document Date", 0, '<Day,2><Month,2><Year4>')); //h7 newline
tabulator tabulator tabulator OutStream.WriteText(Format(SIH."Due Date", 0, '<Day,2><Month,2><Year4>')); //h8 newline
tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(SIH.Amount, 2), 12));//h9 newline
 newline
tabulator tabulator tabulator //Body newline
tabulator tabulator tabulator SA.GroupByShortcutDimension(FCHCodes, SIH); newline
tabulator tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator tabulator FCH.CalcFields("BLG Driver Name"); newline
 newline
tabulator tabulator tabulator tabulator Amount assignment SA.GetAmount(GroupedCode, SIH."No."); newline
tabulator tabulator tabulator tabulator VATamount assignment SA.GetAmountInclVAT(GroupedCode, SIH."No."); newline
tabulator tabulator tabulator tabulator Total_amount += Amount + VATamount; newline
tabulator tabulator tabulator tabulator Total_VATamount += VATamount; newline
 newline
tabulator tabulator tabulator tabulator SIL.SetRange("Document No.", SIH."No."); newline
tabulator tabulator tabulator tabulator SIL.SetFilter(Type, 'notequal%1', SIL.Type::" "); newline
tabulator tabulator tabulator tabulator if SIL.FindFirst() then; newline
 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator tabulator tabulator OutStream.WriteText('B'); //b1 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 22)); //b2 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FO."Licence Plate No.", 31)); //b3 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FO."BLG Name 2", 31)); //b4 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 12)); //b5 + b6 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('N Ceska Republikatabulator  '); //b7 + b8 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 50)); //b9 + b10 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('Nájemné', 39)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('1.00', 8)); //b13 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('Kc 21'); //b14 + b15 + b16 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.FormatDecimal(SIL."VAT %", 2)); //b17 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SIL."API Maintenance Approval No.", 12)); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(Amount, 2), 12));//b19 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(VATamount, 2), 12));//b20 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(Amount + VATamount, 2), 12));//b21 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 9)); //b22 + b23 newline
tabulator tabulator tabulator tabulator CountLines += 1; newline
tabulator tabulator tabulator end; newline
tabulator tabulator end else newline
tabulator tabulator tabulator if SIH."API Invoice Print Type" = "API Invoice Print Type"::"BLG Consolidated Invoice" then begin newline
tabulator tabulator tabulator tabulator //Header newline
tabulator tabulator tabulator tabulator OutStream.WriteText('H'); //h1 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('25361478'); //h2 newline
tabulator tabulator tabulator tabulator OutStream.WriteText('F'); //h3 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(Format(Today(), 0, '<Day,2><Month,2><Year4>')); //h4 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SIH."No.", 10)); //h5 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(Format(SIH."Posting Date", 0, '<Day,2><Month,2><Year4>')); //h7 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(Format(SIH."Document Date", 0, '<Day,2><Month,2><Year4>')); //h8 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(SIH.Amount, 2), 12));//h9 newline
 newline
tabulator tabulator tabulator tabulator //Body newline
tabulator tabulator tabulator tabulator SA.GroupByMaintancePermision(FCHCodes, SIH); newline
tabulator tabulator tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator tabulator tabulator SIL.SetRange("Document No.", SIH."No."); newline
tabulator tabulator tabulator tabulator tabulator SIL.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator tabulator tabulator tabulator SIL.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator if GroupedCode notequal '' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator CPL.SetRange("Document No.", SIH."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator CPL.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator tabulator tabulator tabulator tabulator CPL.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator tabulator CPL.FindSet(); newline
tabulator tabulator tabulator tabulator tabulator tabulator if CPL.Count() notequal 1 then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator MPL.SetRange("Maintenance Permission No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator MPL.SetRange("Category Code", ''); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPL.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineNo assignment MPL."Line No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SIL2.SetRange("Document No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SIL2.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SIL2.SetRange("BLG Maint. Perm. Line No.", LineNo); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SIL2.FindFirst(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineNo assignment SIL2."Line No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator First assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator AmountCL assignment SIL2.GetLineAmountExclVAT(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SIL.SetFilter("Line No.", 'notequal%1', LineNo); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator if SIL.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Amount assignment SIL.GetLineAmountExclVAT(); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if First then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Amount += AmountCL; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator First assignment false; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Total_VATamount += SIL."Amount Including VAT"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Total_amount += Amount + SIL."Amount Including VAT"; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if FCH.Get(SIL."Shortcut Dimension 2 Code") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if FO.Get(FCH."Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment FO."Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinObjName assignment FO."BLG Name 2"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if ROH.Get(ROH."Contract Type"::Rent, SIL."API Rent No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ROL.SetRange("Contract Type", ROH."Contract Type"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ROL.SetRange("Contract No.", ROH."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if not ROL.FindFirst then Clear(ROL); //řádek má být vždy jediný newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ROL.CalcFields("Car Description"); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinObjName assignment ROL."Car Description"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment SIL."API RC Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(ROH); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(ROL); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('B'); //b1 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 22)); //b2 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(LicPlateNo, 31)); //b3 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FinObjName, 31)); //b4 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 12)); //b5 + b6 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('N Ceska Republikatabulator  '); //b7 + b8 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 50)); //b9 + b10 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SIL.Description, 39)); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('1.00', 8)); //b13 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText('Kc 21'); //b14 + b15 + b16 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.FormatDecimal(SIL."VAT %", 2)); //b17 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SIL."API Maintenance Approval No.", 12)); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(Amount, 2), 12));//b19 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(SIL."Amount Including VAT", 2), 12));//b20 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(Amount + SIL."Amount Including VAT", 2), 12));//b21 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 9)); //b22 + b23 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator CountLines += 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator //Trail newline
tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator OutStream.WriteText('T'); //t1 newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(Format(CountLines), 8)); //t2 newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(Total_VATamount, 2), 14)); //t3 newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SA.FormatDecimal(Total_amount, 2), 14)); //t4 newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
