tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator CDCDocument: Record "CDC Document"; newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator MasterAgreement: Record "API Master Agreement"; newline
tabulator tabulator SalesPrice: Record "Sales Price"; newline
tabulator tabulator Item: Record Item; newline
 newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
 newline
tabulator tabulator FinesMethod: Text; newline
tabulator tabulator UnitPrice: Decimal; newline
tabulator tabulator ItemNo: Code[20]; newline
tabulator tabulator FileName: Label 'Define_Fines_Extract_%1'; newline
tabulator tabulator SheetName: Label 'Define Fines Extract'; newline
tabulator tabulator NoDocumentError: Label 'There are no OPEN documents in the FINES section.'; newline
tabulator begin newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(); newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
tabulator tabulator CDCDocument.SetFilter(Status, '%1', CDCDocument.Status::Open); newline
tabulator tabulator CDCDocument.SetRange("Document Category Code", 'FINES'); newline
tabulator tabulator if CDCDocument.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FinesMethod assignment 'SEND'; newline
tabulator tabulator tabulator tabulator CDCDocument.CalcFields("BLG Contract", "BLG Document Type", "BLG Data Message ID", "BLG Amount Incl. VAT", "BLG Vendor Invoice No.", "BLG Invoice Date", "BLG Offense Date", "BLG File Name"); newline
tabulator tabulator tabulator tabulator Clear(FinancingContractHeader); newline
tabulator tabulator tabulator tabulator if FinancingContractHeader.Get(CDCDocument."BLG Contract") then begin newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("Licence Plate No."); newline
tabulator tabulator tabulator tabulator tabulator if MasterAgreement.Get(FinancingContractHeader."Master Agreement No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator FinesMethod assignment MasterAgreement."BLG Resolving Fines Method"; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator if CDCDocument."BLG Document Type" = 'VÝZVA' then newline
tabulator tabulator tabulator tabulator tabulator ItemNo assignment 'FINE_ASSIST_AUTH' newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator if CDCDocument."BLG Document Type" = 'PŘÍKAZ' then newline
tabulator tabulator tabulator tabulator tabulator tabulator ItemNo assignment 'FINE2_ASSIST_AUTH'; newline
 newline
tabulator tabulator tabulator tabulator SalesPrice.SetRange("Sales Code", MasterAgreement."Customer No."); newline
tabulator tabulator tabulator tabulator SalesPrice.SetRange("Sales Type", SalesPrice."Sales Type"::Customer); newline
tabulator tabulator tabulator tabulator SalesPrice.SetRange("Item No.", ItemNo); newline
tabulator tabulator tabulator tabulator SalesPrice.SetFilter("Starting Date", '..%1|%2', Today, 0D); newline
tabulator tabulator tabulator tabulator SalesPrice.SetFilter("Ending Date", '%1..|%2', Today, 0D); newline
tabulator tabulator tabulator tabulator if SalesPrice.FindFirst() and (MasterAgreement."No." notequal '') then newline
tabulator tabulator tabulator tabulator tabulator UnitPrice assignment SalesPrice."Unit Price" newline
tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator if Item.Get(ItemNo) then newline
tabulator tabulator tabulator tabulator tabulator tabulator UnitPrice assignment Item."Unit Price"; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Data Message ID", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_1 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_2 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Document Type", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_3 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_4 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_5 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_6 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Amount Incl. VAT", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); //ID_7 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Vendor Invoice No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_8 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_9 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_10 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinesMethod, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_11 newline
tabulator tabulator tabulator tabulator if (CDCDocument."BLG Document Type" = 'PŘÍKAZ') or (MasterAgreement."BLG Resolving Fines Method" = 'PAY') then newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('ANO', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text) //ID_12 newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('NE', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_12 newline
 newline
tabulator tabulator tabulator tabulator if (CDCDocument."BLG Document Type" in ['PŘÍKAZ', 'VÝZVA', 'VYSVĚTLENÍ']) and (UnitPrice > 0) then newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('ANO', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text) //ID_13 newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('NE', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_13 newline
 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader.Status, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_14 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Contractual End Date", false, 'dd.MM.yyyy', false, false, false, '', TmpExcelBuffer."Cell Type"::Date); //ID_15 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_16 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_17 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_18 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Invoice Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); //ID_19 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG Offense Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); //ID_20 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Financed Object No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_21 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_22 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(UnitPrice, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); //ID_23 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."BLG File Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_24 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CDCDocument."Source Record No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_25 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Empty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); //ID_26 newline
tabulator tabulator tabulator until CDCDocument.Next() = 0; newline
tabulator tabulator end else newline
tabulator tabulator tabulator Error(NoDocumentError); newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
