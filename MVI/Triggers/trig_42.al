tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FuelCardTransaction: Record "API Fuel Card Transaction"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator Customer: Record Customer; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator Len: Integer; newline
tabulator tabulator Quantity: Decimal; newline
tabulator tabulator Amount: Decimal; newline
tabulator tabulator VAT: Decimal; newline
tabulator tabulator VatRateCustomer: Decimal; newline
tabulator tabulator VatCustomer: Decimal; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator Quantity assignment 0; newline
tabulator tabulator Amount assignment 0; newline
tabulator tabulator VAT assignment 0; newline
 newline
tabulator tabulator Customer.Get(SIH."Sell-to Customer No."); newline
 newline
tabulator tabulator CreateHeader(); newline
tabulator tabulator Len assignment 13; newline
 newline
tabulator tabulator FuelCardTransaction.SetRange("Posted Sales Invoice No.", SIH."No."); newline
tabulator tabulator if FuelCardTransaction.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FinancingContractHeader.Get(FuelCardTransaction."Financing Contract No."); newline
tabulator tabulator tabulator tabulator FinancedObject.Get(FinancingContractHeader."Financed Object No."); newline
tabulator tabulator tabulator tabulator FuelCardTransaction.CalcFields("Card Vendor Name"); newline
 newline
tabulator tabulator tabulator tabulator if Customer."VAT Bus. Posting Group" in ['EU', 'EXPORT'] then begin newline
tabulator tabulator tabulator tabulator tabulator VatRateCustomer assignment 0; newline
tabulator tabulator tabulator tabulator tabulator VatCustomer assignment 0; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator VatRateCustomer assignment FuelCardTransaction."VAT Rate % - Customer"; newline
tabulator tabulator tabulator tabulator tabulator VatCustomer assignment FuelCardTransaction."VAT Amount - Customer"; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Financing Contract No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Card No. - Company", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Card Vendor Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SIH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Transaction Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Transaction Time", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Time); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Petrol Station Location", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Round(FuelCardTransaction."Unit Pr. Ex. VAT (LCY) - Cust."), false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction.Quantity, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator Quantity += FuelCardTransaction.Quantity; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Tot. Pr. Ex. VAT (LCY) - Cust.", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator Amount += FuelCardTransaction."Tot. Pr. Ex. VAT (LCY) - Cust."; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatCustomer, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator VAT += VatCustomer; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Item Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatRateCustomer, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('CZK', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator if FuelCardTransaction."Odometer Status" = 0 then newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(0, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number) newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelCardTransaction."Odometer Status", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator until FuelCardTransaction.Next() = 0; newline
tabulator tabulator SA.GrandTotal(Quantity, Amount, VAT, TmpExcelBuffer, Len); newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Fuel spec no 5 obv doc'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
