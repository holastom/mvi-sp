tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator RegNoVATRegNoCZ assignment Utils.IdVatIdNo(J_Customer, LH.GetCompanyLanguageId()); newline
tabulator tabulator tabulator tabulator tabulator RegNoVATRegNoEN assignment Utils.IdVatIdNo(J_Customer, LH.GetENlanguageId()); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem(MasterAgreement; "API Master Agreement") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Master Agreement No."); newline
tabulator tabulator tabulator tabulator column(No; "No.") { } newline
tabulator tabulator tabulator tabulator column(SigningDate; LH.FormatVar("Signing Date")) { } newline
tabulator tabulator tabulator tabulator dataitem(MA_Customer; Customer) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Customer No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator column(Z_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator column(Z_Addr; Address) { } newline
tabulator tabulator tabulator tabulator tabulator column(Z_City; City + Utils.AddToCityCountryName("Country/Region Code")) { } newline
tabulator tabulator tabulator tabulator tabulator column(Z_PSC; "Post Code") { } newline
tabulator tabulator tabulator tabulator tabulator column(Z_RegNoCZ; RegNoVATRegNoCZ) { } newline
tabulator tabulator tabulator tabulator tabulator column(Z_RegNoEN; RegNoVATRegNoEN) { } newline
 newline
tabulator tabulator tabulator tabulator tabulator dataitem(Z_Contact; Contact) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Primary Contact No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator column(Z_JuridictionPlace; "API Jurisdiction Place") { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(Z_FileNo; Utils.nbsp("API File No.")) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(Z_Signers_CZ; Z_Signers_CZ) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(Z_Signers_EN; Z_Signers_EN) { } newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator dataitem(CustSigners; Contact) newline
tabulator tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator UseTemporary = true; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator column(CS_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator column(CS_Company; "Company Name") { } newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator column(CS_JobTitle; "Job Title") { } newline
tabulator tabulator tabulator tabulator tabulator tabulator } newline
 newline
