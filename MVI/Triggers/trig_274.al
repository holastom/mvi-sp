tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator TempDate: Date; newline
tabulator tabulator tabulator tabulator sil: Record "Sales Invoice Line"; newline
tabulator tabulator tabulator tabulator OSH: Record "API Odometer Status History"; newline
tabulator tabulator tabulator tabulator CBR: Record "Contact Business Relation"; newline
tabulator tabulator tabulator tabulator IdNoCZ: Label 'IČO'; newline
tabulator tabulator tabulator tabulator idNoEN: Label 'Id No.'; newline
tabulator tabulator tabulator tabulator TaxIdNoCZ: Label 'DIČ'; newline
tabulator tabulator tabulator tabulator TaxIdNoEN: Label 'VAT Id. No.'; newline
tabulator tabulator tabulator tabulator PersIdNoCZ: Label 'rodné číslo'; newline
tabulator tabulator tabulator tabulator PersIdNoEN: Label 'Birth Nr.'; newline
tabulator tabulator tabulator tabulator BirthdateCZ: Label 'datum narození'; newline
tabulator tabulator tabulator tabulator BirthdateEN: Label 'Birth date'; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator OSP.SetRange("Financed Object No.", "No."); newline
tabulator tabulator tabulator tabulator OSP.SetRange("BLG Sale", true); newline
tabulator tabulator tabulator tabulator if not OSP.FindFirst then CurrReport.Skip; newline
tabulator tabulator tabulator tabulator OSP.CalcFields("Buyer Name"); //=Customer.Name newline
tabulator tabulator tabulator tabulator Customer.Get(OSP."Buyer No."); newline
tabulator tabulator tabulator tabulator CBR.SetRange("Link to Table", "Contact Business Relation Link To Table"::Customer); newline
tabulator tabulator tabulator tabulator CBR.SetRange("No.", Customer."No."); newline
tabulator tabulator tabulator tabulator CBR.FindFirst(); newline
tabulator tabulator tabulator tabulator Contact.Get(CBR."Contact No."); newline
 newline
tabulator tabulator tabulator tabulator CustIdentInfoCZ assignment Utils.IdentInfo(Contact, IdNoCZ, PersIdNoCZ, BirthdateCZ, TaxIdNoCZ); newline
tabulator tabulator tabulator tabulator CustIdentInfoEN assignment Utils.IdentInfo(Contact, IdNoEN, PersIdNoEN, BirthdateEN, TaxIdNoEN); newline
 newline
tabulator tabulator tabulator tabulator sil.SetRange("Document No.", OSP."Posted Sales Invoice No."); newline
tabulator tabulator tabulator tabulator sil.SetRange(Type, sil.Type::Item); newline
tabulator tabulator tabulator tabulator sil.SetRange("No.", 'VEH_REGISTRATION'); newline
tabulator tabulator tabulator tabulator if sil.FindFirst then; newline
tabulator tabulator tabulator tabulator RegrFeeInclVAT assignment sil."Amount Including VAT"; newline
 newline
tabulator tabulator tabulator tabulator OSH.SetRange("Financed Object No.", FO."No."); newline
tabulator tabulator tabulator tabulator OSH.SetCurrentKey("Mileage Date"); newline
tabulator tabulator tabulator tabulator OSH.SetAscending("Mileage Date", false); newline
tabulator tabulator tabulator tabulator if OSH.FindFirst() then begin newline
tabulator tabulator tabulator tabulator tabulator TempDate assignment OSH."Mileage Date"; newline
tabulator tabulator tabulator tabulator tabulator OSH.SetRange("Mileage Date", TempDate); newline
tabulator tabulator tabulator tabulator tabulator OSH.SetCurrentKey(Mileage); newline
tabulator tabulator tabulator tabulator tabulator OSH.SetAscending(Mileage, false); newline
tabulator tabulator tabulator tabulator tabulator if OSH.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator Mileage assignment OSH.Mileage; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem(Company_Information; "Company Information") newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = sorting("Primary Key"); newline
 newline
tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator column(CI_Addr; Address) { } newline
tabulator tabulator tabulator column(CI_Addr2; "Address 2") { } newline
tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator column(CI_PSC; "Post Code") { } newline
tabulator tabulator tabulator column(CI_PhoneNo; LangHndl.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator column(CI_Email; "E-Mail") { } newline
tabulator tabulator tabulator column(CI_HomePage; "Home Page") { } newline
tabulator tabulator tabulator column(CI_RegNo; "Registration No.") { } newline
tabulator tabulator tabulator column(CI_VATRegNo; "VAT Registration No.") { } newline
tabulator tabulator tabulator column(CI_Country; Utils.CountryName("Country/Region Code")) { } newline
tabulator tabulator tabulator column(BankName; BankAcct.Name) { } newline
tabulator tabulator tabulator column(BankAcctNo; BankAcct."Bank Account No.") { } newline
tabulator tabulator tabulator column(EmpName; Employee."Full Name") { } newline
tabulator tabulator tabulator column(EmpJobTitle; Employee."Job Title") { } newline
tabulator tabulator tabulator dataitem(CI_Contact; Contact) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Registration No. CZL" = field("Registration No."); newline
tabulator tabulator tabulator tabulator column(CI_JurisdictionPlace; "API Jurisdiction Place") { } newline
tabulator tabulator tabulator tabulator column(CI_FileNo; Utils.nbsp("API File No.")) { } newline
tabulator tabulator tabulator } newline
 newline
