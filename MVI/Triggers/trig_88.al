tabulator tabulator tabulator tabulator trigger OnPreDataItem() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator RIM_ContractService.SetFilter("Valid From", '<=%1', Today()); newline
tabulator tabulator tabulator tabulator tabulator RIM_ContractService.SetFilter("Valid To", '>=%1', Today()); newline
tabulator tabulator tabulator tabulator tabulator RIM_ContractService.SetRange("Service Type Code", 'RIM'); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem(FinancedObject; "API Financed Object") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Financed Object No."); newline
tabulator tabulator tabulator tabulator column(FO_VendorContPersName; "BLG Vendor Contact Person Name") { } newline
tabulator tabulator tabulator tabulator column(FO_VendorBusPlaceName; VendorBusinessPlaceName) { } newline
tabulator tabulator tabulator tabulator column(FO_PurchOrderNo; PurchaseOrderNo) { } newline
tabulator tabulator tabulator tabulator column(FO_Model; "Make Name" + ' ' + "Model Name" + ' ' + "Model Type Name") { } newline
tabulator tabulator tabulator tabulator dataitem(Vendor; Vendor) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Vendor No."); newline
tabulator tabulator tabulator tabulator tabulator column(V_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator column(V_Addr; Address) { } newline
tabulator tabulator tabulator tabulator tabulator column(V_City; City) { } newline
tabulator tabulator tabulator tabulator tabulator column(V_PSC; "Post Code") { } newline
 newline
