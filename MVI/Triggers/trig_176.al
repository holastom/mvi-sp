tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() //ROH newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator cont: Record Contact; newline
tabulator tabulator tabulator tabulator tabulator cust: Record Customer; newline
tabulator tabulator tabulator tabulator tabulator i: Integer; newline
tabulator tabulator tabulator tabulator begin newline
 newline
tabulator tabulator tabulator tabulator tabulator if cont.Get("Authorized User 1") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator i += 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator AUserName[i] assignment cont.Name; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator if cont.Get("Authorized User 2") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator i += 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator AUserName[i] assignment cont.Name; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator cust.Get("Renter No."); newline
tabulator tabulator tabulator tabulator tabulator Renter.Get(cust."Primary Contact No."); newline
tabulator tabulator tabulator tabulator tabulator if not RenterBank.Get(cust."No.", cust."Preferred Bank Account Code") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator RenterBank.SetRange("Customer No.", cust."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator if RenterBank.FindFirst() then; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator RenterIds assignment CopyStr(Utils.CondCat(Renter."Registration No. CZL", ', ' + RegNoLbl + ': ') + Utils.CondCat(Renter."VAT Registration No.", ', ' + VATRegNoLbl + ': '), 3); newline
tabulator tabulator tabulator tabulator tabulator if (Renter."API Jurisdiction Place" notequal '') and (Renter."API File No." notequal '') then newline
tabulator tabulator tabulator tabulator tabulator tabulator RenterJurReg assignment StrSubstNo('%1, %2: %3', Renter."API Jurisdiction Place", FileNoLbl, Renter."API File No."); newline
tabulator tabulator tabulator tabulator tabulator if "Delivery Address" = '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator DelivPlace assignment WillBeSpecifiedLbl else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator DelivPlace assignment Utils.CondCat("Delivery Address") + Utils.CondCat("Delivery Address 2") + Utils.CondCat(DelChr(StrSubstNo(' %1 %2', "Delivery Post Code", "Delivery City"), '<')); newline
tabulator tabulator tabulator tabulator tabulator tabulator DelivPlace assignment CopyStr(DelivPlace, 3); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator column(Today; LanguageHandler.FormatDateSpace(Today)) { } newline
tabulator tabulator tabulator column(ReportTitleLbl; ReportTitleLbl) { } newline
tabulator tabulator tabulator column(ContrLessorLbl; ContrLessorLbl) { } newline
tabulator tabulator tabulator column(LesseeLbl; LesseeLbl) { } newline
tabulator tabulator tabulator column(PhoneLbl; PhoneLbl) { } newline
tabulator tabulator tabulator column(RepresentedByLbl; RepresentedByLbl) { } newline
tabulator tabulator tabulator column(ContactPersonLbl; ContactPersonLbl) { } newline
tabulator tabulator tabulator column(RentInfoLbl; RentInfoLbl) { } newline
tabulator tabulator tabulator column(ContractLbl; ContractLbl) { } newline
tabulator tabulator tabulator column(RentFromLbl; RentFromLbl) { } newline
tabulator tabulator tabulator column(RentToLbl; RentToLbl) { } newline
tabulator tabulator tabulator column(DelivPlaceLbl; DelivPlaceLbl) { } newline
tabulator tabulator tabulator column(CarTypeLbl; CarTypeLbl) { } newline
tabulator tabulator tabulator column(ProviderLbl; ProviderLbl) { } newline
tabulator tabulator tabulator column(AuthPersonsLbl; AuthPersonsLbl) { } newline
tabulator tabulator tabulator column(AuthPersTxt1; AuthPersTxt1) { } newline
tabulator tabulator tabulator column(AuthPersTxt2; AuthPersTxt2) { } newline
tabulator tabulator tabulator column(NameLbl; NameLbl) { } newline
tabulator tabulator tabulator column(IdCardNoLbl; IdCardNoLbl) { } newline
tabulator tabulator tabulator column(DriverLicLbl; DriverLicLbl) { } newline
tabulator tabulator tabulator column(AuthPersTxt3; AuthPersTxt3) { } newline
tabulator tabulator tabulator column(RentLbl; RentLbl) { } newline
tabulator tabulator tabulator column(RentTxt1; StrSubstNo(RentTxt1, Currency)) { } newline
tabulator tabulator tabulator column(RentTxt2; RentTxt2) { } newline
tabulator tabulator tabulator column(OtherProvisionsLbl; OtherArrangementsLbl) { } newline
tabulator tabulator tabulator column(OtherProvisionsTxt1; OtherArrangementsTxt1) { } newline
tabulator tabulator tabulator column(ContractorLbl; ContractorLbl) { } newline
tabulator tabulator tabulator column(ClientLbl; ClientLbl) { } newline
tabulator tabulator tabulator column(InPragueOnLbl; InPragueOnLbl) { } newline
tabulator tabulator tabulator column(InLbl; InLbl) { } newline
tabulator tabulator tabulator column(OnLbl; OnLbl) { } newline
tabulator tabulator tabulator column(RegNoLbl; RegNoLbl) { } newline
tabulator tabulator tabulator column(VATRegNoLbl; VATRegNoLbl) { } newline
 newline
