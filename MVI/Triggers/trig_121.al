tabulator trigger OnPreReport() newline
tabulator begin newline
tabulator tabulator CI.Get; newline
tabulator tabulator FS.FindFirst; newline
tabulator tabulator FCH.Get(FS."Financing Contract No."); newline
tabulator tabulator Cust.Get(FCH."Customer No."); newline
tabulator tabulator CurrReport.Language assignment LangHndl.ReportLanguage(RequestPageLanguage, Cust."Language Code"); newline
tabulator end; newline
