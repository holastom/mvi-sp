tabulator trigger OnPostReport() newline
tabulator begin newline
tabulator tabulator if UpdateSigners then UpdateMinimumSigners(); newline
tabulator tabulator if ClearJRRoles then ClearAllJRRoles(); newline
tabulator tabulator if UpdateActiveContractStatus notequal '' then UpdateActiveContractStatuses(UpdateActiveContractStatus); newline
tabulator tabulator if FixTires then FixAllTires(); newline
tabulator end; newline
