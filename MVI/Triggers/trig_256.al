tabulator trigger OnPreReport() newline
tabulator var newline
tabulator tabulator UserSetupRec: Record "User Setup"; newline
tabulator tabulator EmployeeRec: Record Employee; newline
tabulator begin newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage; newline
tabulator tabulator Utils.ErrorIfFilterEmpty("Purchase Header".GetFilter("No."), 'No'); newline
tabulator end; newline
