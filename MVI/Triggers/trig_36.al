tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator SalesCrMemoLine: Record "Sales Cr.Memo Line"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator Customer: Record Customer; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator Len: Integer; newline
tabulator tabulator Quantity: Decimal; newline
tabulator tabulator Amount: Decimal; newline
tabulator tabulator VAT: Decimal; newline
tabulator tabulator VatRateCustomer: Decimal; newline
tabulator tabulator VatCustomer: Decimal; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator Quantity assignment 0; newline
tabulator tabulator Amount assignment 0; newline
tabulator tabulator VAT assignment 0; newline
 newline
tabulator tabulator Customer.Get(SCMH."Sell-to Customer No."); newline
 newline
tabulator tabulator CreateHeader(); newline
tabulator tabulator Len assignment 13; newline
 newline
tabulator tabulator SalesCrMemoLine.SetRange("Document No.", SCMH."No."); newline
tabulator tabulator SalesCrMemoLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator if SalesCrMemoLine.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FinancingContractHeader.Get(SalesCrMemoLine."Shortcut Dimension 2 Code"); newline
tabulator tabulator tabulator tabulator FinancedObject.Get(FinancingContractHeader."Financed Object No."); newline
 newline
tabulator tabulator tabulator tabulator if Customer."VAT Bus. Posting Group" in ['EU', 'EXPORT'] then begin newline
tabulator tabulator tabulator tabulator tabulator VatRateCustomer assignment 0; newline
tabulator tabulator tabulator tabulator tabulator VatCustomer assignment 0; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator VatRateCustomer assignment SalesCrMemoLine."VAT %"; newline
tabulator tabulator tabulator tabulator tabulator VatCustomer assignment SalesCrMemoLine."Amount Including VAT" - SalesCrMemoLine."VAT Base Amount"; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Bill-to Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SalesCrMemoLine."Shortcut Dimension 2 Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Round(SalesCrMemoLine."Unit Price"), false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SalesCrMemoLine.Quantity, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator Quantity += SalesCrMemoLine.Quantity; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SalesCrMemoLine."VAT Base Amount", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator Amount += SalesCrMemoLine."VAT Base Amount"; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatCustomer, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator VAT += VatCustomer; newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SalesCrMemoLine.Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatRateCustomer, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('CZK', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator until SalesCrMemoLine.Next() = 0; newline
tabulator tabulator SA.GrandTotal(Quantity, Amount, VAT, TmpExcelBuffer, Len); newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Fuel spec no 5 obv doc Cr. Memo'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
