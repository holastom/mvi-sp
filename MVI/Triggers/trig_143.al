tabulator tabulator tabulator tabulator tabulator trigger OnPreDataItem() newline
tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator SetFilter("Vendor No.", '%1|%2', "API Financed Object"."Vendor No.", ''); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator tabulator dataitem("API Object Add. Equipment 2"; "API Object Add. Equipment") //OAE2 newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "Financed Object No." = field("No."); newline
tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("Financed Object No.", "Line No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator column(OAE2___; '') { } newline
tabulator tabulator tabulator tabulator tabulator column(OAE2_Vendor_No_; "Vendor No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(OAE2_Description; Description) { } //ID_73 newline
tabulator tabulator tabulator tabulator tabulator column(OAE2_List_Price_Excl_VAT_LCY_; LanguageHandler.FormatVar("List Price Excl. VAT (LCY)")) { } //ID_74 newline
tabulator tabulator tabulator tabulator tabulator column(OAE2_Discount_; LanguageHandler.FormatVar("Discount %", 2)) { } //ID_75 newline
tabulator tabulator tabulator tabulator tabulator column(OAE2_Price_Excl_VAT_LCY_; LanguageHandler.FormatVar("Price Excl. VAT (LCY)")) { } //ID_76 newline
 newline
