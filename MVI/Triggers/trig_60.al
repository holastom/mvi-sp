tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator tabulator SCML2: Record "Sales Cr.Memo Line"; newline
tabulator tabulator MPL: Record "API Maint. Permission Line"; newline
tabulator tabulator CPL: Record "Sales Cr.Memo Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
tabulator tabulator Driver: Record Contact; newline
tabulator tabulator ROH: Record "API Rent Order Header"; newline
tabulator tabulator ROL: Record "API Rent Order Line"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
tabulator tabulator OutStream: OutStream; newline
 newline
tabulator tabulator TotalNetto: Decimal; newline
tabulator tabulator VatChargeable: Decimal; newline
tabulator tabulator VatNotChargeable: Decimal; newline
tabulator tabulator LineNo: Integer; newline
tabulator tabulator First: Boolean; newline
tabulator tabulator Amount: Decimal; newline
tabulator tabulator DriverName: Text; newline
tabulator tabulator LicPlateNo: Text; newline
tabulator tabulator FinObjName: Text; newline
tabulator tabulator ContractNo: Text; newline
 newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator SA.GroupByMaintancePermisionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator SCML.SetRange("Document No.", SCMH."No."); newline
tabulator tabulator tabulator SCML.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator tabulator SCML.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator if GroupedCode notequal '' then begin newline
tabulator tabulator tabulator tabulator CPL.SetRange("Document No.", SCMH."No."); newline
tabulator tabulator tabulator tabulator CPL.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator tabulator tabulator CPL.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator CPL.FindSet(); newline
tabulator tabulator tabulator tabulator if CPL.Count() notequal 1 then begin newline
tabulator tabulator tabulator tabulator tabulator MPL.SetRange("Maintenance Permission No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator MPL.SetRange("Category Code", ''); newline
tabulator tabulator tabulator tabulator tabulator if MPL.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator LineNo assignment MPL."Line No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator SCML2.SetRange("Document No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator SCML2.SetRange("API Maintenance Approval No.", GroupedCode); newline
tabulator tabulator tabulator tabulator tabulator tabulator SCML2.SetRange("BLG Maint. Perm. Line No.", LineNo); newline
tabulator tabulator tabulator tabulator tabulator tabulator SCML2.FindFirst(); newline
tabulator tabulator tabulator tabulator tabulator tabulator LineNo assignment SCML2."Line No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator First assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator Amount assignment SCML2.GetLineAmountExclVAT(); newline
tabulator tabulator tabulator tabulator tabulator tabulator SCML.SetFilter("Line No.", 'notequal%1', LineNo); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator tabulator SCML.FindSet(); newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator VatChargeable assignment 0; newline
tabulator tabulator tabulator tabulator VatNotChargeable assignment 0; newline
tabulator tabulator tabulator tabulator TotalNetto assignment 0; newline
 newline
tabulator tabulator tabulator tabulator if SCML."VAT %" notequal 0 then newline
tabulator tabulator tabulator tabulator tabulator VatChargeable assignment SCML.GetLineAmountExclVAT() newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator VatNotChargeable assignment SCML.GetLineAmountExclVAT(); newline
 newline
tabulator tabulator tabulator tabulator if First then begin newline
tabulator tabulator tabulator tabulator tabulator VatChargeable += Amount; newline
tabulator tabulator tabulator tabulator tabulator First assignment false; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TotalNetto assignment VatChargeable + VatNotChargeable; newline
 newline
tabulator tabulator tabulator tabulator if FCH.Get(SCML."Shortcut Dimension 2 Code") then begin newline
tabulator tabulator tabulator tabulator tabulator FCH.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator tabulator if FO.Get(FCH."Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment FO."Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator DriverName assignment FCH."BLG Driver Name"; newline
tabulator tabulator tabulator tabulator tabulator FinObjName assignment FO."BLG Name 2"; newline
tabulator tabulator tabulator tabulator tabulator ContractNo assignment FCH."No."; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator if ROH.Get(ROH."Contract Type"::Rent, SCML."API Rent No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ROL.SetRange("Contract Type", ROH."Contract Type"); newline
tabulator tabulator tabulator tabulator tabulator tabulator ROL.SetRange("Contract No.", ROH."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator if not ROL.FindFirst then Clear(ROL); //řádek má být vždy jediný newline
tabulator tabulator tabulator tabulator tabulator tabulator ROL.CalcFields("Car Description"); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator DriverName assignment Utils.iif(Driver.Get(ROH."Authorized User 1"), Driver.Name, ROH."Authorized User 1"); newline
tabulator tabulator tabulator tabulator tabulator tabulator FinObjName assignment ROL."Car Description"; newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment SCML."API RC Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator ContractNo assignment SCML."API Rent No."; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(ROH); newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(ROL); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Sell-to Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Posting Date", false, '', false, false, false, 'dd-MM-yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCML.Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(DriverName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LicPlateNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinObjName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatChargeable, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatNotChargeable, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(VatChargeable, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(TotalNetto, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Zentiva'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
