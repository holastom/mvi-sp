tabulator tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator tabulator OptEquipmentLbl: Label 'Příplatková výbava (od výrobce)'; newline
tabulator tabulator tabulator tabulator tabulator tabulator AddEquipmentLbl: Label 'Dodatečná výbava'; newline
tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if "Type" = "Type"::" " then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PL_Direct_Unit_Cost assignment ''; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PL_Line_Discount_ assignment ''; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PL_Amount assignment ''; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PL_AmtInclVAT assignment ''; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator case LowerCase(Description) of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'optional equipment:': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Description assignment OptEquipmentLbl + ':'; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'additional equipment:': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Description assignment AddEquipmentLbl + ':'; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator end newline
tabulator tabulator tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PL_Direct_Unit_Cost assignment LanguageHandler.FormatVar("Direct Unit Cost"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PL_Line_Discount_ assignment LanguageHandler.FormatVar("Line Discount %", 2); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PL_Amount assignment LanguageHandler.FormatVar(Amount); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator PL_AmtInclVAT assignment LanguageHandler.FormatVar("Amount Including VAT"); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator tabulator dataitem("API Financed Object"; "API Financed Object") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLinkReference = "Purchase Header"; newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("API Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator column(FO_No_; "No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_Make_Name; "Make Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_Model_Name; "Model Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_Model_Type_Name; "Model Type Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_Catalogue_Group_Name; "Catalogue Group Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_Name; "Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_Merged_Name; StrSubstNo('%1 %2 %3', "Make Name", "Model Name", "Model Type Name")) { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_Fuel_Type_Code; "Fuel Type Code") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_FuelType; FuelType) { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_BLG_Ext_Colour_Code; "BLG Ext. Colour Code") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_BLG_Ext_Colour_Description; "BLG Ext. Colour Description") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_BLG_Int_Colour_Code; "BLG Int. Colour Code") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_BLG_Int_Colour_Description; "BLG Int. Colour Description") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_VIN; VIN) { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_VendorName; "Vendor Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_CBL_Address; VehVenBussLocAddr) { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_VehVenContPersName; "BLG Vendor Contact Person Name") { } newline
tabulator tabulator tabulator tabulator tabulator column(FO_VehVenContPhoneMail; VehVenContPhoneMail) { } newline
 newline
