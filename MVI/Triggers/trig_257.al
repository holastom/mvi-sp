tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator PurchaseHeader: Record "Purchase Header"; newline
tabulator tabulator BlFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
 newline
tabulator tabulator FileName: Label 'Dealer_Delivery_Confirmation_%1_%2'; newline
tabulator tabulator SheetName: Label 'Dealer Delivery Confirmation'; newline
tabulator begin newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(); newline
 newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator PurchaseHeader.SetRange("Buy-from Vendor No.", Vendor."No."); newline
tabulator tabulator PurchaseHeader.SetRange("Document Type", "Purchase Document Type"::Order); newline
tabulator tabulator if PurchaseHeader.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator if FinancedObject.Get(PurchaseHeader."API Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator if FinancedObject."Detail Object Status Code" = 'OBJ_CONF' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(FinancingContractHeader); newline
tabulator tabulator tabulator tabulator tabulator tabulator if not FinancingContractHeader.Get(FinancedObject."Financing Contract No.") then; newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator PurchaseHeader.CalcFields("BLG Customer Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Vendor.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Vendor."BLG Object Order Email Address", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PurchaseHeader."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PurchaseHeader."Document Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Make Name" + ' ' + FinancedObject."Model Name" + ' ' + FinancedObject."Model Type Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PurchaseHeader."BLG Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."BLG Driver Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(PurchaseHeader."API Exp.Handover Date to Cust.", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until PurchaseHeader.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
 newline
tabulator tabulator BlFileManager.CustomizeCells(TmpExcelBuffer, 9); newline
 newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, Vendor.Name, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
