tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator if not (FCH.Status in ["API Financing Contract Status"::Active, "API Financing Contract Status"::Signed]) then newline
tabulator tabulator tabulator tabulator tabulator FieldError(FCH.Status, 'Must be "Active" or "Signed"'); newline
tabulator tabulator tabulator tabulator TestField(FCH."New Customer No."); newline
tabulator tabulator tabulator tabulator TestField(FCH."Contract Transfer Status", "API Contract Transfer Status"::"Request Received"); newline
tabulator tabulator tabulator tabulator TestField(FCH."Transfer Request Receipt Date"); newline
 newline
tabulator tabulator tabulator tabulator FCH.CalcFields("Licence Plate No."); newline
tabulator tabulator tabulator tabulator if NewCustomer.Get(FCH."New Customer No.") then begin newline
tabulator tabulator tabulator tabulator tabulator NCustRegNoVATRegNoCZ assignment Utils.IdVatIdNo(NewCustomer, LanguageHandler.GetCompanyLanguageId()); newline
tabulator tabulator tabulator tabulator tabulator NCustRegNoVATRegNoEN assignment Utils.IdVatIdNo(NewCustomer, LanguageHandler.GetENlanguageId()); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator if Customer.Get(FCH."Customer No.") then begin newline
tabulator tabulator tabulator tabulator tabulator CustRegNoVATRegNoCZ assignment Utils.IdVatIdNo(NewCustomer, LanguageHandler.GetCompanyLanguageId()); newline
tabulator tabulator tabulator tabulator tabulator CustRegNoVATRegNoEN assignment Utils.IdVatIdNo(NewCustomer, LanguageHandler.GetENlanguageId()); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator GetFinancingContractHeaders(FCHColection, FCH); newline
tabulator tabulator tabulator tabulator if FCHColection.Count() <= 1 then begin newline
tabulator tabulator tabulator tabulator tabulator if FCH."Financing Type" = "Financing Type"::"Operative Leasing" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ConLabelEN assignment 'individual lease contract'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ConLabelCZ assignment 'individuální leasingové smlouvy'; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ConLabelEN assignment 'individual contract'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ConLabelCZ assignment 'individuální smlouvy'; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end newline
tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator if FCH."Financing Type" = "Financing Type"::"Operative Leasing" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ConLabelEN assignment 'individual lease contracts'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ConLabelCZ assignment 'individuálních leasingových smluv'; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ConLabelEN assignment 'individual contracts'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ConLabelCZ assignment 'individuálních smluv'; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
tabulator tabulator dataitem(CI; "Company Information") newline
tabulator tabulator { newline
tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator column(CI_Addr; Address) { } newline
tabulator tabulator tabulator column(CI_Addr2; "Address 2") { } newline
tabulator tabulator tabulator column(CI_PSC; "Post Code") { } newline
tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator column(CI_Phone; LanguageHandler.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator column(CI_Email; "E-Mail") { } newline
tabulator tabulator tabulator column(CI_HomePage; "Home Page") { } newline
tabulator tabulator tabulator column(CI_RegNo; "Registration No.") { } newline
tabulator tabulator tabulator column(CI_VATRegNo; "VAT Registration No.") { } newline
tabulator tabulator tabulator column(CI_BankName; "Bank Name") { } newline
tabulator tabulator tabulator column(CI_BankAccNo; "Bank Account No.") { } newline
tabulator tabulator tabulator column(CI_IBAN; IBAN) { } newline
 newline
tabulator tabulator tabulator dataitem(CI_CounReg; "Country/Region") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = Code = field("Country/Region Code"); newline
tabulator tabulator tabulator tabulator column(CI_CounReg_Name; Name) { } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator } newline
tabulator var newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator NewCustomer: Record Customer; newline
tabulator tabulator Customer: Record Customer; newline
tabulator tabulator ConLabelEN: Text; newline
tabulator tabulator ConLabelCZ: Text; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator NCustRegNoVATRegNoCZ: Text; newline
tabulator tabulator CustRegNoVATRegNoCZ: Text; newline
tabulator tabulator NCustRegNoVATRegNoEN: Text; newline
tabulator tabulator CustRegNoVATRegNoEN: Text; newline
 newline
tabulator local procedure GetFinancingContractHeaders(var FCHColection: Record "API Financing Contract Header" temporary; FCHOrigin: Record "API Financing Contract Header") newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator begin newline
tabulator tabulator FCH.SetRange("New Customer No.", FCHOrigin."New Customer No."); newline
tabulator tabulator FCH.SetRange("Customer No.", FCHOrigin."Customer No."); newline
tabulator tabulator FCH.SetFilter(Status, '%1|%2', "API Financing Contract Status"::Active, "API Financing Contract Status"::Signed); newline
tabulator tabulator if FCHOrigin."Financing Type" = FCHOrigin."Financing Type"::"Operative Leasing" then newline
tabulator tabulator tabulator FCH.SetRange("Financing Type", FCHOrigin."Financing Type") newline
tabulator tabulator else newline
tabulator tabulator tabulator FCH.SetFilter("Financing Type", 'notequal%1', FCHOrigin."Financing Type"::"Operative Leasing"); newline
tabulator tabulator FCH.SetRange("Contract Transfer Status", "API Contract Transfer Status"::"Request Received"); newline
tabulator tabulator FCH.SetFilter("Transfer Request Receipt Date", '%1..%2', CalcDate('<-CM>', Today()), CalcDate('<+CM>', Today())); newline
tabulator tabulator if FCH.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FCHColection assignment FCH; newline
tabulator tabulator tabulator tabulator FCHColection.Insert(); newline
tabulator tabulator tabulator until FCH.Next = 0; newline
tabulator end; newline
