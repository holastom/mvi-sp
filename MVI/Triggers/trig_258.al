tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
tabulator tabulator FuelType: Record "API Fuel Type"; newline
tabulator tabulator Gearbox: Record "API Gearbox"; newline
tabulator tabulator Customer: Record Customer; newline
 newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
 newline
tabulator tabulator FileName: Label 'Call_Center_%1'; newline
tabulator tabulator SheetName: Label 'Call Center'; newline
tabulator begin newline
tabulator tabulator FinancingContractHeader.SetFilter("Financing Product Type Code", 'notequal%1', 'FUEL'); newline
tabulator tabulator FinancingContractHeader.SetFilter("Financed Object No.", 'notequal%1', ''); newline
tabulator tabulator FinancingContractHeader.SetFilter("Customer No.", 'notequal%1', ''); newline
tabulator tabulator FinancingContractHeader.SetFilter(Status, 'Active|Signed'); newline
tabulator tabulator FinancingContractHeader.SetFilter("Detailed Contract Status", 'ACTIVE|OB_REGI|OB_HANSET|OB_REHAND'); newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage(); newline
 newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator if FinancingContractHeader.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator if FinancedObject.Get(FinancingContractHeader."Financed Object No.") then newline
tabulator tabulator tabulator tabulator tabulator if Customer.Get(FinancingContractHeader."Customer No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(Gearbox); newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(FuelType); newline
tabulator tabulator tabulator tabulator tabulator tabulator if FinancedObject.Gearbox notequal '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Gearbox.Get(FinancedObject.Gearbox); newline
tabulator tabulator tabulator tabulator tabulator tabulator if FinancedObject."Fuel Type Code" notequal '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator FuelType.Get(FinancedObject."Fuel Type Code"); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Financing Type", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LanguageHandler.FormatDateNoSpace(FinancingContractHeader."Expected Termination Date"), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Contract Status", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Homologation Class Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Make Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Model Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Type of Body", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FuelType.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Gearbox.Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Engine CCM", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Engine Power (kW)", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Emisson Standard Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject.VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Production Year", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Stand. Warranty No. of Months", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until FinancingContractHeader.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
 newline
tabulator tabulator BLFileManager.CustomizeCells(TmpExcelBuffer, 17); newline
 newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
