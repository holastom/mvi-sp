tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator Customer: Record Customer; newline
tabulator tabulator Company: Record Contact; newline
tabulator tabulator Person: Record Contact; newline
tabulator tabulator CBR: Record "Contact Business Relation"; newline
tabulator tabulator JR: Record "Contact Job Responsibility"; newline
 newline
tabulator tabulator FileName: Label 'Contact_per_Client_%1'; newline
tabulator tabulator SheetName: Label 'Contact per Client'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator if CustomerNo notequal '' then newline
tabulator tabulator tabulator Customer.SetRange("No.", CustomerNo); newline
tabulator tabulator Customer.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator CBR.SetRange("No.", Customer."No."); newline
tabulator tabulator tabulator CBR.SetRange("Business Relation Code", 'CLIENT'); newline
tabulator tabulator tabulator CBR.FindFirst(); newline
tabulator tabulator tabulator Company.Get(CBR."Contact No."); newline
tabulator tabulator tabulator Person.SetRange("Company No.", CBR."Contact No."); newline
tabulator tabulator tabulator Person.SetRange(Type, Person.Type::Person); newline
tabulator tabulator tabulator Person.SetRange("BLG Subject Status", "BLG Subject Status"::Active); newline
tabulator tabulator tabulator if Person.FindSet() then newline
tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator JR.SetFilter("Contact No.", Person."No."); newline
tabulator tabulator tabulator tabulator tabulator if JRFilter notequal '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator JR.SetFilter("Job Responsibility Code", JRFilter); newline
tabulator tabulator tabulator tabulator tabulator if JR.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if not ((not Driver) and (JR."Job Responsibility Code" = 'DRIVER')) or (not u.isJobRespValid(JR, Today())) then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Company.CalcFields("BLG Customer Support Name", "BLG Customer Owner Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator JR.CalcFields("Job Responsibility Description"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer."Registration No. CZL", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.Address, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.City + ' ' + Customer."Post Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer."Language Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(JR."Job Responsibility Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(JR."Job Responsibility Description", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Person.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Person."E-Mail", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(lh.FormatPhoneNumber(Person."Mobile Phone No."), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Company."BLG Customer Support Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Company."BLG Customer Owner Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator until JR.Next() = 0; newline
tabulator tabulator tabulator tabulator until Person.Next() = 0; newline
tabulator tabulator until Customer.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator CreateCellWidth(); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
