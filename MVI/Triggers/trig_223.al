tabulator tabulator tabulator trigger OnAfterGetRecord() //SIH newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tTypeErr: Label 'Unable to print invoice of type "%1"', Locked = true; newline
tabulator tabulator tabulator tabulator sil: Record "Sales Invoice Line"; newline
tabulator tabulator tabulator tabulator silbuf2: Record "Sales Invoice Line" temporary; newline
tabulator tabulator tabulator tabulator mpl: Record "API Maint. Permission Line"; newline
tabulator tabulator tabulator tabulator mpl2: Record "API Maint. Permission Line"; newline
tabulator tabulator tabulator tabulator contact: Record Contact; newline
tabulator tabulator tabulator tabulator cust: Record Customer; newline
tabulator tabulator tabulator tabulator usersetup: Record "User Setup"; newline
tabulator tabulator tabulator tabulator tContactPerson: Label 'Kontaktní osoba', Comment = 'cs-CZ=Kontaktní osoba'; newline
tabulator tabulator tabulator tabulator tPhone: Label 'Telefon', Comment = 'cs-CZ=Telefon'; newline
tabulator tabulator tabulator tabulator tEMail: Label 'E-mail', Comment = 'cs-CZ=E-mail'; newline
tabulator tabulator tabulator tabulator cnt: Integer; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator if cust.Get("Bill-to Customer No.") then newline
tabulator tabulator tabulator tabulator tabulator case "API Invoice Print Type" of newline
tabulator tabulator tabulator tabulator tabulator tabulator "API Invoice Print Type"::Payment: newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvExtOrdNo assignment cust."BLG Invoice External Order No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator GetBusLocAdress(cust, "API Customer Bus. Place No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator "API Invoice Print Type"::"BLG Consolidated Invoice": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvExtOrdNo assignment cust."BLG Cons. Inv. Ext. Order No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator GetBusLocAdress(cust, "API Customer Bus. Place No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator "API Invoice Print Type"::"Fuel Card": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvExtOrdNo assignment cust."BLG Fuel External Order No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator GetBusLocAdress(cust, "API Customer Bus. Place No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(ContAltAdr) newline
tabulator tabulator tabulator tabulator tabulator end newline
tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator Clear(ContAltAdr); newline
tabulator tabulator tabulator tabulator tabulator Clear(InvExtOrdNo); newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator Currency assignment LanguageHandler.CurrSymbol("Currency Code"); newline
 newline
tabulator tabulator tabulator tabulator Utils.ComposeFullAdress(BillToFullAddr, "Bill-to Address", "Bill-to Address 2", "Bill-to Post Code", "Bill-to City", "Bill-to Country/Region Code"); newline
tabulator tabulator tabulator tabulator Utils.ComposeFullAdress(ShipToFullAddr, "Ship-to Address", "Ship-to Address 2", "Ship-to Post Code", "Ship-to City", "Ship-to Country/Region Code"); newline
 newline
tabulator tabulator tabulator tabulator sil.SetRange("Document No.", "No."); //tvorba filtru vyskytujících se smluv newline
tabulator tabulator tabulator tabulator ContractFilter assignment '|'; newline
tabulator tabulator tabulator tabulator Utils.BufferInit(SILbuf, 'SILbuf'); newline
tabulator tabulator tabulator tabulator if sil.FindSet then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator SILbuf assignment sil; newline
tabulator tabulator tabulator tabulator tabulator tabulator //workaround pro odstranění nulových zaokrouhlovacích řádek, než se dohodneme jak to udělat čistě newline
tabulator tabulator tabulator tabulator tabulator tabulator if not ((sil.Type notequal sil.type::" ") and (sil.Description = 'Rounding') and (sil."VAT %" = 0) and (sil."VAT Base Amount" = 0)) then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator SILbuf.Insert; newline
tabulator tabulator tabulator tabulator tabulator tabulator if sil.Type notequal sil.Type::" " then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if strpos(ContractFilter, '|' + sil."Shortcut Dimension 2 Code" + '|') = 0 then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ContractFilter += sil."Shortcut Dimension 2 Code" + '|'; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator until sil.Next = 0; newline
tabulator tabulator tabulator tabulator ContractFilter assignment DelChr(ContractFilter, 'notequal', '|'); newline
 newline
tabulator tabulator tabulator tabulator case "API Invoice Print Type" of newline
tabulator tabulator tabulator tabulator tabulator "API Invoice Print Type"::Payment: newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceType assignment InvoiceType::P; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator FCH.SetFilter("No.", ContractFilter); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator cnt assignment FCH.Count; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator FCH.SetRange("Financing Product Type Code", 'OL'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if FCH.Count = cnt then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceLegalText assignment InvoiceLegalTextPO newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FCH.SetRange("Financing Product Type Code", 'CFM'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if FCH.Count = cnt then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceLegalText assignment InvoiceLegalTextPC newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceLegalText assignment InvoiceLegalTextPX; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceLegalText assignment StrSubstNo(InvoiceLegalText, Format("VAT Date CZL", 0, '<Month,2>/<Year4>')); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator "API Invoice Print Type"::"Fuel Card": newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceType assignment InvoiceType::F; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceLegalText assignment StrSubstNo(InvoiceLegalTextF, LanguageHandler.FormatVar("VAT Date CZL")); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator "API Invoice Print Type"::"BLG Consolidated Invoice", "API Invoice Print Type"::Service: newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceType assignment InvoiceType::C; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceLegalText assignment InvoiceLegalTextOC; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator // U řádků Service se musím zbavit "korekčních" řádků (tj. těch, které mají v řádku povolenky prázdný Category a Subcategory kód) a to jejich přičtením k některému "hlavnímu" řádku stejné povolenky. newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator // Musím to udělat v TEMP tabulce, protože až z výsledku můžu nechat vygenerovat VATCounter newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator SILbuf.SetFilter(Type, 'notequal%1', SILbuf.Type::" "); //jen hodnotové řádky newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator SILbuf.SetFilter("API Maintenance Approval No.", 'notequal%1', ''); //započítání zúčastní se jen Service řádky newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.Copy(SILbuf, true); //tady budu hledat hlavní řádek newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if SILbuf.FindSet then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SILbuf.ModifyAll(Nonstock, false); //tady si označím korekční řádky newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator mpl.Get(SILbuf."API Maintenance Approval No.", SILbuf."BLG Maint. Perm. Line No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if mpl."Category Code" + MPL."Subcategory Code" = '' then //jedná se o korekční řádek newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.SetRange("API Maintenance Approval No.", SILbuf."API Maintenance Approval No."); //musí mít shodné Approval No. newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.SetRange("VAT Identifier", SILbuf."VAT Identifier"); //a shodné zdanění newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.SetRange("VAT Calculation Type", SILbuf."VAT Calculation Type"); // dtto. newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.SetRange("Tax Group Code", SILbuf."Tax Group Code"); //dtto. newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.SetFilter("Line No.", 'notequal%1', SILbuf."Line No."); //vynechám aktuální řádek newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.SetFilter("BLG Maint. Perm. Line No.", 'notequal%1', SILbuf."BLG Maint. Perm. Line No."); //...a řádky odkazující na stejný ř. povolenky newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator mpl2.SetRange("Maintenance Permission No.", SILbuf."API Maintenance Approval No."); //připravím si filtr pro test newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator mpl2.SetRange("Category Code", ''); //je korekční newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator mpl2.SetRange("Subcategory Code", ''); //dtto. newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if silbuf2.FindSet then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator mpl2.SetRange("Line No.", silbuf2."BLG Maint. Perm. Line No."); //příslušný MPL řádek newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if mpl2.IsEmpty then begin //pokud není mpl2 korekční, beru řádek silbuf2 jako hlavní newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2."Line Amount" += SILbuf."Line Amount"; //nasčítám částky (taky pro CalcVATAmountLines) newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.Amount += SILbuf.Amount; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2."Amount Including VAT" += SILbuf."Amount Including VAT"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2."VAT Base Amount" += SILbuf."VAT Base Amount"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2."Inv. Discount Amount" += SILbuf."Inv. Discount Amount"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2."Quantity (Base)" += SILbuf."Quantity (Base)"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2."VAT Difference" += SILbuf."VAT Difference"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator silbuf2.Modify; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SILBuf.Nonstock assignment true; //označím kor. řádek pro odstranění newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SILbuf.Modify; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator until SILBuf.Nonstock or (silbuf2.Next = 0); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator until SILbuf.Next = 0; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SILbuf.SetRange(Nonstock, true); //korekční řádky newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SILbuf.DeleteAll; //odstraním newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator "API Invoice Print Type"::" ": newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceType assignment InvoiceType::O; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceLegalText assignment InvoiceLegalTextOC; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator "API Invoice Print Type"::"BLG Fin. Object Sale": newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceType assignment InvoiceType::B; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator InvoiceLegalText assignment InvoiceLegalTextB; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator error(tTypeErr, "API Invoice Print Type"); newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator SILbuf.Reset; newline
tabulator tabulator tabulator tabulator Utils.BufferInit(VATCounter, 'VATCounter'); newline
tabulator tabulator tabulator tabulator SILbuf.CalcVATAmountLines(SIH, VATCounter); newline
tabulator tabulator tabulator tabulator MergeVATCounterPlusMinusLines(VATCounter); newline
tabulator tabulator tabulator tabulator VATCounter.UpdateVATEntryLCYAmountsCZL(SIH); newline
 newline
tabulator tabulator tabulator tabulator if InvoiceType in [InvoiceType::B, InvoiceType::O] then begin newline
tabulator tabulator tabulator tabulator tabulator if usersetup.Get(UserId) then; newline
tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator if contact.Get("Bill-to Contact No.") then; newline
tabulator tabulator tabulator tabulator tabulator if contact."BLG Customer Support Code" notequal '' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator usersetup.SetRange("Salespers./Purch. Code", contact."BLG Customer Support Code"); newline
tabulator tabulator tabulator tabulator tabulator tabulator if usersetup.FindFirst then; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator if usersetup."API Employee No." notequal '' then Employee.Get(usersetup."API Employee No."); newline
tabulator tabulator tabulator tabulator EmployeeContact assignment StrSubstNo('%1: <b>%2</b>&nbsp;&nbsp;', tContactPerson, Employee."Full Name"); //Kontaktní info do hlavičky v html kvůli tučným labelům newline
tabulator tabulator tabulator tabulator if InvoiceType notequal InvoiceType::B then EmployeeContact += StrSubstNo('%1: <b>%2</b>&nbsp;&nbsp;', tPhone, LanguageHandler.FormatPhoneNumber(Employee."API Company Mobile Phone No.")); //u Bazaru se netiskne telefon newline
tabulator tabulator tabulator tabulator EmployeeContact += StrSubstNo('%1: <b>%2</b>', tEMail, Employee."Company E-Mail"); newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem(CI; "Company Information") newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = sorting("Primary Key"); newline
 newline
tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator column(CI_FullAddr; CI_FullAddr) { } newline
tabulator tabulator tabulator column(CI_Addr; Address) { } newline
tabulator tabulator tabulator column(CI_Addr2; "Address 2") { } newline
tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator column(CI_PSC; "Post Code") { } newline
tabulator tabulator tabulator column(CI_Phone; LanguageHandler.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator column(CI_EMail; "E-mail") { } newline
tabulator tabulator tabulator column(CI_HomePage; "Home Page") { } newline
tabulator tabulator tabulator column(CI_VATRegNo; "VAT Registration No.") { } newline
tabulator tabulator tabulator column(CI_RegNo; "Registration No.") { } newline
tabulator tabulator tabulator column(CI_Country; Utils.CountryName("Country/Region Code")) { } newline
tabulator tabulator tabulator column(CI_Logo; Picture) { } newline
 newline
