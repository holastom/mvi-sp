tabulator tabulator tabulator tabulator trigger OnPostDataItem() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator CSEmpty assignment CSSkipped = "API Contract Service".Count(); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem("API Contract Service Billable"; "API Contract Service") //CSB newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Contract No." = field("No."); newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("No.", "Service Contract Type") where(Charge = const(true), "Service Status" = filter(Active | Preparation), "Service Type Code" = const('REPLACE_C'));// 24.10.2022 - pridani podminky pro ReplSetl. newline
tabulator tabulator tabulator tabulator column(CSB___; '') { } newline
tabulator tabulator tabulator tabulator column(CSB_Service_Description; CS_Service_Description) { } //ID_109 newline
tabulator tabulator tabulator tabulator column(CSB_Service_Description_Detail; CS_B_Service_Description_Detail) { } //ID_109a? newline
tabulator tabulator tabulator tabulator column(CSB_Calculation_Amount_PerPayment; LanguageHandler.FormatVar("Calculation Amount Per Payment")) { } //ID_110 newline
tabulator tabulator tabulator tabulator column(CSB_BLG_Service_Profit_Share_BL; LanguageHandler.FormatVar("BLG Service Profit Share BL", 0)) { } //ID_57? newline
tabulator tabulator tabulator tabulator column(CSB_BLG_Service_Profit_Share_Cust_; LanguageHandler.FormatVar("BLG Service Profit Share Cust.", 0)) { } //ID_58? newline
tabulator tabulator tabulator tabulator column(CSB_BLG_Service_Loss_Share_BL; LanguageHandler.FormatVar("BLG Service Loss Share BL", 0)) { } //ID_59? newline
tabulator tabulator tabulator tabulator column(CSB_BLG_Service_Loss_Share_Cust_; LanguageHandler.FormatVar("BLG Service Loss Share Cust.", 0)) { } //ID_60? newline
tabulator tabulator tabulator tabulator column(CSB_Charge; Charge) { } newline
 newline
