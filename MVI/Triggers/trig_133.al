tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator RegNoVATRegNo assignment "Registration No. CZL"; newline
tabulator tabulator tabulator tabulator tabulator if "VAT Registration No." notequal '' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator RegNoVATRegNoCZLbl assignment '/ DIČ'; newline
tabulator tabulator tabulator tabulator tabulator tabulator RegNoVATRegNoENLbl assignment '/ VAT Id No.'; newline
tabulator tabulator tabulator tabulator tabulator tabulator RegNoVATRegNoDELbl assignment '/ USt-IdNr.'; newline
tabulator tabulator tabulator tabulator tabulator tabulator RegNoVATRegNo += ' / ' + "VAT Registration No."; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem(MA; "API Master Agreement") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Master Agreement No."); newline
tabulator tabulator tabulator tabulator column(MA_No; "No.") { } newline
tabulator tabulator tabulator tabulator column(MA_ContactNo; "Contact No.") { } newline
tabulator tabulator tabulator tabulator column(SysDate; LanguageHandler.FormatDateSpace(Today())) { } newline
tabulator tabulator tabulator tabulator column(QA1_G; CheckBox(answ[1, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA1_K; CheckBox(answ[1, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA1_M; CheckBox(answ[1, 3])) { } newline
tabulator tabulator tabulator tabulator column(QA1_D; CheckBox(answ[1, 4])) { } newline
tabulator tabulator tabulator tabulator column(QA5_1_Y; CheckBox(answ[2, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_1_N; CheckBox(answ[2, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA5_2_Y; CheckBox(answ[3, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_2_N; CheckBox(answ[3, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA5_3_Y; CheckBox(answ[4, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_3_N; CheckBox(answ[4, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA5_4_Y; CheckBox(answ[5, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_4_N; CheckBox(answ[5, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA5_5_Y; CheckBox(answ[6, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_5_N; CheckBox(answ[6, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA5_6_Y; CheckBox(answ[7, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_6_N; CheckBox(answ[7, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA5_7_Y; CheckBox(answ[8, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_7_N; CheckBox(answ[8, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA5_8_Y; CheckBox(answ[9, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_8_N; CheckBox(answ[9, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA5_9_Y; CheckBox(answ[10, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA5_9_N; CheckBox(answ[10, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA7_E; CheckBox(answ[11, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA7_L; CheckBox(answ[11, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA7_G_K; CheckBox(answ[12, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA7_G_P; CheckBox(answ[12, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA7_K_K; CheckBox(answ[13, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA7_K_P; CheckBox(answ[13, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA7_M_K; CheckBox(answ[14, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA7_M_P; CheckBox(answ[14, 2])) { } newline
tabulator tabulator tabulator tabulator column(QA7_D_K; CheckBox(answ[15, 1])) { } newline
tabulator tabulator tabulator tabulator column(QA7_D_P; CheckBox(answ[15, 2])) { } newline
 newline
tabulator tabulator tabulator tabulator dataitem("Company Information"; "Company Information") newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator column(CI_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_Address; Address) { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_Address2; "Address 2") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_City; City) { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_PostCode; "Post Code") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_PhoneNo; LanguageHandler.FormatPhoneNumber("Phone No.")) { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_EMail; "E-mail") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_HomePage; "Home Page") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_VATRegNo; "VAT Registration No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_RegNo; "Registration No.") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_Country; "Country/Region Code") { } newline
tabulator tabulator tabulator tabulator tabulator column(CI_Logo; Picture) { } newline
 newline
tabulator tabulator tabulator tabulator tabulator dataitem(CI_CounReg; "Country/Region") newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = Code = field("Country/Region Code"); newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CI_CounReg_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator tabulator tabulator dataitem(CI_Contact; Contact) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemLink = "Registration No. CZL" = field("Registration No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CI_JurisdictionPlace; "API Jurisdiction Place") { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CI_FileNo; Utils.nbsp("API File No.")) { } newline
tabulator tabulator tabulator tabulator tabulator } newline
 newline
