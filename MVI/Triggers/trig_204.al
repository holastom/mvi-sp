tabulator tabulator trigger OnOpenPage() newline
tabulator tabulator begin newline
tabulator tabulator tabulator CIlng assignment LanguageHandler.GetCompanyLanguageId; newline
tabulator tabulator end; newline
tabulator } newline
 newline
tabulator labels newline
tabulator { newline
tabulator tabulator CultureCode = 'CultureCode', Comment = 'cs-CZ=cs-CZ;en-US=en-US'; newline
tabulator tabulator ReportTitle = 'Opravný daňový doklad', Comment = 'cs-CZ=Opravný daňový doklad'; newline
tabulator tabulator CrMemoNoLbl = 'Číslo dokladu', Comment = 'cs-CZ=Číslo dokladu'; newline
tabulator tabulator PostingPeriodLbl = 'Účtované období', Comment = 'cs-CZ=Účtované období'; newline
tabulator tabulator RegNoLbl = 'IČO', Comment = 'cs-CZ=IČO'; newline
tabulator tabulator CustomerLbl = 'ZÁKAZNÍK', Comment = 'cs-CZ=ZÁKAZNÍK'; newline
tabulator tabulator CustomerNoLbl = 'Číslo zákazníka', Comment = 'cs-CZ=Číslo zákazníka'; newline
tabulator tabulator VATRegNoLbl = 'DIČ', Comment = 'cs-CZ=DIČ'; newline
tabulator tabulator CustPostAddressLbl = 'Poštovní adresa zákazníka', Comment = 'cs-CZ=Poštovní adresa zákazníka'; newline
tabulator tabulator CustBussPlacePostAddressLbl = 'Obchodní místo', Comment = 'cs-CZ=Obchodní místo'; newline
tabulator tabulator VendorLbl = 'DODAVATEL', Comment = 'cs-CZ=DODAVATEL'; newline
tabulator tabulator IssueDateLbl = 'Datum vystavení', Comment = 'cs-CZ=Datum vystavení'; newline
tabulator tabulator DueDateLbl = 'Datum splatnosti', Comment = 'cs-CZ=Datum splatnosti'; newline
tabulator tabulator VATDateLbl = 'Datum uskutečnění zdanitelného plnění', Comment = 'cs-CZ=Datum uskutečnění zdanitelného plnění'; newline
tabulator tabulator VarSymLbl = 'Variabilní symbol', Comment = 'cs-CZ=Variabilní symbol'; newline
tabulator tabulator SpecSymLbl = 'Specifický symbol', Comment = 'cs-CZ=Specifický symbol'; newline
tabulator tabulator ConstSymLbl = 'Konstantní symbol', Comment = 'cs-CZ=Konstantní symbol'; newline
tabulator tabulator BankInfoLbl = 'Bankovní spojení', Comment = 'cs-CZ=Bankovní spojení'; newline
tabulator tabulator BankNameLbl = 'Banka', Comment = 'cs-CZ=Banka'; newline
tabulator tabulator BankAcctNoLbl = 'Číslo účtu', Comment = 'cs-CZ=Číslo účtu'; newline
tabulator tabulator DeliverablesLbl = 'PŘEDMĚT PLNĚNÍ', Comment = 'cs-CZ=PŘEDMĚT PLNĚNÍ'; newline
tabulator tabulator VATSummaryLbl = 'DAŇOVÁ REKAPITULACE', Comment = 'cs-CZ=DAŇOVÁ REKAPITULACE'; newline
tabulator tabulator DescriptionLbl = 'Popis', Comment = 'cs-CZ=Popis'; newline
tabulator tabulator VATIdLbl = 'Id. sazby', Comment = 'cs-CZ=Id. sazby'; newline
tabulator tabulator VATPctLbl = 'Sazba DPH', Comment = 'cs-CZ=Sazba DPH'; newline
tabulator tabulator VATBaseLbl = 'Základ daně', Comment = 'cs-CZ=Základ daně'; newline
tabulator tabulator VATAmountLbl = 'Celkem DPH', Comment = 'cs-CZ=Celkem DPH'; newline
tabulator tabulator AmtInclVATlbl = 'Celkem s DPH', Comment = 'cs-CZ=Celkem s DPH'; newline
tabulator tabulator AmtToPayTotalLbl = 'Celkem k úhradě', Comment = 'cs-CZ=Celkem k úhradě'; newline
tabulator tabulator TotalPerContractLbl = 'Celkem za smlouvu', Comment = 'cs-CZ=Celkem za smlouvu'; newline
tabulator tabulator TotalPerMasterAgreementLbl = 'Celkem za rámcovou smlouvu č.', Comment = 'cs-CZ=Celkem za rámcovou smlouvu č.'; newline
tabulator tabulator TotalPerCardLbl = 'Celkem za kartu', Comment = 'cs-CZ=Celkem za kartu'; newline
tabulator tabulator TotalPerDriverLbl = 'Celkem za řidiče', Comment = 'cs-CZ=Celkem za řidiče'; newline
tabulator tabulator TotalPerCCxLbl = 'Středisko % celkem', Comment = 'cs-CZ=Středisko % celkem'; newline
tabulator tabulator SemiTotalLbl = 'Mezisoučet', Comment = 'cs-CZ=Mezisoučet'; newline
tabulator tabulator TotalLbl = 'Celkem', Comment = 'cs-CZ=Celkem'; newline
 newline
tabulator tabulator LeaseContractLbl = 'Leasingová smlouva', Comment = 'cs-CZ=Leasingová smlouva'; newline
tabulator tabulator LeaseContractNoLbl = 'Číslo leasingové smlouvy', Comment = 'cs-CZ=Číslo leasingové smlouvy'; newline
tabulator tabulator RentContractNoLbl = 'Číslo smlouvy o zápůjčce', Comment = 'cs-CZ=Číslo smlouvy o zápůjčce'; newline
tabulator tabulator LicPlateLbl = 'Reg. značka', Comment = 'cs-CZ=Reg. značka'; newline
tabulator tabulator LicensePlateLbl = 'Registrační značka', Comment = 'cs-CZ=Registrační značka'; newline
tabulator tabulator VehBookValueLbl = 'Účetní hodnota vozidla', Comment = 'cs-CZ=Účetní hodnota vozidla'; newline
tabulator tabulator VehSellPriceLbl = 'Prodejní cena vozidla', Comment = 'cs-CZ=Prodejní cena vozidla'; newline
tabulator tabulator SettlementLbl = 'Rozdíl', Comment = 'cs-CZ=Rozdíl'; newline
tabulator tabulator MaintenanceInvoicedLbl = 'Opravy, údržba, pneumatiky (vyfakturováno)'; //cs-CZ=Opravy, údržba, pneumatiky (vyfakturováno) newline
tabulator tabulator MaintenanceCostsLbl = 'Opravy, údržba, pneumatiky (vynaložené náklady)'; //cs-CZ=Opravy, údržba, pneumatiky (vynaložené náklady) newline
tabulator tabulator DriverLbl = 'Řidič', Comment = 'cs-CZ=Řidič'; newline
tabulator tabulator NoOfOrderLbl = 'Objednávka č.', Comment = 'cs-CZ=Objednávka č.'; newline
tabulator tabulator AmtExclVATlbl = 'Celkem bez DPH', Comment = 'cs-CZ=Celkem bez DPH'; newline
tabulator tabulator exclVATlbl = 'bez DPH', Comment = 'cs-CZ=bez DPH'; newline
tabulator tabulator inclVATlbl = 's DPH', Comment = 'cs-CZ=s DPH'; newline
tabulator tabulator QuantityLbl = 'Množství', Comment = 'cs-CZ=Množství'; newline
tabulator tabulator VINlbl = 'Identifikační číslo vozidla', Comment = 'cs-CZ=Identifikační číslo vozidla'; newline
tabulator tabulator FinSettlmtOpenItmsLbl = 'FINÁLNÍ ZÚČTOVÁNÍ OTEVŘENÝCH POLOŽEK', Comment = 'cs-CZ=FINÁLNÍ ZÚČTOVÁNÍ OTEVŘENÝCH POLOŽEK'; newline
tabulator tabulator RentsLbl = 'ZÁPŮJČKY', Comment = 'cs-CZ=ZÁPŮJČKY'; newline
tabulator tabulator ConsLbl = 'POLOŽKY DLE INDIVIDUÁLNÍCH SMLUV', Comment = 'cs-CZ=POLOŽKY DLE INDIVIDUÁLNÍCH SMLUV'; newline
tabulator tabulator SettlClientLbl = 'Přeneseno na klienta'; //cs-CZ=Přeneseno na klienta newline
tabulator tabulator TotalSettlOlOpenLbl = 'Celkem na základě typu zúčtování otevřených položek s klientem', Comment = 'cs-CZ=Celkem na základě typu zúčtování otevřených položek s klientem'; newline
tabulator tabulator MasterAgreementLbl = 'RÁMCOVÁ SMLOUVA č.', Comment = 'cs-CZ=RÁMCOVÁ SMLOUVA č.'; newline
tabulator tabulator InvoicePeriodTextLbl = 'Dobropis za období', Comment = 'cs-CZ=Dobropis za období'; newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator #region GlobalVars newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator ReqPgLanguage: Enum LanguageEnum; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator isLocalLanguage: Boolean; newline
 newline
tabulator tabulator InvoiceType: option P,F,C,O,B; //Payment, Fuel, Consolidated, Oneshot, Bazar (BLG Sale) newline
tabulator tabulator Employee: Record Employee; newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator MPH: Record "API Maint. Permission Header"; newline
tabulator tabulator MPL: Record "API Maint. Permission Line"; newline
tabulator tabulator ROH: Record "API Rent Order Header"; newline
tabulator tabulator ROL: Record "API Rent Order Line"; newline
tabulator tabulator FCH_MA: Record "API Master Agreement"; newline
tabulator tabulator ContAltAdr: Record "Contact Alt. Address"; newline
tabulator tabulator LineDescription: Text; newline
tabulator tabulator FinObjName: Text; newline
tabulator tabulator FS: Record "API Financial Settlement"; newline
tabulator tabulator OSS: Record "BLG Open Services Settlement"; newline
tabulator tabulator CMHCopy: Record "Sales Cr.Memo Header"; newline
tabulator tabulator ContractFilter: Text; newline
tabulator tabulator EmployeeContact: Text; newline
tabulator tabulator InvoiceLegalText: Text; newline
tabulator tabulator FinalSettlementProductText: Text; newline
tabulator tabulator BillToFullAddr: Text; newline
tabulator tabulator ShipToFullAddr: Text; newline
tabulator tabulator CI_FullAddr: Text; newline
tabulator tabulator InvoiceLegalTextPC: Label 'Dobropis za správu vozového parku vyplývající z uvedených Rámcových a Individuálních smluv (§)', Comment = 'cs-CZ=Dobropis za správu vozového parku vyplývající z uvedených Rámcových a Individuálních smluv (§)'; newline
tabulator tabulator InvoiceLegalTextPO: Label 'Dobropis nájmu vyplývající z uvedených Rámcových a Individuálních Leasingových smluv (§)', Comment = 'cs-CZ=Dobropis nájmu vyplývající z uvedených Rámcových a Individuálních Leasingových smluv (§)'; newline
tabulator tabulator InvoiceLegalTextPX: Label 'Dobropis měsíčního poplatku vyplývající z uvedených Rámcových a Individuálních smluv (§)', Comment = 'cs-CZ=Dobropis měsíčního poplatku vyplývající z uvedených Rámcových a Individuálních smluv (§)'; newline
tabulator tabulator InvoiceLegalTextOC: Label 'Dobropisujeme Vám dle rozpisu (§)', Comment = 'cs-CZ=Dobropisujeme Vám dle rozpisu (§)'; newline
tabulator tabulator InvoiceLegalTextF: Label 'Dobropis spotřeby pohon. hmot (§). Poslední zahrnutá použití karet jsou z %1', Comment = 'cs-CZ=Dobropis spotřeby pohon. hmot (§). Poslední zahrnutá použití karet jsou z %1'; newline
tabulator tabulator InvoiceLegalTextB: Label 'Dobropis ojetého vozu (§)', Comment = 'cs-CZ=Dobropis ojetého vozu (§)'; newline
tabulator tabulator VATParagraph: Label 'oprava dle § 42 zákona č. 235/2004 Sb. ve znění pozdějších předpisů', Comment = 'cs-CZ=oprava dle § 42 zákona č. 235/2004 Sb. ve znění pozdějších předpisů'; newline
tabulator tabulator InvoicesNoLblA: Label 'Opravný daňový doklad se vztahuje k fakturám č.', Comment = 'cs-CZ=Opravný daňový doklad se vztahuje k fakturám č.'; newline
tabulator tabulator InvoicesNoLblB: Label 'Opravný daňový doklad se vztahuje k faktuře č.', Comment = 'cs-CZ=Opravný daňový doklad se vztahuje k faktuře č.'; newline
tabulator tabulator FinSettlDesc: Label 'Finální zúčtování (opravy, údržba, pneumatiky)', Comment = 'cs-CZ=Finální zúčtování (opravy, údržba, pneumatiky)'; newline
 newline
tabulator tabulator Currency, ContractNo, LicPlateNo, DriverName, ContrDriverName, VATClauseText, VATClauseCode, S_Section, LineID, CardID : Text; newline
tabulator tabulator CO_Section, LineOrder, FCHcount : Integer; newline
tabulator tabulator SumDetLines: Boolean; newline
tabulator tabulator OL_REVISIO_Exists: Boolean; newline
tabulator tabulator FSCodeFlag: List of [Code[20]]; newline
tabulator tabulator AppliesToDocsNosList: List of [Text]; newline
tabulator tabulator AppliesToDocsNo: Text; newline
 newline
tabulator tabulator CML_FCL_DateFr, CML_FCL_DateTo, CML_FCH_DateFr, CML_FCH_DateTo : Text; newline
tabulator tabulator CML_CostCenterName: Text; newline
tabulator tabulator InvExtOrdNo: Text; newline
 newline
tabulator #endregion GlovalVars newline
 newline
tabulator #region Constants newline
tabulator #endregion Constants newline
 newline
