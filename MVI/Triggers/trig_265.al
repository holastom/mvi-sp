tabulator trigger OnPreReport() newline
tabulator begin newline
tabulator tabulator if FO.Count notequal 1 then CurrReport.Quit; newline
tabulator tabulator FO.FindFirst; newline
tabulator tabulator OSP.SetRange("Financed Object No.", FO."No."); newline
tabulator tabulator OSP.SetRange("BLG Sale", true); newline
tabulator tabulator if not OSP.FindFirst then CurrReport.Quit; newline
tabulator tabulator CurrReport.Language assignment LangHndl.ReportLanguage; newline
tabulator end; newline
