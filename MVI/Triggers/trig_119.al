tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator eUnsuppSettlmt: Label 'Settlement Type %1 is not supported'; newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator FCH.Get(FS."Financing Contract No."); newline
tabulator tabulator tabulator tabulator Cust.Get(FCH."Customer No."); newline
tabulator tabulator tabulator tabulator FOBJ.Get("Financed Object No."); newline
tabulator tabulator tabulator tabulator Settlmt1to3 assignment true; newline
tabulator tabulator tabulator tabulator case "Fin. Settlement Type" of newline
tabulator tabulator tabulator tabulator tabulator 'OL_EARLY_1': newline
tabulator tabulator tabulator tabulator tabulator tabulator ; newline
tabulator tabulator tabulator tabulator tabulator 'OL_EARLY_2': newline
tabulator tabulator tabulator tabulator tabulator tabulator "BLG Compens. from Contract T1" assignment "BLG Compens. from Contract T2"; newline
tabulator tabulator tabulator tabulator tabulator 'OL_EARLY_3': newline
tabulator tabulator tabulator tabulator tabulator tabulator "BLG Compens. from Contract T1" assignment "BLG Compens. from Contract T3"; newline
tabulator tabulator tabulator tabulator tabulator 'OL_EARLY_4': newline
tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Settlmt1to3 assignment false; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator "BLG Compens. from Contract T1" assignment "BLG Compens. from Contract T4"; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator Error(eUnsuppSettlmt, "Fin. Settlement Type"); newline
tabulator tabulator tabulator tabulator end newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
 newline
tabulator } newline
 newline
tabulator requestpage newline
tabulator { newline
tabulator tabulator layout newline
tabulator tabulator { newline
tabulator tabulator tabulator area(content) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator group(Options) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator Caption = 'Options'; newline
tabulator tabulator tabulator tabulator tabulator field(CsLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1029; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1029; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(HuLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1038; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1038; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(PlLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1045; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1045; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(RoLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1048; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1048; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator field(SkLang; RequestPageLanguage) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator Caption = 'Select language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ToolTip = 'If "Automatic" is selected, language is determinated by company language'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Visible = CIlng = 1051; newline
tabulator tabulator tabulator tabulator tabulator tabulator ValuesAllowed = 0, 1033, 1051; newline
tabulator tabulator tabulator tabulator tabulator tabulator ApplicationArea = Basic, Suite; newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
tabulator tabulator var newline
tabulator tabulator tabulator CIlng: Integer; newline
 newline
