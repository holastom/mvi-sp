tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator SalesCrMemoLine: Record "Sales Cr.Memo Line"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator Attachment: File; newline
tabulator begin newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::UTF8); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
 newline
tabulator tabulator //HEADER newline
tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 10)); //h0 newline
tabulator tabulator OutStream.WriteText('0025071025'); //h1 newline
tabulator tabulator OutStream.WriteText(SA.FillZeros(SCMH."No.", 10)); //h2 newline
tabulator tabulator OutStream.WriteText(SA.FillZeros(SCMH."No.", 10)); //h3 newline
tabulator tabulator OutStream.WriteText('  '); newline
tabulator tabulator if SCMH."API Invoice Print Type" notequal "API Invoice Print Type"::"Fuel Card" then newline
tabulator tabulator tabulator OutStream.WriteText(Format(SCMH."Document Date", 0, '<Day,2>-<Month,2>-<Year>')) newline
tabulator tabulator else newline
tabulator tabulator tabulator OutStream.WriteText(Format(CalcDate('<-CM-1D>', SCMH."Document Date"), 0, '<Day,2>-<Month,2>-<Year>')); newline
tabulator tabulator OutStream.WriteText(Format(SCMH."Document Date", 0, '<Day,2>-<Month,2>-<Year>')); newline
 newline
tabulator tabulator SalesCrMemoLine.SetRange("Document No.", SCMH."No."); newline
tabulator tabulator SalesCrMemoLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator if SalesCrMemoLine.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FinancingContractHeader.Get(SalesCrMemoLine."Shortcut Dimension 2 Code"); newline
tabulator tabulator tabulator tabulator FinancedObject.Get(FinancingContractHeader."Financed Object No."); newline
tabulator tabulator tabulator tabulator //BODY newline
tabulator tabulator tabulator tabulator OutStream.WriteText(); newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FinancingContractHeader."No.", 10)); //b1 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(FinancedObject."Licence Plate No.", 10)); //b2 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SCMH."Return Order No.", 10)); //b3 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 10)); //b4 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 4)); //b5 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(SalesCrMemoLine.Description, 10)); //b6 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces('', 9)); //b7 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces(Format(SalesCrMemoLine."VAT %"), 2)); //b8 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(SA.FormatDecimal(SalesCrMemoLine."Amount Including VAT", 2), 11)); //b9 newline
tabulator tabulator tabulator tabulator OutStream.WriteText(SA.MakeTextWithSpaces_2(SA.FormatDecimal(SalesCrMemoLine."Amount Including VAT" - SalesCrMemoLine."VAT Base Amount", 2), 11)); //b10 newline
tabulator tabulator tabulator until SalesCrMemoLine.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
