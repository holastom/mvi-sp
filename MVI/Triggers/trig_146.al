tabulator tabulator tabulator tabulator trigger OnPreDataItem() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator contractMileageRate: Record "BLG Contract Mileage Rate"; newline
tabulator tabulator tabulator tabulator tabulator found: Boolean; newline
tabulator tabulator tabulator tabulator tabulator prevLineMileageTo: Integer; newline
tabulator tabulator tabulator tabulator tabulator isNotFirst: Boolean; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator Utils.BufferInit("BLG Contract Mileage Rate Buffer", 'BLG Contract Mileage Rate Buffer'); newline
tabulator tabulator tabulator tabulator tabulator contractMileageRate.SetRange("Financing Contract No.", "API Financing Contract Header"."No."); newline
tabulator tabulator tabulator tabulator tabulator if contractMileageRate.FindSet then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Init; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator "Code" assignment CopyStr(contractMileageRate.Code, 1, StrLen(contractMileageRate.Code) - 1); //prefix spojuje nájezd s překročením newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Reset; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator found assignment Find; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if (contractMileageRate."Mileage From" >= 0) and (contractMileageRate."Mileage To (Incl.)" >= 0) then //znaménka odlišují nájezd (-) a překročení (+) newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "Mileage From" assignment contractMileageRate."Mileage From"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "Mileage To (Incl.)" assignment contractMileageRate."Mileage To (Incl.)"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "Rate excl.VAT" assignment contractMileageRate."Rate excl.VAT"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "Purchase Price Coefficient" assignment contractMileageRate."Rate excl.VAT"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if found then modify else insert; newline
tabulator tabulator tabulator tabulator tabulator tabulator until contractMileageRate.Next = 0; newline
 newline
tabulator tabulator tabulator tabulator tabulator prevLineMileageTo assignment -1; newline
tabulator tabulator tabulator tabulator tabulator SetCurrentKey("Mileage From"); newline
tabulator tabulator tabulator tabulator tabulator if FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if isNotFirst then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "BLG Contract Mileage Rate Buffer"."Mileage From" assignment prevLineMileageTo; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Modify(); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator prevLineMileageTo assignment "BLG Contract Mileage Rate Buffer"."Mileage To (Incl.)"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator isNotFirst assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator until Next() = 0; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem("API Contract Service"; "API Contract Service") //CS newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Contract No." = field("No."); newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("No.", "Service Contract Type") where("Service Status" = filter(Active | Preparation)); // 15.11.2022 - uprava podminky pro ReplSetl. newline
 newline
tabulator tabulator tabulator tabulator column(CS___; '') { } newline
tabulator tabulator tabulator tabulator column(CS_Service_Description; CS_Service_Description) { } //ID_55 newline
tabulator tabulator tabulator tabulator column(CS_Service_Description_Detail; CS_B_Service_Description_Detail) { } //ID_55a? newline
tabulator tabulator tabulator tabulator column(CS_Calculation_Amount_PerPayment; LanguageHandler.FormatVar("Calculation Amount Per Payment")) { } //ID_56 newline
tabulator tabulator tabulator tabulator column(CS_BLG_Service_Profit_Share_BL; EmptyIfClosed(LanguageHandler.FormatVar("BLG Service Profit Share BL", 0))) { } //ID_57 newline
tabulator tabulator tabulator tabulator column(CS_BLG_Service_Profit_Share_Cust_; EmptyIfClosed(LanguageHandler.FormatVar("BLG Service Profit Share Cust.", 0))) { } //ID_58 newline
tabulator tabulator tabulator tabulator column(CS_BLG_Service_Loss_Share_BL; EmptyIfClosed(LanguageHandler.FormatVar("BLG Service Loss Share BL", 0))) { } //ID_59 newline
tabulator tabulator tabulator tabulator column(CS_BLG_Service_Loss_Share_Cust_; EmptyIfClosed(LanguageHandler.FormatVar("BLG Service Loss Share Cust.", 0))) { } //ID_60 newline
tabulator tabulator tabulator tabulator column(CS_BLG_Service_Kind_Code_Order; ServiceKindCodeOrder) { } newline
tabulator tabulator tabulator tabulator column(CS_ShowSettlementNote; ShowSettlement and not Reinvoice) { } newline
tabulator tabulator tabulator tabulator column(CS_Reinvoice; Reinvoice) { } newline
 newline
 newline
