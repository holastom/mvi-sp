tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator FO: Record "API Financed Object"; newline
 newline
tabulator tabulator SA: Codeunit "Structured Attachements"; newline
tabulator tabulator Amount: Decimal; newline
 newline
tabulator tabulator OutStream: OutStream; newline
tabulator tabulator GroupedCode: Code[20]; newline
tabulator tabulator FCHCodes: List of [Code[20]]; newline
 newline
tabulator tabulator Lbl_DescriptionText: Label 'Fakturace nájmu za období %1-%2'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator SA.GroupByShortcutDimensionCrMemo(FCHCodes, SCMH); newline
tabulator tabulator foreach GroupedCode in FCHCodes do begin newline
tabulator tabulator tabulator FCH.Get(GroupedCode); newline
tabulator tabulator tabulator FO.Get(FCH."Financed Object No."); newline
tabulator tabulator tabulator FCH.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator FCL assignment SA.GetFinancingContractLineCM(GroupedCode, SCMH."No."); newline
tabulator tabulator tabulator Amount assignment SA.GetAmountCM(GroupedCode, SCMH."No."); newline
 newline
tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Sell-to Customer No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Posting Date", false, '', false, false, false, 'dd-MM-yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(StrSubstNo(Lbl_DescriptionText, Format(FCL."Date From", 0, '<Day,2>-<Month,2>-<Year4>'), Format(FCL."Date To", 0, '<Day,2>-<Month,2>-<Year4>')), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FCH."BLG Driver Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(SCMH."Shortcut Dimension 2 Code", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FO."BLG Name 2", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Amount, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(0, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Amount, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Amount, false, '# ### ##0.,00', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator end; newline
tabulator tabulator case CustInvSendingMethod."Attachment Format" of newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::Excel: newline
tabulator tabulator tabulator tabulator SA.CreateExcelBuffer(TmpExcelBuffer, AttachmentFilePath, 0, OutStream, 'Zentiva'); newline
tabulator tabulator tabulator CustInvSendingMethod."Attachment Format"::CSV: newline
tabulator tabulator tabulator tabulator SA.CreateCSVFile(TmpExcelBuffer, AttachmentFilePath, OutStream); newline
tabulator tabulator end; newline
tabulator end; newline
