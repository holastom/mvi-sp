tabulator tabulator trigger OnOpenPage() newline
tabulator tabulator begin newline
tabulator tabulator tabulator CIlng assignment LanguageHandler.GetCompanyLanguageId; newline
tabulator tabulator end; newline
tabulator } newline
 newline
tabulator labels newline
tabulator { newline
tabulator tabulator CultureCode = 'CultureCode', Comment = 'cs-CZ=cs-CZ;en-US=en-US'; newline
tabulator tabulator ExclVATlbl = 'ACQ01_Calculation_exclVAT', Comment = 'cs-CZ=bez DPH;en-US=excl. VAT'; //bez DPH newline
tabulator tabulator InclVATlbl = 'ACQ01_Calculation_inclVAT', Comment = 'cs-CZ=vč. DPH;en-US=incl. VAT'; //s DPH newline
tabulator tabulator SettlementLbl = 'Settlement', Comment = 'cs-CZ=Vyrovnání;en-US=Settlement'; newline
tabulator tabulator SettlementNoteLbl = 'SettlementNote', Comment = 'cs-CZ=* Vyrovnání je pouze z prodeje vozidla.;en-US=* The settlement is only from the sale of the vehicle.'; newline
tabulator tabulator BLvsCustLbl = 'BL / Customer', Comment = 'cs-CZ=BL / Zákazník;en-US=BL / Customer'; newline
 newline
tabulator tabulator LBL_2 = 'ACQ01_Calculation_LBL_2', Comment = 'cs-CZ=Kontaktní osoba;en-US=Contact person'; newline
tabulator tabulator LBL_3 = 'ACQ01_Calculation_LBL_3', Comment = 'cs-CZ=Telefon;en-US=Telephone'; newline
tabulator tabulator LBL_4 = 'ACQ01_Calculation_LBL_4', Comment = 'cs-CZ=E-mail;en-US=E-mail'; newline
tabulator tabulator LBL_10 = 'ACQ01_Calculation_LBL_10', Comment = 'cs-CZ=IČO;en-US=Id. No.'; newline
tabulator tabulator LBL_11 = 'ACQ01_Calculation_LBL_11', Comment = 'cs-CZ=DIČ;en-US=VAT Id. No.'; newline
tabulator tabulator LBL_12 = 'ACQ01_Calculation_LBL_12', Comment = 'cs-CZ=Středisko;en-US=Cost Center'; newline
tabulator tabulator LBL_13 = 'ACQ01_Calculation_LBL_13', Comment = 'cs-CZ=Pobočka;en-US=Branch'; newline
tabulator tabulator LBL_14 = 'ACQ01_Calculation_LBL_14', Comment = 'cs-CZ=Náhrada za vozidlo;en-US=Vehicle replacement'; newline
tabulator tabulator LBL_15 = 'ACQ01_Calculation_LBL_15', Comment = 'cs-CZ=Datum vyhotovení;en-US=Creation Date'; newline
tabulator tabulator LBL_16 = 'ACQ01_Calculation_LBL_16', Comment = 'cs-CZ=Platnost nabídky;en-US=Offer Validity Date'; newline
tabulator tabulator LBL_17 = 'ACQ01_Calculation_LBL_17', Comment = 'cs-CZ=Uživatel / Řidič;en-US=User / Driver'; newline
tabulator tabulator LBL_18 = 'ACQ01_Calculation_LBL_18', Comment = 'cs-CZ=Požadovaný termín dodání;en-US=Requested Delivery Date'; newline
tabulator tabulator LBL_23 = 'ACQ01_Calculation_LBL_23', Comment = 'cs-CZ=Druh paliva;en-US=Fuel Type'; newline
tabulator tabulator LBL_24 = 'ACQ01_Calculation_LBL_24', Comment = 'cs-CZ=Spotřeba paliva (kombinovaná);en-US=Fuel Consumption (combined)'; newline
tabulator tabulator LBL_25 = 'ACQ01_Calculation_LBL_25', Comment = 'cs-CZ=Barva karoserie;en-US=Exterior Colour'; newline
tabulator tabulator LBL_26 = 'ACQ01_Calculation_LBL_26', Comment = 'cs-CZ=Barva a provedení interiéru;en-US=Interior Colour and Material'; newline
tabulator tabulator LBL_27 = 'ACQ01_Calculation_LBL_27', Comment = 'cs-CZ=Převodovka;en-US=Gearbox'; newline
tabulator tabulator LBL_28 = 'ACQ01_Calculation_LBL_28', Comment = 'cs-CZ=Emise;en-US=Emissions'; newline
tabulator tabulator LBL_29 = 'ACQ01_Calculation_LBL_29', Comment = 'cs-CZ=Provozní hmotnost;en-US=Kerb Weight'; newline
tabulator tabulator LBL_30 = 'ACQ01_Calculation_LBL_30', Comment = 'cs-CZ=Povolená hmotnost;en-US=Max. Total Weight'; newline
tabulator tabulator LBL_31 = 'ACQ01_Calculation_LBL_31', Comment = 'cs-CZ=Objem motoru;en-US=Engine Volume'; newline
tabulator tabulator LBL_32 = 'ACQ01_Calculation_LBL_32', Comment = 'cs-CZ=Dodavatel;en-US=Vendor Name'; newline
tabulator tabulator LBL_33 = 'ACQ01_Calculation_LBL_33', Comment = 'cs-CZ=Cena před slevou;en-US=Price before discount'; newline
tabulator tabulator LBL_34 = 'ACQ01_Calculation_LBL_34', Comment = 'cs-CZ=Sleva;en-US=Discount'; newline
tabulator tabulator LBL_35 = 'ACQ01_Calculation_LBL_35', Comment = 'cs-CZ=Sleva;en-US=Discount'; newline
tabulator tabulator LBL_36 = 'ACQ01_Calculation_LBL_36', Comment = 'cs-CZ=Sazba DPH;en-US=VAT Rate'; newline
tabulator tabulator LBL_37 = 'ACQ01_Calculation_LBL_37', Comment = 'cs-CZ=Cena po slevě;en-US=Price after discount'; newline
tabulator tabulator LBL_38 = 'ACQ01_Calculation_LBL_38', Comment = 'cs-CZ=Cena po slevě;en-US=Price after discount'; newline
tabulator tabulator LBL_39 = 'ACQ01_Calculation_LBL_39', Comment = 'cs-CZ=Cena před slevou bez DPH;en-US=Price before discount VAT excluded'; newline
tabulator tabulator LBL_53 = 'ACQ01_Calculation_LBL_53', Comment = 'cs-CZ=Pořizovací cena;en-US=Acquisition price'; newline
tabulator tabulator LBL_54 = 'ACQ01_Calculation_LBL_54', Comment = 'cs-CZ=Počet dveří;en-US=Number of doors'; newline
tabulator tabulator LBL_55 = 'ACQ01_Calculation_LBL_55', Comment = 'cs-CZ=Typ karoserie;en-US=Body Type'; newline
tabulator tabulator LBL_88 = 'ACQ01_Calculation_LBL_88', Comment = 'cs-CZ=Klient;en-US=Client'; newline
tabulator tabulator LBL_90a = 'ACQ01_Calculation_LBL_90a', Comment = 'cs-CZ=Měsíční nájemné bez zúčtovatelných položek;en-US=Monthly payment excl. billable items'; newline
tabulator tabulator LBL_90b = 'ACQ01_Calculation_LBL_90b', Comment = 'cs-CZ=Měsíční nájemné;en-US=Monthly rent'; newline
tabulator tabulator LBL_91 = 'ACQ01_Calculation_LBL_91', Comment = 'cs-CZ=Měsíční nájemné vč. zúčtovatelných položek;en-US=Monthly payment incl. billable items'; newline
tabulator tabulator LBL_92 = 'ACQ01_Calculation_LBL_92', Comment = 'cs-CZ=Finanční část nájemného;en-US=Financial part'; newline
tabulator tabulator LBL_93 = 'ACQ01_Calculation_LBL_93', Comment = 'cs-CZ=Servisní část nájemného;en-US=Service part'; newline
tabulator tabulator LBL_94 = 'ACQ01_Calculation_LBL_94', Comment = 'cs-CZ=Doba nájmu;en-US=Financing Period'; newline
tabulator tabulator LBL_95 = 'ACQ01_Calculation_LBL_95', Comment = 'cs-CZ=Roční projezd;en-US=Annual Mileage'; newline
tabulator tabulator LBL_96 = 'ACQ01_Calculation_LBL_96', Comment = 'cs-CZ=Smluvní projezd;en-US=Contractual Distance'; newline
tabulator tabulator LBL_102 = 'ACQ01_Calculation_LBL_102', Comment = 'cs-CZ=úroková míra;en-US=interest rate'; newline
tabulator tabulator LBL_115 = 'ACQ01_Calculation_LBL_115', Comment = 'cs-CZ=VOZIDLO;en-US=VEHICLE'; newline
tabulator tabulator LBL_116 = 'ACQ01_Calculation_LBL_116', Comment = 'cs-CZ=CENA;en-US=PRICE'; newline
tabulator tabulator LBL_117 = 'ACQ01_Calculation_LBL_117', Comment = 'cs-CZ=PODMÍNKY;en-US=CONDITIONS'; newline
tabulator tabulator LBL_118 = 'ACQ01_Calculation_LBL_118', Comment = 'cs-CZ=SLUŽBY;en-US=SERVICES'; newline
tabulator tabulator LBL_119 = 'ACQ01_Calculation_LBL_119', Comment = 'cs-CZ=VÝBAVA;en-US=EQUIPMENT'; newline
 newline
tabulator tabulator LBL_121 = 'ACQ01_Calculation_LBL_121', Comment = 'cs-CZ=Cena;en-US=Price'; newline
tabulator tabulator LBL_122 = 'ACQ01_Calculation_LBL_122', Comment = 'cs-CZ=Výbava celkem;en-US=Equipment in total'; newline
tabulator tabulator LBL_123 = 'ACQ01_Calculation_LBL_123', Comment = 'cs-CZ=Základní model;en-US=Basic model'; newline
tabulator tabulator LBL_124 = 'ACQ01_Calculation_LBL_124', Comment = 'cs-CZ=Příplatková výbava;en-US=Optional equipment'; newline
tabulator tabulator LBL_125 = 'ACQ01_Calculation_LBL_125', Comment = 'cs-CZ=Doplňková výbava;en-US=Additional equipment'; newline
tabulator tabulator LBL_126 = 'ACQ01_Calculation_LBL_126', Comment = 'cs-CZ=Celkem;en-US=Total'; newline
tabulator tabulator LBL_127 = 'ACQ01_Calculation_LBL_127', Comment = 'cs-CZ=Odchylka od smluvního projezdu;en-US=Deviation from contractual distance'; newline
tabulator tabulator LBL_128 = 'ACQ01_Calculation_LBL_128', Comment = 'cs-CZ=Fakturovaná sazba za přečerpaný km;en-US=Invoiced rate per overdrawn kilometer'; newline
tabulator tabulator LBL_129 = 'ACQ01_Calculation_LBL_129', Comment = 'cs-CZ=Dobropisovaná sazba za nedočerpaný km;en-US=Credited rate per underdrawn kilometer'; newline
tabulator tabulator LBL_130 = 'ACQ01_Calculation_LBL_130', Comment = 'cs-CZ=Měsíční nájemné zahrnuje následující položky;en-US=Monthly rent includes'; newline
tabulator tabulator LBL_131 = 'ACQ01_Calculation_LBL_131', Comment = 'cs-CZ=úspory;en-US=profit'; newline
tabulator tabulator LBL_132 = 'ACQ01_Calculation_LBL_132', Comment = 'cs-CZ=nedoplatky;en-US=loss'; newline
tabulator tabulator LBL_133 = 'ACQ01_Calculation_LBL_133', Comment = 'cs-CZ=Odpisy a úroky;en-US=Depreciation and Interest'; newline
tabulator tabulator LBL_134 = 'ACQ01_Calculation_LBL_134', Comment = 'cs-CZ=Kalkulovaná zůstatková hodnota bez DPH;en-US=Calculated residual value excluded VAT'; newline
tabulator tabulator LBL_135 = 'ACQ01_Calculation_LBL_135', Comment = 'cs-CZ=měsíční pojistné;en-US=monthly payment'; newline
tabulator tabulator LBL_137 = 'ACQ01_Calculation_LBL_137', Comment = 'cs-CZ=Typ úročení;en-US=Interest type'; newline
tabulator tabulator LBL_138 = 'ACQ01_Calculation_LBL_138', Comment = 'cs-CZ=Dealerská výbava;en-US=Additional equipment'; newline
tabulator tabulator LBL_139 = 'ACQ01_Calculation_LBL_139', Comment = 'cs-CZ=Výbava třetích stran;en-US=Third party equipment'; newline
tabulator tabulator LBL_140 = 'ACQ01_Calculation_LBL_140', Comment = 'cs-CZ=Katalogová cena;en-US=Catalogue price'; newline
tabulator tabulator LBL_141 = 'ACQ01_Calculation_LBL_141', Comment = 'cs-CZ=Sleva;en-US=Discount'; newline
tabulator tabulator LBL_142 = 'ACQ01_Calculation_LBL_142', Comment = 'cs-CZ=Pořizovací cena;en-US=Acquisition price'; newline
tabulator tabulator LBL_143 = 'ACQ01_Calculation_LBL_143', Comment = 'cs-CZ=Zúčtovatelné položky;en-US=Billiable items'; newline
tabulator tabulator LBL_144 = 'ACQ01_Calculation_LBL_144', Comment = 'cs-CZ=Příplatková výbava;en-US=Optional equipment'; newline
tabulator tabulator LBL_146 = 'ACQ01_Calculation_LBL_146', Comment = 'cs-CZ=Zahrnuto;en-US=Included'; newline
tabulator tabulator LBL_149 = 'ACQ01_Calculation_LBL_149', Comment = 'cs-CZ=Návrh smlouvy č.;en-US=Contract Proposal No.'; newline
 newline
tabulator tabulator LBL_160 = 'ACQ01_Calculation_LBL_160', Comment = 'cs-CZ=Registrační značka;en-US=Licence Plate No.'; newline
tabulator tabulator LBL_161 = 'ACQ01_Calculation_LBL_161', Comment = 'cs-CZ=VIN;en-US=VIN'; newline
tabulator tabulator LBL_162 = 'ACQ01_Calculation_LBL_162', Comment = 'cs-CZ=Vyčíslení nájemného dle pravidel IFRS;en-US=Calculation of monthly payment under IFRS rules'; newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator RequestPageLanguage: Enum LanguageEnum; newline
tabulator tabulator isLocalLanguage: Boolean; newline
tabulator tabulator ENLangId: Integer; newline
tabulator tabulator CZLangId: Integer; newline
 newline
tabulator tabulator CustIdentInfo: Text; newline
tabulator tabulator CompanyInformation: Record "Company Information"; newline
tabulator tabulator FPTrec: Record "API Financing Product/Template"; newline
tabulator tabulator FPTypeRec: Record "API Financing Product Type"; newline
tabulator tabulator MArec: Record "API Master Agreement"; newline
tabulator tabulator InsProd: Record "API Insurance Product"; newline
tabulator tabulator InsProdVar: Record "API Insurance Product Variant"; newline
tabulator tabulator typeOfBody: Record "API Type of Body"; newline
tabulator tabulator StartOfNextMonth: Date; newline
tabulator tabulator ReLicPlateNo: Text; newline
tabulator tabulator C_BLG_Customer_Support_Name: Text; newline
tabulator tabulator E_Job_Title: Text; newline
tabulator tabulator E_Company_E_Mail: Text; newline
tabulator tabulator E_API_Company_Mobile_Phone_No: Text; newline
tabulator tabulator SignType: Text; newline
 newline
tabulator tabulator FO_FuelType: Text; newline
tabulator tabulator FO_Gearbox: Text; newline
tabulator tabulator CS_Service_Description: Text; newline
tabulator tabulator CS_B_Service_Description_Detail: Text; newline
tabulator tabulator CI_CountryRegion: Text; newline
tabulator tabulator CSB_Billable_Calculation_PerPayment_Sum: Decimal; newline
tabulator tabulator Equipment_List_Price_Sum: Decimal; newline
tabulator tabulator Equipment_Price_Sum: Decimal; newline
tabulator tabulator Currency: Text; newline
tabulator tabulator AA_IFRS, AA_CALC_RV, AA_CALC_INTER : Boolean; newline
tabulator tabulator FCH_CALCULATION: Boolean; newline
tabulator tabulator FCH_ACTIVATION: Boolean; newline
tabulator tabulator FCH_VERSION: Text; newline
tabulator tabulator FCH_NOTIFICATION: Boolean; newline
tabulator tabulator FCH_Offer_Creation_Date: Date; newline
tabulator tabulator FPT_Name: Text; newline
tabulator tabulator FPT_Descr: Text; newline
tabulator tabulator ReINTRECALC: Decimal; newline
 newline
tabulator tabulator CM_MILEAGE_RANGE: Text; newline
tabulator tabulator CM_MILEAGE_OVER: Decimal; newline
tabulator tabulator CM_MILEAGE_UNFINISHED: Decimal; newline
tabulator tabulator MileageDictionary: Dictionary of [Integer, Decimal]; newline
 newline
tabulator tabulator NotificationSelectEnum: Enum YesNoSelectorEnum; newline
tabulator tabulator ServiceKindCodeOrder: Integer; newline
tabulator tabulator InsuranceProductNoOrder: Integer; newline
tabulator tabulator InterestRateTypeInfo: Text; newline
tabulator tabulator ShowSettlement: Boolean; newline
 newline
tabulator tabulator CSSkipped: Integer; newline
tabulator tabulator CSBSkipped: Integer; newline
tabulator tabulator CSEmpty: Boolean; newline
tabulator tabulator CSBEmpty: Boolean; newline
 newline
tabulator tabulator #region Constants newline
tabulator tabulator ReportTitle: Text; newline
tabulator tabulator Err_011: Label 'Duplicate found in table "API Tire Detail Line".', Comment = 'cs-CZ=Nalezen duplikát v tabulce "API Tire Detail Line".;en-US=Duplicate found in table "API Tire Detail Line".'; newline
 newline
tabulator tabulator LBL_10: Label 'ACQ01_Calculation_LBL_10', Comment = 'cs-CZ=IČO;en-US=Id. No.'; newline
tabulator tabulator LBL_11: Label 'ACQ01_Calculation_LBL_11', Comment = 'cs-CZ=DIČ;en-US=VAT Id. No.'; newline
tabulator tabulator LBL_1a: Label 'ACQ01_Calculation_LBL_1a', Comment = 'cs-CZ=Návrh smlouvy č.;en-US=Contract Proposal No.'; newline
tabulator tabulator LBL_1b: label 'ACQ01_Calculation_LBL_1b', Comment = 'cs-CZ=Individuální leasingová smlouva č.;en-US=Individual lease contract no.:'; newline
tabulator tabulator LBL_1c: label 'ACQ01_Calculation_LBL_1c', Comment = 'cs-CZ=Individuální smlouva č.;en-US=Individual contract no.'; newline
tabulator tabulator LBL_120a: label 'ACQ01_Calculation_LBL_120a', Comment = 'cs-CZ=měsíc;en-US=month'; newline
tabulator tabulator LBL_120b: label 'ACQ01_Calculation_LBL_120b', Comment = 'cs-CZ=měsíce;en-US=months'; newline
tabulator tabulator LBL_120c: label 'ACQ01_Calculation_LBL_120c', Comment = 'cs-CZ=měsíců;en-US=months'; newline
tabulator tabulator LBL_120CZa: label 'měsíc'; newline
tabulator tabulator LBL_120CZb: label 'měsíce'; newline
tabulator tabulator LBL_120CZc: label 'měsíců'; newline
tabulator tabulator LBL_120ENa: label 'month'; newline
tabulator tabulator LBL_120ENb: label 'months'; newline
 newline
tabulator tabulator OpeningText: Text; newline
tabulator tabulator LBL_111: Label 'ACQ01_Calculation_LBL_111'; //Vážený kliente, pokud nám tento dokument doručíte podepsaný, platí jako Váš návrh na uzavření individuální leasingové smlouvy. Individuální leasingová smlouva vzniká až podpisem návrhu ze strany BUSINESS LEASE s.r.o. O podpisu budete informován. Individuální leasingová smlouva se řídí ustanoveními Rámcové smlouvy č. %1 uzavřené dne %2. newline
tabulator tabulator LBL_112: Label 'ACQ01_Calculation_LBL_112'; //Vážený kliente, pokud nám tento dokument doručíte podepsaný, platí jako Váš návrh na uzavření individuální leasingové smlouvy. Individuální leasingová smlouva vzniká až podpisem návrhu ze strany BUSINESS LEASE s.r.o. O podpisu budete informován. newline
tabulator tabulator LBL_113: Label 'ACQ01_Calculation_LBL_113'; //Vážený kliente, pokud nám tento dokument doručíte podepsaný, platí jako Váš návrh na uzavření individuální smlouvy. Individuální smlouva vzniká až podpisem návrhu ze strany BUSINESS LEASE s.r.o. O podpisu budete informován. Individuální smlouva se řídí ustanoveními Rámcové smlouvy č. %1 uzavřené dne %2. newline
tabulator tabulator LBL_114: Label 'ACQ01_Calculation_LBL_114'; //Vážený kliente, pokud nám tento dokument doručíte podepsaný, platí jako Váš návrh na uzavření individuální smlouvy. Individuální smlouva vzniká až podpisem návrhu ze strany BUSINESS LEASE s.r.o. O podpisu budete informován. newline
tabulator tabulator LBL_150: Label 'ACQ01_Calculation_LBL_150'; //Vážený kliente, zasíláme Vám tento dokument, který představuje nové znění individuální leasingové smlouvy, která se řídí ustanoveními Rámcové smlouvy č. %1 uzavřené dne %2. Nové znění ILS je účinné od %3. newline
tabulator tabulator LBL_151: Label 'ACQ01_Calculation_LBL_151'; //Vážený kliente, zasíláme Vám tento dokument, který představuje nové znění individuální smlouvy, která se řídí ustanoveními Rámcové smlouvy č. %1 uzavřené dne %2. Nové znění ILS je účinné od %3. newline
tabulator tabulator LBL_152: Label 'ACQ01_Calculation_LBL_152'; //Vážený kliente, zasíláme Vám potvrzení o vzniku a účinnosti nové individuální leasingové smlouvy na základě návrhu smlouvy č. %1. Individuální leasingová smlouva bude v našem systému evidována pod č. %2, které bude uváděno na dokumentech vystavovaných v souvislosti s touto smlouvou, zejména na daňových dokladech. Individuální leasingová smlouva se řídí ustanovením rámcové smlouvy č. %3  uzavřené dne %4. newline
tabulator tabulator LBL_153: Label 'ACQ01_Calculation_LBL_153'; //Vážený kliente, zasíláme Vám potvrzení o vzniku a účinnosti nové individuální smlouvy na základě návrhu smlouvy č. %1. Individuální smlouva bude v našem systému evidována pod č. %2, které bude uváděno na dokumentech vystavovaných v souvislosti s touto smlouvou, zejména na daňových dokladech. Individuální smlouva se řídí ustanovením rámcové smlouvy č. %3 uzavřené dne %4. newline
tabulator tabulator FinalText: Text; newline
tabulator tabulator LBL_154: Label 'ACQ01_Calculation_LBL_154'; //Tento dokument byl zpracován na základě nejlepších vědomostí Dodavatele a na základě cenové nabídky ... a těšíme se na další spolupráci. newline
tabulator tabulator LBL_155: Label 'ACQ01_Calculation_LBL_155'; //Tato smlouva byla zpracovaná na základě nejlepších vědomostí Dodavatele. ... poskytnout Vám nejlepší péči při zajištění všech služeb. newline
tabulator tabulator LBL_156: Label 'ACQ01_Calculation_LBL_156'; //Tato smlouva byla zpracovaná na základě nejlepších vědomostí Dodavatele. ... Všichni naši zaměstnanci a smluvní partneři jsou připraveni poskytnout Vám nejlepší péči při zajištění všech služeb. newline
tabulator tabulator LBL_157: Label 'ACQ01_Calculation_LBL_157'; //Tento dokument byl zpracován na základě nejlepších vědomostí Dodavatele ... poskytnout Vám nejlepší péči při zajištění všech našich služeb. newline
tabulator tabulator LBL_158: Label 'ACQ01_Calculation_LBL_158'; //Tento dokument byl zpracován na základě nejlepších vědomostí Dodavatele. ... poskytnout Vám nejlepší péči při zajištění všech služeb. newline
tabulator tabulator LBL_159: Label 'ACQ01_Calculation_LBL_159'; //Tato smlouva byla zpracovaná na základě nejlepších vědomostí Dodavatele. ... Všichni naši zaměstnanci a smluvní partneři jsou připraveni poskytnout Vám nejlepší péči při zajištění všech služeb. newline
tabulator tabulator LBL_170a: label 'ACQ01_Calculation_LBL_170', Comment = 'cs-CZ=s přefakturací;en-US=with re-invoicing'; newline
tabulator tabulator LBL_170b: label 'ACQ01_Calculation_LBL_170', Comment = 'cs-CZ=S přefakturací;en-US=With re-invoicing'; newline
tabulator tabulator LBL_180: Label 'ACQ01_Calculation_LBL_180', Comment = 'cs-CZ=fixace: ;en-US=fixation: '; newline
tabulator tabulator LBL_181a: Label 'ACQ01_Calculation_LBL_180a', Comment = 'cs-CZ=3 měsíce;en-US=3 months'; newline
tabulator tabulator LBL_181b: Label 'ACQ01_Calculation_LBL_180b', Comment = 'cs-CZ=6 měsíců;en-US=6 months'; newline
tabulator tabulator LBL_181c: Label 'ACQ01_Calculation_LBL_180c', Comment = 'cs-CZ=1 rok;en-US=1 year'; newline
tabulator tabulator LBL_181d: Label 'ACQ01_Calculation_LBL_180d', Comment = 'cs-CZ=k datu aktivace;en-US=as of activation date'; newline
tabulator tabulator LBL_190: Label 'ACQ01_Calculation_LBL_190', Comment = 'cs-CZ=Typ pneu: ;en-US=Tire type: '; newline
tabulator tabulator LBL_191: Label 'ACQ01_Calculation_LBL_191', Comment = 'cs-CZ=pneu: ;en-US=tires: '; newline
tabulator tabulator LBL_192a: Label 'ACQ01_Calculation_LBL_192a', Comment = 'cs-CZ=kategorie: ;en-US=category: '; newline
tabulator tabulator LBL_192b: Label 'ACQ01_Calculation_LBL_192b', Comment = 'cs-CZ=Kategorie pneu: ;en-US=Tire category: '; newline
tabulator tabulator LBL_193: Label 'ACQ01_Calculation_LBL_193', Comment = 'cs-CZ=rozměr;en-US=size'; newline
tabulator tabulator LBL_194: Label 'ACQ01_Calculation_LBL_194', Comment = 'cs-CZ=Počet pneu: ;en-US=Tire count: '; newline
tabulator tabulator LBL_195a: Label 'ACQ01_Calculation_LBL_195a', Comment = 'cs-CZ=omezeno;en-US=limited'; newline
tabulator tabulator LBL_195b: Label 'ACQ01_Calculation_LBL_195b', Comment = 'cs-CZ=neomezeno;en-US=suitable'; newline
tabulator tabulator LBL_196a: Label 'ACQ01_Calculation_LBL_196a', Comment = 'cs-CZ=celoroční;en-US=year-round'; newline
tabulator tabulator LBL_196b: Label 'ACQ01_Calculation_LBL_196b', Comment = 'cs-CZ=sezónní;en-US=seasonal'; newline
 newline
tabulator #endregion newline
 newline
