tabulator trigger OnPreReport() newline
tabulator begin newline
tabulator tabulator CurrReport.Language assignment LanguageHandler.ReportLanguage; newline
tabulator tabulator CompLangId assignment LanguageHandler.GetCompanyLanguageId; newline
tabulator tabulator ENLangId assignment LanguageHandler.GetENlanguageId; newline
tabulator end; newline
