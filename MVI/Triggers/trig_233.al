tabulator tabulator trigger OnOpenPage() newline
tabulator tabulator begin newline
tabulator tabulator tabulator CIlng assignment LanguageHandler.GetCompanyLanguageId; newline
tabulator tabulator end; newline
tabulator } newline
tabulator */ newline
 newline
tabulator labels newline
tabulator { newline
tabulator tabulator CultureCode = 'CultureCode', Comment = 'cs-CZ=cs-CZ;en-US=en-US'; newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator Vendor: Record Vendor; newline
tabulator tabulator AFO: Record "API Financed Object"; newline
tabulator tabulator ATB: Record "API Type of Body"; newline
tabulator tabulator Customer: Record Customer; newline
tabulator tabulator Contact: Record Contact; newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator UserSetup: Record "User Setup"; newline
tabulator tabulator Employee: Record Employee; newline
tabulator tabulator CI: Record "Company Information"; newline
tabulator tabulator CountryRegion: Record "Country/Region"; newline
tabulator tabulator VendBussLoc: Record "API Contact Business Location"; newline
tabulator tabulator ContAltAddr: Record "Contact Alt. Address"; newline
tabulator tabulator FuelType: Record "API Fuel Type"; newline
tabulator tabulator FuelLvl: Integer; newline
tabulator tabulator FuelRange: Text; newline
tabulator tabulator Counter: Integer; newline
tabulator tabulator RowNum: Integer; newline
tabulator tabulator CO: Record "Company Official CZL"; newline
tabulator tabulator BHA_AccDescr: Text; newline
tabulator tabulator Cust_FullAddr: Text; newline
 newline
tabulator tabulator #region Language newline
tabulator tabulator ReqPageLanguage: Enum LanguageEnum; newline
tabulator tabulator #endregion newline
 newline
tabulator tabulator #region Labels newline
tabulator tabulator tVendor: Label 'Dodavatel'; newline
tabulator tabulator tHandoverPlace: Label 'Místo předání'; newline
tabulator tabulator LBL_1: Label 'DED04_Handover_Protocol_LBL_1'; newline
tabulator tabulator LBL_2: Label 'DED04_Handover_Protocol_LBL_2'; newline
tabulator tabulator LBL_9: Label 'DED04_Handover_Protocol_LBL_9'; newline
tabulator tabulator LBL_22: Label 'DED04_Handover_Protocol_LBL_22'; newline
tabulator tabulator LBL_20: Label 'DED04_Handover_Protocol_LBL_20'; newline
tabulator tabulator LBL_21: Label 'DED04_Handover_Protocol_LBL_21'; newline
tabulator tabulator LBL_14: Label 'DED04_Handover_Protocol_LBL_14'; newline
tabulator tabulator LBL_15: Label 'DED04_Handover_Protocol_LBL_15'; newline
tabulator tabulator LBL_13: Label 'DED04_Handover_Protocol_LBL_13'; newline
tabulator tabulator LBL_17: Label 'DED04_Handover_Protocol_LBL_17'; newline
tabulator tabulator LBL_16: Label 'DED04_Handover_Protocol_LBL_16'; newline
tabulator tabulator LBL_27: Label 'DED04_Handover_Protocol_LBL_27'; newline
tabulator tabulator LBL_28: Label 'DED04_Handover_Protocol_LBL_28'; newline
tabulator tabulator LBL_29: Label 'DED04_Handover_Protocol_LBL_29'; newline
tabulator tabulator LBL_30: Label 'DED04_Handover_Protocol_LBL_30'; newline
tabulator tabulator LBL_31: Label 'DED04_Handover_Protocol_LBL_31'; newline
tabulator tabulator LBL_33: Label 'DED04_Handover_Protocol_LBL_33'; newline
tabulator tabulator LBL_34: Label 'DED04_Handover_Protocol_LBL_34'; newline
tabulator tabulator LBL_32: Label 'DED04_Handover_Protocol_LBL_32'; newline
tabulator tabulator LBL_85: Label 'DED04_Handover_Protocol_LBL_85'; newline
tabulator tabulator LBL_37: Label 'DED04_Handover_Protocol_LBL_37'; newline
tabulator tabulator LBL_38: Label 'DED04_Handover_Protocol_LBL_38'; newline
tabulator tabulator LBL_39: Label 'DED04_Handover_Protocol_LBL_39'; newline
tabulator tabulator LBL_40: Label 'DED04_Handover_Protocol_LBL_40'; newline
tabulator tabulator LBL_41: Label 'DED04_Handover_Protocol_LBL_41'; newline
tabulator tabulator LBL_42: Label 'DED04_Handover_Protocol_LBL_42'; newline
tabulator tabulator LBL_43: Label 'DED04_Handover_Protocol_LBL_43'; newline
tabulator tabulator LBL_44: Label 'DED04_Handover_Protocol_LBL_44'; newline
tabulator tabulator LBL_45: Label 'DED04_Handover_Protocol_LBL_45'; newline
tabulator tabulator LBL_46: Label 'DED04_Handover_Protocol_LBL_46'; newline
tabulator tabulator LBL_47: Label 'DED04_Handover_Protocol_LBL_47'; newline
tabulator tabulator LBL_48: Label 'DED04_Handover_Protocol_LBL_48'; newline
tabulator tabulator LBL_49: Label 'DED04_Handover_Protocol_LBL_49'; newline
tabulator tabulator LBL_49e: Label 'DED04_Handover_Protocol_LBL_49e'; newline
tabulator tabulator LBL_50: Label 'DED04_Handover_Protocol_LBL_50'; newline
tabulator tabulator LBL_50e: Label 'DED04_Handover_Protocol_LBL_50e'; newline
tabulator tabulator LBL_51: Label 'DED04_Handover_Protocol_LBL_51'; newline
tabulator tabulator LBL_51e: Label 'DED04_Handover_Protocol_LBL_51e'; newline
tabulator tabulator LBL_52: Label 'DED04_Handover_Protocol_LBL_52'; newline
tabulator tabulator LBL_52e: Label 'DED04_Handover_Protocol_LBL_52e'; newline
tabulator tabulator LBL_53: Label 'DED04_Handover_Protocol_LBL_53'; newline
tabulator tabulator LBL_53e: Label 'DED04_Handover_Protocol_LBL_53e'; newline
tabulator tabulator LBL_54: Label 'DED04_Handover_Protocol_LBL_54'; newline
tabulator tabulator LBL_55: Label 'DED04_Handover_Protocol_LBL_55'; newline
tabulator tabulator LBL_56: Label 'DED04_Handover_Protocol_LBL_56'; newline
tabulator tabulator LBL_57: Label 'DED04_Handover_Protocol_LBL_57'; newline
tabulator tabulator LBL_58: Label 'DED04_Handover_Protocol_LBL_58'; newline
tabulator tabulator LBL_59: Label 'DED04_Handover_Protocol_LBL_59'; newline
tabulator tabulator LBL_60: Label 'DED04_Handover_Protocol_LBL_60'; newline
tabulator tabulator LBL_61: Label 'DED04_Handover_Protocol_LBL_61'; newline
tabulator tabulator LBL_62: Label 'DED04_Handover_Protocol_LBL_62'; newline
tabulator tabulator LBL_63: Label 'DED04_Handover_Protocol_LBL_63'; newline
tabulator tabulator LBL_64: Label 'DED04_Handover_Protocol_LBL_64'; newline
tabulator tabulator LBL_65: Label 'DED04_Handover_Protocol_LBL_65'; newline
tabulator tabulator LBL_66: Label 'DED04_Handover_Protocol_LBL_66'; newline
tabulator tabulator LBL_67: Label 'DED04_Handover_Protocol_LBL_67'; newline
tabulator tabulator LBL_68: Label 'DED04_Handover_Protocol_LBL_68'; newline
tabulator tabulator LBL_69: Label 'DED04_Handover_Protocol_LBL_69'; newline
tabulator tabulator LBL_70: Label 'DED04_Handover_Protocol_LBL_70'; newline
tabulator tabulator LBL_71: Label 'DED04_Handover_Protocol_LBL_71'; newline
tabulator tabulator LBL_72: Label 'DED04_Handover_Protocol_LBL_72'; newline
tabulator tabulator LBL_73: Label 'DED04_Handover_Protocol_LBL_73'; newline
tabulator tabulator LBL_74: Label 'DED04_Handover_Protocol_LBL_74'; newline
tabulator tabulator LBL_75: Label 'DED04_Handover_Protocol_LBL_75'; newline
tabulator tabulator LBL_76: Label 'DED04_Handover_Protocol_LBL_76'; newline
tabulator tabulator LBL_77: Label 'DED04_Handover_Protocol_LBL_77'; newline
tabulator tabulator LBL_78: Label 'DED04_Handover_Protocol_LBL_78'; newline
tabulator tabulator LBL_79: Label 'DED04_Handover_Protocol_LBL_79'; newline
tabulator tabulator LBL_80: Label 'DED04_Handover_Protocol_LBL_80'; newline
tabulator tabulator LBL_88: Label 'DED04_Handover_Protocol_LBL_88'; newline
tabulator tabulator LBL_89: Label 'DED04_Handover_Protocol_LBL_89'; newline
tabulator tabulator LBL_90: Label 'DED04_Handover_Protocol_LBL_90'; newline
tabulator tabulator LBL_91: Label 'DED04_Handover_Protocol_LBL_91'; newline
tabulator tabulator LBL_92: Label 'DED04_Handover_Protocol_LBL_92'; newline
tabulator tabulator LBL_93: Label 'DED04_Handover_Protocol_LBL_93'; newline
tabulator tabulator LBL_94: Label 'DED04_Handover_Protocol_LBL_94'; //>S přátelským pozdravem, newline
tabulator tabulator LBL_95: Label 'DED04_Handover_Protocol_LBL_95'; //Doklady k předání: newline
tabulator tabulator LBL_96: Label 'DED04_Handover_Protocol_LBL_96'; newline
tabulator tabulator LBL_97: Label 'DED04_Handover_Protocol_LBL_97'; newline
tabulator tabulator LBL_98: Label 'DED04_Handover_Protocol_LBL_98'; newline
tabulator tabulator LBL_99: Label 'DED04_Handover_Protocol_LBL_99'; newline
tabulator tabulator LBL_100: Label 'DED04_Handover_Protocol_LBL_100'; newline
tabulator tabulator LBL_101: Label 'DED04_Handover_Protocol_LBL_101'; newline
tabulator tabulator LBL_102: Label 'DED04_Handover_Protocol_LBL_102'; newline
tabulator tabulator LBL_103: Label 'DED04_Handover_Protocol_LBL_103'; newline
tabulator tabulator LBL_104: Label 'DED04_Handover_Protocol_LBL_104'; newline
tabulator tabulator LBL_105: Label 'DED04_Handover_Protocol_LBL_105'; //Martin Bulíř newline
tabulator tabulator LBL_106: Label 'DED04_Handover_Protocol_LBL_106'; //jednatel newline
tabulator tabulator LBL_110: Label 'DED04_Handover_Protocol_LBL_110'; newline
tabulator tabulator LBL_111: Label 'DED04_Handover_Protocol_LBL_111'; newline
tabulator tabulator LBL_112: Label 'DED04_Handover_Protocol_LBL_112'; newline
tabulator tabulator LBL_113: Label 'DED04_Handover_Protocol_LBL_113'; newline
tabulator tabulator LBL_114: Label 'DED04_Handover_Protocol_LBL_114'; newline
tabulator tabulator LBL_115: Label 'DED04_Handover_Protocol_LBL_115'; newline
tabulator tabulator LBL_116: Label 'DED04_Handover_Protocol_LBL_116'; newline
tabulator tabulator LBL_117: Label 'DED04_Handover_Protocol_LBL_117'; newline
tabulator tabulator LBL_118: Label 'DED04_Handover_Protocol_LBL_118'; //Předávající za dodavatele newline
tabulator tabulator LBL_119: Label 'DED04_Handover_Protocol_LBL_119'; //Přejímající za zákazníka newline
tabulator tabulator LBL_120: Label 'DED04_Handover_Protocol_LBL_120'; //Datum a čas převzetí newline
tabulator tabulator LBL_121: Label 'DED04_Handover_Protocol_LBL_121'; //Jméno a přijmení oprávněného zástupce\dealera (hůlkovým písmem) newline
tabulator tabulator LBL_122: Label 'DED04_Handover_Protocol_LBL_122'; //Jméno a přijmení oprávněného zástupce\zákazníka (hůlkovým písmem) newline
tabulator tabulator LBL_123: Label 'DED04_Handover_Protocol_LBL_123'; //Předávající za BUSINESS LEASE s.r.o. newline
tabulator tabulator LBL_124: Label 'DED04_Handover_Protocol_LBL_124'; //Podpis newline
tabulator tabulator LBL_125: Label 'DED04_Handover_Protocol_LBL_125'; //DIČ newline
tabulator #endregion newline
 newline
