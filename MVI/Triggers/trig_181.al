tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator cus: Record Customer; newline
tabulator tabulator tabulator tabulator tabulator car: Record "API Rent Car"; newline
tabulator tabulator tabulator tabulator tabulator fch: Record "API Financing Contract Header"; newline
tabulator tabulator tabulator tabulator tabulator fue: Record "API Fuel Type"; newline
tabulator tabulator tabulator tabulator tabulator tpl: Record "API Tariff Pricelist"; newline
tabulator tabulator tabulator tabulator tabulator gbx: Record "API Gearbox"; newline
tabulator tabulator tabulator tabulator tabulator chr179: Char; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator chr179 assignment 179; newline
tabulator tabulator tabulator tabulator tabulator ROH.Get("Contract Type", "Contract No."); newline
tabulator tabulator tabulator tabulator tabulator cus.Get(ROH."Renter No."); newline
tabulator tabulator tabulator tabulator tabulator Renter.Get(cus."Primary Contact No."); newline
tabulator tabulator tabulator tabulator tabulator RenterIds assignment Utils.CondCat(Renter."Registration No. CZL", ', ' + tRegNo + ': ') + Utils.CondCat(Renter."VAT Registration No.", ', ' + tVATRegNo + ': '); newline
tabulator tabulator tabulator tabulator tabulator RenterIds assignment CopyStr(RenterIds, 3); newline
tabulator tabulator tabulator tabulator tabulator AUser.Get(ROH."Authorized User 1"); newline
tabulator tabulator tabulator tabulator tabulator if AUser2.Get(ROH."Authorized User 2") then; newline
tabulator tabulator tabulator tabulator tabulator if ROH."Delivery Address" = '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator DelivPlace assignment tWillBeSpecified newline
tabulator tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator DelivPlace assignment Utils.CondCat(ROH."Delivery Address") + Utils.CondCat(ROH."Delivery Address 2") + Utils.CondCat(DelChr(StrSubstNo(' %1 %2', ROH."Delivery Post Code", ROH."Delivery City"), '<')); newline
tabulator tabulator tabulator tabulator tabulator tabulator DelivPlace assignment CopyStr(DelivPlace, 3); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator if ROH."Pick-up Address" notequal '' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator ReturnPlace assignment Utils.CondCat(ROH."Pick-up Address") + Utils.CondCat(ROH."Pick-up Address 2") + Utils.CondCat(DelChr(StrSubstNo(' %1 %2', ROH."Pick-up Post Code", ROH."Pick-up City"), '<')); newline
tabulator tabulator tabulator tabulator tabulator tabulator ReturnPlace assignment CopyStr(ReturnPlace, 3); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator car.Get("Vehicle No."); newline
tabulator tabulator tabulator tabulator tabulator if fch.Get(car."Financing Contract No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator FOB.Get(fch."Financed Object No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator CarModel assignment Utils.CondCat(FOB."Make Name", ' ') + Utils.CondCat(FOB."Model Code", ' '); newline
tabulator tabulator tabulator tabulator tabulator tabulator CarModelType assignment CopyStr(CarModel + Utils.CondCat(FOB."Model Type Name", ' '), 2); newline
tabulator tabulator tabulator tabulator tabulator tabulator CarModel assignment CopyStr(CarModel, 2); newline
tabulator tabulator tabulator tabulator tabulator tabulator gbx.Get(FOB.Gearbox); newline
tabulator tabulator tabulator tabulator tabulator tabulator Gear assignment gbx.Description; newline
tabulator tabulator tabulator tabulator tabulator tabulator fue.Get(FOB."Fuel Type Code"); newline
tabulator tabulator tabulator tabulator tabulator tabulator Fuel assignment fue.Name; newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment FOB."Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator VIN assignment FOB.VIN; newline
tabulator tabulator tabulator tabulator tabulator tabulator EngineCubat assignment LangHndl.FormatVar(FOB."Engine CCM") + ' cm' + Format(chr179); newline
tabulator tabulator tabulator tabulator tabulator tabulator EnginePower assignment LangHndl.FormatVar(FOB."Engine Power (kW)", 0) + ' kW'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Colour assignment FOB."BLG Ext. Colour Description"; newline
tabulator tabulator tabulator tabulator tabulator tabulator Price assignment LangHndl.FormatVar(FOB.ObjPriceInclVATLCY) + ' ' + LangHndl.CurrSymbol(''); newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlate assignment Utils.iif(FOB."Licence Plate No." = '', '__________', FOB."Licence Plate No."); newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator CarModelType assignment car."Car Description"; newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlate assignment car."Car Licence Number"; newline
tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment car."Car Licence Number"; newline
tabulator tabulator tabulator tabulator tabulator tabulator EngineCubat assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator tabulator EnginePower assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Price assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Colour assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator tabulator CarModel assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Gear assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator tabulator Fuel assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator tabulator VIN assignment '-'; newline
tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator CarDescription assignment car."Car Description"; newline
tabulator tabulator tabulator tabulator tabulator tpl.Get("Tariff Group", "Tariff Subgroup", "Pricelist Code"); newline
tabulator tabulator tabulator tabulator tabulator TariffSubgr assignment tpl.Description; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem(AUserLoop; Integer) newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = sorting(Number) where(Number = filter(1 .. 2)); newline
 newline
tabulator tabulator tabulator column(AUserNo; Number) { } newline
tabulator tabulator tabulator column(AUserName; AUser.Name) { } newline
 newline
tabulator tabulator tabulator dataitem(LangLoop; Integer) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting(Number); newline
 newline
tabulator tabulator tabulator tabulator column(LangId; Number) { } newline
tabulator tabulator tabulator tabulator column(Whom; TranslateLbl(tWhom, Number)) { } newline
tabulator tabulator tabulator tabulator column(Sentence1a; StrSubstNo(TranslateLbl(tSentence1a, Number), CompanyManager, CI.Name)) { } newline
tabulator tabulator tabulator tabulator column(Sentence1b; StrSubstNo(TranslateLbl(tSentence1b, Number), CompanyManager, CI.Name)) { } newline
tabulator tabulator tabulator tabulator column(Sentence2a; StrSubstNo(TranslateLbl(tSentence2a, Number), AUser.Name, Renter.Name, LicPlate)) { } newline
tabulator tabulator tabulator tabulator column(Sentence2b; StrSubstNo(TranslateLbl(tSentence2b, Number), AUser.Name, Renter.Name, LicPlate)) { } newline
tabulator tabulator tabulator tabulator column(Sentence2c; StrSubstNo(TranslateLbl(tSentence2c, Number), AUser.Name, Renter.Name, LicPlate)) { } newline
tabulator tabulator tabulator tabulator column(Family; TranslateLbl(tFamily, Number)) { } newline
tabulator tabulator tabulator tabulator column(ManagDirector; TranslateLbl(tManagDirector, Number)) { } newline
 newline
