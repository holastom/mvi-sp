tabulator tabulator tabulator trigger OnPreDataItem() newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator CurrReport.Break(); newline
tabulator tabulator tabulator end; newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem(CHbuff; "Compensation Header CZC") newline
tabulator tabulator { newline
tabulator tabulator tabulator UseTemporary = true; newline
tabulator tabulator tabulator DataItemTableView = SORTING("No."); newline
 newline
tabulator tabulator tabulator column(No; "No.") { } newline
 newline
tabulator tabulator tabulator column(PartnerName; PartnerName) { } newline
tabulator tabulator tabulator column(PartnerAddr; LongAddr("Company Address", "Company Address 2", "Company City", "Company Post Code")) { } newline
tabulator tabulator tabulator column(PartnerAddress; "Company Address") { } newline
tabulator tabulator tabulator column(PartnerCity; "Company City") { } newline
tabulator tabulator tabulator column(PartnerPostCode; "Company Post Code") { } newline
 newline
tabulator tabulator tabulator column(PostingDate; LangHandler.FormatDateSpace("Posting Date")) { } newline
tabulator tabulator tabulator column(PartnerRegNo; PartnerRegNo) { } newline
tabulator tabulator tabulator column(ReturnEMail; ReturnEMail) { } newline
tabulator tabulator tabulator column(Document_Date; LangHandler.FormatVar("Document Date")) { } newline
 newline
tabulator tabulator tabulator dataitem(CLbuff; "Compensation Line CZC") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator UseTemporary = true; newline
tabulator tabulator tabulator tabulator DataItemLink = "Compensation No." = FIELD("No."); newline
tabulator tabulator tabulator tabulator DataItemTableView = SORTING("Compensation No.", "Line No."); newline
 newline
tabulator tabulator tabulator tabulator column(LineNo; "Line No.") { } newline
tabulator tabulator tabulator tabulator column(LineTypeCust; "Source Type" = "Source Type"::Customer) { } newline
tabulator tabulator tabulator tabulator column(LinePostDate_; "Posting Date") { IncludeCaption = true; } newline
tabulator tabulator tabulator tabulator column(LinePostDate; LangHandler.FormatVar("Posting Date")) { } newline
tabulator tabulator tabulator tabulator column(LineDocType; "Document Type") { IncludeCaption = true; } newline
tabulator tabulator tabulator tabulator column(LineDocNo; "Document No.") { IncludeCaption = true; } newline
tabulator tabulator tabulator tabulator column(LineVarSym; "Variable Symbol") { IncludeCaption = true; } newline
tabulator tabulator tabulator tabulator column(LineCurr; Currency) { } newline
tabulator tabulator tabulator tabulator column(LineDueDate; LangHandler.FormatVar(DueDate)) { } newline
tabulator tabulator tabulator tabulator column(LineLEOrigAmt; "Ledg. Entry Original Amount") { IncludeCaption = true; } newline
tabulator tabulator tabulator tabulator column(LineLERemAmt; "Ledg. Entry Remaining Amount") { IncludeCaption = true; } newline
tabulator tabulator tabulator tabulator column(LineAmount; Amount) { IncludeCaption = true; } newline
tabulator tabulator tabulator tabulator column(LineRemAmt; "Remaining Amount") { IncludeCaption = true; } newline
tabulator tabulator tabulator tabulator column(SelfRemRecvPayblLbl; SelfRemRecvPayblLbl) { } newline
tabulator tabulator tabulator tabulator column(PartnerRemRecvPayblLbl; PartnerRemRecvPayblLbl) { } newline
tabulator tabulator tabulator tabulator column(ForVarSymAmtAfterCredit; ForVarSymAmtAfterCredit) { } newline
 newline
