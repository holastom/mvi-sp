tabulator tabulator trigger OnOpenPage() newline
tabulator tabulator begin newline
tabulator tabulator tabulator CIlng assignment LangHandler.GetCompanyLanguageId; newline
tabulator tabulator end; newline
tabulator } newline
 newline
tabulator labels newline
tabulator { newline
tabulator tabulator CultureCode = 'CultureCode', Comment = 'cs-CZ=cs-CZ;en-US=en-US'; newline
tabulator tabulator ReportTitle1 = 'ReportTitle1', Comment = 'cs-CZ=Oznámení o jednostranném zápočtu;en-US=Notice of unilateral set-off'; newline
tabulator tabulator ReportTitle2 = 'ReportTitle2', Comment = 'cs-CZ=Dohoda o vzájemném zápočtu;en-US=Agreement on mutual set-off'; newline
tabulator tabulator ToDateLbl = 'To date', Comment = 'cs-CZ=ke dni;en-US=To date'; newline
tabulator tabulator AgreementNoLbl = 'Agreement no.', Comment = 'cs-CZ=Dohoda číslo;en-US=Agreement no.'; newline
tabulator tabulator NotificationNoLbl = 'Notification no.', Comment = 'cs-CZ=Oznámení číslo;en-US=Notification no.'; newline
tabulator tabulator NameCaptionLbl = 'Business Name:', Comment = 'cs-CZ=Obchodní jméno:;en-US=Business Name:'; newline
tabulator tabulator AddrCaptionLbl = 'Domicile:', Comment = 'cs-CZ=Sídlo:;en-US=Domicile:'; newline
tabulator tabulator BankConnLbl = 'Bank connection:', Comment = 'cs-CZ=Bankovní spojení:;en-US=Bank connection:'; newline
tabulator tabulator LegalText1Lbl = 'LegalText1Lbl', Comment = 'cs-CZ=s odvoláním na § 1982 -1991 zákona č. 89/2012 Sb., Občanského zákoníku, Vám oznamujeme,(\n)že provádíme jednostranný zápočet jistých a určitých pohledávek viz níže.;en-US=With reference to Section 1982 to Section 1991 of Act No. 89/2012 Sb., the Civil Code, we hereby notify you(\n)that we are carrying out a unilateral set-off of certain and definite receivables, see below.'; newline
tabulator tabulator LegalText2Lbl = 'by reciprocally credit receivables by PAR. 1982-1991 of Civil Code No. 89/2012', Comment = 'cs-CZ=o vzájemném započtení pohledávek dle §1982-1991 Občanského zákoníku č. 89/2012;en-US=by reciprocally credit receivables by PAR. 1982-1991 of Civil Code No. 89/2012'; newline
tabulator tabulator FinalText1Lbl = 'FinalText1Lbl', Comment = 'cs-CZ=Tento zápočet nabývá platnosti dnem vystavení.;en-US=This set-off shall take effect on the date of issue.'; newline
tabulator tabulator FinalText2Lbl = 'FinalText2Lbl', Comment = 'cs-CZ=Tato dohoda nabývá platnosti a účinnosti dnem podpisu obou smluvních stran. Neobdržíme-li Vaši odpověď do 5 pracovních dní, budeme brát tento zápočet za odsouhlasený.;en-US=This agreement enters into force and effect on the date of execution by both parties. If we do not receive your response within 5 working days, we will consider this set-off to have been approved.'; newline
tabulator tabulator IssuedOnLbl = 'IssuedOnLbl', Comment = 'cs-CZ=Vystaveno dne:;en-US=Issued on:'; newline
tabulator tabulator IssuedInLbl = 'In Prague on', Comment = 'cs-CZ=V Praze dne;en-US=In Prague on'; newline
tabulator tabulator NameSignatureLbl = 'NameSignatureLbl', Comment = 'cs-CZ=jméno a podpis oprávněné osoby;en-US=name and signature of the authorized person'; newline
tabulator tabulator ReturnLbl = 'ReturnLbl', Comment = 'cs-CZ=V případě souhlasu zašlete jedno potvrzené provedení dohody zpět na naši e-mailovou adresu;en-US=If you agree, please send one confirmed copy of the agreement back to our address.'; newline
 newline
tabulator tabulator RcvblPayblsLbl = 'Receivables and Payables', Comment = 'cs-CZ=Pohledávky a závazky;en-US=Receivables and Payables'; newline
tabulator tabulator RemAmtLbl = 'Remaining Amount', Comment = 'cs-CZ=Zůstatek po zápočtu;en-US=Remaining Amount'; newline
tabulator tabulator AmountLbl = 'Amount', Comment = 'cs-CZ=Částka k započtení;en-US=Amount'; newline
tabulator tabulator LERemAmtLbl = 'Ledg. Entry Remaining Amount', Comment = 'cs-CZ=Částka k úhradě;en-US=Ledg. Entry Remaining Amount'; newline
tabulator tabulator LEOrigAmtLbl = 'Ledg. Entry Original Amount', Comment = 'cs-CZ=Původní částka;en-US=Ledg. Entry Original Amount'; newline
tabulator tabulator DueDateLbl = 'Due Date', Comment = 'cs-CZ=Datum splatnosti;en-US=Due Date'; newline
tabulator tabulator AgreementForceLbl = 'This agreement shall enter into force upon signature by both parties.', Comment = 'cs-CZ=Tato dohoda nabývá platnosti a účinnosti dnem podpisu obou smluvních stran.;en-US=This agreement shall enter into force upon signature by both parties.'; newline
tabulator tabulator NameSigLbl = 'Name and Signature', Comment = 'cs-CZ=(Jméno a podpis odpovědné osoby);en-US=(Name and Signature of the Authorized Person)'; newline
tabulator tabulator InDateLblA = 'InLbl', Comment = 'cs-CZ=V ____________________;en-US=In ____________________'; newline
tabulator tabulator InDateLblB = 'DateLbl', Comment = 'cs-CZ=dne ___________________;en-US=date __________________'; newline
tabulator tabulator ConfirmLbl = 'In case you agree, send one confirmed agreement back to our address.', Comment = 'cs-CZ=V případě souhlasu zašlete jedno potvrzené provedení dohody zpět na naši adresu.;en-US=In case you agree, send one confirmed agreement back to our address.'; newline
tabulator tabulator ForLbl = 'For', Comment = 'cs-CZ=za ;en-US=For '; newline
tabulator tabulator WorkDateLbl = 'Make Date', Comment = 'cs-CZ=Vystaveno dne;en-US=Make Date'; newline
tabulator tabulator CurrencyLbl = 'Currency', Comment = 'cs-CZ=Měna;en-US=Currency'; newline
tabulator tabulator ContactPersonLbl = 'Contact person', Comment = 'cs-CZ=Kontaktní osoba;en-US=Contact person'; newline
tabulator tabulator PhoneLbl = 'Telephone', Comment = 'cs-CZ=Telefon;en-US=Telephone'; newline
tabulator tabulator EMailLbl = 'E-mail', Comment = 'cs-CZ=E-mail;en-US=E-mail'; newline
tabulator tabulator RegNoLbl = 'Reg. No.', Comment = 'cs-CZ=IČO;en-US=Reg. No.'; newline
tabulator tabulator VATRegNoLbl = 'VAT Reg.No', Comment = 'cs-CZ=DIČ;en-US=VAT Reg.No'; newline
tabulator tabulator VendorLbl = 'Vendor', Comment = 'cs-CZ=Dodavatel;en-US=Vendor'; newline
tabulator tabulator PartnerLbl = 'Partner', Comment = 'cs-CZ=Zákazník;en-US=Partner'; newline
tabulator } newline
 newline
tabulator var newline
tabulator tabulator LangHandler: Codeunit LanguageHandler; newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator ReqPageLanguage: Enum LanguageEnum; newline
tabulator tabulator BLGSurnameSalutation: Text; newline
tabulator tabulator SurnameSalutationDescription: Text; newline
tabulator tabulator Description_: Text; newline
tabulator tabulator runas: Option unposted,posted; newline
tabulator tabulator CountryRegion: Record "Country/Region"; newline
tabulator tabulator VendLedgEntry: Record "Vendor Ledger Entry"; newline
tabulator tabulator CustLedgEntry: Record "Cust. Ledger Entry"; newline
tabulator tabulator GLSetup: Record "General Ledger Setup"; newline
tabulator tabulator UserSetup: Record "User Setup"; newline
tabulator tabulator Employee: Record Employee; newline
tabulator tabulator PartnerName: Text; newline
tabulator tabulator PartnerRegNo: Text; newline
tabulator tabulator ReturnEMail: Text; newline
tabulator tabulator Currency: Text; newline
tabulator tabulator DueDate: Date; newline
tabulator tabulator OneSidedInclusion: Boolean; newline
tabulator tabulator tRemainingReceivablesAndPayables: Label 'Remaining Receivable and Payables of %1 to pay after realize Credit.', Comment = 'cs-CZ=Po započtení pohledávek a závazků zůstává uhradit doklad firmy %1.'; //%1=Company Name newline
tabulator tabulator SelfRemRecvPayblLbl: Text; newline
tabulator tabulator PartnerRemRecvPayblLbl: Text; newline
tabulator tabulator tForVarSymAmtAfterCredit: Label 'for the variable symbol %1 are %2 %3 after the credit.', Comment = 'cs-CZ=pod variabilním symbolem %1 ve výši %2 %3.'; //%1=variable symbol;%2=remaining amount;%3=currency code newline
tabulator tabulator ForVarSymAmtAfterCredit: Text; newline
tabulator tabulator VendorRegNoVATRegNo: Text; newline
tabulator tabulator RegNoLbl: Label 'Reg. No.', Comment = 'cs-CZ=IČO;en-US=Reg. No.'; newline
tabulator tabulator VATRegNoLbl: Label 'VAT Reg.No', Comment = 'cs-CZ=DIČ;en-US=VAT Reg.No'; newline
 newline
