tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FinancedObject: Record "API Financed Object"; newline
 newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
 newline
tabulator tabulator SheetName: Label 'Odometer Request', Comment = 'cs-CZ=Odometer Request;en-US=Odometer Request'; newline
tabulator tabulator FileName: Label 'Odometer_Request_%1_%2', Comment = 'cs-CZ=Odometer_Request_%1_%2;en-US=Odometer_Request_%1_%2'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
tabulator tabulator CreateHeader(); newline
 newline
tabulator tabulator FinancingContractHeader.SetRange("Customer No.", Customer."No."); newline
tabulator tabulator FinancingContractHeader.SetRange(Status, "API Financing Contract Status"::Active); newline
tabulator tabulator FinancingContractHeader.SetFilter("Financing Product Type Code", FinancingProductType); newline
tabulator tabulator FinancingContractHeader.SetRange("Change Copy", false); newline
tabulator tabulator FinancingContractHeader.SetRange("Calculation Variant", false); newline
tabulator tabulator FinancingContractHeader.SetCurrentKey("Expected Termination Date"); newline
tabulator tabulator FinancingContractHeader.SetAscending("Expected Termination Date", false); newline
 newline
tabulator tabulator if FinancingContractHeader.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator if FinancedObject.Get(FinancingContractHeader."Financed Object No.") then begin newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer."Registration No. CZL", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Customer.Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancedObject."Make Name" + ' ' + FinancedObject."Model Name" + ' ' + FinancedObject."Model Type Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Contract Signing Date (Comp.)", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Expected Termination Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until FinancingContractHeader.Next() = 0; newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator BLFileManager.CustomizeCells(TmpExcelBuffer, 9); newline
tabulator tabulator CreateCellWidth(); newline
 newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, Customer.Name, Format(Today, 0, '<Day,2>.<Month,2>.<Year4>')) + '.xlsx'); newline
tabulator end; newline
