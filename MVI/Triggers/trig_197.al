tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() //CMLbuf newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator tAliqPayment: Label 'Poměrné nájemné', Comment = 'cs-CZ=Poměrné nájemné'; newline
tabulator tabulator tabulator tabulator tabulator tRecalcPayment: Label 'Rekalkulační vyúčtování', Comment = 'cs-CZ=Rekalkulační vyúčtování'; newline
tabulator tabulator tabulator tabulator tabulator tRegularPayment: Label 'Řádné nájemné', Comment = 'cs-CZ=Řádné nájemné'; newline
tabulator tabulator tabulator tabulator tabulator tServKind: Label 'Typ služby', Comment = 'cs-CZ=Typ služby'; newline
tabulator tabulator tabulator tabulator tabulator tDescription: Label 'Popis', Comment = 'cs-CZ=Popis'; newline
tabulator tabulator tabulator tabulator tabulator tReinvReason: Label 'Důvod přefakturace', Comment = 'cs-CZ=Důvod přefakturace'; newline
tabulator tabulator tabulator tabulator tabulator tComment: Label 'Komentář', Comment = 'cs-CZ=Komentář'; newline
tabulator tabulator tabulator tabulator tabulator tApprNo: Label 'Schvalovací číslo', Comment = 'cs-CZ=Schvalovací číslo'; newline
tabulator tabulator tabulator tabulator tabulator tApprDate: Label 'Datum schválení', Comment = 'cs-CZ=Datum schválení'; newline
tabulator tabulator tabulator tabulator tabulator tProdPurch: Label 'Značka', Comment = 'cs-CZ=Značka'; newline
tabulator tabulator tabulator tabulator tabulator tRegNumber: Label 'Evidenční počet', Comment = 'cs-CZ=Evidenční počet'; newline
tabulator tabulator tabulator tabulator tabulator tInsurClaim: Label 'Číslo pojistné události', Comment = 'cs-CZ=Číslo pojistné události'; newline
tabulator tabulator tabulator tabulator tabulator tInsClaimDate: Label 'Datum vzniku události', Comment = 'cs-CZ=Datum vzniku události'; newline
tabulator tabulator tabulator tabulator tabulator tRentCarDate: Label 'Datum zapůjčení vozidla', Comment = 'cs-CZ=Datum zapůjčení vozidla'; newline
tabulator tabulator tabulator tabulator tabulator tVehReturnDate: Label 'Datum navrácení vozidla', Comment = 'cs-CZ=Datum navrácení vozidla'; newline
tabulator tabulator tabulator tabulator tabulator tRental: Label 'Nájemné autopůjčovny', Comment = 'cs-CZ=Nájemné autopůjčovny'; newline
tabulator tabulator tabulator tabulator tabulator tNewTariff: Label 'Vyrovnání sazby za pronájem vozidla', Comment = 'cs-CZ=Vyrovnání sazby za pronájem vozidla'; newline
tabulator tabulator tabulator tabulator tabulator tPerPeriod: Label 'za období', Comment = 'cs-CZ=za období'; newline
tabulator tabulator tabulator tabulator tabulator tPerPeriodVar: Label 'Období', Comment = 'cs-CZ=Období'; newline
tabulator tabulator tabulator tabulator tabulator tLicPlNo: Label 'RZ', Comment = 'cs-CZ=RZ'; newline
tabulator tabulator tabulator tabulator tabulator tRentDurationD: Label 'počet dnů zápůjčky', Comment = 'cs-CZ=počet dnů zápůjčky'; newline
tabulator tabulator tabulator tabulator tabulator tTariffD: Label 'denní sazba', Comment = 'cs-CZ=denní sazba'; newline
tabulator tabulator tabulator tabulator tabulator tNew: Label 'nová', Comment = 'cs-CZ=nová'; newline
tabulator tabulator tabulator tabulator tabulator tRentDurationM: Label 'počet měsíců zápůjčky', Comment = 'cs-CZ=počet měsíců zápůjčky'; newline
tabulator tabulator tabulator tabulator tabulator tTariffM: Label 'měsíční sazba', Comment = 'cs-CZ=měsíční sazba'; newline
tabulator tabulator tabulator tabulator tabulator tExclVAT: Label 'bez DPH', Comment = 'cs-CZ=bez DPH'; newline
tabulator tabulator tabulator tabulator tabulator tDriverName: Label 'jméno řidiče', Comment = 'cs-CZ=jméno řidiče'; newline
tabulator tabulator tabulator tabulator tabulator tActKilometers: Label 'Aktuální stav kilometrů - pro fakturaci', Comment = 'cs-CZ=Aktuální stav kilometrů - pro fakturaci'; newline
tabulator tabulator tabulator tabulator tabulator tRefuel: Label 'Dotankování PHM', Comment = 'cs-CZ=Dotankování PHM'; newline
tabulator tabulator tabulator tabulator tabulator tExcKmFee: Label 'Poplatek za přejeté kilometry', Comment = 'cs-CZ=Poplatek za přejeté kilometry'; newline
tabulator tabulator tabulator tabulator tabulator tCarPullFee: Label 'Poplatek za přistavení vozidla', Comment = 'cs-CZ=Poplatek za přistavení vozidla'; newline
tabulator tabulator tabulator tabulator tabulator tCarPullCount: Label 'Počet přistavení', Comment = 'cs-CZ=Počet přistavení'; newline
tabulator tabulator tabulator tabulator tabulator tCarPullPrice: Label 'Cena za 1 přistavení', Comment = 'cs-CZ=Cena za 1 přistavení'; newline
tabulator tabulator tabulator tabulator tabulator tCarReturned: Label 'Vozidlo vráceno', Comment = 'cs-CZ=Vozidlo vráceno'; newline
tabulator tabulator tabulator tabulator tabulator tBillingKm: Label 'Vyúčtování +/- km', Comment = 'cs-CZ=Vyúčtování +/- km'; newline
tabulator tabulator tabulator tabulator tabulator tOdometerState: Label 'Stav tachometru', Comment = 'cs-CZ=Stav tachometru'; newline
tabulator tabulator tabulator tabulator tabulator tExpOdometerState: Label 'Očekávaný stav tachometru', Comment = 'cs-CZ=Očekávaný stav tachometru'; newline
tabulator tabulator tabulator tabulator tabulator tRecalcTotal: Label 'Rekalkulace při ukončení', Comment = 'cs-CZ=Rekalkulace při ukončení'; newline
tabulator tabulator tabulator tabulator tabulator txt: Text; newline
tabulator tabulator tabulator tabulator tabulator ic: Record "API Insurance Claim"; newline
tabulator tabulator tabulator tabulator tabulator rcb: Record "API RC Car Rental Billing"; newline
tabulator tabulator tabulator tabulator tabulator cont: Record Contact; newline
tabulator tabulator tabulator tabulator tabulator mrs: Record "BLG Mileage Rate Settlement"; newline
tabulator tabulator tabulator tabulator tabulator ict: Record "API Insurance Claim Type"; newline
tabulator tabulator tabulator tabulator tabulator rr: Record "BLG Reinvoicing Reason"; newline
tabulator tabulator tabulator tabulator tabulator fob: Record "API Financed Object"; newline
tabulator tabulator tabulator tabulator tabulator fp: Record "API Financing Product/Template"; newline
tabulator tabulator tabulator tabulator tabulator objDriverHist: Record "API Object Driver History"; newline
tabulator tabulator tabulator tabulator tabulator ccc: Record "BLG Contract Cost Center"; newline
tabulator tabulator tabulator tabulator tabulator contCostCenter: Record "BLG Contact Cost Center"; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator Clear(FS); newline
tabulator tabulator tabulator tabulator tabulator SumDetLines assignment false; //použito pro grupování detailových řádek v Consol. faktuře u třá typů Settlementů newline
 newline
tabulator tabulator tabulator tabulator tabulator if (FCH."No." notequal "Shortcut Dimension 2 Code") or (FCH.Get("Shortcut Dimension 2 Code")) then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator FCH.CalcFields("Licence Plate No.", "BLG Driver Name", "BLG Cost Center Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator CML_CostCenterName assignment FCH."BLG Cost Center Name"; newline
tabulator tabulator tabulator tabulator tabulator tabulator if FCH."Master Agreement No." notequal '' then FCH_MA.Get(FCH."Master Agreement No."); newline
tabulator tabulator tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(FCH); newline
 newline
tabulator tabulator tabulator tabulator tabulator ContractNo assignment "Shortcut Dimension 2 Code"; newline
tabulator tabulator tabulator tabulator tabulator LineDescription assignment Description; newline
tabulator tabulator tabulator tabulator tabulator ContrDriverName assignment FCH."BLG Driver Name"; newline
tabulator tabulator tabulator tabulator tabulator DriverName assignment ContrDriverName; newline
tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment FCH."Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator if fob.Get(FCH."Financed Object No.") then FinObjName assignment StrSubstNo('%1 %2 %3', fob."Make Name", fob."Model Name", fob."Model Type Name") else FinObjName assignment ''; newline
tabulator tabulator tabulator tabulator tabulator case InvoiceType of newline
tabulator tabulator tabulator tabulator tabulator tabulator InvoiceType::P: newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if (FCL."Financing Contract No." notequal "Shortcut Dimension 2 Code") or (FCL.Type = "API Contract Line Type") or (FCL."Line No." notequal "API Contract Line No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FCL.Get("Shortcut Dimension 2 Code", "API Contract Line Type", "API Contract Line No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator case true of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FCL."Aliquot Payment" or FCL."Partial Payment Credit": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment tAliqPayment; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_FCL_DateFr assignment LanguageHandler.FormatDateNoSpace(FCL."Date From"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_FCL_DateTo assignment LanguageHandler.FormatDateNoSpace(FCL."Date To"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FCL."Recalculation Settlement": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment tRecalcPayment; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_FCL_DateFr assignment LanguageHandler.FormatDateNoSpace(FCL."Recalc. Settl. Period From"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_FCL_DateTo assignment LanguageHandler.FormatDateNoSpace(FCL."Recalc. Settl. Period To"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment tRegularPayment; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_FCL_DateFr assignment LanguageHandler.FormatDateNoSpace(FCL."Date From"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_FCL_DateTo assignment LanguageHandler.FormatDateNoSpace(FCL."Date To"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineID assignment 'SumLine' + '-' + FCL."Part Payment No." + '-' + "VAT Identifier"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator InvoiceType::C: newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator //Konsolidované fa. mají řádky 4 typů: newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // Services - pokud je neprázdný "API Maintenance Approval No." newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // Settlement - pokud je neprázdný "API Fin. Settlement No." newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // Rent - pokud je neprázdný "API Rent No." newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // ostatní (?) newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator case true of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "API Maintenance Approval No." notequal '': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SumDetLines assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator S_Section assignment "API Maintenance Approval No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPH."No." notequal "API Maintenance Approval No." then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPH.Get("API Maintenance Approval No.") then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator MPH.CalcFields("Driver Name", "Customer Business Place Name") newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(MPH); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator DriverName assignment MPH."Driver Name"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPL.Get("API Maintenance Approval No.", "BLG Maint. Perm. Line No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator case MPL."Service Kind" of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator MPL."Service Kind"::"Tire Service": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator MPL."Service Kind"::"Insurance Event": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 2; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator MPL."Service Kind"::Maintenance: newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 3; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator MPL."Service Kind"::"Replacement Car": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 4; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator MPL."Service Kind"::"Fee/Service": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 5; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 6; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment ''; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPL."Service Kind" notequal MPL."Service Kind"::"Insurance Event" then LineDescription += lCondCat(MPH."Approval No.", tApprNo); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if not (MPL."Service Kind" in [MPL."Service Kind"::"Insurance Event", MPL."Service Kind"::"Replacement Car"]) then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(LanguageHandler.FormatVar(MPH."Approval Date"), tApprDate); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(Format(MPL."Service Kind"), tServKind); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(MPL.Description, tDescription); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(MPL."BLG Reinv. Reason Description", tReinvReason); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(MPL."Comment for Customer", tComment); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPL."Service Kind" = MPL."Service Kind"::"Tire Service" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPL."Registered Number" notequal 0 then LineDescription += lCondCat(LanguageHandler.FormatVar(MPL."Registered Number"), tRegNumber); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(MPL."BLG Producer Purchase", tProdPurch); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPL."Service Kind" = MPL."Service Kind"::"Insurance Event" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ic.Get(MPL."Insurance Claim No."); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator objDriverHist.SetRange("Financed Object No.", fob."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator objDriverHist.SetFilter("Date From", '<=%1', ic."Event Creation Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator objDriverHist.SetCurrentKey("Date From"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if objDriverHist.FindLast() then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator DriverName assignment objDriverHist."Driver Name" newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(DriverName); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ccc.SetRange("Financing Contract No.", FCH."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ccc.SetFilter("Date From", '<=%1', ic."Event Creation Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ccc.SetCurrentKey("Date From"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if (ccc.FindLast()) and contCostCenter.Get(ccc."Contact No.", ccc.Code) then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_CostCenterName assignment contCostCenter.Name newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(CML_CostCenterName); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(LanguageHandler.FormatDateSpace(ic."Event Creation Date"), tInsClaimDate); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(MPL."Insurance Claim No.", tInsurClaim); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if MPL."Service Kind" in [MPL."Service Kind"::Maintenance, MPL."Service Kind"::"Replacement Car", MPL."Service Kind"::"Insurance Event", MPL."Service Kind"::"Fee/Service"] then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(LanguageHandler.FormatVar(MPL."BLG Rental Car Date"), tRentCarDate); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += lCondCat(LanguageHandler.FormatVar(MPL."BLG Vehicle Return Date"), tVehReturnDate); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment CopyStr(LineDescription, 3); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(MPL); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 7; //fallback - nemělo by ale nastat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "API Fin. Settlement No." notequal '': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ContractNo assignment "Shortcut Dimension 2 Code"; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator fs.Get("API Fin. Settlement No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator fp.Get("API Fin. Prod./Templ. Type"::"Financing Product", fs."Financing Product No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator case fs."Fin. Settlement Type" of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_EARLY_1', 'OL_EARLY_2', 'OL_EARLY_3', 'OL_EARLY_4': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 10; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment Description; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'CFM_OPEN': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 10; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 4; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment FinSettlDesc; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator S_Section assignment "API Fin. Settlement No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SumDetLines assignment true; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if FSExists() then begin //udržuje správné množství řádků i korektní sumace newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(FS); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(OSS); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OSS.SetRange("Financial Settlement Code", fs.Code); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OSS.CalcSums("Service Revenues", "Service Costs", "Service Balance"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinalSettlementProductText assignment ParseAndTrimDesc(fp.Name, '|'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_KM_CHAR': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 11; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment StrSubstNo('%1: %2 %3; ', tBillingKm, tCarReturned, LanguageHandler.FormatVar(fs."Contract Termination Date")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo('%1: %2 km; ', tOdometerState, LanguageHandler.FormatVar(fs."Mileage upon Return")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo('%1: %2 km; ', tExpOdometerState, LanguageHandler.FormatVar(fs."Expected Mileage")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator mrs.SetRange("Financial Settlement Code", "API Fin. Settlement No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if mrs.FindSet then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator txt += StrSubstNo(', %1 km x %2 %4 (%3 %4)', LanguageHandler.FormatVar(mrs."Applied Mileage"), LanguageHandler.FormatVar(mrs."Rate Excl.VAT"), LanguageHandler.FormatVar(mrs."Settlement Excl.VAT"), Currency); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator until mrs.Next = 0; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator txt assignment CopyStr(txt, 3); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += txt; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator S_Section assignment "API Fin. Settlement No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SumDetLines assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_OPEN': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 2; //samostatná sekce newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment ''; //nepoužívá se newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment fob."Licence Plate No."; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if FSExists() then begin //udržuje správné množství řádků i korektní sumace newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(FS); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(OSS); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OSS.SetRange("Financial Settlement Code", fs.Code); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OSS.CalcSums("Service Revenues", "Service Costs", "Service Balance"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinalSettlementProductText assignment ParseAndTrimDesc(fp.Name, '|'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_REVISIO': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 12; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator S_Section assignment "API Fin. Settlement No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SumDetLines assignment true; //TODO: původně bylo nevím proč assignment fs."Fin. Settlement Status" = fs."Fin. Settlement Status"::Released; //řádky settlementu s Released se budou sčítat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OL_REVISIO_Exists assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_FCH_DateFr assignment LanguageHandler.FormatDateNoSpace(FCH."Handover Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_FCH_DateTo assignment LanguageHandler.FormatDateNoSpace(FCH."Termination Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment StrSubstNo('%1: %2 %3; ', tRecalcTotal, tCarReturned, LanguageHandler.FormatVar(fs."Contract Termination Date")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo('%1: %2 km; ', tOdometerState, LanguageHandler.FormatVar(fs."Mileage upon Return")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo('%1: %2 - %3 ', tPerPeriodVar, CML_FCH_DateFr, CML_FCH_DateTo); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_THEFT', 'OL_TOT': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 13; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if fs."Fin. Settlement Type" = 'OL_THEFT' then ict.SetRange(Theft, true) else ict.SetRange("Total Damage", true); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ict.FindFirst; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment ict.Description + '; ' + tInsurClaim + ': ' + fs."Insurance Claim No." + '; ' + Description + Utils.iif(fs."Comment for Customer" notequal '', '; ' + fs."Comment for Customer", ''); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Error('Unsupported %1 = %2', fs.FieldCaption("Fin. Settlement Type"), fs."Fin. Settlement Type"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "API Rent No." notequal '': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 3; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ContractNo assignment "API Rent No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if ROH."No." notequal "API Rent No." then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if ROH.Get(ROH."Contract Type"::Rent, "API Rent No.") then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ROL.SetRange("Contract Type", ROH."Contract Type"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ROL.SetRange("Contract No.", ROH."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if not ROL.FindFirst then Clear(ROL); //řádek má být vždy jediný newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ROL.CalcFields("Car Description"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ROH.CalcFields("BLG Cost Center Name"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CML_CostCenterName assignment ROH."BLG Cost Center Name"; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(ROH); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(ROL); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(CML_CostCenterName); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinObjName assignment ROL."Car Description"; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment "API RC Licence Plate No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator DriverName assignment Utils.iif(cont.Get(ROH."Authorized User 1"), cont.Name, ROH."Authorized User 1"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator rcb.Get("API Rent Billing Entry No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator case rcb."Rent a Car Type" of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator rcb."Rent a Car Type"::"Rent Amount", rcb."Rent a Car Type"::" ": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if rcb."Rent a Car Type" = rcb."Rent a Car Type"::"Rent Amount" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment tRental; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 2; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment tNewTariff; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo(' %1 %2 - %3', tPerPeriod, LanguageHandler.FormatDateSpace(rcb."Rent From"), LanguageHandler.FormatDateSpace(rcb."Rent To")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if rcb."Rent a Car Type" = rcb."Rent a Car Type"::" " then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo(', %1: %2', tLicPlNo, "API RC Licence Plate No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if rcb."Monthly Rent" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo(', %1: %2,', tRentDurationM, rcb."Invoiced Months"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if rcb."Rent a Car Type" = rcb."Rent a Car Type"::" " then LineDescription += ' ' + tNew; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo(' %1: %2', tTariffM, LanguageHandler.FormatVar(rcb."Tariff (Month)")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo(', %1: %2,', tRentDurationD, rcb."Invoiced Days"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if rcb."Rent a Car Type" = rcb."Rent a Car Type"::" " then LineDescription += ' ' + tNew; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo(' %1: %2', tTariffD, LanguageHandler.FormatVar(rcb."Tariff (Day)")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo(' %1 %2', Currency, tExclVAT); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator //if cont.Get(ROH."Authorized User 1") then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator //tabulator LineDescription += StrSubstNo(', %1: %2', tDriverName, cont.Name); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator rcb."Rent a car Type"::"Fuels Amount": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 3; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment tRefuel; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator rcb."Rent a Car Type"::"Price Per Posted Kms": newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 4; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment StrSubstNo('%1: %2 km', tExcKmFee, LanguageHandler.FormatVar(ROL."Posted Kilometers")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator rcb."Rent a Car Type"::Trips: newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 5; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment StrSubstNo('%1; %2: %3; %4', tCarPullFee, tCarPullCount, LanguageHandler.FormatVar(Quantity), tCarPullPrice, LanguageHandler.FormatVar("Unit Price")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else begin //všechny ostatní typy "Rent a Car Type" newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if rcb."Rent a Car Type" = rcb."Rent a Car Type"::"Lump-sum Compensation" then LineOrder assignment 6 else LineOrder assignment 7; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if rr.Get("BLG Reinvoicing Reason") then LineDescription assignment StrSubstNo('%1, %2', rr.Description, LineDescription); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else //fallback pro ostatní řádky newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 100; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if SumDetLines then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineID assignment 'SumLine' + '-' + S_Section + '-' + Format("VAT %") newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineID assignment 'Line' + '-' + Format("Line No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator InvoiceType::O: newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // One-Shot fa. - přidány řádky Settlement: newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // Settlement - pokud je neprázdný "API Fin. Settlement No." newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator // ostatní (?) newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator case true of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator "API Fin. Settlement No." notequal '': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 5; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ContractNo assignment "Shortcut Dimension 2 Code"; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator fs.Get("API Fin. Settlement No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator fp.Get("API Fin. Prod./Templ. Type"::"Financing Product", fs."Financing Product No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator case fs."Fin. Settlement Type" of newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_EARLY_1', 'OL_EARLY_2', 'OL_EARLY_3', 'OL_EARLY_4': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 10; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment Description; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'CFM_OPEN': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 10; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 4; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment FinSettlDesc; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator S_Section assignment "API Fin. Settlement No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SumDetLines assignment true; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if FSExists() then begin //udržuje správné množství řádků i korektní sumace newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(FS); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(OSS); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OSS.SetRange("Financial Settlement Code", fs.Code); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OSS.CalcSums("Service Revenues", "Service Costs", "Service Balance"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinalSettlementProductText assignment ParseAndTrimDesc(fp.Name, '|'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_KM_CHAR': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 11; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment StrSubstNo('%1: %2 %3; ', tBillingKm, tCarReturned, LanguageHandler.FormatVar(fs."Contract Termination Date")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo('%1: %2 km; ', tOdometerState, LanguageHandler.FormatVar(fs."Mileage upon Return")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo('%1: %2 km; ', tExpOdometerState, LanguageHandler.FormatVar(fs."Expected Mileage")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator mrs.SetRange("Financial Settlement Code", "API Fin. Settlement No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if mrs.FindSet then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator txt += StrSubstNo(', %1 km x %2 %4 (%3 %4)', LanguageHandler.FormatVar(mrs."Applied Mileage"), LanguageHandler.FormatVar(mrs."Rate Excl.VAT"), LanguageHandler.FormatVar(mrs."Settlement Excl.VAT"), Currency); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator until mrs.Next = 0; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator txt assignment CopyStr(txt, 3); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += txt; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator S_Section assignment "API Fin. Settlement No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SumDetLines assignment true; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_OPEN': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 2; //samostatná sekce newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment ''; //nepoužívá se newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LicPlateNo assignment fob."Licence Plate No."; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if FSExists() then begin //udržuje správné množství řádků i korektní sumace newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(FS); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(OSS); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OSS.SetRange("Financial Settlement Code", fs.Code); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator OSS.CalcSums("Service Revenues", "Service Costs", "Service Balance"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinalSettlementProductText assignment ParseAndTrimDesc(fp.Name, '|'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_REVISIO': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 12; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator S_Section assignment "API Fin. Settlement No."; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator SumDetLines assignment true; //TODO: původně bylo nevím proč assignment fs."Fin. Settlement Status" = fs."Fin. Settlement Status"::Released; //řádky settlementu s Released se budou sčítat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment StrSubstNo('%1: %2 %3; ', tRecalcTotal, tCarReturned, LanguageHandler.FormatVar(fs."Contract Termination Date")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription += StrSubstNo('%1: %2 km ', tOdometerState, LanguageHandler.FormatVar(fs."Mileage upon Return")); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator 'OL_THEFT', 'OL_TOT': newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineOrder assignment 13; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if fs."Fin. Settlement Type" = 'OL_THEFT' then ict.SetRange(Theft, true) else ict.SetRange("Total Damage", true); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator ict.FindFirst; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineDescription assignment ict.Description; // + '; ' + tInsurClaim + ': ' + fs."Insurance Claim No." + '; ' + Description + Utils.iif(fs."Comment for Customer" notequal '', '; ' + fs."Comment for Customer", ''); //SIK - popisy neimplementovány pro One-Shot doklady newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Error('Unsupported %1 = %2', fs.FieldCaption("Fin. Settlement Type"), fs."Fin. Settlement Type"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator CO_Section assignment 5; // typ OneShot faktury vždy padá do sekce 5 newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator if SumDetLines then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineID assignment 'SumLine' + '-' + S_Section + '-' + Format("VAT %") newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator LineID assignment 'Line' + '-' + Format("Line No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } //CMLbuf newline
 newline
tabulator tabulator tabulator dataitem(FCT; "API Fuel Card Transaction") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("Entry No."); newline
tabulator tabulator tabulator tabulator DataItemLink = "Posted Sales Invoice No." = field("No."); newline
tabulator tabulator tabulator tabulator #region FCTcolumns newline
tabulator tabulator tabulator tabulator column(FCT___; '') { } newline
tabulator tabulator tabulator tabulator column(FCT_ProdCode; "Product Code") { } newline
tabulator tabulator tabulator tabulator column(FCT_ContractNo; "Financing Contract No.") { } newline
tabulator tabulator tabulator tabulator column(FCT_LineID; LineID) { } newline
tabulator tabulator tabulator tabulator column(FCT_CardID; CardID) { } newline
tabulator tabulator tabulator tabulator column(FCT_LicPlate; "Licence Plate No.") { } newline
tabulator tabulator tabulator tabulator column(FCT_Quantity; Quantity) { } newline
tabulator tabulator tabulator tabulator column(FCT_TransactDate; "Transaction Date") { } newline
tabulator tabulator tabulator tabulator column(FCT_TotPrExVAT; Utils.FlipSign("Tot. Pr. Ex. VAT (LCY) - Cust.")) { } newline
tabulator tabulator tabulator tabulator column(FCT_PriceInclVAT; "Price Total Incl. VAT") { } newline
tabulator tabulator tabulator tabulator column(FCT_VATpct; "VAT Rate % - Customer") { } newline
tabulator tabulator tabulator tabulator column(FCT_CardVendName; "Card Vendor Name") { } newline
tabulator tabulator tabulator tabulator column(FCT_CardNoCompany; "Card No. - Company") { } newline
tabulator tabulator tabulator tabulator column(FCT_ItemName; "Item Name") { } newline
tabulator tabulator tabulator tabulator column(FCT_FinObjName; FinObjName) { } newline
tabulator tabulator tabulator tabulator column(FCT_DriverName; DriverName) { } newline
tabulator tabulator tabulator tabulator column(FCT_CostCenter; "BLG Customer Cost Center Name") { } newline
tabulator tabulator tabulator tabulator column(FCT_CostCenter_Sorting; Utils.iif("BLG Customer Cost Center Name".Trim() = '', '1', '0')) { } newline
tabulator tabulator tabulator tabulator #endregion FCTcolumns newline
 newline
