tabulator trigger OnPreReport() newline
tabulator var newline
tabulator tabulator ErrorNoFS: Label 'Financial Settlement is Empty'; newline
tabulator begin newline
tabulator tabulator CI.Get; newline
tabulator tabulator FIS.copy(FS); newline
tabulator tabulator FIS.FindFirst; newline
tabulator tabulator FCH.Get(fis."Financing Contract No."); newline
tabulator tabulator FIS.Reset; newline
tabulator tabulator FIS.SetFilter("Fin. Settlement Type", '%1|%2|%3|%4', 'OL_EARLY_1', 'OL_EARLY_2', 'OL_EARLY_3', 'OL_EARLY_4'); newline
tabulator tabulator FIS.SetRange("Financial Settlement Date", CalcDate('<-CM>', Today), Today); newline
tabulator tabulator if FIS.IsEmpty then newline
tabulator tabulator tabulator Error(ErrorNoFS); newline
tabulator tabulator Cust.Get(FCH."Customer No."); newline
tabulator tabulator CurrReport.Language assignment LangHndl.ReportLanguage(RequestPageLanguage, Cust."Language Code"); newline
tabulator end; newline
