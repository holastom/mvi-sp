tabulator trigger OnPostReport() newline
tabulator var newline
tabulator tabulator FinancingContractHeader: Record "API Financing Contract Header"; newline
tabulator tabulator FinancingContractLine: Record "API Financing Contract Line"; newline
tabulator tabulator ContractChangeHistory: Record "API Contract Change History"; newline
tabulator tabulator ContractualDistance: Record "API Contractual Distance"; newline
tabulator tabulator Customer: Record Customer; newline
 newline
tabulator tabulator LineNo: Integer; newline
tabulator tabulator ChangeCode: Text; newline
 newline
tabulator tabulator BLFileManager: Codeunit "BL File Manager"; newline
tabulator tabulator LanguageHandler: Codeunit LanguageHandler; newline
 newline
tabulator tabulator FileName: Label 'Contract_Change_History_%1_%2'; newline
tabulator tabulator SheetName: Label 'Contract Change History'; newline
tabulator tabulator ErrorCustomer: Label 'Customer with No. %1 does not exist.'; newline
tabulator tabulator ErrorNoCutomer: Label 'You need to insert Customer No. or Licence Plate No.'; newline
tabulator tabulator ErrorNoRecords: Label 'There are no records.'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.Reset(); newline
tabulator tabulator TmpExcelBuffer.DeleteAll(); newline
 newline
tabulator tabulator FinancingContractHeader.SetRange(Status, Status); newline
tabulator tabulator FinancingContractHeader.SetRange("Financing Type", FinancingType); newline
tabulator tabulator FinancingContractHeader.SetRange("Calculation Variant", false); newline
tabulator tabulator FinancingContractHeader.SetRange("Change Copy", false); newline
 newline
tabulator tabulator if LicencePlateNo notequal '' then begin newline
tabulator tabulator tabulator FinancingContractHeader.SetFilter("Licence Plate No.", LicencePlateNo); newline
tabulator tabulator end else newline
tabulator tabulator tabulator if CustomerNo notequal '' then begin newline
tabulator tabulator tabulator tabulator if not Customer.Get(CustomerNo) then newline
tabulator tabulator tabulator tabulator tabulator Error(ErrorCustomer); newline
tabulator tabulator tabulator tabulator FinancingContractHeader.SetRange("Customer No.", CustomerNo); newline
tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator Error(ErrorNoCutomer); newline
tabulator tabulator if Customer."No." = '' then newline
tabulator tabulator tabulator if not Customer.Get(FinancingContractHeader."Customer No.") then; newline
 newline
tabulator tabulator LangID assignment LanguageHandler.ReportLanguage(RequestPageLanguage, Customer."Language Code"); newline
tabulator tabulator CurrReport.Language assignment LangID; newline
 newline
tabulator tabulator CreateHeader(); newline
tabulator tabulator if FinancingContractHeader.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator LineNo assignment 0; newline
tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("Licence Plate No."); newline
 newline
tabulator tabulator tabulator tabulator ContractChangeHistory.SetRange("Financing Contract No.", FinancingContractHeader."No."); newline
tabulator tabulator tabulator tabulator ContractChangeHistory.SetRange(Closed, true); newline
tabulator tabulator tabulator tabulator ContractChangeHistory.SetAscending("Change Order No.", true); newline
tabulator tabulator tabulator tabulator if ContractChangeHistory.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator Clear(FinancingContractLine); newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractLine.SetRange("Financing Contract No.", FinancingContractHeader."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractLine.SetRange("Recalculation Settlement", false); newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractLine.SetFilter("Date From", '<=%1', ContractChangeHistory."Change Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractLine.SetFilter("Date To", '>=%1', ContractChangeHistory."Change Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator if not FinancingContractLine.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(FinancingContractLine); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator ContractualDistance.SetRange("Financing Contract No.", ContractChangeHistory."Financing Contract No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator ContractualDistance.SetFilter("Date From", '<=%1', ContractChangeHistory."Change Date"); newline
tabulator tabulator tabulator tabulator tabulator tabulator ContractualDistance.SetAscending("Date From", false); newline
tabulator tabulator tabulator tabulator tabulator tabulator if not ContractualDistance.FindFirst() then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(ContractualDistance); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator if ContractChangeHistory."Change Code" = 'ACTIVATION' then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator Clear(FinancingContractLine); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractLine.SetRange("Financing Contract No.", FinancingContractHeader."No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator FinancingContractLine.SetRange("Part Payment No.", '001'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if not FinancingContractLine.FindFirst() then; newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator if ContractChangeHistory."Change Code" = 'REV_PROFIT' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ChangeCode assignment 'REVISION' newline
tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator ChangeCode assignment ContractChangeHistory."Change Code"; newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator LineNo += 1; newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Customer Name", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Licence Plate No.", false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(LineNo, false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(ChangeCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(ContractChangeHistory."Financing Period-New", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractHeader."Handover Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(CalcDate(StrSubstNo('<+%1M>', ContractChangeHistory."Financing Period-New"), FinancingContractHeader."Contractual End Date"), false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(ContractChangeHistory."Change Date", false, '', false, false, false, 'dd.MM.yyyy', TmpExcelBuffer."Cell Type"::Date); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(ContractualDistance."Contractual Distance", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(ContractualDistance."Distance Per Year", false, '', false, false, false, '# ###', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator tabulator TmpExcelBuffer.AddColumn(FinancingContractLine."Lease Payment Total", false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator tabulator tabulator tabulator until ContractChangeHistory.Next() = 0; newline
tabulator tabulator tabulator until FinancingContractHeader.Next() = 0 newline
tabulator tabulator else newline
tabulator tabulator tabulator Error(ErrorNoRecords); newline
 newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
 newline
tabulator tabulator CreateCellWidth(); newline
 newline
tabulator tabulator BLFileManager.CustomizeCells(TmpExcelBuffer, 12); newline
tabulator tabulator BLFileManager.MakeColoredRows(TmpExcelBuffer); newline
 newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
tabulator tabulator TmpExcelBuffer.OpenExcelWithName(StrSubstNo(FileName, FinancingContractHeader."Customer Name", CurrentDateTime) + '.xlsx'); newline
tabulator end; newline
