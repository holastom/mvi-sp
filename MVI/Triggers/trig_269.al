tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator MASA: Record "BLG MA Special Agreement"; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator FinancingContractHeader.CalcFields("BLG Driver Name"); newline
tabulator tabulator tabulator tabulator tabulator MASA.SetRange("Master Agreement No.", FinancingContractHeader."Master Agreement No."); newline
tabulator tabulator tabulator tabulator tabulator MASA.SetRange(Status, "BLG Special Agr. Status"::Active); newline
tabulator tabulator tabulator tabulator tabulator MASA.SetFilter("Valid From", '..%1', Today()); newline
tabulator tabulator tabulator tabulator tabulator MASA.SetFilter("Valid To", '%1..', Today()); newline
tabulator tabulator tabulator tabulator tabulator MASA.SetFilter("SA ID", 'ACTCON001|ACTCON002|ACTCON003|COMMER001|ETHICS001|FUECAR001|INSURA001|RISK005|ROAASS001|SUPPLI001|TIRES001|TIRES003|TIRES004|VEHHAN001|VEHHAN002|VEHHAN003|VEHHAN004|VEHUSA001|VEHUSA002'); newline
tabulator tabulator tabulator tabulator tabulator if MASA.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator SpecialAgreements += Utils.crlf() + MASA."SA Parameters" + ': ' + MASA.Value; newline
tabulator tabulator tabulator tabulator tabulator tabulator until MASA.Next() = 0; newline
tabulator tabulator tabulator tabulator tabulator SpecialAgreements assignment DelChr(SpecialAgreements, '<', Utils.crlf()); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem(FuelType; "API Fuel Type") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = Code = field("Fuel Type Code"); newline
tabulator tabulator tabulator tabulator column(FuelTypeDescription; Name) { } newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem(Vendor; Vendor) newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Vendor No."); newline
tabulator tabulator tabulator tabulator column(VendorAddress; VendorAddress) { } newline
tabulator tabulator tabulator tabulator column(VendorPhone; VendorPhone) { } newline
 newline
