tabulator tabulator tabulator tabulator trigger OnPostDataItem() newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator CSBEmpty assignment CSBSkipped = "API Contract Service Billable".Count(); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator dataitem("API Insurance Contract"; "API Insurance Contract") //IC newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Financing Contract No." = field("No."); newline
tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
 newline
tabulator tabulator tabulator tabulator column(IC___; '') { } newline
tabulator tabulator tabulator tabulator column(IC_No_; "No.") { } newline
tabulator tabulator tabulator tabulator column(IC_Insurance_Company_No_; "Insurance Company No.") { } newline
tabulator tabulator tabulator tabulator column(IC_Insurance_Product_No_; "Insurance Product No.") { } //ID_61 newline
tabulator tabulator tabulator tabulator column(IC_Product; Utils.iif(isLocalLanguage, InsProd."BLG Description Alt.", InsProd.Description)) { } newline
tabulator tabulator tabulator tabulator column(IC_Product_Variant_Code; "Product Variant Code") { } //ID_62 newline
tabulator tabulator tabulator tabulator column(IC_Variant; Utils.iif(isLocalLanguage, InsProdVar."BLG Description Alt.", InsProdVar.Description)) { } newline
tabulator tabulator tabulator tabulator column(IC_Client_Month_Ins_Amount_LCY; LanguageHandler.FormatVar("Client Month Ins. Amount (LCY)")) { } //ID_63+ID_64 newline
tabulator tabulator tabulator tabulator column(IC_BLG_Insurance_Product_No_Order; InsuranceProductNoOrder) { } newline
tabulator tabulator tabulator tabulator column(BL_3; (FPTrec."Calculation Type" = FPTrec."Calculation Type"::Closed) and ("API Insurance Contract"."Insurance Product No." notequal 'DAMAGE_PROGRAM')) { } newline
 newline
