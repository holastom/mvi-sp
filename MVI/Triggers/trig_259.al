tabulator tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator MaintPermissionHeader.CalcFields("Vendor Name"); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator } newline
tabulator tabulator tabulator dataitem(InsuranceClaim; "API Insurance Claim") newline
tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator DataItemLink = "Chassis No. (VIN)" = field(VIN); newline
tabulator tabulator tabulator tabulator column(CDate; LH.FormatDateNoSpace("Event Creation Date")) { } newline
tabulator tabulator tabulator tabulator column(COdoReading; LH.FormatToIntegerCZ(Mileage)) { } newline
tabulator tabulator tabulator tabulator column(CVendor; CVendor) { } newline
tabulator tabulator tabulator tabulator column(CDescription; CDescription) { } newline
tabulator tabulator tabulator tabulator column(CAmount; Utils.iif("Expected Price excl. VAT" = 0, Utils.iif("Estimated Damage Value" = 0, Lbl_NotKnown, LH.FormatVar("Estimated Damage Value")), LH.FormatVar("Expected Price excl. VAT"))) { } newline
 newline
