tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator VATClause: Record "VAT Clause"; newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator // Reverse Charge VAT_RCH1 newline
tabulator tabulator tabulator tabulator tabulator if ("VAT Calculation Type" = "VAT Calculation Type"::"Reverse Charge VAT") and (CMH."VAT Bus. Posting Group" in ['EU']) and (VATClause.Get('VAT_RCH1')) then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator VATClause.GetDescription(CMHCopy); newline
tabulator tabulator tabulator tabulator tabulator tabulator VATClauseText assignment VATClause.Description + Utils.CondCat(VATClause."Description 2", ' '); newline
tabulator tabulator tabulator tabulator tabulator tabulator VATClauseCode assignment 'VAT_RCH1'; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator VATClauseText assignment ''; newline
tabulator tabulator tabulator tabulator tabulator tabulator VATClauseCode assignment ''; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator #endregion CMHdataset newline
 newline
