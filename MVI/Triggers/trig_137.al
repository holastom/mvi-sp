tabulator tabulator tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator tabulator tabulator var newline
tabulator tabulator tabulator tabulator tabulator tabulator ErrorWrongBankAccNoLbl: Label 'In Bank Account No. is "/" missing.'; newline
tabulator tabulator tabulator tabulator tabulator tabulator ErrorBranchNoNotEmptyLbl: Label 'In Bank Account No. is "/" missing and Bank Branch No. is not empty.'; newline
tabulator tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if (not "Bank Account No.".Contains('/')) and ("Bank Account No." notequal '') then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if "Bank Branch No." notequal '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Error(ErrorBranchNoNotEmptyLbl) newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator Error(ErrorWrongBankAccNoLbl); newline
 newline
tabulator tabulator tabulator tabulator tabulator tabulator if "Bank Account No." = '' then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator BankAccountNoOrIban assignment IBAN newline
tabulator tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator BankAccountNoOrIban assignment "Bank Account No."; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator dataitem(Z_Contact; Contact) newline
tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator DataItemLink = "No." = field("Primary Contact No."); newline
tabulator tabulator tabulator tabulator tabulator column(Z_JuridictionPlace; "API Jurisdiction Place") { } newline
tabulator tabulator tabulator tabulator tabulator column(Z_FileNo; Utils.nbsp("API File No.")) { } newline
tabulator tabulator tabulator tabulator tabulator column(Z_Signers_CZ; CustomerSigners_CZ) { } newline
tabulator tabulator tabulator tabulator tabulator column(Z_Signers_EN; CustomerSigners_En) { } newline
 newline
tabulator tabulator tabulator tabulator tabulator dataitem(CustMASigners; Contact) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator UseTemporary = true; newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CuMASi_Name; Name) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CuMASi_Company; "Company Name") { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CuMASi_JobTitle; "Job Title") { } newline
tabulator tabulator tabulator tabulator tabulator } newline
 newline
tabulator tabulator tabulator tabulator tabulator dataitem(CustCPSigners; Contact) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator UseTemporary = true; newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CuCPSi_Name; DashesIfEmpty(30, Name)) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CuCPSi_EMail; DashesIfEmpty(45, "E-Mail")) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CuCPSi_Phone; DashesIfEmpty(20, LanguageHandler.FormatPhoneNumber("Phone No."))) { } newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CuCPSi_Mobile; DashesIfEmpty(20, LanguageHandler.FormatPhoneNumber("Mobile Phone No."))) { } newline
tabulator tabulator tabulator tabulator tabulator } newline
tabulator tabulator tabulator tabulator tabulator dataitem(CustINVSigners; Contact) newline
tabulator tabulator tabulator tabulator tabulator { newline
tabulator tabulator tabulator tabulator tabulator tabulator UseTemporary = true; newline
tabulator tabulator tabulator tabulator tabulator tabulator DataItemTableView = sorting("No."); newline
tabulator tabulator tabulator tabulator tabulator tabulator column(CuINVSi_EMail; "E-Mail") { } newline
tabulator tabulator tabulator tabulator tabulator } newline
 newline
