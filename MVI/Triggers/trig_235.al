tabulator tabulator tabulator trigger OnAfterGetRecord() newline
tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator if "VAT Registration No." notequal '' then newline
tabulator tabulator tabulator tabulator tabulator VendorRegNoVATRegNo assignment Utils.nbsp(RegNoLbl + ': ' + "Registration No.") + ', ' + Utils.nbsp(VATRegNoLbl + ': ' + "VAT Registration No.") newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator VendorRegNoVATRegNo assignment RegNoLbl + ': ' + "Registration No."; newline
tabulator tabulator tabulator tabulator CountryRegion.Get("Country/Region Code"); newline
tabulator tabulator tabulator tabulator UserSetup.Get(UserId); newline
tabulator tabulator tabulator tabulator Employee.Get(UserSetup."API Employee No."); newline
tabulator tabulator tabulator end; newline
 newline
tabulator tabulator } newline
 newline
tabulator tabulator dataitem(CH; "Compensation Header CZC") newline
tabulator tabulator { newline
tabulator tabulator tabulator DataItemTableView = SORTING("No."); newline
tabulator tabulator tabulator RequestFilterFields = "No.", "Company No."; newline
 newline
tabulator tabulator tabulator column(Nono; "No.") { } newline
