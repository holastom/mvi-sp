tabulator trigger OnPreReport() newline
tabulator begin newline
tabulator tabulator CurrReport.Language assignment LangHndl.ReportLanguage(); newline
tabulator tabulator Currency assignment LangHndl.CurrSymbol(''); newline
tabulator tabulator CompLangId assignment LangHndl.GetCompanyLanguageId; newline
tabulator tabulator ENLangId assignment LangHndl.GetENlanguageId; newline
tabulator end; newline
