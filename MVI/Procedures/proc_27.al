tabulator procedure GetSubsituteCar("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator SPL: Record "API Service Payment Line"; newline
tabulator tabulator SC_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator SC_Countrer assignment 0; newline
tabulator tabulator FCL assignment GetFinancingContractLine("Original Financing Contract No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator if not FCL.IsEmpty then begin // radka splatkoveho kalendare pro danou smlouvu newline
tabulator tabulator tabulator SPL.SetRange("Financing Contract No.", FCL."Financing Contract No."); newline
tabulator tabulator tabulator SPL.SetRange("Financing Part Payment No.", FCL."Part Payment No."); newline
tabulator tabulator tabulator SPL.SetRange("Service Kind", "API Service Kind"::"Replacement Car"); newline
tabulator tabulator tabulator if SPL.FindSet() then // radky splatek pro servis newline
tabulator tabulator tabulator tabulator repeat // Mela by byt vzdy a pouze jen 1, pro jistotu ale mame scitat pres vsechny, ktere tam nalezneme newline
tabulator tabulator tabulator tabulator tabulator SC_Countrer += SPL.Amount; newline
tabulator tabulator tabulator tabulator until SPL.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(SC_Countrer); newline
tabulator end; newline
