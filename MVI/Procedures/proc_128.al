tabulator local procedure CheckValid(var VendorBusinessActivity: Record "BLG Vendor Bus. Activity"): Boolean newline
tabulator begin newline
tabulator tabulator if (VendorBusinessActivity."Valid From" = 0D) and (VendorBusinessActivity."Valid To" = 0D) then exit(true); newline
tabulator tabulator if (VendorBusinessActivity."Valid From" = 0D) and (VendorBusinessActivity."Valid To" >= Today()) then exit(true); newline
tabulator tabulator if (VendorBusinessActivity."Valid To" = 0D) and (VendorBusinessActivity."Valid From" <= Today()) then exit(true); newline
tabulator tabulator if (VendorBusinessActivity."Valid From" <= Today()) and (VendorBusinessActivity."Valid To" >= Today()) then exit(true); newline
tabulator tabulator exit(false); newline
tabulator end; newline
