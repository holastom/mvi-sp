tabulator local procedure ParseAndTrimDesc(txt: Text; del: Text): Text newline
tabulator var newline
tabulator tabulator txtList: List of [Text]; newline
tabulator tabulator rett: Text; newline
tabulator begin newline
tabulator tabulator if del notequal '' then begin newline
tabulator tabulator tabulator txtList assignment txt.Split(del); newline
tabulator tabulator tabulator if (txtList.Count > 1) and (not isLocalLanguage) then newline
tabulator tabulator tabulator tabulator rett assignment txtList.Get(2).Trim() newline
tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator rett assignment txtList.Get(1).Trim(); newline
tabulator tabulator end; newline
tabulator tabulator exit(rett); newline
tabulator end; newline
