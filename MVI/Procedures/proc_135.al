tabulator local procedure StringifyWithDelimiter(var str: Text; textList: List of [Text]; del: Text); newline
tabulator var newline
tabulator tabulator item: Text; newline
tabulator begin newline
tabulator tabulator foreach item in textList do begin newline
tabulator tabulator tabulator str += item + del; newline
tabulator tabulator end; newline
tabulator tabulator if (StrLen(str) notequal 0) and (str.LastIndexOf(del) notequal 0) then newline
tabulator tabulator tabulator str assignment DelStr(str, str.LastIndexOf(del), StrLen(del)); newline
tabulator end; newline
