tabulator local procedure FormatSeasonLine(TireDetailLine: Record "API Tire Detail Line"; SecondTireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator begin newline
tabulator tabulator exit(Format(TireDetailLine.Season) + ' ' + LBL_191 + LBL_192a + FormatLine(TireDetailLine, SecondTireDetailLine)) // "zimni/letni pneu: kategorie: " newline
tabulator end; newline
