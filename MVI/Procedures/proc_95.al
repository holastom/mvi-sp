tabulator local procedure GetPaymentTermDescription(TimePeriod: Code[10]) ReturnValue: Text newline
tabulator var newline
tabulator tabulator PaymentTerms: record "Payment Terms"; newline
tabulator begin newline
tabulator tabulator if (TimePeriod = '') or not PaymentTerms.Get(TimePeriod) then newline
tabulator tabulator tabulator exit(''); newline
tabulator tabulator exit(PaymentTerms.Description); newline
tabulator end; newline
