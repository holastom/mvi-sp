tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_AccountCode: Label 'Account code'; newline
tabulator tabulator Lbl_ContractNo: Label 'Contract No'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No'; newline
tabulator tabulator Lbl_CardNo: Label 'Card No'; newline
tabulator tabulator Lbl_Description: Label 'Description'; newline
tabulator tabulator Lbl_StatementID: Label 'Statement Id'; newline
tabulator tabulator Lbl_Date: Label 'Date'; newline
tabulator tabulator Lbl_Time: Label 'Time'; newline
tabulator tabulator Lbl_MerchantArea: Label 'Merchant Area'; newline
tabulator tabulator Lbl_PricePerLiter: Label 'Price p/ltr'; newline
tabulator tabulator Lbl_Quantity: Label 'Quantity'; newline
tabulator tabulator Lbl_AmountExclVAT: Label 'Amount Excl VAT'; newline
tabulator tabulator Lbl_VAT: Label 'VAT'; newline
tabulator tabulator Lbl_ProductDescription: Label 'Product description'; newline
tabulator tabulator Lbl_TaxRate: Label 'Tax Rate'; newline
tabulator tabulator Lbl_Currency: Label 'Currency'; newline
tabulator tabulator Lbl_Odoreading: Label 'Odoreading'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CardNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StatementID, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Date, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Time, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MerchantArea, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PricePerLiter, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Quantity, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AmountExclVAT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VAT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ProductDescription, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TaxRate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Currency, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Odoreading, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
