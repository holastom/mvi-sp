tabulator local procedure MergePhones(Tel1: Text; Tel2: Text): Text newline
tabulator begin newline
tabulator tabulator if (Tel1 notequal '') and (Tel2 notequal '') then newline
tabulator tabulator tabulator exit(Tel1 + ', ' + Tel2); newline
tabulator tabulator if (Tel1 notequal '') then newline
tabulator tabulator tabulator exit(Tel1); newline
tabulator tabulator if (Tel2 notequal '') then newline
tabulator tabulator tabulator exit(Tel2); newline
tabulator tabulator exit(''); newline
tabulator end; newline
