tabulator local procedure FourRecordsWinter(var TireDetailLine: Record "API Tire Detail Line"; var SecondTireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator var newline
tabulator tabulator Lbl_NoFrontWinter: Label 'Missing entry for front winter Tires.'; newline
tabulator tabulator Lbl_NoRearWinter: Label 'Missing entry for rear winter Tires.'; newline
tabulator begin newline
tabulator tabulator TireDetailLine.SetRange(Position, "API Tire Position"::Front); newline
tabulator tabulator TireDetailLine.SetRange(Season, "API Tire Season"::Winter); newline
tabulator tabulator if not TireDetailLine.FindFirst() then Error(Lbl_NoFrontWinter); newline
tabulator tabulator SecondTireDetailLine.SetRange(Position, "API Tire Position"::Rear); newline
tabulator tabulator SecondTireDetailLine.SetRange(Season, "API Tire Season"::Winter); newline
tabulator tabulator if not SecondTireDetailLine.FindFirst() then Error(Lbl_NoRearWinter); newline
tabulator tabulator exit(CompareSeasonTwoDimensions(TireDetailLine, SecondTireDetailLine)); newline
tabulator end; newline
