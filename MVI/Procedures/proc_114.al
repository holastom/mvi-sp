tabulator local procedure TwoRecordsSeason(var TireDetailLine: Record "API Tire Detail Line"; var SecondTireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator var newline
tabulator tabulator Lbl_NoSummer: Label 'Missing entry for summer Tires.'; newline
tabulator tabulator Lbl_NoWinter: Label 'Missing entry for winter Tires.'; newline
tabulator begin newline
tabulator tabulator if TireDetailLine.Season = "API Tire Season"::Winter then begin newline
tabulator tabulator tabulator SecondTireDetailLine.SetRange(Season, "API Tire Season"::Summer); newline
tabulator tabulator tabulator // Vime, ze existuje pouze jeden zaznam newline
tabulator tabulator tabulator if not SecondTireDetailLine.FindFirst() then Error(Lbl_NoSummer); newline
tabulator tabulator end else begin newline
tabulator tabulator tabulator SecondTireDetailLine.SetRange(Season, "API Tire Season"::Winter); newline
tabulator tabulator tabulator // Vime, ze existuje pouze jeden zaznam newline
tabulator tabulator tabulator if not SecondTireDetailLine.FindFirst() then Error(Lbl_NoWinter); newline
tabulator tabulator end; newline
tabulator tabulator exit(FormatSeasonSameDimensionsLine(TireDetailLine) + FormatSeasonSameDimensionsLine(SecondTireDetailLine)); newline
tabulator end; newline
