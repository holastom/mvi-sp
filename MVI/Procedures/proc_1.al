tabulator procedure CustomizeCells(var TmpExcelBuffer: Record "Excel Buffer" temporary; Columns: Integer) newline
tabulator var newline
tabulator tabulator Row: Integer; newline
tabulator begin newline
tabulator tabulator Row assignment 0; newline
tabulator tabulator TmpExcelBuffer.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator TmpExcelBuffer."Font Name" assignment 'Arial'; newline
tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator until TmpExcelBuffer.Next() = 0; newline
 newline
tabulator tabulator repeat newline
tabulator tabulator tabulator Row assignment Row + 1; newline
tabulator tabulator tabulator TmpExcelBuffer.Get(1, Row); newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger('1a91bc')); newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Font Color", TmpExcelBuffer.HexARGBToInteger('FFFFFF')); newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Font Name", 'Arial'); newline
tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator until Row = Columns; newline
tabulator end; newline
