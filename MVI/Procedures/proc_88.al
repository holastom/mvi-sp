tabulator local procedure SignersTextCZ(var Signers: Record Contact): Text newline
tabulator var newline
tabulator tabulator n: Integer; newline
tabulator tabulator i: Integer; newline
tabulator tabulator RetVal: Text; newline
tabulator tabulator AndLbl: Label 'a'; newline
tabulator tabulator Represented1Lbl: Label 'zastupuje '; newline
tabulator tabulator RepresentedMlbl: Label 'zastupují '; newline
tabulator begin newline
tabulator tabulator n assignment Signers.Count; newline
tabulator tabulator if n = 0 then newline
tabulator tabulator tabulator RetVal assignment '' newline
tabulator tabulator else begin newline
tabulator tabulator tabulator if n = 1 then newline
tabulator tabulator tabulator tabulator RetVal assignment Represented1Lbl newline
tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator RetVal assignment RepresentedMlbl; newline
tabulator tabulator end; newline
tabulator tabulator if Signers.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator i += 1; newline
tabulator tabulator tabulator tabulator if i = 1 then newline
tabulator tabulator tabulator tabulator tabulator RetVal += Utils.nbsp(Signers.Name) + ', ' + Signers."Job Title" newline
tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator if i = n then newline
tabulator tabulator tabulator tabulator tabulator tabulator RetVal += ' ' + Utils.nbsp(AndLbl + ' ' + Signers.Name) + ', ' + Signers."Job Title" newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator RetVal += ', ' + Utils.nbsp(Signers.Name) + ', ' + Signers."Job Title"; newline
tabulator tabulator tabulator until Signers.Next = 0; newline
tabulator tabulator exit(RetVal); newline
tabulator end; newline
