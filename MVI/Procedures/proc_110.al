tabulator local procedure FormatSeasonSameDimensionsLine(TireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator begin newline
tabulator tabulator exit(Format(TireDetailLine.Season) + ' ' + LBL_191 + LBL_192a + FormatSameDimensionLine(TireDetailLine));// "zimni/letni pneu: kategorie: " newline
tabulator end; newline
