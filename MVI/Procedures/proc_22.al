tabulator procedure GetSalesCreditMemoLine("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Record "Sales Cr.Memo Line"; newline
tabulator var newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetFilter(Type, 'notequal%1', SCML.Type::" "); newline
tabulator tabulator if SCML.FindFirst() then; newline
tabulator tabulator exit(SCML); newline
tabulator end; newline
