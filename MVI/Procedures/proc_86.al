tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator FirstDate: Date; newline
tabulator tabulator LBL_01: Label 'License Plate No.', Comment = 'cs-CZ=RZ;en-US=License Plate No.'; newline
tabulator tabulator LBL_02: Label 'Contract No.', Comment = 'cs-CZ=Číslo smlouvy;en-US=Contract No.'; newline
tabulator tabulator LBL_03: Label 'Client', Comment = 'cs-CZ=Klient;en-US=Client'; newline
tabulator tabulator LBL_04: Label 'Driver', Comment = 'cs-CZ=Řidič;en-US=Driver'; newline
tabulator tabulator LBL_05: Label 'Model', Comment = 'cs-CZ=Model;en-US=Model'; newline
tabulator tabulator LBL_06: Label 'Start of Contract Date', Comment = 'cs-CZ=Začátek kontraktu;en-US=Start of Contract Date'; newline
tabulator tabulator LBL_07: Label 'End of Contract Date', Comment = 'cs-CZ=Konec kontraktu;en-US=End of Contract Date'; newline
tabulator tabulator LBL_08: Label 'Financing Period', Comment = 'cs-CZ=Smluvní délka kontraktu;en-US=Financing Period'; newline
tabulator tabulator LBL_09: Label 'Yearly Distance', Comment = 'cs-CZ=Smluvní roční nájezd;en-US=Yearly Distance'; newline
tabulator tabulator LBL_10: Label 'Contractual Distance', Comment = 'cs-CZ=Smluvní celkový nájezd;en-US=Contractual Distance'; newline
tabulator tabulator LBL_11: Label 'Payment', Comment = 'cs-CZ=Splátka;en-US=Payment'; newline
tabulator tabulator LBL_12: Label 'New Financing Period', Comment = 'cs-CZ=Nová délka kontraktu;en-US=New Financing Period'; newline
tabulator tabulator LBL_13: Label 'New Yearly Distance', Comment = 'cs-CZ=Nový roční nájezd;en-US=New Yearly Distance'; newline
tabulator tabulator LBL_14: Label 'New Contractual Distance', Comment = 'cs-CZ=Nový celkový nájezd;en-US=New Contractual Distance'; newline
tabulator tabulator LBL_15: Label 'New Payment', Comment = 'cs-CZ=Nová splátka;en-US=New Payment'; newline
tabulator tabulator LBL_16: Label 'Settlement to', Comment = 'cs-CZ=Vyrovnání k;en-US=Settlement to'; newline
tabulator tabulator LBL_17: Label 'New Contract End Date', Comment = 'cs-CZ=Nový konec kontraktu;en-US=New Contract End Date'; newline
tabulator begin newline
tabulator tabulator FirstDate assignment CalcDate('<-CM +1M>', Today()); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_01, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_02, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_03, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_04, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_05, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_06, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_07, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_08, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_09, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_10, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_11, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_12, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_13, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_14, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_15, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_16 + ' ' + LanguageHandler.FormatDateNoSpace(FirstDate), false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(LBL_17, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
