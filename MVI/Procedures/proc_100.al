tabulator local procedure Months(n: integer): Text newline
tabulator begin newline
tabulator tabulator case n of newline
tabulator tabulator tabulator 1: newline
tabulator tabulator tabulator tabulator exit(LBL_120a); newline
tabulator tabulator tabulator 2 .. 4: newline
tabulator tabulator tabulator tabulator exit(LBL_120b); newline
tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator exit(LBL_120c); newline
tabulator tabulator end; newline
tabulator end; newline
