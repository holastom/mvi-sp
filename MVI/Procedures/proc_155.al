tabulator local procedure GetNotesForRecordRef(MyRec: RecordRef): Text newline
tabulator var newline
tabulator tabulator RecordLink: Record "Record Link"; newline
tabulator tabulator TypeHelper: Codeunit "Record Link Management"; newline
tabulator tabulator Result: text; newline
tabulator begin newline
tabulator tabulator Clear(RecordLink); newline
tabulator tabulator clear(Result); newline
tabulator tabulator RecordLink.SetRange("Record ID", MyRec.RecordId()); newline
tabulator tabulator RecordLink.SetRange(Type, RecordLink.Type::Note); newline
tabulator tabulator if RecordLink.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator RecordLink.CalcFields(Note); newline
tabulator tabulator tabulator tabulator Result += Utils.crlf() + LH.FormatDateNoSpace(DT2Date(RecordLink.Created)) + ' ' + RecordLink."User ID" + ': ' + TypeHelper.ReadNote(RecordLink); newline
tabulator tabulator tabulator until RecordLink.Next() = 0; newline
tabulator tabulator Result assignment DelChr(Result, '<', Utils.crlf()); newline
tabulator tabulator exit(Result); newline
tabulator end; newline
