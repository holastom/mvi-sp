tabulator local procedure AddIfNotExists(str: Text; var textList: List of [Text]; var cnt: Integer); newline
tabulator begin newline
tabulator tabulator if (not textList.Contains(str)) and (str notequal '') then begin newline
tabulator tabulator tabulator textList.Add(str); newline
tabulator tabulator tabulator cnt += 1; newline
tabulator tabulator end; newline
tabulator end; newline
