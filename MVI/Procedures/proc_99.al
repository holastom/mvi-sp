tabulator local procedure EmptyIfClosed(originalValue: Text): Text newline
tabulator begin newline
tabulator tabulator if FPTrec."BLG Final Settlement Type" = FPTrec."BLG Final Settlement Type"::Closed then newline
tabulator tabulator tabulator exit('') newline
tabulator tabulator else newline
tabulator tabulator tabulator exit(originalValue) newline
tabulator end; newline
