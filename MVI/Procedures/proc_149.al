tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_Dealer: Label 'Dealer'; newline
tabulator tabulator Lbl_Email: Label 'E-mail'; newline
tabulator tabulator Lbl_Objednavka: Label 'Objednávka'; newline
tabulator tabulator Lbl_Objednano: Label 'Objednáno'; newline
tabulator tabulator Lbl_CarDescriptions: Label 'Car Description'; newline
tabulator tabulator Lbl_Klient: Label 'Klient'; newline
tabulator tabulator Lbl_Driver: Label 'Driver'; newline
tabulator tabulator Lbl_Poznamka: Label 'Poznámka'; newline
tabulator tabulator Lbl_PredKlie: Label 'Předání klientovi od (odsl. VTP+10 dní)'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Dealer, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Email, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Objednavka, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Objednano, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CarDescriptions, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Klient, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Driver, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PredKlie, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Poznamka, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
