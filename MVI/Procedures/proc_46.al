tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 10); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 10); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('N', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('O', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('P', 17); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Q', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('R', 17); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('S', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('T', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('U', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('V', 14); newline
tabulator end; newline
