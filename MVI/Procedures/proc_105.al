tabulator local procedure GetTireDetailString(tireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator begin newline
tabulator tabulator exit(tireDetailLine.Width + ' / ' newline
tabulator tabulator tabulator tabulator + tireDetailLine.Profile + ' R' newline
tabulator tabulator tabulator tabulator + tireDetailLine.Rim + ' ' newline
tabulator tabulator tabulator tabulator + tireDetailLine."Load Index" newline
tabulator tabulator tabulator tabulator + tireDetailLine."Speed Category"); newline
tabulator end; newline
