tabulator local procedure DocTypeOrder(t: Enum "Gen. Journal Document Type"): Integer newline
tabulator begin newline
tabulator tabulator if t = t::Invoice then exit(0); newline
tabulator tabulator if t notequal t::" " then exit(1); newline
tabulator tabulator exit(2); newline
tabulator end; newline
