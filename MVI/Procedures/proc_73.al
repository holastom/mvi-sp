tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 10); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 19); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('N', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('O', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('P', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Q', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('R', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('S', 15); newline
tabulator end; newline
