tabulator local procedure _ContrStatOrder(var fch: Record "API Financing Contract Header"): Integer newline
tabulator begin newline
tabulator tabulator if fch."No." = '' then exit(1); newline
tabulator tabulator if fch.Status = fch.Status::Active then exit(0); newline
tabulator tabulator exit(2); newline
tabulator end; newline
