tabulator local procedure GearDescr(GearCode: Code[1]; lang: Integer): Text newline
tabulator var newline
tabulator tabulator ManualLbl: Label 'manual', Comment = 'cs-CZ=manuální'; newline
tabulator tabulator AutomatLbl: Label 'automatic', Comment = 'cs-CZ=automatická'; newline
tabulator begin newline
tabulator tabulator case GearCode of newline
tabulator tabulator tabulator 'A': newline
tabulator tabulator tabulator tabulator exit(TranslateLbl(AutomatLbl, lang)); newline
tabulator tabulator tabulator 'M': newline
tabulator tabulator tabulator tabulator exit(TranslateLbl(ManualLbl, lang)); newline
tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator exit(GearCode); newline
tabulator tabulator end; newline
tabulator end; newline
