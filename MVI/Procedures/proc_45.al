tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_ContractNumber: Label 'Contract number'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No'; newline
tabulator tabulator Lbl_AccountCode: Label 'Account Code'; newline
tabulator tabulator Lbl_AccountName: Label 'Account Name'; newline
tabulator tabulator Lbl_ProductType: Label 'Product Type'; newline
tabulator tabulator Lbl_Product: Label 'Product'; newline
tabulator tabulator Lbl_UsedVehicle: Label 'Used Vehicle'; newline
tabulator tabulator Lbl_StartDate: Label 'Start Date'; newline
tabulator tabulator Lbl_EndDate: Label 'End Date'; newline
tabulator tabulator Lbl_EndDateAfterExtension: Label 'End Date after Extension'; newline
tabulator tabulator Lbl_ContractPeriod: Label 'Contract Period'; newline
tabulator tabulator Lbl_ContractPeriodAfterExt: Label 'Contract Period after Ext.'; newline
tabulator tabulator Lbl_StartOdo: Label 'Start Odo'; newline
tabulator tabulator Lbl_AnnualMileage: Label 'Annual mileage'; newline
tabulator tabulator Lbl_ContractMileage: Label 'Contract mileage'; newline
tabulator tabulator Lbl_OdoReadingDate: Label 'Odo Reading Date'; newline
tabulator tabulator Lbl_ActualMileage: Label 'Actual Mileage'; newline
tabulator tabulator Lbl_FinalOdoReading: Label 'Final Odo Reading'; newline
tabulator tabulator Lbl_MileageDeviation: Label 'Mileage Deviation '; newline
tabulator tabulator Lbl_PercemtMileageDeviation: Label '% Mileage Deviation '; newline
tabulator tabulator Lbl_InformalBilling: Label 'Informal billing 1/0'; newline
tabulator tabulator Lbl_MonthsDriven: Label 'Months driven'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNumber, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ProductType, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Product, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_UsedVehicle, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StartDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EndDateAfterExtension, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractPeriod, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractPeriodAfterExt, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StartOdo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AnnualMileage, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractMileage, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_OdoReadingDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ActualMileage, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinalOdoReading, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MileageDeviation, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PercemtMileageDeviation, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InformalBilling, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MonthsDriven, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
