tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_ExpectedTerminationDate: Label 'Expected Termination Date'; newline
tabulator tabulator Lbl_StandardWarranty: Label 'Standard warranty No. of Months'; newline
tabulator tabulator Lbl_HomologationClassCode: Label 'Homologation Class Code'; newline
tabulator tabulator Lbl_EmissionStandardCode: Label 'Emission Standard Code'; newline
tabulator tabulator Lbl_LicensePlateNo: Label 'License Plate No.'; newline
tabulator tabulator Lbl_ContractStatus: Label 'Contract Status'; newline
tabulator tabulator Lbl_ProductionYear: Label 'Production Year'; newline
tabulator tabulator Lbl_EnginePower: Label 'Engine Power (kW)'; newline
tabulator tabulator Lbl_FinancingType: Label 'Financing Type'; newline
tabulator tabulator Lbl_FuelTypeCode: Label 'Fuel Type Code'; newline
tabulator tabulator Lbl_TypeOfBody: Label 'Type of body'; newline
tabulator tabulator Lbl_Modelname: Label 'Model Name'; newline
tabulator tabulator Lbl_EngineCCM: Label 'Engine CCM'; newline
tabulator tabulator Lbl_MakeName: Label 'Make Name'; newline
tabulator tabulator Lbl_Gearbox: Label 'Gearbox'; newline
tabulator tabulator Lbl_Name: Label 'Name'; newline
tabulator tabulator Lbl_VIN: Label 'VIN'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinancingType, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_LicensePlateNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ExpectedTerminationDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractStatus, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_HomologationClassCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MakeName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Modelname, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TypeOfBody, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FuelTypeCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Gearbox, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EngineCCM, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EnginePower, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EmissionStandardCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ProductionYear, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StandardWarranty, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
