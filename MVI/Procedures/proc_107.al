tabulator local procedure CompareTwoDimensions(TireDetailLine: Record "API Tire Detail Line"; SecondTireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator begin newline
tabulator tabulator if GetTireDetailString(TireDetailLine) = GetTireDetailString(SecondTireDetailLine) then newline
tabulator tabulator tabulator //Maji stejne rozmery newline
tabulator tabulator    exit(FormatSameDimensionLine(TireDetailLine)) newline
tabulator tabulator else newline
tabulator tabulator tabulator //nemaji stejny rozmer, naformatujeme oba zvlast newline
tabulator tabulator    exit(FormatLine(TireDetailLine, SecondTireDetailLine)); newline
tabulator end; newline
