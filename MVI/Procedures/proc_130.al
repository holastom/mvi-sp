tabulator local procedure SubstParagraph(t: Text; p: Text): Text newline
tabulator var newline
tabulator tabulator i: Integer; newline
tabulator begin newline
tabulator tabulator i assignment StrPos(t, '§'); newline
tabulator tabulator if i = 0 then exit(t); newline
tabulator tabulator exit(InsStr(DelStr(t, i, 1), p, i)); newline
tabulator end; newline
