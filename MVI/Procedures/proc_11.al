tabulator procedure CreateExcelBufferWithColoredText(var TmpExcelBuffer: Record "Excel Buffer" temporary; AttachmentFilePath: Text; OutStream: OutStream; SheetName: Text; Color: Text; FromColumnNo: Integer; ToColumnNo: Integer; FromRowNo: Integer) newline
tabulator var newline
tabulator tabulator Attachment: File; newline
tabulator tabulator BLFM: Codeunit "BL File Manager"; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator BLFM.MakeColoredText(TmpExcelBuffer, Color, FromColumnNo, ToColumnNo, FromRowNo); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
 newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::Windows); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
tabulator tabulator TmpExcelBuffer.SaveToStream(OutStream, true); newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
