tabulator local procedure MergeVATCounterPlusMinusLines(var vatc: Record "VAT Amount Line"); newline
tabulator var newline
tabulator tabulator vatc2: Record "VAT Amount Line" temporary; newline
tabulator tabulator found: Boolean; newline
tabulator begin newline
tabulator tabulator vatc.Reset; newline
tabulator tabulator vatc2.Copy(vatc, true); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if vatc.FindSet then newline
tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator vatc2 assignment vatc; newline
tabulator tabulator tabulator tabulator tabulator vatc2.SetRecFilter; newline
tabulator tabulator tabulator tabulator tabulator vatc2.SetRange(Positive); newline
tabulator tabulator tabulator tabulator tabulator found assignment vatc2.Count > 1; newline
tabulator tabulator tabulator tabulator tabulator if found then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator vatc2.CalcSums("VAT Base", "VAT Amount", "Amount Including VAT"); newline
tabulator tabulator tabulator tabulator tabulator tabulator vatc2.DeleteAll; newline
tabulator tabulator tabulator tabulator tabulator tabulator vatc2.Insert; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator until found or (vatc.Next = 0); newline
tabulator tabulator until not found; newline
tabulator end; newline
