tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_RegNo: Label 'Reg No.'; newline
tabulator tabulator Lbl_ContractNo: Label 'Contract No.'; newline
tabulator tabulator Lbl_MakeDesc: Label 'Make Desc'; newline
tabulator tabulator Lbl_MakeModelDesc: Label 'Make Model Desc'; newline
tabulator tabulator Lbl_ModelDesc: Label 'Model Desc'; newline
tabulator tabulator Lbl_VIN: Label 'Vin'; newline
tabulator tabulator Lbl_StartDate: Label 'Start Date'; newline
tabulator tabulator Lbl_ValidUntil: Label 'Valid until'; newline
tabulator tabulator Lbl_ActualEndDate: Label 'Actual End Date'; newline
tabulator tabulator Lbl_RegDate: Label 'Reg Date'; newline
tabulator tabulator Lbl_FleetCategory: Label 'Fleet Category'; newline
tabulator tabulator Lbl_RegName: Label 'Reg Name'; newline
tabulator tabulator Lbl_StreetNo: Label 'Street No.'; newline
tabulator tabulator Lbl_AdressLine1: Label 'Adress Line 1'; newline
tabulator tabulator Lbl_Postcode: Label 'Postcode'; newline
tabulator tabulator Lbl_TownCity: Label 'Town City'; newline
tabulator tabulator Lbl_Driver: Label 'Driver'; newline
tabulator tabulator Lbl_ContactNumber: Label 'Contact Number'; newline
tabulator tabulator Lbl_ElementName: Label 'Element name'; newline
tabulator tabulator Lbl_StandardVIP: Label 'Standard/VIP'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MakeDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MakeModelDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StartDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator if Activated then newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ValidUntil, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text) newline
tabulator tabulator else //Terminated newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ActualEndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FleetCategory, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StreetNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AdressLine1, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Postcode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TownCity, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Driver, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContactNumber, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ElementName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StandardVIP, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
