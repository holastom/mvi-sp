tabulator local procedure GetNotification(status: Enum "API Financing Contract Status"): Boolean; newline
tabulator begin newline
tabulator tabulator if NotificationSelectEnum = NotificationSelectEnum::"Not selected" then exit(status = status::Active); newline
tabulator tabulator exit(NotificationSelectEnum = NotificationSelectEnum::Yes); newline
tabulator end; newline
