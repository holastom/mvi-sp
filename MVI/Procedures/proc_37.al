tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_CatalogueNo: Label 'Catalogue No.'; newline
tabulator tabulator Lbl_Name: Label 'Name'; newline
tabulator tabulator Lbl_Activa: Label 'Active'; newline
tabulator tabulator Lbl_Visible: Label 'Visible'; newline
tabulator tabulator Lbl_RVGridCode: Label 'RV Grid Code'; newline
tabulator tabulator Lbl_MRGridCode: Label 'MR Grid Code'; newline
tabulator tabulator Lbl_GroupName: Label 'Group Name'; newline
tabulator tabulator Lbl_MakeName: Label 'Make Name'; newline
tabulator tabulator Lbl_ModeLineName: Label 'Model Line Name'; newline
tabulator tabulator Lbl_ModelTypeName: Label 'Model Type Name'; newline
tabulator tabulator Lbl_TrimClassificationCode: Label 'Trim Classification Code'; newline
tabulator tabulator Lbl_TypeOfBody: Label 'Type of Body'; newline
tabulator tabulator Lbl_BodyTypeName: Label 'Body Type Name'; newline
tabulator tabulator Lbl_NumberOfDoors: Label 'Number of Doors'; newline
tabulator tabulator Lbl_EngineDisplacement: Label 'Engine Displacement (ccm)'; newline
tabulator tabulator Lbl_EnginePower: Label 'Engine Power (kW)'; newline
tabulator tabulator Lbl_ConsumptionCombined: Label 'Consumption Combined (l/100 km)'; newline
tabulator tabulator Lbl_CO2Emissions: Label 'CO2 Emissions (g/km)'; newline
tabulator tabulator Lbl_EmissionStandardCode: Label 'Emission Standard Code'; newline
tabulator tabulator Lbl_ObjectPriceWithVAT: Label 'Object Price With VAT (LCY)'; newline
tabulator tabulator Lbl_ValidFrom: Label 'Valid from'; newline
tabulator tabulator Lbl_CreatedAt: Label 'Created At'; newline
tabulator tabulator Lbl_DateModelIntro: Label 'Date Model Intro'; newline
tabulator tabulator Lbl_DriveCode: Label 'Drive Code'; newline
tabulator tabulator Lbl_FuelType: Label 'Fuel Type'; newline
tabulator tabulator Lbl_PowerTrainCode: Label 'Power Train Code'; newline
tabulator tabulator Lbl_Gearbox: Label 'Gearbox'; newline
tabulator tabulator Lbl_NumberOfGears: Label 'Number of Gears'; newline
tabulator tabulator Lbl_JATOSegmentName: Label 'JATO Segment Name'; newline
tabulator tabulator Lbl_SegmentCode: Label 'Segment Code'; newline
tabulator tabulator Lbl_Cylinders: Label 'Cylinders'; newline
tabulator tabulator Lbl_EngineTorgue: Label 'Engine Torgue (Nm)'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CatalogueNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Name, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Activa, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Visible, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RVGridCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MRGridCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_GroupName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_MakeName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModeLineName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelTypeName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TrimClassificationCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TypeOfBody, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_BodyTypeName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_NumberOfDoors, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EngineDisplacement, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EnginePower, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ConsumptionCombined, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CO2Emissions, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EmissionStandardCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ObjectPriceWithVAT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ValidFrom, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CreatedAt, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DateModelIntro, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DriveCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FuelType, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PowerTrainCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Gearbox, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_NumberOfGears, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_JATOSegmentName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_SegmentCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Cylinders, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EngineTorgue, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
