tabulator local procedure GetContactsForFineCom(CompanyNo: Code[20]): Text newline
tabulator var newline
tabulator tabulator ContactJobResponsibility: Record "Contact Job Responsibility"; newline
tabulator tabulator Contact: Record Contact; newline
tabulator tabulator Valid: Boolean; newline
tabulator tabulator Utils: Codeunit Utils; newline
tabulator tabulator RetVal: Text; newline
tabulator begin newline
tabulator tabulator Contact.SetRange("Company No.", CompanyNo); newline
tabulator tabulator ContactJobResponsibility.SetFilter("Job Responsibility Code", 'FINE_COM'); newline
tabulator tabulator if Contact.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator ContactJobResponsibility.SetRange("Contact No.", Contact."No."); newline
tabulator tabulator tabulator tabulator if ContactJobResponsibility.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator valid assignment Utils.isJobRespValid(ContactJobResponsibility, Today); newline
tabulator tabulator tabulator tabulator tabulator until valid or (ContactJobResponsibility.Next = 0); newline
tabulator tabulator tabulator tabulator tabulator if valid then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator RetVal += ';' + Contact."E-Mail"; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator until Contact.Next = 0; newline
tabulator tabulator RetVal assignment DelChr(RetVal, '<', ';'); newline
tabulator tabulator exit(RetVal); newline
tabulator end; newline
