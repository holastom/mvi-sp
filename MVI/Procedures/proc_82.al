tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 17); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 19); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 53); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 24); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 24); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 21); newline
tabulator end; newline
