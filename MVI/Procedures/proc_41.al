tabulator local procedure UpdateMinimumSigners() newline
tabulator var newline
tabulator tabulator c: Record Contact; newline
tabulator begin newline
tabulator tabulator c.SetRange(Type, "Contact Type"::Company); newline
tabulator tabulator c.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if c."BLG Minimum Signers" < 1 then begin newline
tabulator tabulator tabulator tabulator c.Validate("BLG Minimum Signers", 1); newline
tabulator tabulator tabulator tabulator c.Modify(true) newline
tabulator tabulator tabulator end; newline
tabulator tabulator until c.Next() = 0; newline
tabulator end; newline
