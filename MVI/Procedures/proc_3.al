tabulator procedure MakeColoredText(var TmpExcelBuffer: Record "Excel Buffer" temporary; Color: Text; FromColumnNo: Integer; ToColumnNo: Integer; FromRowNo: Integer) newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetFilter("Row No.", '>=%1', FromRowNo); newline
tabulator tabulator TmpExcelBuffer.SetFilter("Column No.", '%1..%2', FromColumnNo, ToColumnNo); newline
tabulator tabulator TmpExcelBuffer.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger(Color)); newline
tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator until TmpExcelBuffer.Next() = 0; newline
tabulator end; newline
