tabulator local procedure GetFinancingContractHeaders(var FCHColection: Record "API Financing Contract Header" temporary; FCHOrigin: Record "API Financing Contract Header") newline
tabulator var newline
tabulator tabulator FCH: Record "API Financing Contract Header"; newline
tabulator begin newline
tabulator tabulator FCH.SetRange("New Customer No.", FCHOrigin."New Customer No."); newline
tabulator tabulator FCH.SetRange("Customer No.", FCHOrigin."Customer No."); newline
tabulator tabulator FCH.SetFilter(Status, '%1|%2', "API Financing Contract Status"::Active, "API Financing Contract Status"::Signed); newline
tabulator tabulator if FCHOrigin."Financing Type" = FCHOrigin."Financing Type"::"Operative Leasing" then newline
tabulator tabulator tabulator FCH.SetRange("Financing Type", FCHOrigin."Financing Type") newline
tabulator tabulator else newline
tabulator tabulator tabulator FCH.SetFilter("Financing Type", 'notequal%1', FCHOrigin."Financing Type"::"Operative Leasing"); newline
tabulator tabulator FCH.SetRange("Contract Transfer Status", "API Contract Transfer Status"::"Request Received"); newline
tabulator tabulator FCH.SetFilter("Transfer Request Receipt Date", '%1..%2', CalcDate('<-CM>', Today()), CalcDate('<+CM>', Today())); newline
tabulator tabulator if FCH.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator FCHColection assignment FCH; newline
tabulator tabulator tabulator tabulator FCHColection.Insert(); newline
tabulator tabulator tabulator until FCH.Next = 0; newline
tabulator end; newline
