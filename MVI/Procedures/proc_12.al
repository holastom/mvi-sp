tabulator procedure GrandTotal(First: Decimal; Second: Decimal; var TmpExcelBuffer: Record "Excel Buffer" temporary; Len: Integer) newline
tabulator var newline
tabulator tabulator x: Integer; newline
tabulator tabulator Lbl_GrandTotal: Label 'Grand Total'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.NewRow(); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_GrandTotal, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator for x assignment 4 to Len do begin // Starts at  +1 on Grand Total +2 on First and Second newline
tabulator tabulator tabulator TmpExcelBuffer.AddColumn('', false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator end; newline
tabulator tabulator TmpExcelBuffer.AddColumn(First, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Second, false, '', false, false, false, '# ### ##0.,00', TmpExcelBuffer."Cell Type"::Number); newline
tabulator end; newline
