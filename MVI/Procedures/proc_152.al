tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 17); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 27); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 24); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('N', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('O', 19); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('P', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Q', 31); newline
tabulator end; newline
