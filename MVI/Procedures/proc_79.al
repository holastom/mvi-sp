tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_CustomerNo: Label 'Customer No.'; newline
tabulator tabulator Lbl_CompRegNo: Label 'Comp Reg No.'; newline
tabulator tabulator Lbl_RegName: Label 'Reg Name'; newline
tabulator tabulator Lbl_BPA: Label 'Business Place - Ulice'; newline
tabulator tabulator Lbl_BPC: Label 'Business Place - Město'; newline
tabulator tabulator Lbl_Language: Label 'Language'; newline
tabulator tabulator Lbl_JR: Label 'Job resp'; newline
tabulator tabulator Lbl_JRDesc: Label 'Job resp - Description'; newline
tabulator tabulator Lbl_CName: Label 'Contact Name'; newline
tabulator tabulator Lbl_Email: Label 'E-Mail'; newline
tabulator tabulator Lbl_Mobile: Label 'Mobile'; newline
tabulator tabulator Lbl_AMBI: Label 'AMBI'; newline
tabulator tabulator Lbl_AMBU: Label 'AMBU'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CustomerNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CompRegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_BPA, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_BPC, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Language, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_JR, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_JRDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Email, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Mobile, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AMBI, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AMBU, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
