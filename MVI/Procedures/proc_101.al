tabulator local procedure Months(n: integer; lang: Text): Text newline
tabulator begin newline
tabulator tabulator case lang of newline
tabulator tabulator tabulator 'CZ': newline
tabulator tabulator tabulator tabulator case n of newline
tabulator tabulator tabulator tabulator tabulator 1: newline
tabulator tabulator tabulator tabulator tabulator tabulator exit(LBL_120CZa); newline
tabulator tabulator tabulator tabulator tabulator 2 .. 4: newline
tabulator tabulator tabulator tabulator tabulator tabulator exit(LBL_120CZb); newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator exit(LBL_120CZc); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'EN': newline
tabulator tabulator tabulator tabulator case n of newline
tabulator tabulator tabulator tabulator tabulator 1: newline
tabulator tabulator tabulator tabulator tabulator tabulator exit(LBL_120ENa); newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator exit(LBL_120ENb); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator end; newline
tabulator end; newline
