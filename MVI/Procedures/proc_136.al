tabulator local procedure GetBusLocAdress(cust: Record Customer; bpcode: Code[20]) newline
tabulator var newline
tabulator tabulator busloc: Record "API Contact Business Location"; newline
tabulator begin newline
tabulator tabulator if bpcode notequal '' then newline
tabulator tabulator tabulator if busloc.Get(cust."Primary Contact No.", bpcode) then newline
tabulator tabulator tabulator tabulator if not ContAltAdr.Get(busloc."Contact No.", busloc."BLG Corr. Address Code") then newline
tabulator tabulator tabulator tabulator tabulator Clear(ContAltAdr); newline
tabulator end; newline
