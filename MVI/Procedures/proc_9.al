tabulator procedure CreateCSVFile(var TmpExcelBuffer: Record "Excel Buffer" temporary; AttachmentFilePath: Text; OutStream: OutStream) newline
tabulator var newline
tabulator tabulator Attachment: File; newline
tabulator begin newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::Windows); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
tabulator tabulator ConvertExcelBufferToOutstream(OutStream, TmpExcelBuffer, ';'); newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
