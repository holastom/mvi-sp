tabulator local procedure TwoRecords(var TireDetailLine: Record "API Tire Detail Line"; var SecondTireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator var newline
tabulator tabulator Lbl_NoRear: Label 'Missing entry for rear Tires (year-round).'; newline
tabulator tabulator Lbl_NoFront: Label ' Missing entry for rear Tires (year-round).'; newline
tabulator begin newline
tabulator tabulator SecondTireDetailLine.SetRange(Season, "API Tire Season"::"Year-Round"); newline
tabulator tabulator if TireDetailLine.Position = "API Tire Position"::Front then begin newline
tabulator tabulator tabulator SecondTireDetailLine.SetRange(Position, "API Tire Position"::Rear); newline
tabulator tabulator tabulator // Vime, ze existuje pouze jeden zaznam newline
tabulator tabulator tabulator if not SecondTireDetailLine.FindFirst() then Error(Lbl_NoRear); newline
tabulator tabulator end else begin newline
tabulator tabulator tabulator SecondTireDetailLine.SetRange(Position, "API Tire Position"::Front); newline
tabulator tabulator tabulator // Vime, ze existuje pouze jeden zaznam newline
tabulator tabulator tabulator if not SecondTireDetailLine.FindFirst() then Error(Lbl_NoFront); newline
tabulator tabulator end; newline
tabulator tabulator exit(CompareTwoDimensions(TireDetailLine, SecondTireDetailLine)); newline
tabulator end; newline
