tabulator procedure GroupByMaintancePermision(var FCHCodes: List of [Code[20]]; SalesInvoiceheader: Record "Sales Invoice Header") newline
tabulator var newline
tabulator tabulator SalesInvoiceLine: Record "Sales Invoice Line"; newline
tabulator begin newline
tabulator tabulator SalesInvoiceLine.SetRange("Document No.", SalesInvoiceheader."No."); newline
tabulator tabulator SalesInvoiceLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator SalesInvoiceLine.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if not FCHCodes.Contains(SalesInvoiceLine."API Maintenance Approval No.") then newline
tabulator tabulator tabulator tabulator FCHCodes.Add(SalesInvoiceLine."API Maintenance Approval No."); newline
tabulator tabulator until SalesInvoiceLine.Next() = 0; newline
tabulator end; newline
