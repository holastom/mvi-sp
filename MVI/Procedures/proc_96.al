tabulator local procedure GetPaymentTermDescriptionEN(TimePeriod: Code[10]) ReturnValue: Text newline
tabulator var newline
tabulator tabulator NoTranslation: Label 'There is no translation for Payment Term: %1'; newline
tabulator tabulator PaymentTerms: record "Payment Terms"; newline
tabulator tabulator PaymentTermTranslation: Record "Payment Term Translation"; newline
tabulator begin newline
tabulator tabulator if (TimePeriod = '') or not PaymentTerms.Get(TimePeriod) then newline
tabulator tabulator tabulator exit(''); newline
tabulator tabulator if not PaymentTermTranslation.Get(TimePeriod, 'EN') then newline
tabulator tabulator tabulator Error(StrSubstNo(NoTranslation, TimePeriod)); newline
tabulator tabulator exit(PaymentTermTranslation.Description); newline
tabulator end; newline
