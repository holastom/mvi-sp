tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_Invoice: Label 'Invoice'; newline
tabulator tabulator Lbl_InvoiceDate: Label 'InvoiceDate'; newline
tabulator tabulator Lbl_Driver: Label 'Driver'; newline
tabulator tabulator Lbl_ContractNo: Label 'Contract No'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No'; newline
tabulator tabulator Lbl_ModelDesc: Label 'Model Desc'; newline
tabulator tabulator Lbl_VatChargeable: Label 'Vat chargeable'; newline
tabulator tabulator Lbl_VatNotChargeable: Label 'Vat not chargeable'; newline
tabulator tabulator Lbl_TotalNetto: Label 'Total netto'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Invoice, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InvoiceDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Driver, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelDesc, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VatChargeable, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VatNotChargeable, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_TotalNetto, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
