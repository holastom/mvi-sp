tabulator local procedure ClearAllJRRoles() newline
tabulator var newline
tabulator tabulator jr: Record "Job Responsibility"; newline
tabulator begin newline
tabulator tabulator jr.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator jr.Validate("BLG Roles Allowed Assign", ''); newline
tabulator tabulator tabulator jr.Modify(true); newline
tabulator tabulator until jr.Next() = 0; newline
tabulator end; newline
