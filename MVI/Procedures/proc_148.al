tabulator local procedure FillEmailTexts(ObjOrEquip: Enum "API OrderContentType") newline
tabulator var newline
tabulator tabulator CI: Record "Company Information"; newline
tabulator begin newline
tabulator tabulator CI.Get(); newline
tabulator tabulator EmailTxt1 assignment 'Vážený obchodní partnere,' + Utils.crlf() + Utils.crlf(); newline
tabulator tabulator if ObjOrEquip = "API OrderContentType"::"Additional Equipment" then begin newline
tabulator tabulator tabulator // TEXT 1 newline
tabulator tabulator tabulator EmailTxt1 += 'tímto u Vás objednáváme dodatečnou výbavu dle objednávky v příloze.'; newline
tabulator tabulator tabulator EmailTxt1 += Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt1 += 'Prosíme o potvrzení objednávky.'; newline
tabulator tabulator tabulator EmailTxt1 += Utils.crlf() + Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt1 += 'Děkujeme.'; newline
tabulator tabulator tabulator // TEXT 2 newline
tabulator tabulator tabulator EmailTxt2 assignment ''; newline
tabulator tabulator tabulator // TEXT 3 newline
tabulator tabulator tabulator EmailTxt3 assignment ''; newline
tabulator tabulator end else begin newline
tabulator tabulator tabulator // TEXT 1 newline
tabulator tabulator tabulator EmailTxt1 += 'tímto Vám potvrzujeme naši objednávku na dodání vozidla, jehož specifikaci naleznete v příloze.'; newline
tabulator tabulator tabulator EmailTxt1 += Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt1 += 'Prosíme Vás o její potvrzení a zaslání na e-mailovou adresu kontaktní osoby, která objednávku vystavila, do 2 pracovních dnů od obdržení této objednávky.'; newline
tabulator tabulator tabulator // TEXT 2 newline
tabulator tabulator tabulator EmailTxt2 assignment 'Potvrzené, resp. odsouhlasené, ceny uvedené v objednávce v příloze budou tímto považovány za garantované.'; newline
tabulator tabulator tabulator EmailTxt2 += Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt2 += 'Zároveň Vás žádáme o kontrolu termínu dodání vozidla. V případě změny termínu dodání vozidla nás prosím o této změně neprodleně informujte.'; newline
tabulator tabulator tabulator EmailTxt2 += Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt2 += 'V případě nedodržení ceny nebo neuskutečnění dodávky objednaného vozidla ve sjednaném rozsahu a termínu, si '; newline
tabulator tabulator tabulator EmailTxt2 += CI.Name; newline
tabulator tabulator tabulator EmailTxt2 += ' vyhrazuje právo odstoupit od objednávky a s ní spojené kupní smlouvy bez jakýchkoliv finančních závazků vůči Vaší společnosti.'; newline
tabulator tabulator tabulator // TEXT 3 newline
tabulator tabulator tabulator EmailTxt3 assignment 'Aby bylo možné v plánovaném termínu vozidlo předat uživateli / řidiči, musíme vozidlo v předstihu zaregistrovat a pojistit. '; newline
tabulator tabulator tabulator EmailTxt3 += Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += 'Z tohoto důvodu si Vás dovolujeme požádat o zaslání na níže uvedenou adresu nejméně 10 pracovních dnů před předpokládaným předáním vozidla:'; newline
tabulator tabulator tabulator EmailTxt3 += Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += Utils.tabChar() + '● originálu faktury za vozidlo,' + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += Utils.tabChar() + '● technického průkazu vozidla,' + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += Utils.tabChar() + '● COC listu, ' + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += Utils.tabChar() + '● vyplněného formuláře v příloze (strana 2) - na jakých rozměrech pneumatik přijelo vozidlo z výroby včetně rychlostního, zátěžového indexu a informace o TPMS ventilkách,' + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += Utils.tabChar() + '● typového listu od tažného zařízení (bylo - li montováno).'; newline
tabulator tabulator tabulator EmailTxt3 += Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += 'Po zaregistrování vozidla Vás budeme kontaktovat ve věci koordinace přesného termínu a času předání vozidla uživateli / řidiči, a následně Vám zašleme všechny nezbytné doklady (registrační značky, předávací protokoly, apod.).'; newline
tabulator tabulator tabulator EmailTxt3 += Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += 'Prosíme o potvrzení objednávky.'; newline
tabulator tabulator tabulator EmailTxt3 += Utils.crlf() + Utils.crlf() + Utils.crlf(); newline
tabulator tabulator tabulator EmailTxt3 += 'Děkujeme.'; newline
tabulator tabulator end; newline
tabulator end; newline
