tabulator local procedure UpdateActiveContractStatuses(UpdateActiveContractStatus: Code[10]) newline
tabulator var newline
tabulator tabulator dcs: Record "API Detail Contract Status"; newline
tabulator begin newline
tabulator tabulator dcs.Get(UpdateActiveContractStatus); newline
tabulator tabulator dcs.Validate("Fill Termination Date", true); newline
tabulator tabulator dcs.Validate("Fill Fin. Settlement Date", true); newline
tabulator tabulator dcs.Validate("Fill Real Termination Date", true); newline
tabulator tabulator dcs.Validate(Charge, true); newline
tabulator tabulator dcs.Validate(Reminder, true); newline
tabulator tabulator dcs.Validate("Change without Protocol", true); newline
tabulator tabulator dcs.Validate("Allow Down Payment Posting", true); newline
tabulator tabulator dcs.Validate("Allow Posting from Paym. Cal.", true); newline
tabulator tabulator dcs.Validate("Fill Termination Date", true); newline
tabulator tabulator dcs.Validate("BLG Allow Creating Sales Inv.", true); newline
tabulator tabulator dcs.Validate("Allow Contract Transfer", true); newline
tabulator tabulator dcs.Validate("Allow Multi Calculation", true); newline
tabulator tabulator dcs.Validate(Calculation, true); newline
tabulator tabulator dcs.Validate("Maintenance Permission", true); newline
tabulator tabulator dcs.Validate("BLG Customer Limit Draw", true); newline
tabulator tabulator dcs.Validate("Limit Draw", true); newline
tabulator tabulator dcs.Validate("Reservation Limit Draw", true); newline
tabulator tabulator dcs.Validate("Allow Accr./Cal.In.Posting", true); newline
tabulator tabulator dcs.Validate("Check Unclosed Guarantee", true); newline
tabulator tabulator dcs.Validate("Delete Partial Payment Credit", true); newline
tabulator tabulator dcs.Validate("Allow Object Ordering", true); newline
tabulator tabulator dcs.Validate("BLG Check VIN", true); newline
tabulator tabulator dcs.Validate("BLG Invoicing Guarantee", true); newline
tabulator tabulator dcs.Validate("BLG Secure Replacement Mob.", true); newline
tabulator tabulator dcs.Validate("BLG Check Registration", true); newline
tabulator tabulator dcs.Validate("BLG Create Handover Protocol", true); newline
tabulator tabulator dcs.Validate(Active, true); newline
tabulator tabulator dcs.Modify(true); newline
tabulator end; newline
