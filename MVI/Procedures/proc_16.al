tabulator procedure GroupByShortcutDimension(var FCHCodes: List of [Code[20]]; SalesInvoiceheader: Record "Sales Invoice Header") newline
tabulator var newline
tabulator tabulator SalesInvoiceLine: Record "Sales Invoice Line"; newline
tabulator begin newline
tabulator tabulator SalesInvoiceLine.SetRange("Document No.", SalesInvoiceHeader."No."); newline
tabulator tabulator SalesInvoiceLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator SalesInvoiceLine.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if not FCHCodes.Contains(SalesInvoiceLine."Shortcut Dimension 2 Code") then newline
tabulator tabulator tabulator tabulator FCHCodes.Add(SalesInvoiceLine."Shortcut Dimension 2 Code"); newline
tabulator tabulator until SalesInvoiceLine.Next() = 0; newline
tabulator end; newline
