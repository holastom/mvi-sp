tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 7); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 6); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 6); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 6); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 35); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 10); newline
tabulator end; newline
