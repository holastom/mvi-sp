tabulator local procedure GetFilterOnStatus(): Text newline
tabulator var newline
tabulator tabulator retval: Text; newline
tabulator begin newline
tabulator tabulator if Closed then newline
tabulator tabulator tabulator retval += '|Closed'; newline
tabulator tabulator if Settled then newline
tabulator tabulator tabulator retval += '|Settled'; newline
tabulator tabulator if Archived then newline
tabulator tabulator tabulator retval += '|Archived'; newline
tabulator tabulator exit(DelChr(retval, '<', '|')); newline
tabulator end; newline
