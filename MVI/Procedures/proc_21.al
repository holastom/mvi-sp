tabulator procedure FormatDecimal(Input: Decimal; N: Integer): Text newline
tabulator var newline
tabulator tabulator prc: Text; newline
tabulator tabulator DF: Text; newline
tabulator begin newline
tabulator tabulator DF assignment '<Integer><Decimals,%1><Comma,.>'; newline
tabulator tabulator prc assignment StrSubstNo('%1', N + 1); newline
tabulator tabulator exit(Format(Input, 0, StrSubstNo(DF, prc))); newline
tabulator end; newline
