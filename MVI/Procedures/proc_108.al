tabulator local procedure FormatSameDimensionLine(TireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator begin newline
tabulator tabulator exit(TranslateTireCategory(TireDetailLine."Tire Category", ENLangId) + ', ' // "Basic/Medium/Premium, " newline
tabulator tabulator tabulator  + LBL_193 + ': ' + GetTireDetailString(TireDetailLine) + Utils.crlf()); // "rozmer: ? / ? ?? ?? \n" newline
tabulator end; newline
