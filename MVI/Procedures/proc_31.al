tabulator procedure GetAmountInclVAT("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator tabulator Amount_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator Amount_Countrer assignment 0; newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SIL.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator Amount_Countrer += SIL."Amount Including VAT"; newline
tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(Amount_Countrer); newline
tabulator end; newline
