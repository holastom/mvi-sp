tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 18); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 74); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('N', 25); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('O', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('P', 23); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('Q', 21); newline
tabulator end; newline
