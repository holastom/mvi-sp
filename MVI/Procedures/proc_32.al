tabulator local procedure DueDate(): Date newline
tabulator var newline
tabulator tabulator TempText: Text; newline
tabulator tabulator Index: Integer; newline
tabulator tabulator Days: Integer; newline
tabulator begin newline
tabulator tabulator if SCMH."API Invoice Print Type" = "API Invoice Print Type"::Payment then newline
tabulator tabulator tabulator exit(CalcDate('<-CM>', SCMH."Posting Date")); newline
tabulator tabulator TempText assignment SCMH."Payment Terms Code"; newline
tabulator tabulator Index assignment TempText.IndexOf('D'); newline
tabulator tabulator Evaluate(Days, TempText.Substring(1, Index - 1)); newline
tabulator tabulator exit(SCMH."Posting Date" + Days); newline
tabulator end; newline
