tabulator local procedure ServiceValid(contServ: Record "API Contract Service"): Boolean newline
tabulator begin newline
tabulator tabulator if FCH_CALCULATION then begin newline
tabulator tabulator tabulator if StartOfNextMonth > contServ."Valid To" then newline
tabulator tabulator tabulator tabulator if StartOfNextMonth > contServ."Valid To after Extension" then newline
tabulator tabulator tabulator tabulator tabulator exit(false) newline
tabulator tabulator end else newline
tabulator tabulator tabulator if ((contServ."Service Status" = "API Service Status"::Preparation) newline
tabulator tabulator tabulator tabulator and ((Today() < contServ."Valid From") or (Today() > contServ."Valid To after Extension"))) then newline
tabulator tabulator tabulator tabulator exit(false); newline
 newline
tabulator tabulator exit(true); newline
tabulator end; newline
