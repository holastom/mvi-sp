tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 19); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 27); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 26); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 24); newline
tabulator end; newline
