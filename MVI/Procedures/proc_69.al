tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_ContractNumber: Label 'Contract number'; newline
tabulator tabulator Lbl_LicencePlate: Label 'Licence Plate'; newline
tabulator tabulator Lbl_VIN: Label 'VIN'; newline
tabulator tabulator Lbl_AccountCode: Label 'Account Code'; newline
tabulator tabulator Lbl_AccountName: Label 'Account Name'; newline
tabulator tabulator Lbl_ActualEndDate: Label 'Actual end date'; newline
tabulator tabulator Lbl_ContractualEndDate: Label 'Contractual End Date'; newline
tabulator tabulator Lbl_FinancingPeriod: Label 'Financing Period (in Months)'; newline
tabulator tabulator Lbl_FinancingPeriodReal: Label 'Financing Period Real'; newline
tabulator tabulator Lbl_FinancingPeriodDifference: Label 'Financing Period Difference'; newline
tabulator tabulator Lbl_Status: Label 'Status'; newline
tabulator tabulator Lbl_DetailedContractStatus: Label 'Detailed Contract Status'; newline
tabulator tabulator Lbl_EarlyTerminationReason: Label 'Early Termination Reason'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNumber, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_LicencePlate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VIN, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccountName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ActualEndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractualEndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinancingPeriod, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinancingPeriodReal, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinancingPeriodDifference, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Status, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DetailedContractStatus, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EarlyTerminationReason, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
