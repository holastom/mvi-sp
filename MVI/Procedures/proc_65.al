tabulator local procedure GetAmounts(var PojHav: Decimal; var PojPos: Decimal; var PojPov: Decimal; var AutFM: Decimal; "Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]) newline
tabulator var newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator begin newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SIL.SetRange("Gen. Prod. Posting Group", 'INS_CASCO'); newline
tabulator tabulator if SIL.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojHav += SIL.Amount; newline
tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator tabulator Clear(SIL); newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SIL.SetRange("Gen. Prod. Posting Group", 'INS_SEATS'); newline
tabulator tabulator if SIL.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojPos += SIL.Amount; newline
tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator tabulator Clear(SIL); newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SIL.SetRange("Gen. Prod. Posting Group", 'INS_TPL'); newline
tabulator tabulator if SIL.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojPov += SIL.Amount; newline
tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator tabulator Clear(SIL); newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SIL.SetFilter("Gen. Prod. Posting Group", 'notequal%1|notequal%2|notequal%3', 'INS_CASCO', 'INS_SEATS', 'INS_TPL'); newline
tabulator tabulator if SIL.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator AutFM += SIL.Amount; newline
tabulator tabulator tabulator until SIL.Next() = 0; newline
tabulator end; newline
