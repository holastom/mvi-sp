tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 21); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 31); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 25); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 22); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 27); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('M', 27); newline
tabulator end; newline
