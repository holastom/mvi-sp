tabulator local procedure DashesIfEmpty(N_of_Dashes: Integer; Input: Text): Text newline
tabulator var newline
tabulator tabulator x: Integer; newline
tabulator tabulator RetVal: Text; newline
tabulator begin newline
tabulator tabulator RetVal assignment Input; newline
tabulator tabulator if Input = '' then newline
tabulator tabulator tabulator for x assignment 1 to N_of_Dashes do begin newline
tabulator tabulator tabulator tabulator RetVal += '_'; newline
tabulator tabulator tabulator end; newline
tabulator tabulator exit(RetVal); newline
tabulator end; newline
