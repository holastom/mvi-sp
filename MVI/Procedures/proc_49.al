tabulator local procedure GetAmounts(var PojHav: Decimal; var PojPos: Decimal; var PojPov: Decimal; var AutFM: Decimal; "Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]) newline
tabulator var newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetRange("Gen. Prod. Posting Group", 'INS_CASCO'); newline
tabulator tabulator if SCML.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojHav += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator Clear(SCML); newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetRange("Gen. Prod. Posting Group", 'INS_SEATS'); newline
tabulator tabulator if SCML.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojPos += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator Clear(SCML); newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetRange("Gen. Prod. Posting Group", 'INS_TPL'); newline
tabulator tabulator if SCML.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator PojPov += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator Clear(SCML); newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SCML.SetFilter("Gen. Prod. Posting Group", 'notequal%1|notequal%2|notequal%3', 'INS_CASCO', 'INS_SEATS', 'INS_TPL'); newline
tabulator tabulator if SCML.FindSet() then newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator AutFM += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator end; newline
