tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_InvoiceNo: Label 'Invoice number'; newline
tabulator tabulator Lbl_InvoiceDate: Label 'datum fakturace'; newline
tabulator tabulator Lbl_Description: Label 'Popis'; newline
tabulator tabulator Lbl_BLT: Label ' Basic lease term'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No'; newline
tabulator tabulator Lbl_DateFrom: Label 'období od'; newline
tabulator tabulator Lbl_DateTo: Label 'období do'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InvoiceNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InvoiceDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_BLT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DateFrom, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DateTo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
