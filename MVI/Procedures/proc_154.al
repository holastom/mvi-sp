tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 10); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 24); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 68); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 32); newline
tabulator end; newline
