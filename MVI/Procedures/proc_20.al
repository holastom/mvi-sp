tabulator procedure FillZeros(Input: Text; NoOfspaces: Integer): Text newline
tabulator var newline
tabulator tabulator Zeros: Text; newline
tabulator begin newline
tabulator tabulator Zeros assignment '00000000000000000000'; // 20 zeros newline
tabulator tabulator if StrLen(Input) > NoOfspaces then newline
tabulator tabulator tabulator exit(Input.Substring(1, NoOfspaces)) newline
tabulator tabulator else newline
tabulator tabulator tabulator exit(Zeros.Substring(1, NoOfspaces - StrLen(Input)) + Input); newline
tabulator end; newline
