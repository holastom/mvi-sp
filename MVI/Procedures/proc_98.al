tabulator local procedure SetVersionCalculationOfferCreationDate(contractNo: Text; offerCreationDate: Date) newline
tabulator var newline
tabulator tabulator contractChangeHistoryRecord: Record "API Contract Change History"; newline
tabulator begin newline
tabulator tabulator contractChangeHistoryRecord.SetFilter("Financing Contract No.", contractNo); newline
tabulator tabulator FCH_CALCULATION assignment contractChangeHistoryRecord.IsEmpty; newline
tabulator tabulator if FCH_CALCULATION then begin newline
tabulator tabulator tabulator FCH_Offer_Creation_Date assignment offerCreationDate; newline
tabulator tabulator end else begin newline
tabulator tabulator tabulator FCH_Offer_Creation_Date assignment Today; newline
tabulator tabulator tabulator contractChangeHistoryRecord.FindLast; newline
tabulator tabulator tabulator FCH_ACTIVATION assignment contractChangeHistoryRecord."Change Code" = 'ACTIVATION'; newline
tabulator tabulator tabulator if not FCH_ACTIVATION then newline
tabulator tabulator tabulator tabulator FCH_VERSION assignment strsubstno('_%1', contractChangeHistoryRecord."Change Order No."); newline
tabulator tabulator end; newline
tabulator end; newline
