tabulator local procedure FourRecordsSummer(var TireDetailLine: Record "API Tire Detail Line"; var SecondTireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator var newline
tabulator tabulator Lbl_NoFrontSummer: Label 'Missing entry for front summer Tires.'; newline
tabulator tabulator Lbl_NoRearSummer: Label 'Missing entry for rear summer Tires.'; newline
tabulator begin newline
tabulator tabulator TireDetailLine.SetRange(Position, "API Tire Position"::Front); newline
tabulator tabulator TireDetailLine.SetRange(Season, "API Tire Season"::Summer); newline
tabulator tabulator if not TireDetailLine.FindFirst() then Error(Lbl_NoFrontSummer); newline
tabulator tabulator SecondTireDetailLine.SetRange(Position, "API Tire Position"::Rear); newline
tabulator tabulator SecondTireDetailLine.SetRange(Season, "API Tire Season"::Summer); newline
tabulator tabulator if not SecondTireDetailLine.FindFirst() then Error(Lbl_NoRearSummer); newline
tabulator tabulator exit(CompareSeasonTwoDimensions(TireDetailLine, SecondTireDetailLine)); newline
tabulator end; newline
