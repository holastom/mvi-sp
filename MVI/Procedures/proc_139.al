tabulator local procedure _ContTypeOrder(ph: Record "Purchase Header"): Integer newline
tabulator begin newline
tabulator tabulator case ph."API Order Content Type" of newline
tabulator tabulator tabulator ph."API Order Content Type"::Object: newline
tabulator tabulator tabulator tabulator exit(0); newline
tabulator tabulator tabulator ph."API Order Content Type"::"Additional Equipment": newline
tabulator tabulator tabulator tabulator exit(1); newline
tabulator tabulator tabulator ph."API Order Content Type"::"Tire/Rim": newline
tabulator tabulator tabulator tabulator exit(2); newline
tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator exit(3); newline
tabulator tabulator end; newline
tabulator end; newline
