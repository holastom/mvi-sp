tabulator local procedure CreateHeader() newline
tabulator var newline
tabulator tabulator Lbl_OrderNo: Label 'Číslo objednavky'; newline
tabulator tabulator Lbl_InvoiceNo: Label 'Faktura čislo'; newline
tabulator tabulator Lbl_ContractNo: Label 'Smlouva čislo'; newline
tabulator tabulator Lbl_ModelDescription: Label 'Detail'; newline
tabulator tabulator Lbl_DetailInvoice: Label 'Detail faktura'; newline
tabulator tabulator Lbl_RegNo: Label 'RZ'; newline
tabulator tabulator Lbl_VAT: Label 'Daň'; newline
tabulator tabulator Lbl_Netto: Label 'Netto'; newline
tabulator tabulator Lbl_AmountExclVAT: Label 'Nedaněné'; newline
tabulator tabulator Lbl_TotalNetto: Label 'Total netto'; newline
tabulator tabulator Lbl_PrintDate: Label 'Datum vyst.'; newline
tabulator tabulator Lbl_VATDate: Label 'Datum zd.plnění'; newline
tabulator tabulator Lbl_DueDate: Label 'Datum splatnosti'; newline
tabulator tabulator Lbl_Description: Label 'Popis'; newline
tabulator tabulator Lbl_Driver: Label 'Řidič'; newline
tabulator tabulator Lbl_Department: Label 'Středisko'; newline
tabulator tabulator Lbl_AccCode: Label 'Account code'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_OrderNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_InvoiceNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelDescription, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DetailInvoice, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VAT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Netto, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AmountExclVAT, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PrintDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_PrintDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VATDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DueDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Description, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Driver, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Department, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_AccCode, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
