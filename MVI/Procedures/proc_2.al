tabulator procedure MakeColoredRows(var TmpExcelBuffer: Record "Excel Buffer" temporary) newline
tabulator var newline
tabulator tabulator Row: Integer; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator Row assignment TmpExcelBuffer."Row No."; newline
tabulator tabulator tabulator if (Row > 1) and (Row Mod 2 = 1) then begin newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.Validate("Foreground Color", TmpExcelBuffer.HexARGBToInteger('CCDBDC')); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.Validate("Font Name", 'Arial'); newline
tabulator tabulator tabulator tabulator TmpExcelBuffer.WriteCellFormula(TmpExcelBuffer); newline
tabulator tabulator tabulator end; newline
tabulator tabulator until TmpExcelBuffer.Next() = 0; newline
tabulator end; newline
