tabulator procedure CreateExcelBuffer(var TmpExcelBuffer: Record "Excel Buffer" temporary; AttachmentFilePath: Text; Len: Integer; OutStream: OutStream; SheetName: Text) newline
tabulator var newline
tabulator tabulator Attachment: File; newline
tabulator tabulator BLFM: Codeunit "BL File Manager"; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.CreateNewBook(SheetName); newline
tabulator tabulator TmpExcelBuffer.WriteSheet(SheetName, CompanyName, UserId); newline
tabulator tabulator if Len notequal 0 then newline
tabulator tabulator tabulator BLFM.CustomizeCells(TmpExcelBuffer, Len); newline
tabulator tabulator TmpExcelBuffer.CloseBook(); newline
 newline
tabulator tabulator Attachment.Create(AttachmentFilePath, TextEncoding::Windows); newline
tabulator tabulator Attachment.CreateOutStream(OutStream); newline
tabulator tabulator TmpExcelBuffer.SaveToStream(OutStream, true); newline
tabulator tabulator Attachment.Close(); newline
tabulator end; newline
