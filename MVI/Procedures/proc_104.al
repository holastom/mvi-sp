tabulator local procedure FillInsuranceOrder(InsContract: Record "API Insurance Contract") newline
tabulator begin newline
tabulator tabulator case InsContract."Insurance Product No." of newline
tabulator tabulator tabulator 'MTPL': newline
tabulator tabulator tabulator tabulator InsuranceProductNoOrder assignment 10; newline
tabulator tabulator tabulator 'CASCO': newline
tabulator tabulator tabulator tabulator InsuranceProductNoOrder assignment 20; newline
tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 999; newline
tabulator tabulator end; newline
tabulator end; newline
