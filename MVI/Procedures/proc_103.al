tabulator local procedure FillServiceDescription(Service: Record "API Contract Service") newline
tabulator var newline
tabulator tabulator serviceTypes: Record "API Service Type"; newline
tabulator begin newline
tabulator tabulator serviceTypes.Get(Service."Service Kind", Service."Service Type Code"); newline
tabulator tabulator CS_Service_Description assignment Utils.iif(isLocalLanguage, serviceTypes."BLG Description Alt.", serviceTypes.Description); newline
tabulator end; newline
