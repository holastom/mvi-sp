tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 11); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 9); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 33); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 12); newline
tabulator end; newline
