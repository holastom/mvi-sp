tabulator local procedure FixAllTires() newline
tabulator var newline
tabulator tabulator TireDetailLine: Record "API Tire Detail Line"; newline
tabulator tabulator SecondTireDetailLine: Record "API Tire Detail Line"; newline
tabulator begin newline
tabulator tabulator TireDetailLine.SetRange("Contract No.", FinCon); newline
tabulator tabulator TireDetailLine.SetRange("Service Contract Type", TireDetailLine."Service Contract Type"::Contract); newline
tabulator tabulator TireDetailLine.SetCurrentKey("Line No."); newline
tabulator tabulator TireDetailLine.SetAscending("Line No.", false); newline
tabulator tabulator if TireDetailLine.FindFirst() then begin newline
tabulator tabulator tabulator SecondTireDetailLine assignment TireDetailLine; newline
tabulator tabulator tabulator SecondTireDetailLine."Line No." += 10000; newline
tabulator tabulator tabulator SecondTireDetailLine."Tire Category" assignment TirCat; newline
tabulator tabulator tabulator SecondTireDetailLine.Position assignment Pos; newline
tabulator tabulator tabulator SecondTireDetailLine.Width assignment u.iif(Width notequal '', Width, TireDetailLine.Width); newline
tabulator tabulator tabulator SecondTireDetailLine.Profile assignment u.iif(TProfile notequal '', TProfile, TireDetailLine.Profile); newline
tabulator tabulator tabulator SecondTireDetailLine.RIM assignment u.iif(Diameter notequal '', Diameter, TireDetailLine.RIM); newline
tabulator tabulator tabulator SecondTireDetailLine."Load Index" assignment u.iif(Index notequal '', Index, TireDetailLine."Load Index"); newline
tabulator tabulator tabulator SecondTireDetailLine."Speed Category" assignment u.iif(Speed notequal '', Speed, TireDetailLine."Speed Category"); newline
tabulator tabulator tabulator SecondTireDetailLine.Insert(); newline
tabulator tabulator end; newline
tabulator end; newline
