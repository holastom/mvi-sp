tabulator local procedure CompareSeasonTwoDimensions(TireDetailLine: Record "API Tire Detail Line"; SecondTireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator begin newline
tabulator tabulator if GetTireDetailString(TireDetailLine) = GetTireDetailString(SecondTireDetailLine) then newline
tabulator tabulator tabulator //Maji stejne rozmery newline
tabulator tabulator tabulator exit(FormatSeasonSameDimensionsLine(TireDetailLine)) newline
tabulator tabulator else newline
tabulator tabulator tabulator //nemaji stejny rozmer, naformatujeme oba zvlast newline
tabulator tabulator tabulator exit(FormatSeasonLine(TireDetailLine, SecondTireDetailLine)); newline
tabulator end; newline
