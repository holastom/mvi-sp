tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_CustomerName: Label 'Customer Name', Comment = 'cs-CZ=Název zákazníka'; newline
tabulator tabulator Lbl_ContractNo: Label 'Contract No.', Comment = 'cs-CZ=Číslo smlouvy'; newline
tabulator tabulator Lbl_LicencePlateNo: Label 'Licence Plate No.', Comment = 'cs-CZ=Registrační značka'; newline
tabulator tabulator Lbl_VersionNo: Label 'Version no.', Comment = 'cs-CZ=Číslo verze'; newline
tabulator tabulator Lbl_ChangeReason: Label 'Change Reason', Comment = 'cs-CZ=Důvod změny'; newline
tabulator tabulator Lbl_FinancingPeriod: Label 'Financing Period (months)', Comment = 'cs-CZ=Doba nájmu (měsíce)'; newline
tabulator tabulator Lbl_StartDate: Label 'Start Date', Comment = 'cs-CZ=Datum počátku'; newline
tabulator tabulator Lbl_EndDate: Label 'End Date', Comment = 'cs-CZ=Datum konce'; newline
tabulator tabulator Lbl_EffectiveDate: Label 'Effective Date', Comment = 'cs-CZ=Účinnost změny'; newline
tabulator tabulator Lbl_ContractualDistance: Label 'Contractual Distance (km)', Comment = 'cs-CZ=Smluvní nájezd (km)'; newline
tabulator tabulator Lbl_YearlyDistance: Label 'Yearly Distance (km)', Comment = 'cs-CZ=Roční nájezd (km)'; newline
tabulator tabulator Lbl_Payment: Label 'Payment (CZK w/o VAT)', Comment = 'cs-CZ=Splátka (Kč bez DPH)'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CustomerName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_LicencePlateNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_VersionNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ChangeReason, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_FinancingPeriod, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StartDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EffectiveDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ContractualDistance, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_YearlyDistance, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Payment, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
