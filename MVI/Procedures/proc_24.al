tabulator procedure GetFinancingContractLineCM("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Record "API Financing Contract Line"; newline
tabulator var newline
tabulator tabulator FCL: Record "API Financing Contract Line"; newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SCML.FindFirst() then // Potrebujeme najit radku faktury - je jedno jaka, vsechny k jedne smlouve by mely vest na stejnou splatku newline
tabulator tabulator tabulator if FCL.Get("Original Financing Contract No.", SCML."API Contract Line Type", SCML."API Contract Line No.") then  // radka splatkoveho kalendare pro danou smlouvu newline
tabulator tabulator tabulator tabulator exit(FCL); newline
tabulator end; newline
