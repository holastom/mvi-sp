tabulator local procedure CreateCellWidth() newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('A', 17); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('B', 14); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('C', 19); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('D', 12); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('E', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('F', 26); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('G', 15); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('H', 13); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('I', 16); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('J', 26); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('K', 20); newline
tabulator tabulator TmpExcelBuffer.SetColumnWidth('L', 24); newline
tabulator end; newline
