tabulator local procedure LongAddr(addr: Text; addr2: Text; city: Text; postcode: Text): Text newline
tabulator begin newline
tabulator tabulator if addr2 notequal '' then addr += ' ' + addr2; newline
tabulator tabulator if (city notequal '') or (postcode notequal '') then begin newline
tabulator tabulator tabulator addr += ','; newline
tabulator tabulator tabulator if postcode notequal '' then addr += ' ' + postcode; newline
tabulator tabulator tabulator if city notequal '' then addr += ' ' + city; newline
tabulator tabulator end; newline
tabulator tabulator exit(addr); newline
tabulator end; newline
