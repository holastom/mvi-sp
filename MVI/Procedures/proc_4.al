tabulator procedure FillSalutation(): Text newline
tabulator var newline
tabulator tabulator ReportingParameters: Codeunit "BLG Reporting Parameters"; newline
tabulator tabulator PrimaryContact: Record Contact; newline
tabulator tabulator Salutation: Record Salutation; newline
tabulator tabulator Contact: Record Contact; newline
tabulator begin newline
tabulator tabulator ReportingParameters.GetContactPerson(Contact); newline
tabulator tabulator if Contact."No." notequal '' then begin newline
tabulator tabulator tabulator if (Contact."BLG Surname-Salutation" notequal '') and (Contact."Salutation Code" notequal '') then begin newline
tabulator tabulator tabulator tabulator if Salutation.Get(Contact."Salutation Code") then newline
tabulator tabulator tabulator tabulator tabulator exit(Salutation.Description + ' ' + Contact."BLG Surname-Salutation"); newline
tabulator tabulator tabulator end else newline
tabulator tabulator tabulator tabulator if PrimaryContact.Get(Contact."Company No.") then newline
tabulator tabulator tabulator tabulator tabulator if PrimaryContact."BLG Client Status" notequal "BLG Client Status"::Customer then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator if Salutation.Get('GENERAL') then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator exit(Salutation.Description); newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator end; newline
tabulator tabulator if Salutation.Get('GENCUST') then newline
tabulator tabulator tabulator exit(Salutation.Description); newline
tabulator end; newline
