tabulator local procedure FormatLine(TireDetailLine: Record "API Tire Detail Line"; SecondTireDetailLine: Record "API Tire Detail Line"): Text newline
tabulator begin newline
tabulator tabulator exit(TranslateTireCategory(TireDetailLine."Tire Category", ENLangId) + ', ' // "Basic/Medium/Premium, " newline
tabulator tabulator tabulator  + LBL_193 + ' ' + Text.LowerCase(Format(TireDetailLine.Position)) + ': ' + GetTireDetailString(TireDetailLine) + ', ' // "rozmer predni: ? / ? ?? ??, " newline
tabulator tabulator tabulator  + LBL_193 + ' ' + Text.LowerCase(Format(SecondTireDetailLine.Position)) + ': ' + GetTireDetailString(SecondTireDetailLine) + Utils.crlf()); // "rozmer zadni: ? / ? ?? ?? \n" newline
tabulator end; newline
