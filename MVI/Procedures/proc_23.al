tabulator procedure GetSalesInvoiceLine("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Record "Sales Invoice Line"; newline
tabulator var newline
tabulator tabulator SIL: Record "Sales Invoice Line"; newline
tabulator begin newline
tabulator tabulator SIL.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SIL.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator SIL.SetFilter(Type, 'notequal%1', SIL.Type::" "); newline
tabulator tabulator if SIL.FindFirst() then; newline
tabulator tabulator exit(SIL); newline
tabulator end; newline
