tabulator local procedure FillServiceDetailsAndOrder(Service: Record "API Contract Service") newline
tabulator var newline
tabulator tabulator FSPricelist: Record "API Fee and Service Pricelist"; newline
tabulator tabulator HWTicketPricelist: Record "API Highway Ticket Pricelist"; newline
tabulator tabulator FCPricelist: Record "API Fuel Card Pricelist"; newline
tabulator tabulator RVPricelist: Record "API Repl. Vehicle Pricelist"; newline
tabulator tabulator RADetailLine: Record "API Rim Access. Serv. Det.Line"; newline
tabulator tabulator RAPricelist: Record "API Rim Accessories Pricelist"; newline
tabulator tabulator RIMDetailLine: Record "API Rim Detail Line"; newline
tabulator tabulator TireDetail: Record "API Tire Detail"; newline
tabulator tabulator TireDetailLine: Record "API Tire Detail Line"; newline
tabulator tabulator SecondTireDetailLine: Record "API Tire Detail Line"; newline
tabulator tabulator RIMType: Record "API Rim Type"; newline
tabulator tabulator RIMDescList: List of [Text]; newline
tabulator tabulator descDetail: Text; newline
tabulator tabulator rimDetail: Text; newline
 newline
tabulator tabulator // <-- TireService newline
tabulator tabulator yrfront: Text; newline
tabulator tabulator yrrear: Text; newline
tabulator tabulator sumfront: Text; newline
tabulator tabulator sumrear: Text; newline
tabulator tabulator winfront: Text; newline
tabulator tabulator winrear: Text; newline
tabulator begin newline
tabulator tabulator Clear(CS_B_Service_Description_Detail); newline
tabulator tabulator Clear(ShowSettlement); newline
tabulator tabulator case Service."Service Type Code" of newline
tabulator tabulator tabulator 'INTEREST': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 10; newline
tabulator tabulator tabulator tabulator tabulator ShowSettlement assignment true; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'SERVICE': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 20; newline
tabulator tabulator tabulator tabulator tabulator ShowSettlement assignment true; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'TIRE': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 30; newline
tabulator tabulator tabulator tabulator tabulator ShowSettlement assignment true; newline
tabulator tabulator tabulator tabulator tabulator TireDetail.Get(Service."Service Contract Type", Service."No."); newline
tabulator tabulator tabulator tabulator tabulator TireDetailLine.SetRange("Service Contract Type", Service."Service Contract Type"); newline
tabulator tabulator tabulator tabulator tabulator TireDetailLine.SetRange("Service No.", Service."No."); newline
tabulator tabulator tabulator tabulator tabulator TireDetailLine.SetRange(Season, "API Tire Season"::"Year-Round"); newline
tabulator tabulator tabulator tabulator tabulator SecondTireDetailLine.SetRange("Service Contract Type", Service."Service Contract Type"); newline
tabulator tabulator tabulator tabulator tabulator SecondTireDetailLine.SetRange("Service No.", Service."No."); newline
tabulator tabulator tabulator tabulator tabulator descDetail assignment LBL_190; newline
tabulator tabulator tabulator tabulator tabulator if TireDetailLine.FindSet() then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator // Celorocni pneu existuje -> pridame text Lbl 196a(celorocni) newline
tabulator tabulator tabulator tabulator tabulator tabulator descDetail += LBL_196a + Utils.crlf(); newline
tabulator tabulator tabulator tabulator tabulator tabulator descDetail += LBL_192b; newline
tabulator tabulator tabulator tabulator tabulator tabulator descDetail += TranslateTireCategory(TireDetailLine."Tire Category", ENLangId) + ', '; newline
tabulator tabulator tabulator tabulator tabulator tabulator // Kontrola zda existuje F+R nebo se jedna o predni / zadni newline
tabulator tabulator tabulator tabulator tabulator tabulator if TireDetailLine.Position = "API Tire Position"::"Front+Rear" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator // Existuje poze F+R - pouze jeden zanzam, nic nekontrolujeme pouze vypiseme dany radek newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += LBL_193 + ': ' + GetTireDetailString(TireDetailLine) + Utils.crlf(); newline
tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator // Existuji 2 zaznamy, jeden Front jeden Rear - musime porovnat rozmery newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += TwoRecords(TireDetailLine, SecondTireDetailLine); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator // Neni celorocni ale sezoni -> pridame text Lbl 196b (sezoni) newline
tabulator tabulator tabulator tabulator tabulator tabulator descDetail += LBL_196b + Utils.crlf(); newline
tabulator tabulator tabulator tabulator tabulator tabulator // vycistit filter, jinak to nic nenalezne, protoze ma zafixovane Year-Round newline
tabulator tabulator tabulator tabulator tabulator tabulator TireDetailLine.SetRange(Season); newline
tabulator tabulator tabulator tabulator tabulator tabulator TireDetailLine.FindSet(); newline
tabulator tabulator tabulator tabulator tabulator tabulator // Kontrola zda existuje F+R nebo se jedna o predni / zadni newline
tabulator tabulator tabulator tabulator tabulator tabulator if TireDetailLine.Position = "API Tire Position"::"Front+Rear" then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator // existuej F+R -> Existuji poze dva zaznamutabulator tabulator tabulator tabulator tabulator tabulator     newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += TwoRecordsSeason(TireDetailLine, SecondTireDetailLine); newline
tabulator tabulator tabulator tabulator tabulator tabulator end else begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator // else existuji 4 zaznamy, musim porovnat pro kazdou sezonu rozmery prvni a druhe napravy newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator // existuje jeden zaznam pro kombinaci letni - predni, letni - zadni, zimni - predni, zimni - zadni newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += FourRecordsSummer(TireDetailLine, SecondTireDetailLine); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += FourRecordsWinter(TireDetailLine, SecondTireDetailLine); newline
tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator if TireDetail."BLG Tires Amount Type" = "BLG Tires Amount Type"::Suitable then newline
tabulator tabulator tabulator tabulator tabulator tabulator descDetail += LBL_194 + LBL_195b newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator descDetail += LBL_194 + LBL_195a; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'T_CHANGE': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 40; newline
tabulator tabulator tabulator tabulator tabulator ShowSettlement assignment true; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'T_STORAGE': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 50; newline
tabulator tabulator tabulator tabulator tabulator ShowSettlement assignment true; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'RIM': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 60; newline
tabulator tabulator tabulator tabulator tabulator ShowSettlement assignment true; newline
tabulator tabulator tabulator tabulator tabulator RIMDetailLine.SetRange("Service Contract Type", Service."Service Contract Type"); newline
tabulator tabulator tabulator tabulator tabulator RIMDetailLine.SetRange("Service No.", Service."No."); newline
tabulator tabulator tabulator tabulator tabulator if RIMDetailLine.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator RIMType.Get(RIMDetailLine."Rim Code"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator rimDetail assignment ParseAndTrimDesc(RIMType.Description, '|'); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if (descDetail notequal '') and (rimDetail notequal '') then newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += ', '; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += rimDetail; newline
tabulator tabulator tabulator tabulator tabulator tabulator until RIMDetailLine.Next() = 0; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'RIM_ACC': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 70; newline
tabulator tabulator tabulator tabulator tabulator ShowSettlement assignment true; newline
tabulator tabulator tabulator tabulator tabulator RADetailLine.SetRange("Service No.", Service."No."); newline
tabulator tabulator tabulator tabulator tabulator if RADetailLine.FindSet() then newline
tabulator tabulator tabulator tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator RAPricelist.Get(RADetailLine."Rim Accessories Code"); newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator if (descDetail notequal '') and (Utils.iif(isLocalLanguage, RAPricelist."BLG Description Alt.", RAPricelist.Description) notequal '') then begin newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += ', '; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator tabulator tabulator tabulator tabulator descDetail += Utils.iif(isLocalLanguage, RAPricelist."BLG Description Alt.", RAPricelist.Description); newline
tabulator tabulator tabulator tabulator tabulator tabulator until RADetailLine.Next() = 0; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'ROAD_ASSIS': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 80; newline
tabulator tabulator tabulator tabulator tabulator FSPricelist.Get(Service."Service Code"); newline
tabulator tabulator tabulator tabulator tabulator descDetail assignment Utils.iif(isLocalLanguage, FSPricelist."BLG Fee Description Alt.", FSPricelist."Fee Description"); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'HW_STAMP': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 90; newline
tabulator tabulator tabulator tabulator tabulator HWTicketPricelist.Get(Service."Service Code"); newline
tabulator tabulator tabulator tabulator tabulator descDetail assignment Utils.iif(isLocalLanguage, HWTicketPricelist."BLG Description Alt.", HWTicketPricelist.Description); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'GPS_LOG': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 100; newline
tabulator tabulator tabulator tabulator tabulator FSPricelist.Get(Service."Service Code"); newline
tabulator tabulator tabulator tabulator tabulator descDetail assignment Utils.iif(isLocalLanguage, FSPricelist."BLG Fee Description Alt.", FSPricelist."Fee Description"); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'FUEL_CARD': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 110; newline
tabulator tabulator tabulator tabulator tabulator FCPricelist.Get(Service."Service Code"); newline
tabulator tabulator tabulator tabulator tabulator descDetail assignment FCPricelist."Card Type Description"; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'MAN_FEE': newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 120; newline
tabulator tabulator tabulator 'REPLACE_C': newline
tabulator tabulator tabulator tabulator begin newline
tabulator tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 130; newline
tabulator tabulator tabulator tabulator tabulator RVPricelist.Get(Service."Service Code"); newline
tabulator tabulator tabulator tabulator tabulator descDetail assignment Utils.iif(isLocalLanguage, RVPricelist."BLG Car Type Description Alt.", RVPricelist."Car Type Description"); newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator tabulator 'REG_FEE': newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 140; newline
tabulator tabulator tabulator 'RADIO_TAX': newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 150; newline
tabulator tabulator tabulator 'TV_TAX': newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 160; newline
tabulator tabulator tabulator 'AKONTACE': newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 170; newline
tabulator tabulator tabulator 'INSUR_E': newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 180; newline
tabulator tabulator tabulator 'INC_FEE': newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 190; newline
tabulator tabulator tabulator 'INTRECALC': newline
tabulator tabulator tabulator tabulator CurrReport.Skip(); newline
tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator ServiceKindCodeOrder assignment 999; newline
tabulator tabulator end; newline
 newline
tabulator tabulator if (CS_Service_Description notequal descDetail) and (descDetail notequal '') then begin newline
tabulator tabulator tabulator CS_B_Service_Description_Detail assignment descDetail; newline
tabulator tabulator tabulator if (CS_B_Service_Description_Detail notequal '') and (Service.Reinvoice) then newline
tabulator tabulator tabulator tabulator case Service."Service Type Code" of newline
tabulator tabulator tabulator tabulator tabulator 'TIRE', 'T_CHANGE', 'T_STORAGE', 'RIM', 'RIM_ACC': newline
tabulator tabulator tabulator tabulator tabulator tabulator CS_B_Service_Description_Detail assignment LBL_170b + Utils.crlf() + CS_B_Service_Description_Detail; newline
tabulator tabulator tabulator tabulator tabulator else newline
tabulator tabulator tabulator tabulator tabulator tabulator CS_B_Service_Description_Detail += ', ' + LBL_170a; newline
tabulator tabulator tabulator tabulator end; newline
tabulator tabulator end else newline
tabulator tabulator tabulator if Service.Reinvoice then newline
tabulator tabulator tabulator tabulator CS_B_Service_Description_Detail assignment LBL_170b; newline
tabulator end; newline
