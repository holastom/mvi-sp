tabulator local procedure CreateHeader(); newline
tabulator var newline
tabulator tabulator Lbl_CustomerNumber: Label 'Customer Number', Comment = 'cs-CZ=Klientské číslo;en-US=Customer Number'; newline
tabulator tabulator Lbl_RegNo: Label 'Reg No', Comment = 'cs-CZ=IČO;en-US=Reg No'; newline
tabulator tabulator Lbl_CompanyName: Label 'Company Name', Comment = 'cs-CZ=Název klienta;en-US=Company Name'; newline
tabulator tabulator Lbl_LicencePlate: Label 'Licence Plate', Comment = 'cs-CZ=Registrační značka;en-US=Licence Plate'; newline
tabulator tabulator Lbl_ModelDescription: Label 'Model Description', Comment = 'cs-CZ=Model;en-US=Model Description'; newline
tabulator tabulator Lbl_StartDate: Label 'Start Date', Comment = 'cs-CZ=Datum počátku smlouvy;en-US=Start Date'; newline
tabulator tabulator Lbl_EndDate: Label 'End Date', Comment = 'cs-CZ=Datum konce smlouvy;en-US=End Date'; newline
tabulator tabulator Lbl_Odoreading: Label 'Odoreading', Comment = 'cs-CZ=Stav tachometru;en-US=Odoreading'; newline
tabulator tabulator Lbl_DateOfOdoreading: Label 'Date of Odoreading', Comment = 'cs-CZ=Den odečtu;en-US=Date of Odoreading'; newline
tabulator begin newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CustomerNumber, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_RegNo, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_CompanyName, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_LicencePlate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_ModelDescription, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_StartDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_EndDate, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_Odoreading, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator tabulator TmpExcelBuffer.AddColumn(Lbl_DateOfOdoreading, false, '', false, false, false, '', TmpExcelBuffer."Cell Type"::Text); newline
tabulator end; newline
