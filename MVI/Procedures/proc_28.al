tabulator procedure GetAmountCM("Original Financing Contract No.": Code[20]; "Original Invoice/CrMemo No.": Code[20]): Decimal newline
tabulator var newline
tabulator tabulator SCML: Record "Sales Cr.Memo Line"; newline
tabulator tabulator Amount_Countrer: Decimal; newline
tabulator begin newline
tabulator tabulator Amount_Countrer assignment 0; newline
tabulator tabulator SCML.SetRange("Document No.", "Original Invoice/CrMemo No."); newline
tabulator tabulator SCML.SetRange("Shortcut Dimension 2 Code", "Original Financing Contract No."); newline
tabulator tabulator if SCML.FindSet() then begin newline
tabulator tabulator tabulator repeat newline
tabulator tabulator tabulator tabulator Amount_Countrer += SCML.Amount; newline
tabulator tabulator tabulator until SCML.Next() = 0; newline
tabulator tabulator end; newline
tabulator tabulator exit(u.FlipSign(Amount_Countrer)); newline
tabulator end; newline
