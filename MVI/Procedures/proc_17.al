tabulator procedure GroupByShortcutDimensionCrMemo(var FCHCodes: List of [Code[20]]; SalesCrMemoHeader: Record "Sales Cr.Memo Header") newline
tabulator var newline
tabulator tabulator SalesCrMemoLine: Record "Sales Cr.Memo Line"; newline
tabulator begin newline
tabulator tabulator SalesCrMemoLine.SetRange("Document No.", SalesCrMemoHeader."No."); newline
tabulator tabulator SalesCrMemoLine.SetFilter(Type, 'notequal%1', "Sales Line Type"::" "); newline
tabulator tabulator SalesCrMemoLine.FindSet(); newline
tabulator tabulator repeat newline
tabulator tabulator tabulator if not FCHCodes.Contains(SalesCrMemoLine."Shortcut Dimension 2 Code") then newline
tabulator tabulator tabulator tabulator FCHCodes.Add(SalesCrMemoLine."Shortcut Dimension 2 Code"); newline
tabulator tabulator until SalesCrMemoLine.Next() = 0; newline
tabulator end; newline
